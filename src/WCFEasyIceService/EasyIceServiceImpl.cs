﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using EasyResponses;
using ININ;
using ININ.IceLib;
using ININ.IceLib.Configuration;
using ININ.IceLib.Configuration.Dialer;
using ININ.IceLib.Configuration.Dialer.DataTypes;
using ININ.IceLib.Connection;
using ININ.IceLib.Connection.Extensions;
using ININ.IceLib.Data.TransactionBuilder;
using ININ.IceLib.Dialer;
using ININ.IceLib.Dialer.Supervisor;
using ININ.IceLib.Directories;
using ININ.IceLib.Interactions;
using ININ.IceLib.People;
using ININ.IceLib.ProcessAutomation;
using ININ.IceLib.QualityManagement;
using ININ.IceLib.Reporting;
using ININ.IceLib.Reporting.Interactions;
using ININ.IceLib.Reporting.Interactions.Filters;
using ININ.IceLib.Statistics;
using ININ.IceLib.UnifiedMessaging;
using System.Threading;
using System.Threading.Tasks;

namespace WCFEasyIceService
{
    /// <summary>
    /// Service Implementation
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single, Namespace = "http://easyiceservice.com")]
    [ErrorHandlerAttribute]
    public class EasyIceServiceImpl : IEasyIceServiceImpl, IDisposable
    {
        private const string LICENSENAME = "I3_FEATURE_MARKETPLACE_EXT_EISV";

        #region Variables

        #region CIC

        private string _loginErrorMessage;
        private Session _session;
        private PeopleManager _peopleManager;
        private InteractionsManager _interactionsManager;
        private UserStatusList _userStatusList;
        private ServerParameters _serverParameters;
        private UnifiedMessagingManager _unifiedMessagingManager;
        private ConfigurationManager _configurationManager;
        private StatisticsManager _statisticsManager;
        private readonly List<CreateGenericInteractionData> _activeGenericInteractions = new List<CreateGenericInteractionData>();
        private bool _recreateGenericInteractions;
        private readonly List<CreateCallbackInteractionData> _activeCallbackInteractions = new List<CreateCallbackInteractionData>();
        private bool _recreateCallbackInteractions;
        private readonly List<CreateEmailInteractionData> _activeEmailInteractions = new List<CreateEmailInteractionData>();
        private bool _recreateEmailInteractions;
        private readonly List<string> _interactionAttributesToWatch = new List<string>
        {
            InteractionAttributeName.State,
            InteractionAttributeName.StateDescription
        };
        #endregion

        #region Dialer
        private DialerConfigurationManager _dialerConfigurationManager;
        private DialingManager _dialingManager;
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// True if connected to CIC, false otherwise.
        /// </summary>
        public bool IsConnectedToCIC
        {
            get
            {
                if (_session == null || _session.ConnectionState != ConnectionState.Up)
                    TryLogin();
                return _session != null && _session.ConnectionState == ConnectionState.Up;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor. Calls <see cref="Login"/> to login to CIC.
        /// </summary>
        public EasyIceServiceImpl()
        {
            Trace.EasyIceServicesTopic.note("Starting");
            TryLogin();
        }
        #endregion

        #region Private Methods
        private void LogClientInformation()
        {
            try
            {
                if (HttpContext.Current != null)
                {
                    Trace.EasyIceServicesTopic.note(
                        "Who's calling? IP address: '{}', Name: '{}', User Agent: '{}', URL: '{}'.",
                        HttpContext.Current.Request.UserHostAddress, HttpContext.Current.Request.UserHostName,
                        HttpContext.Current.Request.UserAgent, HttpContext.Current.Request.Url);
                }
            }
            catch (Exception ex)
            {
                Trace.EasyIceServicesTopic.exception(ex);
            }
        }
        private void SetResponseHttpStatus(HttpStatusCode statusCode, string reason)
        {
            try
            {
                if (WebOperationContext.Current == null) return;

                var context = WebOperationContext.Current;
                context.OutgoingResponse.StatusCode = statusCode;
                if (!String.IsNullOrEmpty(reason))
                    context.OutgoingResponse.StatusDescription = reason;
            }
            catch (Exception ex)
            {
                Trace.EasyIceServicesTopic.exception(ex);
            }
        }
        private void CheckEasyIceServiceLicense()
        {
            return;
            //var licenseRequest = new LicenseRequest(LICENSENAME, LicenseRequestType.Query);
            //var licenseRequests = new Collection<LicenseRequest> {licenseRequest};

            //var licenseManagement = new LicenseManagement(_peopleManager);
            //var licenseOperation = licenseManagement.LicenseOperation(licenseRequests);
            //if (licenseOperation.Count > 0 && licenseOperation[0])
            //{
            //    Trace.EasyIceServicesTopic.note("EasyIceService License is available.");
            //    return;
            //}

            //throw new Exception(String.Format("The license for the EasyIceService feature ({0}) could not be acquired. Please check the status of this license in Interaction Administrator (go to File/License Management).", LICENSENAME));
        }
        private void CheckDialerLicense()
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                var lstLicenseRequests = new List<LicenseRequest>
                    {
                        new LicenseRequest("I3_ACCESS_DIALER_ADDON", LicenseRequestType.Query)
                    };

                var licManagement = new LicenseManagement(PeopleManager.GetInstance(_session));
                var lstLicReply = licManagement.LicenseOperation(lstLicenseRequests);
                if (!lstLicReply[0])
                {
                    throw new Exception(String.Format("User {0} is not licensed for Dialer (missing I3_ACCESS_DIALER_ADDON). Please check the license assignment in Interaction Administrator for that user.", _session.UserId));
                }

                lstLicenseRequests.Clear();
                lstLicenseRequests.Add(new LicenseRequest("I3_ACCESS_DIALER_ADDON", LicenseRequestType.Request));
                var lstLicRequest = licManagement.LicenseOperation(lstLicenseRequests);

                if (!lstLicRequest[0])
                {
                    Trace.EasyIceServicesTopic.error("unable to acquire I3_ACCESS_DIALER_ADDON license for user {0}.", _session.UserId);
                    throw new Exception(String.Format("User {0} is not licensed for Dialer (missing I3_ACCESS_DIALER_ADDON). Please check the license assignment in Interaction Administrator for that user.", _session.UserId));
                }
            }
        }
        #endregion

        #region CIC Login/Logout

        #region Private Methods

        private void TryLogin()
        {
            var loginResponse = Login();
            if (loginResponse.Success)
            {
                var smVersion = _session.SessionManagerVersion;
                Trace.EasyIceServicesTopic.note("Login successful. Session Manager Version: {}.{}.{}", smVersion.Major, smVersion.Minor, smVersion.Revision);
                _loginErrorMessage = String.Empty;
            }
            else
            {
                Trace.EasyIceServicesTopic.error("Failed to connect to CIC server: '{}'.", loginResponse.ErrorMessage);
                _loginErrorMessage = loginResponse.ErrorMessage;
            }

        }
        ///// <summary>
        ///// Requires the Logon Authentication Configuration dialog in Interaction Administrator to have the "Allow IC authentication" setting enabled.
        ///// The logged in user must have a specific "Proxy Logins" Access Control right configured on their IC User via the Interaction Administrator application. 
        ///// This setting is in the "Miscellaneous" section of the Access Control settings.
        ///// </summary>
        ///// <param name="username"></param>
        ///// <param name="password"></param>
        //private bool TryProxyLogin(string username)
        //{
        //    var errorMessage = String.Empty;

        //    #region Validation

        //    if (String.IsNullOrEmpty(username))
        //    {
        //        Trace.EasyIceServicesTopic.note("No username was passed. Can't poroxy login.");
        //        return false;
        //    }
            
        //    #endregion

        //    #region Get App Settings
            
        //    Trace.EasyIceServicesTopic.note("Getting application settings from the configuration file.");
        //    if (!System.Configuration.ConfigurationManager.AppSettings.HasKeys())
        //    {
        //        Trace.EasyIceServicesTopic.error("No entries found for CICServer, CICUsername and CICPassword in the configuration file.");
        //        return false;
        //    }

        //    var cicServer = System.Configuration.ConfigurationManager.AppSettings.Get("CICServer");
        //    var cicUsername = System.Configuration.ConfigurationManager.AppSettings.Get("CICUsername");
        //    var cicPassword = System.Configuration.ConfigurationManager.AppSettings.Get("CICPassword");

        //    if (String.IsNullOrEmpty(cicServer))
        //    {
        //        errorMessage = "No CICServer specified in the configuration file.";
        //    }
        //    if (String.IsNullOrEmpty(cicUsername))
        //    {
        //        errorMessage = "No CICUsername specified in the configuration file.";
        //    }
        //    if (String.IsNullOrEmpty(cicPassword))
        //    {
        //        errorMessage = "No CICPassword specified in the configuration file.";
        //    }
        //    if (!String.IsNullOrEmpty(errorMessage))
        //    {
        //        Trace.EasyIceServicesTopic.error(errorMessage);
        //        return false;
        //    }
        //    #endregion

        //    var proxyAuthSettings = new ProxyAuthSettings(_session.UserId, _session, username);
        //}
        ///// <summary>
        ///// Requires the Logon Authentication Configuration dialog in Interaction Administrator to have the "Allow Windows authentication" setting enabled.
        ///// The logged in user must have a specific "Proxy Logins" Access Control right configured on their IC User via the Interaction Administrator application. 
        ///// This setting is in the "Miscellaneous" section of the Access Control settings.
        ///// </summary>
        ///// <param name="username"></param>
        ///// <param name="password"></param>
        //private void TryProxyWindowsLogin(string username)
        //{
        //    var proxyAuthSettings = new ProxyWindowsAuthSettings();
        //}
        private LoginResponse LoginWithCredentials(CICSettings cicSettings)
        {
            var response = new LoginResponse();
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    #region Validation
                    if (!cicSettings.IsValid())
                        throw new Exception("CIC Settings are invalid. Either the CIC server or username or password is null or empty.");
                    #endregion

                    _session = new Session();

                    // Setup events
                    _session.ConnectionStateChanged += _session_ConnectionStateChanged;

                    // Misc settings
                    //_session.AttemptToSetThreadCulture = true;
                    _session.AutoReconnectEnabled = true;

                    // Session settings
                    var sessionSettings = new SessionSettings();

                    // Host settings
                    var hostSettings = new HostSettings(new HostEndpoint(cicSettings.CICServer));

                    // Authentication settings
                    var encryptor = new Encryptor();
                    var authSettings = new ICAuthSettings(cicSettings.CICUsername, encryptor.DecryptFromBase64String(cicSettings.CICPassword, "3#sy!c3S3rv!c3"));

                    // Station settings
                    var stationSettings = new StationlessSettings();

                    // Connect!
                    _session.Connect(sessionSettings, hostSettings, authSettings, stationSettings);
                    if (_session.ConnectionState != ConnectionState.Up)
                    {
                        throw new Exception(String.Format("Failed to connect to CIC server with the following settings: '{0}'.", cicSettings));
                    }

                    if (!InitializeIceLibObjects())
                        throw new Exception("Failed to initialize some IceLib objects. Cannot continue.");

                    authSettings.Dispose();

                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);

                    CleanUpUserStatusList();
                    CleanUpInteractionsManager();
                    CleanUpPeopleManager();
                    CleanUpUnifiedMessagingManager();
                    CleanUpServerParameter();
                    CleanUpStatisticsManager();
                    CleanUpConfigurationManager();
                    DisconnectAndCleanUpSession();
                }
            }
            return response;
        }
        private bool InitializeIceLibObjects()
        {
            return InitStatisticsManager();
            //return InitPeopleManager() && InitUserStatusList() && InitInteractionsManager() && InitServerParameters() && InitConfigurationManager() && InitStatisticsManager() && InitUnifiedMessagingManager();
        }
        private bool DisconnectAndCleanUpSession()
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    if (_session != null)
                    {
                        #region Clean Up Dialer
                        if (IsConnectedToDialer)
                        {
                            if (_dialerConfigurationManager != null)
                            {
                                Trace.EasyIceServicesTopic.note("Disposing DialerConfigurationManager.");
                                _dialerConfigurationManager.Dispose();
                            }

                            if (_dialingManager != null)
                            {
                                Trace.EasyIceServicesTopic.note("Disposing DialerConfigurationManager.");
                                _dialingManager = null;
                            }
                        }
                        #endregion

                        #region Clean Up CIC
                        if (_session.ConnectionState == ConnectionState.Up)
                        {
                            _session.Disconnect();
                        }
                        _session.ConnectionStateChanged -= _session_ConnectionStateChanged;
                        _session = null;
                        #endregion
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    return false;
                }
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Event handler called when the CIC connection state changes.
        /// </summary>
        public event EventHandler<ConnectionStateChangedEventArgs> CICConnectionStateChanged;
        private void _session_ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                Trace.EasyIceServicesTopic.note("CIC connection state has changed to '{}'. Message: '{}'. Reason: '{}'.", e.State.ToString(), e.Message, e.Reason.ToString());
                try
                {
                    switch (e.State)
                    {
                        case ConnectionState.Attempting:
                            Trace.EasyIceServicesTopic.note("Connection to CIC is attempting.");
                            break;
                        case ConnectionState.Down:
                            if (e.Reason == ConnectionStateReason.Switchover)
                            {
                                Trace.EasyIceServicesTopic.note("CIC is down due to a switchover. Generic/Callback/Email interactions will be re-created once the connection is re-established.");
                                _recreateGenericInteractions = true;
                                _recreateCallbackInteractions = true;
                                _recreateEmailInteractions = true;
                            }
                            CleanUpUserStatusList();
                            CleanUpInteractionsManager();
                            CleanUpPeopleManager();
                            CleanUpServerParameter();
                            CleanUpStatisticsManager();
                            CleanUpConfigurationManager();
                            CleanUpUnifiedMessagingManager();
                            break;
                        case ConnectionState.None:
                            Trace.EasyIceServicesTopic.error("Connection state is set to 'None'. This shouldn't happen!");
                            break;
                        case ConnectionState.Up:
                            if (!InitializeIceLibObjects())
                                throw new Exception("Failed to initialize some IceLib objects. Cannot continue.");
                            if (_recreateGenericInteractions)
                            {
                                Trace.EasyIceServicesTopic.note("Connection has been re-established after a switchover. Re-creating generic interactions.");
                                lock (_activeGenericInteractions)
                                {
                                    foreach (var genericInteractionData in _activeGenericInteractions)
                                    {
                                        Trace.EasyIceServicesTopic.note("Re-creating generic interaction (previous Interaction id: '{}').", genericInteractionData.InteractionId);
                                        CreateGenericInteraction(genericInteractionData);
                                        //Need to remove old generic interaction (watch out for the lock)
                                    }
                                    _recreateGenericInteractions = false;
                                }
                            }
                            if (_recreateCallbackInteractions)
                            {
                                Trace.EasyIceServicesTopic.note("Re-creating callback interactions.");
                                lock (_activeCallbackInteractions)
                                {
                                    foreach (var callbackInteractionData in _activeCallbackInteractions)
                                    {
                                        Trace.EasyIceServicesTopic.note("Re-creating callback interaction (previous Interaction id: '{}').", callbackInteractionData.InteractionId);
                                        CreateCallbackInteraction(callbackInteractionData);
                                        //Need to remove old callback interaction (watch out for the lock)
                                    }
                                    _recreateCallbackInteractions = false;
                                }
                            }
                            if (_recreateEmailInteractions)
                            {
                                Trace.EasyIceServicesTopic.note("Re-creating email interactions.");
                                lock (_activeEmailInteractions)
                                {
                                    foreach (var emailInteractionData in _activeEmailInteractions)
                                    {
                                        Trace.EasyIceServicesTopic.note("Re-creating email interaction (previous Interaction id: '{}').", emailInteractionData.InteractionId);
                                        CreateEmailInteraction(emailInteractionData);
                                        //Need to remove old email interaction (watch out for the lock)
                                    }
                                    _recreateEmailInteractions = false;
                                }
                            }
                            break;
                        default:
                            Trace.EasyIceServicesTopic.error("Connection state is unknown. This shouldn't happen!");
                            break;
                    }

                    if (CICConnectionStateChanged != null)
                        CICConnectionStateChanged.BeginInvoke(this, e, null, null);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, ex.ToString());
                }
            }
        }
        #endregion

        /// <inheritdoc/>
        public LoginResponse Login()
        {
            var response = new LoginResponse();
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    #region Get App Settings
                    Trace.EasyIceServicesTopic.note("Getting application settings from the configuration file.");
                    if (!System.Configuration.ConfigurationManager.AppSettings.HasKeys())
                    {
                        response.ErrorMessage = "No entries found for CICServer, CICUsername and CICPassword in the configuration file.";
                        Trace.EasyIceServicesTopic.error(response.ErrorMessage);
                        return response;
                    }

                    var cicServer = System.Configuration.ConfigurationManager.AppSettings.Get("CICServer");
                    var cicUsername = System.Configuration.ConfigurationManager.AppSettings.Get("CICUsername");
                    var cicPassword = System.Configuration.ConfigurationManager.AppSettings.Get("CICPassword");

                    if (String.IsNullOrEmpty(cicServer))
                    {
                        response.ErrorMessage = "No CICServer specified in the configuration file.";
                    }
                    if (String.IsNullOrEmpty(cicUsername))
                    {
                        response.ErrorMessage = "No CICUsername specified in the configuration file.";
                    }
                    if (String.IsNullOrEmpty(cicPassword))
                    {
                        response.ErrorMessage = "No CICPassword specified in the configuration file.";
                    }
                    if (!String.IsNullOrEmpty(response.ErrorMessage))
                    {
                        Trace.EasyIceServicesTopic.error(response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    return LoginWithCredentials(new CICSettings(cicServer, cicUsername, cicPassword));
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
            }
            return response;
        }
        /// <inheritdoc/>
        public LogoutResponse Logout()
        {
            var response = new LogoutResponse();
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    if (!IsConnectedToCIC)
                    {
                        response.Success = true;
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    response.Success = DisconnectAndCleanUpSession();
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
            }
            Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
            return response;
        }
        #endregion

        #region User Status

        #region Private Methods
        private bool InitPeopleManager()
        {
            if (_session == null || _session.ConnectionState != ConnectionState.Up)
            {
                return false;
            }

            if (_peopleManager != null)
            {
                return true;
            }

            try
            {
                _peopleManager = PeopleManager.GetInstance(_session);
            }
            catch
            {
                _peopleManager = null;
                return false;
            }

            return true;
        }
        private bool InitUserStatusList()
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    Trace.EasyIceServicesTopic.note("Initializing User Status Watch.");
                    if (!InitPeopleManager())
                    {
                        throw new Exception("Failed to initialize PeopleManager.");
                    }
                    if (_userStatusList != null)
                    {
                        if (_userStatusList.IsWatching())
                            _userStatusList.StopWatching();

                        _userStatusList = null;
                    }

                    _userStatusList = new UserStatusList(_peopleManager);
                    _userStatusList.WatchedObjectsChanged += _userStatusList_WatchedObjectsChanged;
                    _userStatusList.StartWatching(new[] { _session.UserId });

                    Trace.EasyIceServicesTopic.note("Returning 'true'.");
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
                Trace.EasyIceServicesTopic.note("Returning 'false'.");
                return false;
            }
        }
        private void CleanUpPeopleManager()
        {
            _peopleManager = null;
        }
        private void CleanUpUserStatusList()
        {
            if (_userStatusList != null)
            {
                if (_userStatusList.IsWatching())
                    _userStatusList.StopWatching();
                _userStatusList.WatchedObjectsChanged -= _userStatusList_WatchedObjectsChanged;
                _userStatusList = null;
            }
        }
        #endregion

        #region Events
        void _userStatusList_WatchedObjectsChanged(object sender, WatchedObjectsEventArgs<UserStatusProperty> e)
        {
            try
            {
                var userStatusList = sender as UserStatusList;
                if (userStatusList != null)
                {
                    var userStatuses = userStatusList.GetList();
                    if (userStatuses != null && userStatuses.Count > 0)
                    {
                        Trace.EasyIceServicesTopic.note("User Statuses have changed for '{}'.", userStatuses[0].UserId);
                    }
                }

                if (e.Added != null && e.Added.Count > 0)
                {
                    foreach (var addedStatus in e.Added)
                    {
                        Trace.EasyIceServicesTopic.note("Added Status: '{}'.", addedStatus);
                    }
                }
                if (e.Changed != null && e.Changed.Count > 0)
                {
                    foreach (var changedStatus in e.Changed)
                    {
                        Trace.EasyIceServicesTopic.note("Changed Status: '{}'.", changedStatus.Key);
                        foreach (var changedProperty in changedStatus.Value)
                        {
                            Trace.EasyIceServicesTopic.note("Status Property: '{}'.", changedProperty.ToString());
                        }
                    }
                }
                if (e.Removed != null && e.Removed.Count > 0)
                {
                    foreach (var removedStatus in e.Removed)
                    {
                        Trace.EasyIceServicesTopic.note("Removed Status: '{}'.", removedStatus);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.EasyIceServicesTopic.exception(ex);
            }
        }
        #endregion

        /// <inheritdoc/>
        public GetUserStatusResponse GetUserStatus(string username)
        {
            var response = new GetUserStatusResponse();
            LogClientInformation();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        ////SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }
                    CheckEasyIceServiceLicense();
                    if (String.IsNullOrEmpty(username))
                    {
                        username = _session.UserId;
                        Trace.EasyIceServicesTopic.note("No username was passed. Using logged on user id ('{}').", username);
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting user status for '{}'.", username);
                    if (!_userStatusList.IsWatching(username))
                    {
                        Trace.EasyIceServicesTopic.verbose("Starting user status list watch on '{}'.", username);
                        _userStatusList.ChangeWatchedUsers(new[] { username }, null, false);
                    }
                    var userStatus = _userStatusList.GetUserStatus(username);
                    Trace.EasyIceServicesTopic.verbose("User status '{}' successfully retrieved. Status: '{}'.", username, userStatus.StatusMessageDetails.MessageText);

                    #region Build Response
                    response.MessageText = userStatus.StatusMessageDetails.MessageText;
                    response.IconFilename = userStatus.StatusMessageDetails.IconFileName;
                    response.ForwardNumber = userStatus.ForwardNumber;
                    response.LoggedIn = userStatus.LoggedIn.ToString();
                    response.Notes = userStatus.Notes;
                    response.OnPhone = userStatus.OnPhone.ToString();
                    response.OnPhoneChanged = userStatus.OnPhoneChangedHasValue ? userStatus.OnPhoneChanged.ToString(CultureInfo.InvariantCulture) : String.Empty;
                    response.StatusChanged = userStatus.StatusChangedHasValue ? userStatus.StatusChanged.ToString(CultureInfo.InvariantCulture) : String.Empty;
                    response.UntilDate = userStatus.UntilDate.HasValue ? userStatus.UntilDate.Value.ToString(CultureInfo.InvariantCulture) : String.Empty;
                    response.UntilTime = userStatus.UntilTime.HasValue ? userStatus.UntilTime.Value.ToString(CultureInfo.InvariantCulture) : String.Empty;
                    response.Id = userStatus.StatusMessageDetails.Id;
                    response.IsAfterCallWork = userStatus.StatusMessageDetails.IsAfterCallWorkStatus.ToString();
                    response.IsAcd = userStatus.StatusMessageDetails.IsAcdStatus.ToString();
                    response.IsAllowFollowUp = userStatus.StatusMessageDetails.IsAllowFollowUpStatus.ToString();
                    response.IsDoNotDisturb = userStatus.StatusMessageDetails.IsDoNotDisturbStatus.ToString();
                    response.IsForward = userStatus.StatusMessageDetails.IsForwardStatus.ToString();
                    response.IsPersistent = userStatus.StatusMessageDetails.IsPersistentStatus.ToString();
                    response.IsSelectable = userStatus.StatusMessageDetails.IsSelectableStatus.ToString();
                    response.IsValid = userStatus.StatusMessageDetails.IsValid.ToString();
                    response.Success = true;
                    #endregion
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
            }
            Trace.EasyIceServicesTopic.note("Returning: {}.", response);
            return response;
        }
        /// <inheritdoc/>
        public SetUserStatusResponse SetUserStatus(string username, string statusName)
        {
            var response = new SetUserStatusResponse();
            using (Trace.EasyIceServicesTopic.scope())
            {
                StatusMessageList statusMessageList = null;
                try
                {
                    LogClientInformation();

                    #region Validation

                    Guard.ArgumentNotNullOrEmptyString(statusName, "statusName");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        ////SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (String.IsNullOrEmpty(username))
                    {
                        username = _session.UserId;
                        Trace.EasyIceServicesTopic.note("No username was passed. Using logged on user id ('{}').",
                                                        username);
                    }
                    else
                    {
                        if (!GetUser(username).Success)
                        {
                            throw new Exception(String.Format("User '{0}' does not exist.", username));
                        }
                    }

                    #endregion

                    Trace.EasyIceServicesTopic.note("Setting '{}' status to '{}'.", username, statusName);

                    //Check if status exists
                    statusMessageList = new StatusMessageList(_peopleManager);
                    statusMessageList.StartWatching();
                    var statusesMessageDetails = statusMessageList.GetList();
                    var statusMessageDetails = statusesMessageDetails.FirstOrDefault(currentStatusMessageDetails => currentStatusMessageDetails.MessageText.Equals(statusName));
                    if (statusMessageDetails == null)
                    {
                        throw new Exception(String.Format("Status '{0}' was not found on server '{1}'.", statusName, _session.ICServer));
                    }

                    // Fire update
                    var userStatusUpdate = new UserStatusUpdate(_peopleManager, username)
                        {
                            StatusMessageDetails = statusMessageDetails
                        };
                    userStatusUpdate.UpdateRequest();

                    Trace.EasyIceServicesTopic.note("'{}' was successfully set to '{}'.", username, statusMessageDetails.MessageText);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (statusMessageList != null && statusMessageList.IsWatching())
                    {
                        statusMessageList.StopWatching();
                    }
                }
            }
            Trace.EasyIceServicesTopic.note("Returning: {}.", response);
            return response;
        }
        #endregion

        #region Server Parameter

        #region Private Methods
        private bool InitServerParameters()
        {
            if (_session == null || _session.ConnectionState != ConnectionState.Up)
            {
                return false;
            }

            if (_serverParameters != null)
            {
                return true;
            }

            try
            {
                _serverParameters = new ServerParameters(_session);
                _serverParameters.ServerParametersChanged += _serverParameters_ServerParametersChanged;
            }
            catch
            {
                _serverParameters = null;
                return false;
            }

            return true;
        }
        private void CleanUpServerParameter()
        {
            if (_serverParameters != null)
            {
                _serverParameters.ServerParametersChanged -= _serverParameters_ServerParametersChanged;
                _serverParameters = null;
            }
        }
        #endregion

        #region Events
        void _serverParameters_ServerParametersChanged(object sender, ServerParametersChangedEventArgs e)
        {
            foreach (var changedServerParameter in e.ServerParameters)
            {
                if (changedServerParameter.Exists)
                    Trace.EasyIceServicesTopic.note("Server Parameter '{}' value has changed to '{}'.", changedServerParameter.Name, changedServerParameter.Value);
                else
                    Trace.EasyIceServicesTopic.note("Server Parameter '{}' has been deleted.", changedServerParameter.Name);
            }
        }
        #endregion

        /// <inheritdoc/>
        public GetServerParameterResponse GetServerParameter(string serverParameterName)
        {
            var response = new GetServerParameterResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(serverParameterName, "serverParameterName");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        ////SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.verbose("Getting server parameter '{}' value from CIC server '{}'.", serverParameterName, _session.ICServer);
                    var serverParameterValues = _serverParameters.GetServerParameters(new[] { serverParameterName });
                    if (serverParameterValues.Count > 0)
                    {
                        if (serverParameterValues[0].Exists)
                        {
                            response.ServerParameterValue = serverParameterValues[0].Value;
                            Trace.EasyIceServicesTopic.note("Successfully got server parameter value: '{}'.", response.ServerParameterValue);
                            response.Success = true;
                        }
                        else
                        {
                            response.ErrorMessage = String.Format("Server parameter '{0}' does not exist.", serverParameterName);
                        }
                    }
                    else
                    {
                        response.ErrorMessage = String.Format("Server parameter '{0}' does not exist.", serverParameterName);
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning {}.", response);
                return response;
            }
        }
        #endregion

        #region Interactions

        #region Private Methods
        private bool InitInteractionsManager()
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                if (_session == null)
                {
                    return false;
                }

                if (_interactionsManager != null)
                    return true;

                _interactionsManager = InteractionsManager.GetInstance(_session);
                _interactionsManager.CallMade += _interactionsManager_CallMade;
                _interactionsManager.InteractionAutoAnswered += _interactionsManager_InteractionAutoAnswered;
                _interactionsManager.PerformedAction += _interactionsManager_PerformedAction;
                _interactionsManager.PerformingAction += _interactionsManager_PerformingAction;

                return true;
            }

        }
        private void CleanUpInteractionsManager()
        {
            if (_interactionsManager == null)
                return;

            _interactionsManager.PerformingAction -= _interactionsManager_PerformingAction;
            _interactionsManager.PerformedAction -= _interactionsManager_PerformedAction;
            _interactionsManager.CallMade -= _interactionsManager_CallMade;
            _interactionsManager.InteractionAutoAnswered -= _interactionsManager_InteractionAutoAnswered;
            _interactionsManager = null;
        }
        /// <summary>
        /// Used to get the interaction object from its interaction id.
        /// </summary>
        /// <param name="interactionId">The interaction id</param>
        /// <returns>An Interaction object</returns>
        private Interaction GetInteraction(string interactionId)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                Interaction interaction;
                try
                {
                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        return null;
                    }
                    #endregion

                    interaction = _interactionsManager.CreateInteraction(new InteractionId(interactionId));
                }
                catch (InsufficientRightsException iceEx)
                {
                    Trace.EasyIceServicesTopic.error("Got an insufficient rights exception. This probably means that interaction does not exist or is in the IVR. Message: {}", iceEx);
                    //SetResponseHttpStatus(HttpStatusCode.MethodNotAllowed, String.Format("Got an insufficient rights exception. This probably means that interaction does not exist or is in the IVR. Message: {0}", iceEx));
                    return null;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, ex.ToString());
                    return null;
                }
                Trace.EasyIceServicesTopic.note("Returning Interaction '{}'.", interaction.InteractionId.Id);
                return interaction;
            }
        }
        /// <summary>
        /// Checks if an interaction is disconnected (also starts or changes the interaction watch if Eic_State is not being watched).
        /// </summary>
        /// <param name="interaction">Interaction object</param>
        /// <returns>True if the interaction is disconnected, false otherwise.</returns>CheckEasyIceServiceLicense
        private bool IsDisconnected(Interaction interaction)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    #region Validation
                    Guard.ArgumentNotNull(interaction, "interaction");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        return false;
                    }
                    #endregion

                    if (interaction.IsWatching())
                    {
                        if (!interaction.IsWatching(InteractionAttributeName.State))
                            interaction.ChangeWatchedAttributes(new[] { InteractionAttributeName.State }, null, false);
                    }
                    else
                    {
                        interaction.StartWatching(new[] { InteractionAttributeName.State });
                    }
                    return interaction.IsDisconnected;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
                return false;
            }
        }
        /// <summary>
        /// Gets an interaction attribute (also starts or changes the interaction watch if the attribute is not being watched).
        /// </summary>
        /// <param name="interaction">Interaction object</param>
        /// <param name="attributeName">Name of the interaction attribute</param>
        /// <returns>The value of the interaction attribute or an empty string if the attribute does not exist.</returns>
        private string GetInteractionAttribute(Interaction interaction, string attributeName)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    #region Validation
                    Guard.ArgumentNotNull(interaction, "interaction");
                    Guard.ArgumentNotNullOrEmptyString(attributeName, "attributeName");
                    #endregion

                    if (interaction.IsWatching())
                    {
                        if (!interaction.IsWatching(attributeName))
                            interaction.ChangeWatchedAttributes(new[] { attributeName }, null, false);
                    }
                    else
                    {
                        interaction.StartWatching(new[] { attributeName });
                    }
                    return interaction.GetStringAttribute(attributeName);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
                return String.Empty;
            }
        }
        /// <summary>
        /// Sets the value of an interaction attribute (also starts or changes the interaction watch if the attribute is not being watched).
        /// </summary>
        /// <param name="interaction">Interaction object.</param>
        /// <param name="attributeName">Name of the interaction attribute to set.</param>
        /// <param name="attributeValue">Attribute Value.</param>
        /// <returns>True if successful, false otherwise.</returns>
        private bool SetInteractionAttribute(Interaction interaction, string attributeName, string attributeValue)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    #region Validation
                    Guard.ArgumentNotNull(interaction, "interaction");
                    Guard.ArgumentNotNullOrEmptyString(attributeName, "attributeName");
                    Guard.ArgumentNotNullOrEmptyString(attributeValue, "attributeValue");
                    #endregion

                    interaction.SetStringAttribute(attributeName, attributeValue);
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
                return false;
            }
        }
        /// <summary>
        /// Gets all the interactions from a workgroup.
        /// </summary>
        /// <param name="workgroup">CIC workgroup name.</param>
        /// <returns>A list of interaction objects.</returns>
        private IEnumerable<Interaction> GetInteractionsFromWorkgroup(string workgroup)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(workgroup, "workgroup");
                    #endregion

                    var interactionQueue = new InteractionQueue(_interactionsManager, new QueueId(QueueType.Workgroup, workgroup));
                    return new List<Interaction>(interactionQueue.GetContents());
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
                return null;
            }
        }
        #endregion

        #region Events
        void _interactionsManager_InteractionAutoAnswered(object sender, InteractionEventArgs e)
        {
            Trace.EasyIceServicesTopic.verbose("Interaction '{}' has been auto answered.", e.Interaction.InteractionId.Id);
        }
        void _interactionsManager_CallMade(object sender, CallMadeEventArgs e)
        {
            Trace.EasyIceServicesTopic.note("Call interaction '{}' has been made.", e.Interaction.InteractionId.Id);
        }
        void _interactionsManager_PerformedAction(object sender, PerformedActionEventArgs e)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                string action;
                switch (e.Action)
                {
                    case InteractionCapabilities.Coach:
                        action = "Coach";
                        break;
                    case InteractionCapabilities.Conference:
                        action = "Conference";
                        break;
                    case InteractionCapabilities.Consult:
                        action = "Consult";
                        break;
                    case InteractionCapabilities.Disconnect:
                        action = "Disconnect";
                        break;
                    case InteractionCapabilities.Hold:
                        action = "Hold";
                        break;
                    case InteractionCapabilities.Join:
                        action = "Join";
                        break;
                    case InteractionCapabilities.Listen:
                        action = "Listen";
                        break;
                    case InteractionCapabilities.Messaging:
                        action = "Messaging";
                        break;
                    case InteractionCapabilities.Mute:
                        action = "Mute";
                        break;
                    case InteractionCapabilities.None:
                        action = "None";
                        break;
                    case InteractionCapabilities.ObjectWindow:
                        action = "ObjectWindow";
                        break;
                    case InteractionCapabilities.Park:
                        action = "Park";
                        break;
                    case InteractionCapabilities.Pause:
                        action = "Pause";
                        break;
                    case InteractionCapabilities.Pickup:
                        action = "Pickup";
                        break;
                    case InteractionCapabilities.Private:
                        action = "Private";
                        break;
                    case InteractionCapabilities.Record:
                        action = "Record";
                        break;
                    case InteractionCapabilities.RequestHelp:
                        action = "RequestHelp";
                        break;
                    case InteractionCapabilities.SecureInput:
                        action = "SecureInput";
                        break;
                    case InteractionCapabilities.SecureRecordingPause:
                        action = "SecureRecordingPause";
                        break;
                    case InteractionCapabilities.Suspended:
                        action = "Suspended";
                        break;
                    case InteractionCapabilities.Transfer:
                        action = "Transfer";
                        break;
                    default:
                        action = "Unknown";
                        break;
                }
                Trace.EasyIceServicesTopic.verbose("Action '{}' performed on Interaction '{}'.", action, e.Interaction.InteractionId.Id);
                foreach (var actionParameters in e.ActionParameters)
                {
                    Trace.EasyIceServicesTopic.verbose("Action Parameter: '{}', Value: '{}'.", actionParameters.Key, actionParameters.Value);
                }
            }
        }
        void _interactionsManager_PerformingAction(object sender, PerformingActionEventArgs e)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                string action;
                switch (e.Action)
                {
                    case InteractionCapabilities.Coach:
                        action = "Coach";
                        break;
                    case InteractionCapabilities.Conference:
                        action = "Conference";
                        break;
                    case InteractionCapabilities.Consult:
                        action = "Consult";
                        break;
                    case InteractionCapabilities.Disconnect:
                        action = "Disconnect";
                        break;
                    case InteractionCapabilities.Hold:
                        action = "Hold";
                        break;
                    case InteractionCapabilities.Join:
                        action = "Join";
                        break;
                    case InteractionCapabilities.Listen:
                        action = "Listen";
                        break;
                    case InteractionCapabilities.Messaging:
                        action = "Messaging";
                        break;
                    case InteractionCapabilities.Mute:
                        action = "Mute";
                        break;
                    case InteractionCapabilities.None:
                        action = "None";
                        break;
                    case InteractionCapabilities.ObjectWindow:
                        action = "ObjectWindow";
                        break;
                    case InteractionCapabilities.Park:
                        action = "Park";
                        break;
                    case InteractionCapabilities.Pause:
                        action = "Pause";
                        break;
                    case InteractionCapabilities.Pickup:
                        action = "Pickup";
                        break;
                    case InteractionCapabilities.Private:
                        action = "Private";
                        break;
                    case InteractionCapabilities.Record:
                        action = "Record";
                        break;
                    case InteractionCapabilities.RequestHelp:
                        action = "RequestHelp";
                        break;
                    case InteractionCapabilities.SecureInput:
                        action = "SecureInput";
                        break;
                    case InteractionCapabilities.SecureRecordingPause:
                        action = "SecureRecordingPause";
                        break;
                    case InteractionCapabilities.Suspended:
                        action = "Suspended";
                        break;
                    case InteractionCapabilities.Transfer:
                        action = "Transfer";
                        break;
                    default:
                        action = "Unknown";
                        break;
                }
                Trace.EasyIceServicesTopic.verbose("Performing action '{}' on Interaction '{}'.", action, e.Interaction.InteractionId.Id);
                foreach (var actionParameters in e.ActionParameters)
                {
                    Trace.EasyIceServicesTopic.verbose("Action Parameter: '{}', Value: '{}'.", actionParameters.Key, actionParameters.Value);
                }
            }
        }

        void InteractionAttributesWatchComplete(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                var interaction = sender as Interaction;
                if (interaction == null) return;

                if (e.Error != null)
                {
                    Trace.EasyIceServicesTopic.error("An error occurred when trying to watch interaction '{}' attributes. Exception message will follow.", interaction.InteractionId.Id);
                    throw e.Error;
                }
                if (e.Cancelled)
                {
                    Trace.EasyIceServicesTopic.note("Interaction attributes watch has been cancelled.");
                    return;
                }

                Trace.EasyIceServicesTopic.note("Attributes are now being watched for interaction '{}'.", interaction.InteractionId.Id);
            }
            catch (Exception ex)
            {
                Trace.EasyIceServicesTopic.exception(ex);
            }
        }
        void interaction_AttributesChanged(object sender, AttributesEventArgs e)
        {
            try
            {
                var interaction = sender as Interaction;
                if (interaction == null) return;
                foreach (var interactionAttributeName in e.InteractionAttributeNames)
                {
                    Trace.EasyIceServicesTopic.verbose("Interaction '{}' attribute '{}' value has changed to '{}'.", interaction.InteractionId.Id, interactionAttributeName, interaction.GetStringAttribute(interactionAttributeName));
                }
            }
            catch (Exception ex)
            {
                Trace.EasyIceServicesTopic.exception(ex);
            }
        }
        void interaction_Deallocated(object sender, EventArgs e)
        {
            try
            {
                var interaction = sender as Interaction;
                if (interaction == null) return;
                Trace.EasyIceServicesTopic.verbose("Interaction '{}' has been deallocated.", interaction.InteractionId.Id);
            }
            catch (Exception ex)
            {
                Trace.EasyIceServicesTopic.exception(ex);
            }
        }
        #endregion

        #region Common to All Interaction Types
        /// <inheritdoc/>
        public TransferInteractionResponse TransferInteraction(string interactionId, string target)
        {
            var response = new TransferInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(target, "target");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        ////SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        ////SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    if (IsDisconnected(interaction))
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is disconnected. Exiting.", interaction.InteractionId.Id);
                        response.ErrorMessage = String.Format("Interaction '{0}' is disconnected.", interaction.InteractionId.Id);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Transferring interaction '{}' to '{}'.", interaction.InteractionId.Id, target);
                    interaction.BlindTransfer(target);
                    Trace.EasyIceServicesTopic.note("Successfully transferred interaction '{}' to '{}'.", interaction.InteractionId.Id, target);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public TransferInteractionResponse TransferInteractionToWorkgroup(string interactionId, string workgroup)
        {
            var response = new TransferInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(workgroup, "workgroup");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        ////SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        ////SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    if (IsDisconnected(interaction))
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is disconnected. Exiting.", interaction.InteractionId.Id);
                        response.ErrorMessage = String.Format("Interaction '{0}' is disconnected.", interaction.InteractionId.Id);
                        return response;
                    }

                    if (!GetWorkgroup(workgroup).Success)
                    {
                        Trace.EasyIceServicesTopic.error("Workgroup '{}' does not exist on CIC server '{}'.", workgroup, _session.ICServer);
                        response.ErrorMessage = String.Format("Workgroup '{0}' does not exist on CIC server '{1}'.", workgroup, _session.ICServer);
                        ////SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Transferring interaction '{}' to workgroup '{}'.", interaction.InteractionId.Id, workgroup);
                    interaction.BlindTransfer(new QueueId(QueueType.Workgroup, workgroup));
                    Trace.EasyIceServicesTopic.note("Successfully transferred interaction '{}' to workgroup '{}'.", interaction.InteractionId.Id, workgroup);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public DisconnectInteractionResponse DisconnectInteraction(string interactionId)
        {
            var response = new DisconnectInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        ////SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        ////SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Disconnecting interaction '{}'.", interactionId);
                    interaction.Disconnect();
                    Trace.EasyIceServicesTopic.note("Successfully disconnected interaction '{}'.", interactionId);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetInteractionAttributeResponse GetInteractionAttribute(string interactionId, string attributeName)
        {
            var response = new GetInteractionAttributeResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(attributeName, "attributeName");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        ////SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        ////SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting interaction attribute '{}' from interaction '{}'.", attributeName, interaction.InteractionId.Id);
                    response.AttributeValue = GetInteractionAttribute(interaction, attributeName);
                    Trace.EasyIceServicesTopic.note("Successfully got interaction attribute '{}' from interaction '{}'. Value: '{}'.", attributeName, interaction.InteractionId.Id, response.AttributeValue);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SetInteractionAttributeResponse SetInteractionAttribute(string interactionId, string attributeName, SetInteractionAttributeData setInteractionAttributeData)
        {
            var response = new SetInteractionAttributeResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(attributeName, "attributeName");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Setting interaction attribute '{}' to '{}' for interaction '{}'.", attributeName, setInteractionAttributeData.InteractionAttributeValue, interaction.InteractionId.Id);
                    response.Success = SetInteractionAttribute(interaction, attributeName, setInteractionAttributeData.InteractionAttributeValue);
                    Trace.EasyIceServicesTopic.note("Successfully set interaction attribute '{}' to '{}' for interaction '{}'.", attributeName, setInteractionAttributeData.InteractionAttributeValue, interaction.InteractionId.Id);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SetInteractionAttributeResponse SetInteractionAttributeExtended(string interactionId, string attributeName, string attributeValue)
        {
            return SetInteractionAttribute(interactionId, attributeName, new SetInteractionAttributeData(attributeValue));
        }
        /// <inheritdoc/>
        public GetInteractionsWithAttributeValueResponse GetInteractionsWithAttributeValue(string workgroup, string interactionAttributeName, string interactionAttributeValue)
        {
            var response = new GetInteractionsWithAttributeValueResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(workgroup, "workgroup");
                    Guard.ArgumentNotNullOrEmptyString(interactionAttributeName, "interactionAttributeName");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting interactions with interaction attribute '{}' value '{}' from workgroup '{}'.", interactionAttributeName, interactionAttributeValue, workgroup);
                    var interactions = GetInteractionsFromWorkgroup(workgroup);
                    if (interactions == null)
                    {
                        response.ErrorMessage = String.Format("Interactions could not be retrieved from workgroup '{0}'.", workgroup);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    foreach (var interaction in interactions)
                    {
                        Trace.EasyIceServicesTopic.note("Getting interaction attribute '{}' from interaction '{}'.", interactionAttributeName, interaction.InteractionId.Id);
                        if (GetInteractionAttribute(interaction, interactionAttributeName).Equals(interactionAttributeValue, StringComparison.InvariantCultureIgnoreCase))
                        {
                            Trace.EasyIceServicesTopic.note("Found interaction attribute name '{}' equals to requested value '{}' in interaction id '{}' in workgroup '{}'.", interactionAttributeName, interactionAttributeValue, interaction.InteractionId.Id, workgroup);
                            response.InteractionsId.Add(interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture));
                        }
                    }
                    Trace.EasyIceServicesTopic.note("Found '{}' interactions with interaction attribute '{}' value '{}' from workgroup '{}'.", response.InteractionsId.Count, interactionAttributeName, interactionAttributeValue, workgroup);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public IsInteractionInUserQueueResponse IsInteractionInUserQueue(string interactionId, string username)
        {
            var response = new IsInteractionInUserQueueResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(username, "username");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Checking if interaction '{}' is in '{}' user queue.", interactionId, username);
                    var userQueue = new InteractionQueue(_interactionsManager, new QueueId(QueueType.User, username));

                    if (userQueue.GetContents().Any(interaction => interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture).Equals(interactionId)))
                    {
                        Trace.EasyIceServicesTopic.note("Found interaction '{}' in '{}' queue.", interactionId, username);
                        response.Exists = true;
                    }
                    Trace.EasyIceServicesTopic.note("Is interaction '{}' in '{}' user queue? '{}'.", interactionId, username, response.Exists);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public IsInteractionInWorkgroupQueueResponse IsInteractionInWorkgroupQueue(string interactionId, string workgroup)
        {
            var response = new IsInteractionInWorkgroupQueueResponse();
            InteractionQueue workgroupQueue = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(workgroup, "workgroup");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Checking if interaction '{}' is in '{}' workgroup queue.", interactionId, workgroup);
                    workgroupQueue = new InteractionQueue(_interactionsManager, new QueueId(QueueType.Workgroup, workgroup));
                    workgroupQueue.StartWatching(new[] { InteractionAttributeName.InteractionId });
                    if (workgroupQueue.GetContents().Any(interaction => interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture).Equals(interactionId)))
                    {
                        Trace.EasyIceServicesTopic.note("Found interaction '{}' in '{}' queue.", interactionId, workgroup);
                        response.Exists = true;
                    }
                    Trace.EasyIceServicesTopic.note("Is interaction '{}' in '{}' workgroup queue? '{}'.", interactionId, workgroup, response.Exists);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (workgroupQueue != null && workgroupQueue.IsWatching())
                        workgroupQueue.StopWatching();
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Interaction Actions
        /// <inheritdoc/>
        public AppendInteractionNoteResponse AppendInteractionNote(string interactionId, string note)
        {
            var response = new AppendInteractionNoteResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Appending '{}' to '{}' notes.", interaction.InteractionId.Id, note);
                    interaction.AppendNote(note);
                    Trace.EasyIceServicesTopic.note("Successfully appended note '{}' to interaction '{}'.", note, interaction.InteractionId.Id);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public PickupInteractionResponse PickupInteraction(string interactionId)
        {
            var response = new PickupInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    GetInteractionAttribute(interaction, InteractionAttributeName.Capabilities); // Starts the watch on interaction capabilities if it wasn't started yet.
                    if (!interaction.IsValidCapability(InteractionCapabilities.Pickup))
                        throw new Exception(String.Format("Interaction '{0}' can not be picked up at this time.", interactionId));
                    #endregion

                    Trace.EasyIceServicesTopic.note("Picking up '{}'.", interaction.InteractionId.Id);
                    interaction.Pickup();
                    Trace.EasyIceServicesTopic.note("Successfully picked up interaction '{}'.", interaction.InteractionId.Id);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public HoldInteractionResponse HoldInteraction(string interactionId)
        {
            var response = new HoldInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    GetInteractionAttribute(interaction, InteractionAttributeName.Capabilities); // Starts the watch on interaction capabilities if it wasn't started yet.
                    if (!interaction.IsValidCapability(InteractionCapabilities.Hold))
                        throw new Exception(String.Format("Interaction '{0}' can not be held at this time.", interactionId));
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.State); // Starts the watch on interaction capabilities if it wasn't started yet.
                    if (!interaction.IsHeld)
                    {
                        Trace.EasyIceServicesTopic.note("Holding '{}'.", interaction.InteractionId.Id);
                        interaction.Hold(true);
                        Trace.EasyIceServicesTopic.note("Successfully held interaction '{}'.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is already held.", interaction.InteractionId.Id);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public MuteInteractionResponse MuteInteraction(string interactionId)
        {
            var response = new MuteInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    GetInteractionAttribute(interaction, InteractionAttributeName.Capabilities); // Starts the watch on interaction capabilities if it wasn't started yet.
                    if (!interaction.IsValidCapability(InteractionCapabilities.Mute))
                        throw new Exception(String.Format("Interaction '{0}' can not be muted at this time.", interactionId));
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.Muted); // Starts the watch on interaction capabilities if it wasn't started yet.
                    if (!interaction.IsMuted)
                    {
                        Trace.EasyIceServicesTopic.note("Muting '{}'.", interaction.InteractionId.Id);
                        interaction.Mute(true);
                        Trace.EasyIceServicesTopic.note("Successfully muted interaction '{}'.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is already muted.", interaction.InteractionId.Id);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public MuteInteractionResponse UnmuteInteraction(string interactionId)
        {
            var response = new MuteInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.Muted); // Starts the watch on interaction capabilities if it wasn't started yet.
                    if (interaction.IsMuted)
                    {
                        Trace.EasyIceServicesTopic.note("Unmuting '{}'.", interaction.InteractionId.Id);
                        interaction.Mute(false);
                        Trace.EasyIceServicesTopic.note("Successfully umuted interaction '{}'.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' was not muted.", interaction.InteractionId.Id);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public PrivateInteractionResponse PrivateInteraction(string interactionId)
        {
            var response = new PrivateInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    GetInteractionAttribute(interaction, InteractionAttributeName.Private); // Starts the watch on interaction capabilities if it wasn't started yet.
                    if (!interaction.IsValidCapability(InteractionCapabilities.Private))
                        throw new Exception(String.Format("Interaction '{0}' can not be made private at this time.", interactionId));
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.Private); // Starts the watch on interaction capabilities if it wasn't started yet.
                    if (!interaction.IsPrivate)
                    {
                        Trace.EasyIceServicesTopic.note("Making '{}' private.", interaction.InteractionId.Id);
                        interaction.Private(true);
                        Trace.EasyIceServicesTopic.note("Successfully made interaction '{}' as private.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is already private.", interaction.InteractionId.Id);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public PrivateInteractionResponse UnprivateInteraction(string interactionId)
        {
            var response = new PrivateInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.Private); // Starts the watch on interaction capabilities if it wasn't started yet.
                    if (interaction.IsPrivate)
                    {
                        Trace.EasyIceServicesTopic.note("Stop making '{}' private.", interaction.InteractionId.Id);
                        interaction.Private(false);
                        Trace.EasyIceServicesTopic.note("Successfully stopped making interaction '{}' a private interaction.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is not marked as private.", interaction.InteractionId.Id);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public ParkInteractionResponse ParkInteraction(string interactionId, string username)
        {
            var response = new ParkInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(username, "username");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    if (!GetUser(username).Success)
                        throw new Exception(String.Format("User '{0}' does not exist.", username));
                    #endregion

                    Trace.EasyIceServicesTopic.note("Parking '{}' on user queue '{}'.", interaction.InteractionId.Id, username);
                    interaction.Park(new QueueId(QueueType.User, username));
                    Trace.EasyIceServicesTopic.note("Successfully parked interaction '{}' on user queue '{}'.", interaction.InteractionId.Id, username);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public RecordInteractionResponse RecordInteraction(string interactionId)
        {
            var response = new RecordInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    if (!interaction.IsValidCapability(InteractionCapabilities.Record))
                        throw new Exception(String.Format("Interaction '{0}' can not be recorded at this time.", interactionId));
                    #endregion

                    Trace.EasyIceServicesTopic.note("Recording '{}'.", interaction.InteractionId.Id);
                    interaction.Record(true, true);
                    Trace.EasyIceServicesTopic.note("Successfully started recording interaction '{}'.", interaction.InteractionId.Id);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public RecordInteractionResponse StopRecordInteraction(string interactionId)
        {
            var response = new RecordInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Stop recording '{}'.", interaction.InteractionId.Id);
                    interaction.Record(false, true);
                    Trace.EasyIceServicesTopic.note("Successfully stopped recording interaction '{}'.", interaction.InteractionId.Id);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public PauseInteractionResponse PauseInteraction(string interactionId)
        {
            var response = new PauseInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    if (!interaction.IsValidCapability(InteractionCapabilities.Pause))
                        throw new Exception(String.Format("Interaction '{0}' can not be paused at this time.", interactionId));
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.Recorders); // Required to get .IsPaused
                    GetInteractionAttribute(interaction, InteractionAttributeName.SupervisorRecorders); // Required to get .IsPaused
                    if (!interaction.IsPaused)
                    {
                        Trace.EasyIceServicesTopic.note("Pausing '{}'.", interaction.InteractionId.Id);
                        interaction.Pause(true);
                        Trace.EasyIceServicesTopic.note("Successfully paused interaction '{}'.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is already paused.", interaction.InteractionId.Id);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public ResumeInteractionResponse ResumeInteraction(string interactionId)
        {
            var response = new ResumeInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.Recorders); // Required to get .IsPaused
                    GetInteractionAttribute(interaction, InteractionAttributeName.SupervisorRecorders); // Required to get .IsPaused
                    if (interaction.IsPaused)
                    {
                        Trace.EasyIceServicesTopic.note("Resuming '{}'.", interaction.InteractionId.Id);
                        interaction.Pause(false);
                        Trace.EasyIceServicesTopic.note("Successfully resumed interaction '{}'.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is not paused.", interaction.InteractionId.Id);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public PauseInteractionResponse SecurePauseInteraction(string interactionId)
        {
            var response = new PauseInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.Recorders); // Required to get .IsPaused
                    GetInteractionAttribute(interaction, InteractionAttributeName.SupervisorRecorders); // Required to get .IsPaused
                    Trace.EasyIceServicesTopic.note("Secure pausing '{}'.", interaction.InteractionId.Id);
                    interaction.SecureRecordingPause(new SecureRecordingPauseParameters(SecureRecordingPauseAction.PauseWithDefaultTimeout));
                    Trace.EasyIceServicesTopic.note("Successfully secure paused interaction '{}'.", interaction.InteractionId.Id);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public PauseInteractionResponse SecureResumeInteraction(string interactionId)
        {
            var response = new PauseInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.Recorders); // Required to get .IsPaused
                    GetInteractionAttribute(interaction, InteractionAttributeName.SupervisorRecorders); // Required to get .IsPaused
                    if (interaction.IsPaused)
                    {
                        Trace.EasyIceServicesTopic.note("Secure resuming '{}'.", interaction.InteractionId.Id);
                        interaction.SecureRecordingPause(new SecureRecordingPauseParameters(SecureRecordingPauseAction.ResumeRecording));
                        Trace.EasyIceServicesTopic.note("Successfully secure resumed interaction '{}'.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is not in a secure pause mode.", interaction.InteractionId.Id);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public UpdateInteractionDeallocationTimeResponse UpdateInteractionDeallocationTime(string interactionId, string time)
        {
            var response = new UpdateInteractionDeallocationTimeResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(time, "time");

                    int value;
                    if (!int.TryParse(time, out value))
                        throw new Exception(String.Format("'{0}' is not a valid time value. Please use an integer to represent the number of seconds to set for the deallocation time.", time));

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.State); // Required to get .IsDisconnected
                    if (!interaction.IsDisconnected)
                    {
                        Trace.EasyIceServicesTopic.note("Updating DeallocationSeconds attribute to '{}' for '{}'.", time, interaction.InteractionId.Id);
                        bool success = SetInteractionAttribute(interaction, InteractionAttributeName.DeallocationSeconds, time);
                        if (success)
                        {
                            Trace.EasyIceServicesTopic.note("Successfully updated Deallocation Seconds to '{}' for interaction '{}'.", time, interaction.InteractionId.Id);
                            response.Success = true;
                        }
                        else
                            Trace.EasyIceServicesTopic.error("Failed to update the Deallocation Seconds attribute for interaction '{}'.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Interaction '{}' is already disconnected.", interaction.InteractionId.Id);
                        response.ErrorMessage = String.Format("Interaction '{0}' is already disconnected.", interaction.InteractionId.Id);
                        //SetResponseHttpStatus(HttpStatusCode.MethodNotAllowed, response.ErrorMessage);
                        return response;
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public TransferInteractionToVoicemailResponse TransferInteractionToVoicemail(string interactionId, string username)
        {
            var response = new TransferInteractionToVoicemailResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    if (!interaction.IsValidCapability(InteractionCapabilities.Messaging))
                        throw new Exception(String.Format("Interaction '{0}' can not be transferred to voicemail due to its type or its current state.", interactionId));

                    if (!String.IsNullOrEmpty(username))
                    {
                        if (!GetUser(username).Success)
                            throw new Exception(String.Format("User '{0}' does not exist. Can't transfer to his/her voicemail.", username));
                    }
                    #endregion

                    if (String.IsNullOrEmpty(username))
                    {
                        Trace.EasyIceServicesTopic.note("Transferring interaction '{}' to current user's voicemail.", interaction.InteractionId.Id);
                        interaction.Voicemail();
                        Trace.EasyIceServicesTopic.note("Successfully transferred interaction '{}' to current user's voicemail.", interaction.InteractionId.Id);
                    }
                    else
                    {
                        Trace.EasyIceServicesTopic.note("Transferring interaction '{}' to '{}''s voicemail.", interaction.InteractionId.Id, username);
                        interaction.TransferToVoicemail(new QueueId(QueueType.User, username));
                        Trace.EasyIceServicesTopic.note("Successfully transferred interaction '{}' to '{}''s voicemail.", interaction.InteractionId.Id, username);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public PlayWavFileResponse PlayWavFile(string interactionId, PlayWavFileData playWavFileData)
        {
            var response = new PlayWavFileResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(playWavFileData.WavFile, "wavFile");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (!GetInteractionType(interactionId).Type.Equals("Call", StringComparison.InvariantCultureIgnoreCase))
                        throw new Exception(String.Format("Interaction '{0}' is not a call interaction.", interactionId));

                    var interaction = (CallInteraction)GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Playing '{}' to interaction '{}'.", playWavFileData.WavFile, interaction.InteractionId.Id);

                    if (playWavFileData.Remote)
                    {
                        interaction.PlayWaveAudio(RemoteFileHelper.NewRemoteFileName(ServerFileType.Other) + playWavFileData.WavFile, playWavFileData.Remote, playWavFileData.EnableDigits);
                    }
                    else
                    {
                        interaction.PlayWaveAudio(playWavFileData.WavFile, playWavFileData.Remote, playWavFileData.EnableDigits);
                    }
                    Trace.EasyIceServicesTopic.note("Successfully played '{}' to interaction '{}'.", playWavFileData.WavFile, interaction.InteractionId.Id);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public PlayWavFileResponse PlayWavFileExtended(string interactionId, string wavFile, bool remote, bool playDigits)
        {
            return PlayWavFile(interactionId, new PlayWavFileData(wavFile, remote, playDigits));
        }
        /// <inheritdoc/>
        public PlayDigitsResponse PlayDigits(string interactionId, PlayDigitsData playDigitsData)
        {
            var response = new PlayDigitsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(playDigitsData.Digits, "digits");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (!GetInteractionType(interactionId).Type.Equals("Call", StringComparison.InvariantCultureIgnoreCase))
                        throw new Exception(String.Format("Interaction '{0}' is not a call interaction.", interactionId));

                    var interaction = (CallInteraction)GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Playing digits '{}' to interaction '{}'.", playDigitsData.Digits, interaction.InteractionId.Id);
                    interaction.PlayDigits(playDigitsData.Digits);

                    Trace.EasyIceServicesTopic.note("Successfully played digits '{}' to interaction '{}'.", playDigitsData.Digits, interaction.InteractionId.Id);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public PlayDigitsResponse PlayDigitsExtended(string interactionId, string digits)
        {
            return PlayDigits(interactionId, new PlayDigitsData(digits));
        }
        /// <inheritdoc/>
        public StopWaveAudioResponse StopWaveAudio(string interactionId)
        {
            var response = new StopWaveAudioResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (!GetInteractionType(interactionId).Type.Equals("Call", StringComparison.InvariantCultureIgnoreCase))
                        throw new Exception(String.Format("Interaction '{0}' is not a call interaction.", interactionId));

                    var interaction = (CallInteraction)GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Stopping all wave audio on interaction '{}'.", interaction.InteractionId.Id);
                    interaction.StopAllWaveAudio();

                    Trace.EasyIceServicesTopic.note("Successfully stopped all wave audio on interaction '{}'.", interaction.InteractionId.Id);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Interaction Properties
        /// <inheritdoc/>
        public GetInteractionDurationResponse GetInteractionDuration(string interactionId)
        {
            var response = new GetInteractionDurationResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.InitiationTime); // Required to get the duration
                    GetInteractionAttribute(interaction, InteractionAttributeName.DisconnectionTime); // Required to get the duration
                    GetInteractionAttribute(interaction, InteractionAttributeName.State); // Required to get the duration

                    response.Duration = interaction.Duration.TotalSeconds;
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetInteractionInitiationTimeResponse GetInteractionInitiationTime(string interactionId)
        {
            var response = new GetInteractionInitiationTimeResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.InitiationTime); // Required to get the initiation time

                    response.InitiationTime = interaction.InitiationTime;
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetInteractionDisconnectionTimeResponse GetInteractionDisconnectionTime(string interactionId)
        {
            var response = new GetInteractionDisconnectionTimeResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }

                    if (!IsDisconnected(interaction))
                        throw new Exception(String.Format("Interaction '{0}' is not disconnected yet.", interaction.InteractionId.Id));
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.DisconnectionTime); // Required to get the disconnection time

                    response.DisconnectionTime = interaction.DisconnectionTime;
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetInteractionTypeResponse GetInteractionType(string interactionId)
        {
            var response = new GetInteractionTypeResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.InteractionType); // Required to get interaction type

                    switch (interaction.InteractionType)
                    {
                        case InteractionType.Call:
                            response.Type = "Call";
                            break;
                        case InteractionType.Callback:
                            response.Type = "Callback";
                            break;
                        case InteractionType.Chat:
                            response.Type = "Chat";
                            break;
                        case InteractionType.Email:
                            response.Type = "Email";
                            break;
                        case InteractionType.Generic:
                            response.Type = "Generic";
                            break;
                        case InteractionType.Monitor:
                            response.Type = "Monitor";
                            break;
                        case InteractionType.None:
                            response.Type = "None";
                            break;
                        case InteractionType.Recorder:
                            response.Type = "Recorder";
                            break;
                        case InteractionType.WorkItem:
                            response.Type = "WorkItem";
                            break;
                        default:
                            response.Type = "Unknown";
                            break;
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetInteractionStatusesResponse GetInteractionStatuses(string interactionId)
        {
            var response = new GetInteractionStatusesResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.State); // Required to get IsConnected/IsDisconnected/IsHeld
                    GetInteractionAttribute(interaction, InteractionAttributeName.Monitors); // Required to get IsMonitored
                    GetInteractionAttribute(interaction, InteractionAttributeName.SupervisorMonitors); // Required to get IsMonitored
                    GetInteractionAttribute(interaction, InteractionAttributeName.Muted); // Required to get IsMuted
                    GetInteractionAttribute(interaction, InteractionAttributeName.Recorders); // Required to get IsPaused
                    GetInteractionAttribute(interaction, InteractionAttributeName.SupervisorRecorders); // Required to get IsPaused
                    GetInteractionAttribute(interaction, InteractionAttributeName.Private); // Required to get IsPrivate

                    response.IsConnected = interaction.IsConnected;
                    response.IsDisconnected = interaction.IsDisconnected;
                    response.IsHeld = interaction.IsHeld;
                    response.IsMonitored = interaction.IsMonitored;
                    response.IsMuted = interaction.IsMuted;
                    response.IsPaused = interaction.IsPaused;
                    response.IsPrivate = interaction.IsPrivate;
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetInteractionLastSegmentResponse GetInteractionLastSegment(string interactionId)
        {
            var response = new GetInteractionLastSegmentResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    response.SegmentId = interaction.LastSegment.SegmentId;
                    response.QueueId = interaction.LastSegment.QueueId;
                    response.UserId = interaction.LastSegment.UserId;
                    response.WrapUpRequired = interaction.LastSegment.WrapUpRequired;
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetAccountCodeIdResponse GetAccountCodeId(string interactionId)
        {
            var response = new GetAccountCodeIdResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    response.AccountCodeId = GetInteractionAttribute(interaction, InteractionAttributeName.AccountCodeId);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SetAccountCodeIdResponse SetAccountCodeId(string interactionId, string accountCodeId)
        {
            var response = new SetAccountCodeIdResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    interaction.SetAccountCodeId(accountCodeId);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetRemoteNameResponse GetRemoteName(string interactionId)
        {
            var response = new GetRemoteNameResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    response.RemoteName = GetInteractionAttribute(interaction, InteractionAttributeName.RemoteName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SetRemoteNameResponse SetRemoteName(string interactionId, string remoteName)
        {
            var response = new SetRemoteNameResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    interaction.SetRemoteName(remoteName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetWrapUpCodeIdResponse GetWrapUpCodeId(string interactionId)
        {
            var response = new GetWrapUpCodeIdResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    response.WrapUpCodeId = GetInteractionAttribute(interaction, InteractionAttributeName.WrapUpAssignments);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SetWrapUpCodeIdResponse SetWrapUpCodeId(string interactionId, string wrapUpCodeId)
        {
            var response = new SetWrapUpCodeIdResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    _interactionsManager.AddWrapUpAssignment(new WrapUpAssignment(new InteractionId(interactionId), new InteractionId(interactionId), 0, wrapUpCodeId, DateTime.Now));
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetTimeInWorkgroupQueueResponse GetTimeInWorkgroupQueue(string interactionId)
        {
            var response = new GetTimeInWorkgroupQueueResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    var interaction = GetInteraction(interactionId);
                    if (interaction == null)
                    {
                        response.ErrorMessage = String.Format("Interaction '{0}' either does not exist or can't be accessed.", interactionId);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                        return response;
                    }
                    #endregion

                    GetInteractionAttribute(interaction, InteractionAttributeName.WorkgroupQueueTimestamp); // Required to get TimeInWorkgroupQueue
                    GetInteractionAttribute(interaction, InteractionAttributeName.DisconnectionTime); // Required to get TimeInWorkgroupQueue
                    GetInteractionAttribute(interaction, InteractionAttributeName.State); // Required to get TimeInWorkgroupQueue

                    response.TimeInWorkgroupQueue = interaction.TimeInWorkgroupQueue.TotalSeconds;
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Call Interactions
        /// <inheritdoc/>
        public CreateCallInteractionResponse CreateCallInteraction(CreateCallInteractionData createCallInteractionData)
        {
            var response = new CreateCallInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    Trace.EasyIceServicesTopic.note("Making call to '{}'. Workgroup: '{}', AccountCodeId: '{}'.", createCallInteractionData.PhoneNumber, createCallInteractionData.FromWorkgroup, createCallInteractionData.AccountCodeId);

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(createCallInteractionData.PhoneNumber, "phoneNumber");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    var callInteractionParameters = new CallInteractionParameters(createCallInteractionData.PhoneNumber)
                        {
                            CallMadeStage = CallMadeStage.Allocated,
                            OnBehalfOfWorkgroup = createCallInteractionData.FromWorkgroup,
                            AccountCodeId = createCallInteractionData.AccountCodeId
                        };

                    var interaction = _interactionsManager.MakeCall(callInteractionParameters);
                    interaction.AttributesChanged += interaction_AttributesChanged;
                    interaction.Deallocated += interaction_Deallocated;
                    interaction.StartWatching(_interactionAttributesToWatch.ToArray());

                    #region Populate Response
                    Trace.EasyIceServicesTopic.note("Call Interaction '{}' was successfully created. State: '{}'.", interaction.InteractionId.Id, interaction.State);
                    response.InteractionId = interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture);
                    response.InteractionIdKey = GetInteractionAttribute(interaction, InteractionAttributeName.CallIdKey);
                    response.Success = true;
                    #endregion
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CreateCallInteractionResponse CreateCallInteractionExtended(string phoneNumber, string fromWorkgroup, string accountCodeId)
        {
            return CreateCallInteraction(new CreateCallInteractionData(phoneNumber, fromWorkgroup, accountCodeId));
        }
        #endregion

        #region Chat Interactions
        /// <inheritdoc/>
        public CreateChatInteractionResponse CreateChatInteraction(CreateChatInteractionData createChatInteractionData)
        {
            var response = new CreateChatInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    Trace.EasyIceServicesTopic.note("Creating chat to '{}'.", createChatInteractionData.Username);

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(createChatInteractionData.Username, "phoneNumber");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    var interaction = _interactionsManager.MakeChat(createChatInteractionData.Username);
                    interaction.AttributesChanged += interaction_AttributesChanged;
                    interaction.Deallocated += interaction_Deallocated;
                    interaction.StartWatching(_interactionAttributesToWatch.ToArray());

                    #region Populate Response
                    Trace.EasyIceServicesTopic.note("Chat Interaction '{}' was successfully created. State: '{}'.", interaction.InteractionId.Id, interaction.State);
                    response.InteractionId = interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture);
                    response.InteractionIdKey = GetInteractionAttribute(interaction, InteractionAttributeName.CallIdKey);
                    response.Success = true;
                    #endregion
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CreateChatInteractionResponse CreateChatInteractionExtended(string username)
        {
            return CreateChatInteraction(new CreateChatInteractionData(username));
        }
        /// <inheritdoc/>
        public WriteToChatInteractionResponse WriteToChatInteraction(string interactionId, WriteToChatInteractionData writeToChatInteractionData)
        {
            var response = new WriteToChatInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    Trace.EasyIceServicesTopic.note("Writing message '{}' to chat '{}'.", interactionId, writeToChatInteractionData.Message);

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    Guard.ArgumentNotNullOrEmptyString(writeToChatInteractionData.Message, "message");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    if (!GetInteractionType(interactionId).Type.Equals("chat", StringComparison.InvariantCultureIgnoreCase))
                        throw new Exception(String.Format("Interaction '{0}' is not a chat interaction.", interactionId));
                    #endregion

                    var interaction = (ChatInteraction)GetInteraction(interactionId);
                    interaction.SendText(writeToChatInteractionData.Message);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public WriteToChatInteractionResponse WriteToChatInteractionExtended(string interactionId, string message)
        {
            return WriteToChatInteraction(interactionId, new WriteToChatInteractionData(message));
        }
        #endregion

        #region Email Interactions

        #region Private Methods
        private void AddEmailInteractionToList(CreateEmailInteractionData emailInteraction)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                lock (_activeEmailInteractions)
                {
                    if (!_activeEmailInteractions.Contains(emailInteraction))
                    {
                        Trace.EasyIceServicesTopic.verbose("Adding email interaction '{}' to the list of active email interactions.", emailInteraction.InteractionId.Id);
                        _activeEmailInteractions.Add(emailInteraction);
                    }
                }
            }
        }
        private void RemoveEmailInteractionFromList(CreateEmailInteractionData emailInteraction)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                lock (_activeEmailInteractions)
                {
                    if (_activeEmailInteractions.Contains(emailInteraction))
                    {
                        Trace.EasyIceServicesTopic.verbose("Removing email interaction '{}' from the list of active email interactions.", emailInteraction.InteractionId.Id);
                        _activeEmailInteractions.Remove(emailInteraction);
                    }
                }
            }
        }
        #endregion

        #region Events
        void emailInteraction_AttributesChanged(object sender, AttributesEventArgs e)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    var emailInteraction = sender as EmailInteraction;
                    if (emailInteraction == null) return;

                    foreach (var interactionAttributeName in e.InteractionAttributeNames)
                    {
                        Trace.EasyIceServicesTopic.verbose("Interaction '{}' attribute '{}' value has changed to '{}'.", emailInteraction.InteractionId.Id, interactionAttributeName, emailInteraction.GetStringAttribute(interactionAttributeName));
                    }

                    if (!e.InteractionAttributeNames.Contains(InteractionAttributeName.State)) return;

                    var emailInteractionData = _activeEmailInteractions.FirstOrDefault(activeEmailInteraction => activeEmailInteraction.InteractionId.Equals(emailInteraction.InteractionId));
                    //Very unlikely to happen but who knows?
                    if (emailInteractionData == null)
                    {
                        Trace.EasyIceServicesTopic.note("Email Interaction was not found in the list of active email interactions.");
                        return;
                    }

                    switch (emailInteraction.State)
                    {
                        case InteractionState.Alerting:
                        case InteractionState.Held:
                        case InteractionState.Offering:
                        case InteractionState.Parked:
                        case InteractionState.Proceeding:
                        case InteractionState.Suspended:
                        case InteractionState.System:
                            AddEmailInteractionToList(emailInteractionData);
                            break;
                        case InteractionState.Connected:
                        case InteractionState.ExternalDisconnect:
                        case InteractionState.InternalDisconnect:
                        case InteractionState.Messaging:
                        case InteractionState.None:
                            RemoveEmailInteractionFromList(emailInteractionData);
                            break;
                        default:
                            Trace.EasyIceServicesTopic.error("Unknown state for email interaction '{}'.", emailInteraction.InteractionId.Id);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
            }
        }
        #endregion

        /// <inheritdoc/>
        public CreateEmailInteractionResponse CreateEmailInteraction(CreateEmailInteractionData createEmailInteractionData)
        {
            var response = new CreateEmailInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    Trace.EasyIceServicesTopic.note("Making email to '{}' ('{}') with subject: '{}', body: '{}', from workgroup: '{}'.", createEmailInteractionData.SenderEmailAddress, createEmailInteractionData.SenderDisplayName, createEmailInteractionData.Subject, createEmailInteractionData.Body, createEmailInteractionData.FromWorkgroup);

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(createEmailInteractionData.SenderEmailAddress, "senderEmailAddress");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    var emailContent = new EmailContent
                        {
                            Sender =
                                new EmailAddress(createEmailInteractionData.SenderEmailAddress,
                                                 createEmailInteractionData.SenderDisplayName),
                            Subject = createEmailInteractionData.Subject,
                            Body = createEmailInteractionData.Body, 
                        };

                    var emailInteractionParameters = new EmailInteractionParameters(emailContent)
                        {
                            OnBehalfOfWorkgroup = createEmailInteractionData.FromWorkgroup
                        };
                    
                    var interaction = _interactionsManager.MakeEmail(emailInteractionParameters);

                    #region Start Watching in case it needs to be re-created after a switchover
                    createEmailInteractionData.InteractionId = interaction.InteractionId;
                    interaction.AttributesChanged += emailInteraction_AttributesChanged;
                    interaction.Deallocated += interaction_Deallocated;
                    interaction.StartWatching(_interactionAttributesToWatch.ToArray());
                    AddEmailInteractionToList(createEmailInteractionData);
                    #endregion

                    #region Populate Response
                    Trace.EasyIceServicesTopic.note("Email Interaction '{}' was successfully created.", interaction.InteractionId.Id);
                    response.InteractionId = interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture);
                    response.InteractionIdKey = GetInteractionAttribute(interaction, InteractionAttributeName.CallIdKey);
                    response.Success = true;
                    #endregion
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    if (!String.IsNullOrEmpty(createEmailInteractionData.FromWorkgroup))
                        response.ErrorMessage += ". Make sure the workgroup has its routing configured to handle email and that the mailbox is configured to send outgoing emails (Workgroup Configuration/ACD/Routing/Edit Mailbox/Check 'Use this mailbox to send outgoing emails' checkbox).";
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CreateEmailInteractionResponse CreateEmailInteractionExtended(string senderEmailAddress, string senderDisplayName, string subject, string body, string fromWorkgroup)
        {
            return CreateEmailInteraction(new CreateEmailInteractionData(senderEmailAddress, senderDisplayName, subject, body, fromWorkgroup));
        }
        #endregion

        #region Fax Interactions

        #region Private Methods
        private bool InitUnifiedMessagingManager()
        {
            if (_session == null || _session.ConnectionState != ConnectionState.Up)
            {
                return false;
            }

            if (_unifiedMessagingManager != null)
            {
                return true;
            }

            try
            {
                _unifiedMessagingManager = UnifiedMessagingManager.GetInstance(_session);
                _unifiedMessagingManager.FaxUpdated += _unifiedMessagingManager_FaxUpdated;
            }
            catch
            {
                _unifiedMessagingManager = null;
                return false;
            }

            return true;
        }
        private void CleanUpUnifiedMessagingManager()
        {
            if (_unifiedMessagingManager != null)
            {
                _unifiedMessagingManager.FaxUpdated -= _unifiedMessagingManager_FaxUpdated;
                _unifiedMessagingManager = null;
            }
        }
        #endregion

        #region Events
        void _unifiedMessagingManager_FaxUpdated(object sender, FaxEventArgs e)
        {
            try
            {
                var interaction = sender as Interaction;
                if (interaction == null) return;

                Trace.EasyIceServicesTopic.verbose("Fax interaction '{}' has been updated. Message: '{}'.", interaction.InteractionId.Id, e.Message.ToString());
            }
            catch (Exception ex)
            {
                Trace.EasyIceServicesTopic.exception(ex);
            }
        }
        #endregion

        /// <inheritdoc/>
        public SendFaxResponse SendFax(SendFaxData sendFaxData)
        {
            var response = new SendFaxResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    Trace.EasyIceServicesTopic.note("Sending fax to '{}', image: '{}', recipient name: '{}', company: '{}', phone number: '{}', account code: '{}', email success: '{}', email failure: '{}', fax page size: '{}'.", sendFaxData.FaxNumber, sendFaxData.Image, sendFaxData.RecipientName, sendFaxData.Company, sendFaxData.PhoneNumber, sendFaxData.AccountCodeId, sendFaxData.EmailSuccess, sendFaxData.EmailFailure, sendFaxData.FaxPageSize);

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(sendFaxData.FaxNumber, "faxNumber");
                    Guard.ArgumentNotNullOrEmptyString(sendFaxData.Image, "Image");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (!sendFaxData.Image.EndsWith(".pdf", StringComparison.InvariantCultureIgnoreCase) &&
                        !sendFaxData.Image.EndsWith(".tif", StringComparison.InvariantCultureIgnoreCase))
                    {
                        throw new Exception("File format not supported. Only .TIF or .PDF files can be faxed.");
                    }
                    #endregion

                    //Create fax file
                    var faxFile = new FaxFile();

                    //Set recipient
                    var faxAddressee = new FaxAddressee
                        {
                            Fax = sendFaxData.FaxNumber,
                            Name = sendFaxData.RecipientName,
                            Phone = sendFaxData.PhoneNumber,
                            AccountCode = sendFaxData.AccountCodeId,
                            Company = sendFaxData.Company
                        };

                    //Create other objects required by the fax
                    var faxCoverPageTemplate = new FaxCoverPageTemplate();
                    var faxDeliverySchedule = new FaxDeliverySchedule
                        {
                            Type = FaxDeliveryType.Immediate
                        };
                    var faxDeliveryNotification = new FaxDeliveryNotification
                        {
                            FailureEmail = sendFaxData.EmailFailure,
                            NotifyEmail = sendFaxData.EmailSuccess,
                            NotifyOnFailure = true,
                            NotifyOnSuccess = true
                        };
                    var faxTransmitOptions = new FaxTransmitOptions();

                    Trace.EasyIceServicesTopic.note("Creating fax envelope and assigning it to the fax file.");
                    var faxEnvelope = new FaxEnvelope(faxAddressee, faxCoverPageTemplate, faxDeliverySchedule, faxDeliveryNotification, faxTransmitOptions)
                    {
                        SenderId = _session.UserId,
                        TimeSent = DateTime.Now
                    };
                    faxFile.AddEnvelope(faxEnvelope);

                    var faxPageAttributes = new FaxPageAttributes
                        {
                            ColorScheme = FaxColorScheme.Normal,
                            Orientation = FaxOrientation.Normal,
                            Rotation = FaxRotation.Normal
                        };

                    FaxPageSize faxPageSizeSettings;
                    switch (sendFaxData.FaxPageSize.ToLower())
                    {
                        case "a4":
                            faxPageSizeSettings = FaxPageSize.A4;
                            break;
                        case "legal":
                            faxPageSizeSettings = FaxPageSize.Legal;
                            break;
                        case "letter":
                            faxPageSizeSettings = FaxPageSize.Letter;
                            break;
                        default:
                            faxPageSizeSettings = FaxPageSize.Letter;
                            break;
                    }
                    var faxPageSettings = new FaxPageSettings(FaxImageSettings.FitToPage, FaxImageQuality.Fine, faxPageSizeSettings);

                    //Convert file from PDF to TIF if needed
                    if (sendFaxData.Image.EndsWith(".pdf", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!PDFConvert.ConvertPDFToTiff(sendFaxData.Image, Path.GetDirectoryName(typeof(EasyIceServiceImpl).Assembly.Location) + "\\Conversion"))
                        {
                            throw new Exception("Failed to convert PDF file to TIFF.");
                        }
                        sendFaxData.Image = sendFaxData.Image.Replace(".pdf", ".tif");
                    }

                    //Add the pages
                    if (sendFaxData.Image.EndsWith(".tif", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //Get the image
                        var imageStream = new StreamReader(sendFaxData.Image);
                        var imageFromStream = Image.FromStream(imageStream.BaseStream);
                        faxFile.AddPage(imageFromStream, faxPageAttributes, faxPageSettings);
                    }

                    string tempFilename = Path.GetTempFileName();
                    Trace.EasyIceServicesTopic.note("Fax temp filename: '{}'.", tempFilename);
                    faxFile.SaveAs(tempFilename, StorageFormat.I3F);

                    Trace.EasyIceServicesTopic.note("Sending fax now.");
                    var faxResult = _unifiedMessagingManager.SendFax(tempFilename);
                    Trace.EasyIceServicesTopic.note("Fax Result: ", faxResult.ToString());
                    Trace.EasyIceServicesTopic.note("Deleting temp filename: '{}'.", tempFilename);
                    File.Delete(tempFilename);

                    #region Populate Response
                    response.Success = true;
                    #endregion

                    faxFile.Dispose();
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SendFaxResponse SendFaxExtended(string faxNumber, string image, string recipientName, string company, string phoneNumber, string accountCodeId, string emailSuccess, string emailFailure, string faxPageSize)
        {
            return SendFax(new SendFaxData(faxNumber, image, recipientName, company, phoneNumber, accountCodeId, emailSuccess, emailFailure, faxPageSize));
        }
        #endregion

        #region Generic Interactions

        #region Private Methods
        private void AddGenericInteractionToList(CreateGenericInteractionData genericInteraction)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                lock (_activeGenericInteractions)
                {
                    if (!_activeGenericInteractions.Contains(genericInteraction))
                    {
                        Trace.EasyIceServicesTopic.verbose("Adding generic interaction '{}' to the list of active generic interactions.", genericInteraction.InteractionId.Id);
                        _activeGenericInteractions.Add(genericInteraction);
                    }
                }
            }
        }
        private void RemoveGenericInteractionFromList(CreateGenericInteractionData genericInteraction)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                lock (_activeGenericInteractions)
                {
                    if (_activeGenericInteractions.Contains(genericInteraction))
                    {
                        Trace.EasyIceServicesTopic.verbose("Removing generic interaction '{}' from the list of active generic interactions.", genericInteraction.InteractionId.Id);
                        _activeGenericInteractions.Remove(genericInteraction);
                    }
                }
            }
        }
        #endregion

        #region Events
        void genericInteraction_AttributesChanged(object sender, AttributesEventArgs e)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    var genericInteraction = sender as GenericInteraction;
                    if (genericInteraction == null) return;

                    foreach (var interactionAttributeName in e.InteractionAttributeNames)
                    {
                        Trace.EasyIceServicesTopic.verbose("Interaction '{}' attribute '{}' value has changed to '{}'.", genericInteraction.InteractionId.Id, interactionAttributeName, genericInteraction.GetStringAttribute(interactionAttributeName));
                    }

                    if (!e.InteractionAttributeNames.Contains(InteractionAttributeName.State)) return;

                    var genericInteractionData = _activeGenericInteractions.FirstOrDefault(activeGenericInteraction => activeGenericInteraction.InteractionId.Equals(genericInteraction.InteractionId));
                    //Very unlikely to happen but who knows?
                    if (genericInteractionData == null)
                    {
                        Trace.EasyIceServicesTopic.note("Generic Interaction was not found in the list of active generic interactions.");
                        return;
                    }

                    switch (genericInteraction.State)
                    {
                        case InteractionState.Alerting:
                        case InteractionState.Held:
                        case InteractionState.Offering:
                        case InteractionState.Parked:
                        case InteractionState.Proceeding:
                        case InteractionState.Suspended:
                        case InteractionState.System:
                            AddGenericInteractionToList(genericInteractionData);
                            break;
                        case InteractionState.Connected:
                        case InteractionState.ExternalDisconnect:
                        case InteractionState.InternalDisconnect:
                        case InteractionState.Messaging:
                        case InteractionState.None:
                            RemoveGenericInteractionFromList(genericInteractionData);
                            break;
                        default:
                            Trace.EasyIceServicesTopic.error("Unknown state for generic interaction '{}'.", genericInteraction.InteractionId.Id);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
            }
        }
        #endregion

        /// <inheritdoc/>
        public CreateGenericInteractionResponse CreateGenericInteraction(CreateGenericInteractionData createGenericInteractionData)
        {
            var response = new CreateGenericInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    Trace.EasyIceServicesTopic.note("Creating generic interaction with: Remote Name: '{}', Remote Id: '{}', Workgroup: '{}', Skill Name: '{}', Interaction Attribute Name: '{}', Interaction Attribute Value: '{}; .", createGenericInteractionData.RemoteName, createGenericInteractionData.RemoteId, createGenericInteractionData.Workgroup, createGenericInteractionData.SkillName, createGenericInteractionData.InteractionAttributeName, createGenericInteractionData.InteractionAttributeValue);

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(createGenericInteractionData.RemoteName, "remoteName");
                    Guard.ArgumentNotNullOrEmptyString(createGenericInteractionData.RemoteId, "remoteId");
                    Guard.ArgumentNotNullOrEmptyString(createGenericInteractionData.Workgroup, "workgroup");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    var genericInteractionParameters = new GenericInteractionParameters(new QueueId(QueueType.Workgroup, createGenericInteractionData.Workgroup), InteractionState.System);
                    if (!String.IsNullOrEmpty(createGenericInteractionData.InteractionAttributeName))
                        genericInteractionParameters.AdditionalAttributes.Add(createGenericInteractionData.InteractionAttributeName, createGenericInteractionData.InteractionAttributeValue);
                    genericInteractionParameters.InteractionDirection = InteractionDirection.Incoming;
                    genericInteractionParameters.RemoteName = createGenericInteractionData.RemoteName;
                    genericInteractionParameters.RemoteId = createGenericInteractionData.RemoteId;
                    genericInteractionParameters.RemotePartyType = InteractionPartyType.Internal;
                    if (!String.IsNullOrEmpty(createGenericInteractionData.SkillName))
                    {
                        //The semicolon is obligatory otherwise the skills won't be set correctly. ??
                        genericInteractionParameters.AdditionalAttributes.Add("Custom_ACDSkills", createGenericInteractionData.SkillName);
                    }
                    var interaction = _interactionsManager.MakeGenericInteraction(genericInteractionParameters);

                    #region Start Watching in case it needs to be re-created after a switchover
                    createGenericInteractionData.InteractionId = interaction.InteractionId;
                    interaction.AttributesChanged += genericInteraction_AttributesChanged;
                    interaction.Deallocated += interaction_Deallocated;
                    interaction.StartWatching(_interactionAttributesToWatch.ToArray());
                    AddGenericInteractionToList(createGenericInteractionData);
                    #endregion

                    #region Populate Response
                    Trace.EasyIceServicesTopic.note("Generic Interaction '{}' was successfully created.", interaction.InteractionId.Id);
                    response.InteractionId = interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture);
                    response.InteractionIdKey = GetInteractionAttribute(interaction, InteractionAttributeName.CallIdKey);
                    response.Success = true;
                    #endregion
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CreateGenericInteractionResponse CreateGenericInteractionExtended(string remoteName, string remoteId, string workgroup, string skillName, string interactionAttributeName, string interactionAttributeValue)
        {
            return CreateGenericInteraction(new CreateGenericInteractionData(remoteName, remoteId, workgroup, skillName, interactionAttributeName, interactionAttributeValue));
        }
        #endregion

        #region Callback Interactions

        #region Private Methods
        private void AddCallbackInteractionToList(CreateCallbackInteractionData callbackInteraction)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                lock (_activeCallbackInteractions)
                {
                    if (!_activeCallbackInteractions.Contains(callbackInteraction))
                    {
                        Trace.EasyIceServicesTopic.verbose("Adding callback interaction '{}' to the list of active callback interactions.", callbackInteraction.InteractionId.Id);
                        _activeCallbackInteractions.Add(callbackInteraction);
                    }
                }
            }
        }
        private void RemoveCallbackInteractionFromList(CreateCallbackInteractionData callbackInteraction)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                lock (_activeCallbackInteractions)
                {
                    if (_activeCallbackInteractions.Contains(callbackInteraction))
                    {
                        Trace.EasyIceServicesTopic.verbose("Removing callback interaction '{}' from the list of active callback interactions.", callbackInteraction.InteractionId.Id);
                        _activeCallbackInteractions.Remove(callbackInteraction);
                    }
                }
            }
        }
        #endregion

        #region Events
        void callbackInteraction_AttributesChanged(object sender, AttributesEventArgs e)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    var callbackInteraction = sender as CallbackInteraction;
                    if (callbackInteraction == null) return;

                    foreach (var interactionAttributeName in e.InteractionAttributeNames)
                    {
                        Trace.EasyIceServicesTopic.verbose("Interaction '{}' attribute '{}' value has changed to '{}'.", callbackInteraction.InteractionId.Id, interactionAttributeName, callbackInteraction.GetStringAttribute(interactionAttributeName));
                    }

                    if (!e.InteractionAttributeNames.Contains(InteractionAttributeName.State)) return;

                    var callbackInteractionData = _activeCallbackInteractions.FirstOrDefault(activeCallbackInteraction => activeCallbackInteraction.InteractionId.Equals(callbackInteraction.InteractionId));
                    //Very unlikely to happen but who knows?
                    if (callbackInteractionData == null)
                    {
                        Trace.EasyIceServicesTopic.note("Callback Interaction was not found in the list of active callback interactions.");
                        return;
                    }

                    switch (callbackInteraction.State)
                    {
                        case InteractionState.Alerting:
                        case InteractionState.Held:
                        case InteractionState.Offering:
                        case InteractionState.Parked:
                        case InteractionState.Proceeding:
                        case InteractionState.Suspended:
                        case InteractionState.System:
                            AddCallbackInteractionToList(callbackInteractionData);
                            break;
                        case InteractionState.Connected:
                        case InteractionState.ExternalDisconnect:
                        case InteractionState.InternalDisconnect:
                        case InteractionState.Messaging:
                        case InteractionState.None:
                            RemoveCallbackInteractionFromList(callbackInteractionData);
                            break;
                        default:
                            Trace.EasyIceServicesTopic.error("Unknown state for callback interaction '{}'.", callbackInteraction.InteractionId.Id);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
            }
        }
        #endregion

        /// <inheritdoc/>
        public CreateCallbackInteractionResponse CreateCallbackInteraction(CreateCallbackInteractionData createCallbackInteractionData)
        {
            var response = new CreateCallbackInteractionResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    Trace.EasyIceServicesTopic.note("Creating callback interaction with: Phone number: '{}', Name: '{}', Message: '{}', Workgroup: '{}', Skill Name: '{}', ACD Priority: '{}', Notes: '{}; .", createCallbackInteractionData.PhoneNumber, createCallbackInteractionData.Name, createCallbackInteractionData.Message, createCallbackInteractionData.Workgroup, createCallbackInteractionData.SkillName, createCallbackInteractionData.AcdPriority, createCallbackInteractionData.Notes);

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(createCallbackInteractionData.PhoneNumber, "phoneNumber");
                    Guard.ArgumentNotNullOrEmptyString(createCallbackInteractionData.Workgroup, "workgroup");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    var additionalAttributes = new Dictionary<string, string>();
                    if (!String.IsNullOrEmpty(createCallbackInteractionData.Name))
                        additionalAttributes.Add(InteractionAttributeName.RemoteName, createCallbackInteractionData.Name);
                    if (!String.IsNullOrEmpty(createCallbackInteractionData.Notes))
                        additionalAttributes.Add(InteractionAttributeName.Notes, createCallbackInteractionData.Notes);
                    if (!String.IsNullOrEmpty(createCallbackInteractionData.SkillName))
                    {
                        additionalAttributes.Add("Custom_ACDSkills", String.Format("{0}", createCallbackInteractionData.SkillName));
                    }
                    if (createCallbackInteractionData.AcdPriority > 0)
                    {
                        additionalAttributes.Add("Custom_ACDPriority", createCallbackInteractionData.AcdPriority.ToString(CultureInfo.InvariantCulture));
                    }

                    var callbackInteractionParameters = new CallbackInteractionParameters(createCallbackInteractionData.Workgroup, createCallbackInteractionData.PhoneNumber, createCallbackInteractionData.Message, additionalAttributes);
                    var interaction = _interactionsManager.MakeCallbackInteraction(callbackInteractionParameters);

                    #region Start Watching in case it needs to be re-created after a switchover
                    createCallbackInteractionData.InteractionId = interaction.InteractionId;
                    interaction.AttributesChanged += callbackInteraction_AttributesChanged;
                    interaction.Deallocated += interaction_Deallocated;
                    interaction.StartWatching(_interactionAttributesToWatch.ToArray());
                    AddCallbackInteractionToList(createCallbackInteractionData);
                    #endregion

                    #region Populate Response
                    Trace.EasyIceServicesTopic.note("Callback Interaction '{}' was successfully created.", interaction.InteractionId.Id);
                    response.InteractionId = interaction.InteractionId.Id.ToString(CultureInfo.InvariantCulture);
                    response.InteractionIdKey = GetInteractionAttribute(interaction, InteractionAttributeName.CallIdKey);
                    response.Success = true;
                    #endregion
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CreateCallbackInteractionResponse CreateCallbackInteractionExtended(string phoneNumber, string name, string message, string workgroup, string skillName, int acdPriority, string notes)
        {
            return CreateCallbackInteraction(new CreateCallbackInteractionData(phoneNumber, name, message, workgroup, skillName, acdPriority, notes));
        }
        #endregion

        #endregion

        #region Configuration

        #region Private Methods
        private bool InitConfigurationManager()
        {
            if (_session == null || _session.ConnectionState != ConnectionState.Up)
            {
                return false;
            }

            if (_configurationManager != null)
            {
                return true;
            }

            try
            {
                _configurationManager = ConfigurationManager.GetInstance(_session);
            }
            catch
            {
                _configurationManager = null;
                return false;
            }

            return true;
        }
        private void CleanUpConfigurationManager()
        {
            _configurationManager = null;
        }
        #endregion

        #region Users
        /// <inheritdoc/>
        public GetUsersResponse GetUsers()
        {
            var response = new GetUsersResponse();
            UserConfigurationList userConfigurationList = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting users.");
                    userConfigurationList = new UserConfigurationList(_configurationManager);
                    var querySettings = userConfigurationList.CreateQuerySettings();
                    querySettings.SetPropertiesToRetrieve(new[] { UserConfiguration.Property.Id });
                    userConfigurationList.StartCaching(querySettings);
                    var userConfigurations = userConfigurationList.GetConfigurationList();

                    foreach (var userConfiguration in userConfigurations)
                    {
                        response.Users.Add(userConfiguration.ConfigurationId.Id);
                    }
                    Trace.EasyIceServicesTopic.note("Got '{}' users.", response.Users.Count);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (userConfigurationList != null && userConfigurationList.IsCaching)
                        userConfigurationList.StopCaching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetUserResponse GetUser(string username)
        {
            var response = new GetUserResponse();
            UserConfigurationList userConfigurationList = null;
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(username, "username");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting user '{}'.", username);
                    userConfigurationList = new UserConfigurationList(_configurationManager);
                    var querySettings = userConfigurationList.CreateQuerySettings();
                    var filter = new BasicFilterDefinition<UserConfiguration, UserConfiguration.Property>(UserConfiguration.Property.Id, username);
                    querySettings.SetPropertiesToRetrieve(new[] {
                        UserConfiguration.Property.AutoAnswerAcdInteractions,
                        UserConfiguration.Property.AutoAnswerNonAcdInteractions,
                        UserConfiguration.Property.Alias,
                        UserConfiguration.Property.DefaultWorkstation,
                        UserConfiguration.Property.DisplayName,
                        UserConfiguration.Property.ExcludeFromDirectory,
                        UserConfiguration.Property.ExtCallTimeout,
                        UserConfiguration.Property.Extension,
                        UserConfiguration.Property.HomeSite,
                        UserConfiguration.Property.Id,
                        UserConfiguration.Property.NtDomainUser,
                        UserConfiguration.Property.OutboundAni,
                        UserConfiguration.Property.PersonalInformation_City,
                        UserConfiguration.Property.PersonalInformation_CompanyName,
                        UserConfiguration.Property.PersonalInformation_Country,
                        UserConfiguration.Property.PersonalInformation_DepartmentName,
                        UserConfiguration.Property.PersonalInformation_EmailAddress,
                        UserConfiguration.Property.PersonalInformation_GivenName,
                        UserConfiguration.Property.PersonalInformation_PostalCode,
                        UserConfiguration.Property.PersonalInformation_Title,
                        UserConfiguration.Property.PersonalInformation_StreetAddress,
                        UserConfiguration.Property.PersonalInformation_StateOrProvince,
                        UserConfiguration.Property.PersonalInformation_Surname,
                        UserConfiguration.Property.PreferredLanguage, 
                        UserConfiguration.Property.Roles, 
                        UserConfiguration.Property.Skills, 
                        UserConfiguration.Property.Workgroups
                    });
                    querySettings.SetFilterDefinition(filter);
                    userConfigurationList.StartCaching(querySettings);

                    var userConfigurations = userConfigurationList.GetConfigurationList();

                    #region User Exists?
                    if (userConfigurations.Count == 0)
                    {
                        Trace.EasyIceServicesTopic.error("Username '{}' does not exist.", username);
                        response.ErrorMessage = String.Format("Username '{0}' does not exist.", username);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        userConfigurationList.StopCaching();
                        return response;
                    }
                    #endregion

                    var userConfiguration = userConfigurations[0];
                    foreach (var workgroupConfiguration in userConfiguration.Workgroups.Value)
                    {
                        response.UserConfiguration.Workgroups.Add(workgroupConfiguration.Id);
                    }
                    foreach (var skillConfiguration in userConfiguration.Skills.Value)
                    {
                        response.UserConfiguration.Skills.Add(skillConfiguration.Id.Id);
                    }
                    foreach (var roleConfiguration in userConfiguration.Roles.Value)
                    {
                        response.UserConfiguration.Roles.Add(roleConfiguration.Id);
                    }
                    response.UserConfiguration.PreferredLanguage = userConfiguration.PreferredLanguage.Value;
                    response.UserConfiguration.Title = userConfiguration.PersonalInformation.Title.Value;
                    response.UserConfiguration.Surname = userConfiguration.PersonalInformation.Surname.Value;
                    response.UserConfiguration.StreetAddress = userConfiguration.PersonalInformation.StreetAddress.Value;
                    response.UserConfiguration.StateOrProvince = userConfiguration.PersonalInformation.StateOrProvince.Value;
                    response.UserConfiguration.PostalCode = userConfiguration.PersonalInformation.PostalCode.Value;
                    response.UserConfiguration.GivenName = userConfiguration.PersonalInformation.GivenName.Value;
                    response.UserConfiguration.EmailAddress = userConfiguration.PersonalInformation.EmailAddress.Value;
                    response.UserConfiguration.DepartmentName = userConfiguration.PersonalInformation.DepartmentName.Value;
                    response.UserConfiguration.Country = userConfiguration.PersonalInformation.Country.Value;
                    response.UserConfiguration.CompanyName = userConfiguration.PersonalInformation.CompanyName.Value;
                    response.UserConfiguration.City = userConfiguration.PersonalInformation.City.Value;
                    response.UserConfiguration.NtDomainUser = userConfiguration.NtDomainUser.Value;
                    response.UserConfiguration.Id = userConfiguration.ConfigurationId.Id;
                    response.UserConfiguration.HomeSite = userConfiguration.HomeSite.Value;
                    response.UserConfiguration.Extension = userConfiguration.Extension.Value;
                    response.UserConfiguration.DisplayName = userConfiguration.ConfigurationId.DisplayName;
                    response.UserConfiguration.DefaultWorkstation = userConfiguration.DefaultWorkstation.Value != null ? userConfiguration.DefaultWorkstation.Value.Id : String.Empty;
                    response.UserConfiguration.Alias = userConfiguration.Alias.Value;
                    response.UserConfiguration.AutoAnswerAcdInteractions = userConfiguration.AutoAnswerAcdInteractions.Value.ToString();
                    response.UserConfiguration.AutoAnswerNonAcdInteractions = userConfiguration.AutoAnswerNonAcdInteractions.Value.ToString();
                    response.UserConfiguration.ExcludeFromDirectory = userConfiguration.ExcludeFromDirectory.Value.ToString();
                    response.UserConfiguration.ExtCallTimeout = userConfiguration.ExtCallTimeout.Value.ToString(CultureInfo.InvariantCulture);
                    response.UserConfiguration.InteractionOfferingTimeout = userConfiguration.InteractionOfferingTimeout.Value.TotalSeconds.ToString(CultureInfo.InvariantCulture);
                    response.UserConfiguration.OutboundAni = userConfiguration.OutboundAni.Value;


                    Trace.EasyIceServicesTopic.note("Successfully got user '{}'.", username);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (userConfigurationList != null && userConfigurationList.IsCaching)
                        userConfigurationList.StopCaching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetUserInteractionsResponse GetUserInteractions(string username)
        {
            var response = new GetUserInteractionsResponse();
            InteractionQueue userQueue = null;
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(username, "username");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (!GetUser(username).Success)
                    {
                        Trace.EasyIceServicesTopic.error("User {} does not exist.", username);
                        response.ErrorMessage = String.Format("User {0} does not exist.", username);
                        return response;
                    }

                    #endregion

                    userQueue = new InteractionQueue(_interactionsManager, new QueueId(QueueType.User, username));
                    userQueue.StartWatching(new[] { InteractionAttributeName.InteractionId });
                    foreach (var interaction in userQueue.GetContents())
                    {
                        response.InteractionIds.Add(interaction.InteractionId.Id.ToString());
                    }

                    Trace.EasyIceServicesTopic.note("Successfully retrieved user '{}' interactions. Got {} interaction.", username, response.InteractionIds.Count);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (userQueue != null && userQueue.IsWatching())
                        userQueue.StopWatching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CreateUserResponse CreateUser(string username, CreateUserData createUserData)
        {
            var response = new CreateUserResponse();
            var success = false;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(username, "username");
                    Guard.ArgumentNotNull(createUserData, "createUserData");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (GetUser(username).Success)
                        throw new Exception(String.Format("User '{0}' already exists.", username));
                    #endregion

                    Trace.EasyIceServicesTopic.note("Creating user '{}'.", username);
                    var userConfigurationList = new UserConfigurationList(_configurationManager);
                    var userConfiguration = userConfigurationList.CreateObject();
                    userConfiguration.PrepareForEdit();
                    userConfiguration.SetConfigurationId(username);
                    if (!String.IsNullOrEmpty(createUserData.Extension))
                        userConfiguration.Extension.Value = createUserData.Extension;
                    if (!String.IsNullOrEmpty(createUserData.Password))
                        userConfiguration.SetPassword(createUserData.Password, true);

                    try
                    {
                        userConfiguration.Commit();
                        success = true;
                    }
                    catch (ConfigurationValidationException configurationValidationException)
                    {
                        foreach (var configurationValidationIssue in configurationValidationException.GetValidationIssues<UserConfiguration.Property>())
                        {
                            var message = configurationValidationIssue.DisplayMessage;
                            var property = configurationValidationIssue.PropertyEnum;
                            var value = configurationValidationIssue.PropertyValue;
                            var issueKey = configurationValidationIssue.IssueKey;

                            var errorMessage = String.Format("Configuration Validation Exception: Message '{0}', Property: '{1}', Value: '{2}', Issue Key: '{3}'.", message, property, value, issueKey);
                            Trace.EasyIceServicesTopic.error(errorMessage);
                            response.ErrorMessage += errorMessage + " ";
                        }
                    }
                    if (success)
                    {
                        Trace.EasyIceServicesTopic.note("Successfully created user '{}'.", username);
                        response.Success = true;
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CreateUserResponse CreateUserExtended(string username, string password, string extension)
        {
            return CreateUser(username, new CreateUserData(password, extension));
        }
        /// <inheritdoc/>
        public GetUsersFromWorkgroupResponse GetUsersFromWorkgroup(string workgroup)
        {
            var response = new GetUsersFromWorkgroupResponse();
            WorkgroupConfigurationList workgroupConfigurationList = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(workgroup, "workgroup");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting users from workgroup: '{}'.", workgroup);
                    workgroupConfigurationList = new WorkgroupConfigurationList(_configurationManager);
                    var querySettings = workgroupConfigurationList.CreateQuerySettings();
                    var filter = new BasicFilterDefinition<WorkgroupConfiguration, WorkgroupConfiguration.Property>(WorkgroupConfiguration.Property.Id, workgroup);
                    querySettings.SetPropertiesToRetrieve(new[] { WorkgroupConfiguration.Property.Id, WorkgroupConfiguration.Property.Members });
                    querySettings.SetFilterDefinition(filter);
                    workgroupConfigurationList.StartCaching(querySettings);
                    var workgroupConfigurations = workgroupConfigurationList.GetConfigurationList();
                    if (workgroupConfigurations.Count == 0)
                    {
                        Trace.EasyIceServicesTopic.error("Workgroup '{}' does not exist.", workgroup);
                        response.ErrorMessage = String.Format("Workgroup '{0}' does not exist.", workgroup);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        workgroupConfigurationList.StopCaching();
                        return response;
                    }
                    var workgroupConfiguration = workgroupConfigurations[0];
                    foreach (var workgroupMemberConfigurationId in workgroupConfiguration.Members.Value)
                    {
                        response.Users.Add(workgroupMemberConfigurationId.Id);
                    }
                    Trace.EasyIceServicesTopic.note("Successfully got users from workgroup: '{}'.", workgroup);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (workgroupConfigurationList != null && workgroupConfigurationList.IsCaching)
                        workgroupConfigurationList.StopCaching();
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public DeleteUserResponse DeleteUser(string username)
        {
            var response = new DeleteUserResponse();
            UserConfigurationList userConfigurationList = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(username, "username");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Deleting user '{}'.", username);
                    userConfigurationList = new UserConfigurationList(_configurationManager);
                    var querySettings = userConfigurationList.CreateQuerySettings();
                    var filter = new BasicFilterDefinition<UserConfiguration, UserConfiguration.Property>(UserConfiguration.Property.Id, username);
                    querySettings.SetFilterDefinition(filter);
                    userConfigurationList.StartCaching(querySettings);
                    var userConfigurations = userConfigurationList.GetConfigurationList();
                    if (userConfigurations.Count == 0)
                    {
                        Trace.EasyIceServicesTopic.error("User '{}' does not exist.", username);
                        response.ErrorMessage = String.Format("User '{0}' does not exist.", username);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        userConfigurationList.StopCaching();
                        return response;
                    }
                    userConfigurations[0].Delete();

                    Trace.EasyIceServicesTopic.note("Successfully deleted user '{}'.", username);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (userConfigurationList != null && userConfigurationList.IsCaching)
                        userConfigurationList.StopCaching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Workgroups
        /// <inheritdoc/>
        public GetWorkgroupResponse GetWorkgroup(string workgroupName)
        {
            var response = new GetWorkgroupResponse();
            WorkgroupConfigurationList workgroupConfigurationList = null;
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(workgroupName, "workgroup");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting workgroup '{}'.", workgroupName);
                    workgroupConfigurationList = new WorkgroupConfigurationList(_configurationManager);
                    var querySettings = workgroupConfigurationList.CreateQuerySettings();
                    var filter = new BasicFilterDefinition<WorkgroupConfiguration, WorkgroupConfiguration.Property>(WorkgroupConfiguration.Property.Id, workgroupName);
                    querySettings.SetPropertiesToRetrieve(new[] {
                        WorkgroupConfiguration.Property.DisplayName, 
                        WorkgroupConfiguration.Property.Extension,
                        WorkgroupConfiguration.Property.HasQueue,
                        WorkgroupConfiguration.Property.Id,
                        WorkgroupConfiguration.Property.InteractionOfferingTimeout,
                        WorkgroupConfiguration.Property.IsActive,
                        WorkgroupConfiguration.Property.IsWrapUpActive,
                        WorkgroupConfiguration.Property.Members,
                        WorkgroupConfiguration.Property.MonitoredMailboxes,
                        WorkgroupConfiguration.Property.NoAnswerStatus,
                        WorkgroupConfiguration.Property.OnCallStatus,
                        WorkgroupConfiguration.Property.OnHoldMessage,
                        WorkgroupConfiguration.Property.OnHoldMusic,
                        WorkgroupConfiguration.Property.PreferredLanguage,
                        WorkgroupConfiguration.Property.QueueType,
                        WorkgroupConfiguration.Property.Roles,
                        WorkgroupConfiguration.Property.Skills,
                        WorkgroupConfiguration.Property.Supervisors,
                        WorkgroupConfiguration.Property.WrapUpCodes,
                        WorkgroupConfiguration.Property.WrapUpStatus,
                        WorkgroupConfiguration.Property.WrapUpTimeout
                    });
                    querySettings.SetFilterDefinition(filter);
                    workgroupConfigurationList.StartCaching(querySettings);

                    var workgroupConfigurations = workgroupConfigurationList.GetConfigurationList();
                    if (workgroupConfigurations.Count == 0)
                    {
                        Trace.EasyIceServicesTopic.error("Workgroup '{}' does not exist.", workgroupName);
                        response.ErrorMessage = String.Format("Workgroup '{0}' does not exist.", workgroupName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        workgroupConfigurationList.StopCaching();
                        return response;
                    }

                    var workgroupConfiguration = workgroupConfigurations[0];

                    response.WorkgroupConfiguration.DisplayName = workgroupConfiguration.ConfigurationId.DisplayName;
                    response.WorkgroupConfiguration.Extension = workgroupConfiguration.Extension.Value;
                    response.WorkgroupConfiguration.HasQueue = workgroupConfiguration.HasQueue.Value.ToString();
                    response.WorkgroupConfiguration.Id = workgroupConfiguration.ConfigurationId.Id;
                    response.WorkgroupConfiguration.InteractionOfferingTimeout = workgroupConfiguration.InteractionOfferingTimeout.IsValueDefined() ? workgroupConfiguration.InteractionOfferingTimeout.Value.TotalSeconds.ToString(CultureInfo.InvariantCulture) : String.Empty;
                    response.WorkgroupConfiguration.IsActive = workgroupConfiguration.IsActive.Value.ToString();
                    response.WorkgroupConfiguration.IsWrapUpActive = workgroupConfiguration.IsWrapUpActive.Value.ToString();
                    foreach (var userConfiguration in workgroupConfiguration.Members.Value)
                    {
                        response.WorkgroupConfiguration.Members.Add(userConfiguration.Id);
                    }
                    foreach (var workgroupMonitoredMailboxConfiguration in workgroupConfiguration.MonitoredMailboxes.Value)
                    {
                        response.WorkgroupConfiguration.MonitoredMailboxes.Add(workgroupMonitoredMailboxConfiguration.EmailAddress.Value);
                    }
                    response.WorkgroupConfiguration.NoAnswerStatus = workgroupConfiguration.NoAnswerStatus.Value != null ? workgroupConfiguration.NoAnswerStatus.Value.Id : String.Empty;
                    response.WorkgroupConfiguration.OnCallStatus = workgroupConfiguration.OnCallStatus.Value != null ? workgroupConfiguration.OnCallStatus.Value.Id : String.Empty;
                    response.WorkgroupConfiguration.OnHoldMessage = workgroupConfiguration.OnHoldMessage.Value;
                    response.WorkgroupConfiguration.OnHoldMusic = workgroupConfiguration.OnHoldMusic.Value;
                    response.WorkgroupConfiguration.PreferredLanguage = workgroupConfiguration.PreferredLanguage.Value;
                    response.WorkgroupConfiguration.QueueType = workgroupConfiguration.QueueType.Value.ToString();
                    foreach (var roleConfiguration in workgroupConfiguration.Roles.Value)
                    {
                        response.WorkgroupConfiguration.Roles.Add(roleConfiguration.Id);
                    }
                    foreach (var skillConfiguration in workgroupConfiguration.Skills.Value)
                    {
                        response.WorkgroupConfiguration.Skills.Add(skillConfiguration.Id.Id);
                    }
                    foreach (var supervisorConfiguration in workgroupConfiguration.Supervisors.Value)
                    {
                        response.WorkgroupConfiguration.Supervisors.Add(supervisorConfiguration.Id);
                    }
                    foreach (var wrapUpCodeConfiguration in workgroupConfiguration.WrapUpCodes.Value)
                    {
                        response.WorkgroupConfiguration.WrapUpCodes.Add(wrapUpCodeConfiguration.Id);
                    }
                    response.WorkgroupConfiguration.WrapUpStatus = workgroupConfiguration.WrapUpStatus.Value != null ? workgroupConfiguration.WrapUpStatus.Value.Id : String.Empty;
                    response.WorkgroupConfiguration.WrapUpTimeout = workgroupConfiguration.WrapUpTimeout.Value.ToString(CultureInfo.InvariantCulture);

                    Trace.EasyIceServicesTopic.note("Successfully got workgroup '{}'.", workgroupName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (workgroupConfigurationList != null && workgroupConfigurationList.IsCaching)
                        workgroupConfigurationList.StopCaching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetWorkgroupInteractionsResponse GetWorkgroupInteractions(string workgroupName)
        {
            var response = new GetWorkgroupInteractionsResponse();
            InteractionQueue workgroupQueue = null;
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(workgroupName, "workgroup");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (!GetWorkgroup(workgroupName).Success)
                    {
                        Trace.EasyIceServicesTopic.error("Workgroup {} does not exist.", workgroupName);
                        response.ErrorMessage = String.Format("Workgroup {0} does not exist.", workgroupName);
                        return response;
                    }

                    #endregion

                    workgroupQueue = new InteractionQueue(_interactionsManager, new QueueId(QueueType.Workgroup, workgroupName));
                    workgroupQueue.StartWatching(new[] {InteractionAttributeName.InteractionId});
                    foreach (var interaction in workgroupQueue.GetContents())
                    {
                        response.InteractionIds.Add(interaction.InteractionId.Id.ToString());
                    }

                    Trace.EasyIceServicesTopic.note("Successfully retrieved workgroup '{}' interactions. Got {} interaction.", workgroupName, response.InteractionIds.Count);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (workgroupQueue != null && workgroupQueue.IsWatching())
                        workgroupQueue.StopWatching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc />
        public CreateWorkgroupResponse CreateWorkgroup(string workgroupName, CreateWorkgroupData createWorkgroupData)
        {
            var response = new CreateWorkgroupResponse();
            var success = false;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(workgroupName, "workgroupName");
                    Guard.ArgumentNotNull(createWorkgroupData, "createWorkgroupData");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (GetWorkgroup(workgroupName).Success)
                        throw new Exception(String.Format("Workgroup '{0}' already exists.", workgroupName));
                    #endregion

                    Trace.EasyIceServicesTopic.note("Creating workgroup '{}'.", workgroupName);
                    var workgroupConfigurationList = new WorkgroupConfigurationList(_configurationManager);
                    var workgroupConfiguration = workgroupConfigurationList.CreateObject();
                    workgroupConfiguration.PrepareForEdit();
                    workgroupConfiguration.SetConfigurationId(workgroupName);
                    if (!String.IsNullOrEmpty(createWorkgroupData.Extension))
                        workgroupConfiguration.Extension.Value = createWorkgroupData.Extension;

                    try
                    {
                        workgroupConfiguration.Commit();
                        success = true;
                    }
                    catch (ConfigurationValidationException configurationValidationException)
                    {
                        foreach (var configurationValidationIssue in configurationValidationException.GetValidationIssues<WorkgroupConfiguration.Property>())
                        {
                            var message = configurationValidationIssue.DisplayMessage;
                            var property = configurationValidationIssue.PropertyEnum;
                            var value = configurationValidationIssue.PropertyValue;
                            var issueKey = configurationValidationIssue.IssueKey;

                            var errorMessage = String.Format("Configuration Validation Exception: Message '{0}', Property: '{1}', Value: '{2}', Issue Key: '{3}'.", message, property, value, issueKey);
                            Trace.EasyIceServicesTopic.error(errorMessage);
                            response.ErrorMessage += errorMessage + " ";
                        }
                    }
                    if (success)
                    {
                        Trace.EasyIceServicesTopic.note("Successfully created workgroup '{}'.", workgroupName);
                        response.Success = true;
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc />
        public CreateWorkgroupResponse CreateWorkgroupExtended(string workgroupName, string extension)
        {
            return CreateWorkgroup(workgroupName, new CreateWorkgroupData(extension));
        }
        /// <inheritdoc/>
        public GetWorkgroupsResponse GetWorkgroups()
        {
            var response = new GetWorkgroupsResponse();
            WorkgroupConfigurationList workgroupConfigurationList = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting workgroups.");
                    workgroupConfigurationList = new WorkgroupConfigurationList(_configurationManager);
                    var querySettings = workgroupConfigurationList.CreateQuerySettings();
                    querySettings.SetPropertiesToRetrieve(new[] { WorkgroupConfiguration.Property.Id, WorkgroupConfiguration.Property.HasQueue });
                    workgroupConfigurationList.StartCaching(querySettings);
                    var workgroupConfigurations = workgroupConfigurationList.GetConfigurationList();

                    foreach (var workgroupConfiguration in workgroupConfigurations)
                    {
                        response.Workgroups.Add(workgroupConfiguration.ConfigurationId.Id);
                    }
                    Trace.EasyIceServicesTopic.note("Successfully got '{}' workgroups.", response.Workgroups.Count);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (workgroupConfigurationList != null && workgroupConfigurationList.IsCaching)
                        workgroupConfigurationList.StopCaching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public DeleteWorkgroupResponse DeleteWorkgroup(string workgroupName)
        {
            var response = new DeleteWorkgroupResponse();
            WorkgroupConfigurationList workgroupConfigurationList = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(workgroupName, "workgroup");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Deleting workgroup '{}'.", workgroupName);
                    workgroupConfigurationList = new WorkgroupConfigurationList(_configurationManager);
                    var querySettings = workgroupConfigurationList.CreateQuerySettings();
                    var filter = new BasicFilterDefinition<WorkgroupConfiguration, WorkgroupConfiguration.Property>(WorkgroupConfiguration.Property.Id, workgroupName);
                    querySettings.SetFilterDefinition(filter);
                    workgroupConfigurationList.StartCaching(querySettings);
                    var workgroupConfigurations = workgroupConfigurationList.GetConfigurationList();
                    if (workgroupConfigurations.Count == 0)
                    {
                        Trace.EasyIceServicesTopic.error("Workgroup '{}' does not exist.", workgroupName);
                        response.ErrorMessage = String.Format("Workgroup '{0}' does not exist.", workgroupName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        workgroupConfigurationList.StopCaching();
                        return response;
                    }
                    workgroupConfigurations[0].Delete();

                    Trace.EasyIceServicesTopic.note("Successfully deleted workgroup '{}'.", workgroupName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (workgroupConfigurationList != null && workgroupConfigurationList.IsCaching)
                        workgroupConfigurationList.StopCaching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Stations
        /// <inheritdoc/>
        public GetStationResponse GetStation(string stationName)
        {
            var response = new GetStationResponse();
            StationConfigurationList stationConfigurationList = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(stationName, "station");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting station '{}'.", stationName);
                    stationConfigurationList = new StationConfigurationList(_configurationManager);
                    var querySettings = stationConfigurationList.CreateQuerySettings();
                    var filter = new BasicFilterDefinition<StationConfiguration, StationConfiguration.Property>(StationConfiguration.Property.Id, stationName);
                    querySettings.SetPropertiesToRetrieve(new[] {
                        StationConfiguration.Property.ConnectionAddress,
                        StationConfiguration.Property.ContactLine,
                        StationConfiguration.Property.DisplayName, 
                        StationConfiguration.Property.Extension,
                        StationConfiguration.Property.Id,
                        StationConfiguration.Property.IdentificationAddress,
                        StationConfiguration.Property.IsActive,
                        StationConfiguration.Property.IsShareable,
                        StationConfiguration.Property.StationType
                    });
                    querySettings.SetFilterDefinition(filter);
                    stationConfigurationList.StartCaching(querySettings);

                    var stationConfigurations = stationConfigurationList.GetConfigurationList();
                    if (stationConfigurations.Count == 0)
                    {
                        Trace.EasyIceServicesTopic.error("Station '{}' does not exist.", stationName);
                        response.ErrorMessage = String.Format("Station '{0}' does not exist.", stationName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        stationConfigurationList.StopCaching();
                        return response;
                    }

                    var stationConfiguration = stationConfigurations[0];

                    response.StationConfiguration.ConnectionAddress = stationConfiguration.ConnectionAddress.Value != null ? stationConfiguration.ConnectionAddress.Value.Address : String.Empty;
                    response.StationConfiguration.ContactLine = stationConfiguration.ContactLine.IsValueDefined() ? stationConfiguration.ContactLine.Value.Id : String.Empty;
                    response.StationConfiguration.DisplayName = stationConfiguration.ConfigurationId.DisplayName;
                    response.StationConfiguration.Id = stationConfiguration.ConfigurationId.Id;
                    response.StationConfiguration.IdentificationAddress = stationConfiguration.IdentificationAddress.Value != null ? stationConfiguration.IdentificationAddress.Value.Address : String.Empty;
                    response.StationConfiguration.Extension = stationConfiguration.Extension.Value ?? String.Empty;
                    response.StationConfiguration.IsActive = stationConfiguration.IsActive.Value.ToString();
                    response.StationConfiguration.IsShareable = stationConfiguration.IsShareable.Value.ToString();
                    response.StationConfiguration.StationType = stationConfiguration.StationType.Value.ToString();

                    Trace.EasyIceServicesTopic.note("Successfully got station '{}'.", stationName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (stationConfigurationList != null && stationConfigurationList.IsCaching)
                        stationConfigurationList.StopCaching();
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetStationInteractionsResponse GetStationInteractions(string stationName)
        {
            var response = new GetStationInteractionsResponse();
            InteractionQueue stationQueue = null;
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(stationName, "stationName");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (!GetStation(stationName).Success)
                    {
                        Trace.EasyIceServicesTopic.error("Station {} does not exist.", stationName);
                        response.ErrorMessage = String.Format("Station {0} does not exist.", stationName);
                        return response;
                    }

                    #endregion

                    stationQueue = new InteractionQueue(_interactionsManager, new QueueId(QueueType.Station, stationName));
                    stationQueue.StartWatching(new[] { InteractionAttributeName.InteractionId });
                    foreach (var interaction in stationQueue.GetContents())
                    {
                        response.InteractionIds.Add(interaction.InteractionId.Id.ToString());
                    }

                    Trace.EasyIceServicesTopic.note("Successfully retrieved station '{}' interactions. Got {} interaction.", stationName, response.InteractionIds.Count);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (stationQueue != null && stationQueue.IsWatching())
                        stationQueue.StopWatching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CreateStationResponse CreateStation(string stationName, CreateStationData createStationData)
        {
            var response = new CreateStationResponse();
            var success = false;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(stationName, "stationName");
                    Guard.ArgumentNotNullOrEmptyString(createStationData.Extension, "extension");
                    Guard.ArgumentNotNullOrEmptyString(createStationData.UserPortion, "userPortion");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    if (GetStation(stationName).Success)
                        throw new Exception(String.Format("Station '{0}' already exists.", stationName));

                    int port;
                    if (!String.IsNullOrEmpty(createStationData.PortPortion) && !int.TryParse(createStationData.PortPortion, out port))
                        throw new Exception(String.Format("Invalid port '{0}'. Use an empty string if you do not want to specify a port.", createStationData.PortPortion));
                    #endregion

                    Trace.EasyIceServicesTopic.note("Creating station '{}'.", stationName);
                    var stationConfigurationList = new StationConfigurationList(_configurationManager);
                    var stationConfiguration = stationConfigurationList.CreateObject();
                    stationConfiguration.PrepareForEdit();
                    stationConfiguration.SetConfigurationId(stationName);
                    stationConfiguration.SetDisplayName(stationName);
                    if (!String.IsNullOrEmpty(createStationData.Extension))
                        stationConfiguration.Extension.Value = createStationData.Extension;
                    if (String.IsNullOrEmpty(createStationData.HostPortion) && String.IsNullOrEmpty(createStationData.PortPortion))
                    {
                        stationConfiguration.IdentificationAddress.Value = new ININ.IceLib.Configuration.DataTypes.SipAddress(createStationData.UserPortion);
                        stationConfiguration.ConnectionAddress.Value = new ININ.IceLib.Configuration.DataTypes.SipAddress(createStationData.UserPortion);
                    }
                    else
                    {
                        stationConfiguration.IdentificationAddress.Value = new ININ.IceLib.Configuration.DataTypes.SipAddress(createStationData.UserPortion, createStationData.HostPortion, Convert.ToInt32(createStationData.PortPortion));
                        stationConfiguration.ConnectionAddress.Value = new ININ.IceLib.Configuration.DataTypes.SipAddress(createStationData.UserPortion);
                    }

                    stationConfiguration.IsActive.Value = true;
                    stationConfiguration.ShouldAlwaysRing.Value = false;
                    stationConfiguration.StationType.Value = ININ.IceLib.Configuration.DataTypes.StationType.Workstation;

                    try
                    {
                        stationConfiguration.Commit();
                        success = true;
                    }
                    catch (ConfigurationValidationException configurationValidationException)
                    {
                        foreach (var configurationValidationIssue in configurationValidationException.GetValidationIssues<StationConfiguration.Property>())
                        {
                            var message = configurationValidationIssue.DisplayMessage;
                            var property = configurationValidationIssue.PropertyEnum;
                            var value = configurationValidationIssue.PropertyValue;
                            var issueKey = configurationValidationIssue.IssueKey;

                            var errorMessage = String.Format("Configuration Validation Exception: Message '{0}', Property: '{1}', Value: '{2}', Issue Key: '{3}'.", message, property, value, issueKey);
                            Trace.EasyIceServicesTopic.error(errorMessage);
                            response.ErrorMessage += errorMessage + " ";
                        }
                    }
                    if (success)
                    {
                        Trace.EasyIceServicesTopic.note("Successfully created station '{}'.", stationName);
                        response.Success = true;
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CreateStationResponse CreateStationExtended(string stationName, string extension, string userPortion, string hostPortion, string portPortion)
        {
            return CreateStation(stationName, new CreateStationData(extension, userPortion, hostPortion, portPortion));
        }
        /// <inheritdoc/>
        public GetStationsResponse GetStations()
        {
            var response = new GetStationsResponse();
            StationConfigurationList stationConfigurationList = null;
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting stations.");
                    stationConfigurationList = new StationConfigurationList(_configurationManager);
                    var querySettings = stationConfigurationList.CreateQuerySettings();
                    querySettings.SetPropertiesToRetrieve(StationConfiguration.Property.Id);
                    stationConfigurationList.StartCaching(querySettings);
                    var stationConfigurations = stationConfigurationList.GetConfigurationList();

                    foreach (var stationConfiguration in stationConfigurations)
                    {
                        response.Stations.Add(stationConfiguration.ConfigurationId.Id);
                    }

                    Trace.EasyIceServicesTopic.note("Successfully got '{}' stations.", response.Stations.Count);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (stationConfigurationList != null && stationConfigurationList.IsCaching)
                        stationConfigurationList.StopCaching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public DeleteStationResponse DeleteStation(string stationName)
        {
            var response = new DeleteStationResponse();
            StationConfigurationList stationConfigurationList = null;
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(stationName, "station");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Deleting station '{}'.", stationName);
                    stationConfigurationList = new StationConfigurationList(_configurationManager);
                    var querySettings = stationConfigurationList.CreateQuerySettings();
                    var filter = new BasicFilterDefinition<StationConfiguration, StationConfiguration.Property>(StationConfiguration.Property.Id, stationName);
                    querySettings.SetFilterDefinition(filter);
                    stationConfigurationList.StartCaching(querySettings);
                    var stationConfigurations = stationConfigurationList.GetConfigurationList();
                    if (stationConfigurations.Count == 0)
                    {
                        Trace.EasyIceServicesTopic.error("Station '{}' does not exist.", stationName);
                        response.ErrorMessage = String.Format("Station '{0}' does not exist.", stationName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        stationConfigurationList.StopCaching();
                        return response;
                    }
                    stationConfigurations[0].Delete();

                    Trace.EasyIceServicesTopic.note("Successfully deleted station '{}'.", stationName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (stationConfigurationList != null && stationConfigurationList.IsCaching)
                        stationConfigurationList.StopCaching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Lines
        /// <inheritdoc/>
        public GetLineResponse GetLine(string lineName)
        {
            var response = new GetLineResponse();
            LineConfigurationList lineConfigurationList = null;
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(lineName, "lineName");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting line '{}'.", lineName);
                    lineConfigurationList = new LineConfigurationList(_configurationManager);
                    var querySettings = lineConfigurationList.CreateQuerySettings();
                    var filter = new BasicFilterDefinition<LineConfiguration, LineConfiguration.Property>(LineConfiguration.Property.Id, lineName);
                    querySettings.SetPropertiesToRetrieve(new[] {
                        LineConfiguration.Property.DisplayName, 
                        LineConfiguration.Property.Id,
                    });
                    querySettings.SetFilterDefinition(filter);
                    lineConfigurationList.StartCaching(querySettings);

                    var lineConfigurations = lineConfigurationList.GetConfigurationList();
                    if (lineConfigurations.Count == 0)
                    {
                        Trace.EasyIceServicesTopic.error("Line '{}' does not exist.", lineName);
                        response.ErrorMessage = String.Format("Line '{0}' does not exist.", lineName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        lineConfigurationList.StopCaching();
                        return response;
                    }

                    var lineConfiguration = lineConfigurations[0];

                    response.LineConfiguration.DisplayName = lineConfiguration.ConfigurationId.DisplayName;
                    response.LineConfiguration.Id = lineConfiguration.ConfigurationId.Id;

                    Trace.EasyIceServicesTopic.note("Successfully got line '{}'.", lineName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (lineConfigurationList != null && lineConfigurationList.IsCaching)
                        lineConfigurationList.StopCaching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc />
        public CreateLineResponse CreateLine(string lineName, CreateLineData createLineData)
        {
            var response = new CreateLineResponse();
            var success = false;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(lineName, "lineName");
                    Guard.ArgumentNotNull(createLineData, "createLineData");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    //if (GetWorkgroup(workgroupName).Success)
                    //    throw new Exception(String.Format("Line '{0}' already exists.", lineName));
                    #endregion

                    Trace.EasyIceServicesTopic.note("Creating line '{}'.", lineName);
                    var lineConfigurationList = new LineConfigurationList(_configurationManager);
                    var lineConfiguration = lineConfigurationList.CreateObject();
                    lineConfiguration.PrepareForEdit();
                    lineConfiguration.SetConfigurationId(lineName);

                    try
                    {
                        lineConfiguration.Commit();
                        success = true;
                    }
                    catch (Exception lineException)
                    {
                        Trace.EasyIceServicesTopic.error(lineException.Message);
                    }
                    if (success)
                    {
                        Trace.EasyIceServicesTopic.note("Successfully created line '{}'.", lineName);
                        response.Success = true;
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #endregion

        #region Recordings

        #region Private Methods
        private List<String> GetRecordingIds(string interactionIdKey)
        {
            var recordingIds = new List<String>();
            const int curResultSet = 1;
            var transactionClient = new TransactionClient(_session);

            transactionClient.TransactionSettingsTransactionScope.Timeout = Int32.Parse("30", CultureInfo.InvariantCulture);
            transactionClient.TransactionSettingsTransactionScope.RowLimit = Int32.Parse("10000000", CultureInfo.InvariantCulture);
            transactionClient.TransactionSettingsTransactionScope.ReadOnly = Boolean.Parse("false");
            transactionClient.TransactionSettingsTransactionScope.Provider = "ININ.I3DBTranSPProvider.1";

            //  create the parameters for the transaction
            var sidiCallIdKey = new TransactionParameter
            {
                Type = ININ.IceLib.Data.TransactionBuilder.ParameterType.In,
                DataType = ParameterDataType.String,
                Value = interactionIdKey
            };

            // Note: need to create a new TransactionData object for each call to Execute to avoid strange behavior with process results
            var transactionData = new TransactionData
            {
                ProcedureName = "spir_get_recids_by_callidkey",
                TransactionName = "spir_get_recids_by_callidkey"
            };

            transactionData.Parameters.Add(sidiCallIdKey);

            // Execute the tran                         
            if (transactionClient.Execute(ref transactionData))
            {
                // Process the result sets 
                var rowCount = transactionData.ResultSet.Tables[curResultSet].Rows.Count;

                if (rowCount < 1)
                {
                    Trace.EasyIceServicesTopic.note("zero rows in transactionData.ResultSet.Tables[curResultSet]; no result data available.");
                    return recordingIds;
                }

                Trace.EasyIceServicesTopic.status("ResultsSet rowCount={}.", rowCount.ToString(CultureInfo.InvariantCulture));

                for (var rowIndex = 1; rowIndex < rowCount; ++rowIndex)
                {
                    var values = transactionData.ResultSet.Tables[curResultSet].Rows[rowIndex].ItemArray;

                    // Keep in mind there can be multiple recordingIds associated with one callidKey
                    recordingIds.AddRange(values.Select(value => DatabaseUuidHelper.AdjustDatabaseUuidString(values[0].ToString())));
                }
            }
            else
            {
                // Reset transaction settings to clear any local (tran specific) settings
                transactionClient.TransactionSettingsTransactionScope.Reset();

                Trace.EasyIceServicesTopic.error("Error executing transaction.  ReturnCode: {}, Message: {}.", transactionData.ReturnCode, transactionData.WarningMessage);

                if ((long)transactionData.ReturnCode == 0) // IRDB.ErrorFailed
                {
                    Trace.EasyIceServicesTopic.status("ReturnCode was generic ErrorFail, no warningMessage; return zero rows.");
                }
                else
                {
                    Trace.EasyIceServicesTopic.status("setting warningMessage as LastError; returning -1 to indicate an error.");

                    // Trace error
                    transactionClient.LastError = transactionData.WarningMessage;
                }
            }
            transactionData.Dispose();

            return recordingIds;
        }
        #endregion

        private DownloadRecordingResponse DownloadRecording(DownloadRecordingData downloadRecordingData)
        {
            var response = new DownloadRecordingResponse();
            WebClient webClient = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNull(downloadRecordingData, "downloadRecordingData");
                    if (String.IsNullOrEmpty(downloadRecordingData.InteractionId) &&
                        String.IsNullOrEmpty(downloadRecordingData.InteractionIdKey) &&
                        String.IsNullOrEmpty(downloadRecordingData.RecordingId))
                    {
                        Trace.EasyIceServicesTopic.error("At least one of the DownloadRecordingData parameters must be populated.");
                        response.ErrorMessage = "At least one of the DownloadRecordingData parameters must be populated.";
                        //SetResponseHttpStatus(HttpStatusCode.BadRequest, response.ErrorMessage);
                        return response;
                    }

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    var recordingIds = new List<String>();
                    string interactionIdKey;

                    #region Got Recording Id
                    if (!String.IsNullOrEmpty(downloadRecordingData.RecordingId))
                    {
                        Trace.EasyIceServicesTopic.note("Using Recording Id: {}.", downloadRecordingData.RecordingId);
                        recordingIds.Add(downloadRecordingData.RecordingId);
                    }
                    #endregion

                    #region Got Interaction Id Key
                    else if (!String.IsNullOrEmpty(downloadRecordingData.InteractionIdKey))
                    {
                        Trace.EasyIceServicesTopic.note("Using Interaction Id Key: {}.", downloadRecordingData.InteractionIdKey);
                        interactionIdKey = downloadRecordingData.InteractionIdKey;
                        recordingIds = GetRecordingIds(interactionIdKey);
                        Trace.EasyIceServicesTopic.note("Found {} recording id(s).", recordingIds.Count);
                    }
                    #endregion

                    #region Got Interaction Id
                    else if (!String.IsNullOrEmpty(downloadRecordingData.InteractionId))
                    {
                        Trace.EasyIceServicesTopic.note("Searching recording based on interaction id: {}.", downloadRecordingData.InteractionId);

                        var interaction = GetInteraction(downloadRecordingData.InteractionId);
                        if (interaction == null)
                        {
                            var searchInteractionResponse = SearchInteractionsByInteractionId(downloadRecordingData.InteractionId); // Won't work until SU4
                            if (!searchInteractionResponse.Success)
                            {
                                throw new Exception(String.Format("Could not find interaction id '{0}'. Error Message from searching the interaction id: '{1}'.", downloadRecordingData.InteractionId, searchInteractionResponse.ErrorMessage));
                            }
                            interactionIdKey = searchInteractionResponse.InteractionSnapshots[0].InteractionIdKey;
                            Trace.EasyIceServicesTopic.note("Got interaction id key: {}.", interactionIdKey);
                            recordingIds = GetRecordingIds(interactionIdKey);
                            Trace.EasyIceServicesTopic.note("Found {} recording id(s).", recordingIds.Count);
                        }
                        else
                        {
                            interactionIdKey = GetInteractionAttribute(interaction, InteractionAttributeName.CallIdKey);
                            if (String.IsNullOrEmpty(interactionIdKey))
                                throw new Exception(String.Format("Could not get interaction id key from interaction '{0}'.", downloadRecordingData.InteractionId));

                            Trace.EasyIceServicesTopic.note("Got interaction id key: {}.", interactionIdKey);
                            var recordingId = GetInteractionAttribute(interaction, "Eic_RecordingId");
                            if (String.IsNullOrEmpty(recordingId))
                                throw new Exception(String.Format("Could not get recording id from interaction '{0}'.", downloadRecordingData.InteractionId));

                            Trace.EasyIceServicesTopic.note("Using Recording Id: {}.", recordingId);
                            recordingIds.Add(recordingId);
                            Trace.EasyIceServicesTopic.note("Found {} recording id(s).", recordingIds.Count);
                        }
                    }
                    #endregion

                    #region Downloading recordings
                    response.Streams = new List<Stream>();

                    foreach (var recordingId in recordingIds)
                    {
                        Trace.EasyIceServicesTopic.note("Downloading recording '{}'.", recordingId);
                        var qualityManagementManager = QualityManagementManager.GetInstance(_session);
                        var recordingsManager = qualityManagementManager.RecordingsManager;

                        var recordingUri = recordingsManager.GetExportUri(recordingId, RecordingMediaType.PrimaryMedia, String.Empty, 0);
                        var wavFilename = String.Format("{0}.wav", Path.GetTempFileName());
                        Trace.EasyIceServicesTopic.note("Wav filename: '{0}'.", wavFilename);

                        webClient = new WebClient();
                        webClient.DownloadFile(recordingUri, wavFilename);
                        var currentStream = File.OpenRead(wavFilename);

                        // Only valid if using REST (WebHttpBinding)
                        if (WebOperationContext.Current != null)
                        {
                            //WebOperationContext.Current.OutgoingResponse.ContentType = "application/octet-stream";
                            WebOperationContext.Current.OutgoingResponse.ContentType = "audio/wav";
                            WebOperationContext.Current.OutgoingResponse.ContentLength = currentStream.Length;
                        }

                        response.Streams.Add(currentStream);
                        Trace.EasyIceServicesTopic.note("Successfully got recording '{}'.", recordingId);
                    }
                    #endregion

                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (webClient != null)
                    {
                        webClient.Dispose();
                    }
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public DownloadRecordingResponse DownloadRecordingByRecordingId(string recordingId)
        {
            return DownloadRecording(new DownloadRecordingData(String.Empty, String.Empty, recordingId));
        }
        /// <inheritdoc/>
        public DownloadRecordingResponse DownloadRecordingByInteractionId(string interactionId)
        {
            return DownloadRecording(new DownloadRecordingData(String.Empty, interactionId, String.Empty));
        }
        /// <inheritdoc/>
        public DownloadRecordingResponse DownloadRecordingByInteractionIdKey(string interactionIdKey)
        {
            return DownloadRecording(new DownloadRecordingData(interactionIdKey, String.Empty, String.Empty));
        }
        #endregion

        #region Custom Notification
        /// <inheritdoc/>
        public SendCustomNotificationResponse SendCustomNotification(string objectId, string eventId, string customData)
        {
            var response = new SendCustomNotificationResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(objectId, "objectId");
                    Guard.ArgumentNotNullOrEmptyString(eventId, "eventId");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Sending custom notification. Object Id: '{}', Event Id: '{}', Custom Data: '{}'.", objectId, eventId, customData);
                    var customNotification = new CustomNotification(_session);
                    var customMessageHeader = new CustomMessageHeader(CustomMessageType.ServerNotification, objectId, eventId);
                    var customRequest = new CustomRequest(customMessageHeader);
                    if (!String.IsNullOrEmpty(customData))
                        customRequest.Write(customData.Split('|'));
                    customNotification.SendServerRequestNoResponse(customRequest);

                    Trace.EasyIceServicesTopic.note("Successfully sent custom notification. Object Id: '{}', Event Id: '{}', Custom Data: '{}'.", objectId, eventId, customData);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Statistics

        #region Private Methods
        private bool InitStatisticsManager()
        {
            if (_session == null || _session.ConnectionState != ConnectionState.Up)
            {
                return false;
            }

            if (_statisticsManager != null)
            {
                return true;
            }

            try
            {
                _statisticsManager = StatisticsManager.GetInstance(_session);
            }
            catch
            {
                _statisticsManager = null;
                return false;
            }

            return true;
        }
        private void CleanUpStatisticsManager()
        {
            _statisticsManager = null;
        }
        private StatisticIdentifier GetStatisticIdentifier(string uri)
        {
            var statisticsCatalog = new StatisticCatalog(_statisticsManager);
            statisticsCatalog.StartWatching();
            var statistics = statisticsCatalog.GetStatisticDefinitions();
            StatisticIdentifier statisticIdentifierToReturn = null;
            foreach (var statistic in statistics)
            {
                if (statistic.Id.Uri.Equals(uri, StringComparison.InvariantCultureIgnoreCase))
                    statisticIdentifierToReturn = statistic.Id;
            }
            statisticsCatalog.StopWatching();
            return statisticIdentifierToReturn;
        }
        private List<String> GetValuesForParameterType(string parameterTypeId)
        {
            var parameterType = new ParameterTypeId(parameterTypeId);
            var pvd = new ParameterValuesDepot(_statisticsManager);
            var pq = new ParameterQuery(parameterType);
            var pqr = pvd.ExecuteQuery(pq);

            return pqr.Values.Select(result => result.Value).ToList();
        }
        #endregion

        /// <inheritdoc/>
        public GetAllStatisticsResponse GetAllStatistics()
        {
            var response = new GetAllStatisticsResponse();
            StatisticCatalog statisticsCatalog = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting statistics catalog.");
                    statisticsCatalog = new StatisticCatalog(_statisticsManager);
                    
                    Trace.EasyIceServicesTopic.note("Starting statistics catalog watch.");
                    statisticsCatalog.StartWatching();
                    
                    var statisticCategories = statisticsCatalog.GetStatisticCategories();
                    Trace.EasyIceServicesTopic.note("Got {} statistic categories.", statisticCategories.Count);
                    
                    foreach (var statisticCategory in statisticCategories)
                    {
                        var statisticDefinitions = new Dictionary<String, List<String>>();
                        foreach (var statisticDefinition in statisticsCatalog.GetStatisticDefinitions(statisticCategory.Id))
                        {
                            var statisticRequiredParameters = (from requiredParameterDefinition in statisticDefinition.RequiredParameters where !requiredParameterDefinition.ShouldBeSuppressed from parameterTypeId in requiredParameterDefinition select String.Format("{0} ({1})", statisticsCatalog.GetParameterDisplayString(parameterTypeId), parameterTypeId.Id)).ToList();
                            statisticDefinitions.Add(String.Format("{0} ({1})", statisticDefinition.DisplayString, statisticDefinition.Id.Uri), statisticRequiredParameters);
                        }
                        response.Statistics.Add(String.Format("{0} ({1})", statisticCategory.DisplayString, statisticCategory.Id), statisticDefinitions);
                    }

                    Trace.EasyIceServicesTopic.note("Successfully got {} statistics definitions.", response.Statistics.Count);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                }
                finally
                {
                    if (statisticsCatalog != null && statisticsCatalog.IsWatching())
                    {
                        Trace.EasyIceServicesTopic.note("Stopping statistics catalog watch.");
                        statisticsCatalog.StopWatching();
                    }
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }

        /// <inheritdoc/>
        public GetStatisticResponse GetStatistic(string statisticId, string parameters, string parameterTypeIds)
        {
            var response = new GetStatisticResponse();
            StatisticListener statisticListener = null;
            StatisticCatalog statisticsCatalog = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(statisticId, "statisticId");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting statistic '{}' with parameters '{}' and parameter type ids '{}'.", statisticId, parameters, parameterTypeIds);
                    //Get statistic identifier
                    statisticListener = new StatisticListener(_statisticsManager);
                    var statisticIdentifier = GetStatisticIdentifier(statisticId);
                    if (statisticIdentifier == null)
                        throw new Exception(String.Format("Unknown statistic id: '{0}'.", statisticId));

                    //Get statistic definition
                    statisticsCatalog = new StatisticCatalog(_statisticsManager);
                    statisticsCatalog.StartWatching();
                    var statisticDefinition = statisticsCatalog.GetStatisticDefinition(statisticIdentifier);
                    if (statisticDefinition == null)
                        throw new Exception(String.Format("Failed to get statistic definition for: '{0}'.", statisticId));

                    ParameterValueKeyedCollection parameterValueKeyedCollection = null;

                    if (!String.IsNullOrEmpty(parameters))
                    {
                        if (String.IsNullOrEmpty(parameterTypeIds))
                            throw new Exception("No parameter type ids were passed.");

                        //Parse parameters
                        var inputParameters = new List<String>(parameters.Split('|'));
                        var inputParameterTypeIds = new List<String>(parameterTypeIds.Split('|'));

                        //Number of input parameters and number of input parameter type ids do not match?
                        if (!inputParameters.Count.Equals(inputParameterTypeIds.Count))
                            throw new Exception(String.Format("The number of parameters ('{0}') and parameter type ids ('{1}') does not match.", inputParameters.Count, inputParameterTypeIds.Count));

                        if (inputParameters.Count > 0)
                        {
                            //Build parameter collection
                            parameterValueKeyedCollection = new ParameterValueKeyedCollection();
                            for (var i = 0; i < inputParameters.Count; i++)
                            {
                                parameterValueKeyedCollection.Add(new ParameterValue(new ParameterTypeId(inputParameterTypeIds[i]), inputParameters[i]));
                            }
                        }
                    }

                    //Get value
                    StatisticKey watchedStatisticKey;
                    if (parameterValueKeyedCollection == null || parameterValueKeyedCollection.Count == 0)
                        watchedStatisticKey = new StatisticKey(statisticIdentifier);
                    else
                        watchedStatisticKey = new StatisticKey(statisticIdentifier, parameterValueKeyedCollection);

                    StatisticValue statisticValue = null;
                    AutoResetEvent autoResetEvent = new AutoResetEvent(false);

                    statisticListener.StatisticValueUpdated += (sender, args) =>
                    {
                        for (var i = 0; i < args.StatisticKeys.Count; i++)
                        {
                            if (args.StatisticKeys[i].Identifier.Equals(statisticIdentifier))
                            {
                                statisticValue = args.StatisticValues[i];
                                if (!statisticValue.IsNull)
                                {
                                    autoResetEvent.Set();
                                }
                                break;
                            }
                        }
                    };

                    if (statisticListener.IsWatching())
                    {
                        if (!statisticListener.IsWatching(watchedStatisticKey))
                        {
                            statisticListener.ChangeWatchedKeys(new[] { watchedStatisticKey }, null, true);
                        }
                    }
                    else
                    {
                        statisticListener.StartWatching(new[] { watchedStatisticKey });
                    }

                    //Wait for an hypothetical value (yes, I know, this sucks but that's how statistics work in IceLib
                    while(statisticValue == null)
                    {
                        autoResetEvent.WaitOne(3000);
                    }
                    
                    if (statisticValue.IsError)
                    {
                        response.StatisticValue = statisticValue.NullDisplayString + " (Error)";
                        throw new Exception(String.Format("An error occurred during the retrieval of the statistic id '{0}'. Value can not be trusted.", statisticId));
                    }
                    response.StatisticValue = statisticValue.GetDisplayString();

                    Trace.EasyIceServicesTopic.note("Successfully got statistic '{}' with parameters '{}' and parameter type ids '{}'. Value: '{}'.", statisticId, parameters, parameterTypeIds, response.StatisticValue);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                finally
                {
                    if (statisticsCatalog != null && statisticsCatalog.IsWatching())
                        statisticsCatalog.StopWatching();
                    if (statisticListener != null && statisticListener.IsWatching())
                        statisticListener.StopWatching();
                }

                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Reporting
        private SearchInteractionsResponse SearchInteractions(InteractionSnapshotFilter filter)
        {
            var response = new SearchInteractionsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNull(filter, "filter");

                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    var reportingManager = ReportingManager.GetInstance(_session);
                    var interactionSnapshotBrowser = new InteractionSnapshotBrowser(reportingManager);
                    var interactionSnapshotResults = interactionSnapshotBrowser.GetInteractionSnapshots(new[] { filter });

                    Trace.EasyIceServicesTopic.note("Total count of snapshots: {}", interactionSnapshotResults.TotalCount);

                    response.InteractionSnapshots = new List<SerializedInteractionSnapshot>();
                    foreach (var interactionSnapshotResult in interactionSnapshotResults.InteractionSnapshots)
                    {
                        var serializedInteractionSnapshot = new SerializedInteractionSnapshot
                            {
                                Direction = interactionSnapshotResult.Direction.ToString(),
                                Disposition = interactionSnapshotResult.Disposition.ToString(),
                                InitiationTime = interactionSnapshotResult.InitiationTime,
                                InteractionId = interactionSnapshotResult.InteractionId,
                                InteractionIdKey = interactionSnapshotResult.InteractionIdKey,
                                MediaType = interactionSnapshotResult.MediaType.ToString(),
                                SegmentLog = interactionSnapshotResult.SegmentLog,
                                SequenceNumber = interactionSnapshotResult.SequenceNumber,
                                SiteId = interactionSnapshotResult.SiteId
                            };
                        foreach (var summaryValue in interactionSnapshotResult.SummaryValues)
                        {
                            serializedInteractionSnapshot.SummaryValues.Add(summaryValue.Key, summaryValue.Value);
                        }
                        response.InteractionSnapshots.Add(serializedInteractionSnapshot);
                    }
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SearchInteractionsResponse SearchInteractionsByAni(string ani)
        {
            var response = new SearchInteractionsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(ani, "ani");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Searching for interactions by ANI '{}'.", ani);
                    response = SearchInteractions(new AniFilter(ani));
                    if (response.Success)
                        Trace.EasyIceServicesTopic.note("Successfully searched for interactions by ANI '{}'. Got '{}' interaction snapshots.", ani, response.InteractionSnapshots.Count);
                    else
                        throw new Exception(response.ErrorMessage);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SearchInteractionsResponse SearchInteractionsByDateTimeRange(DateTime minimumValue, DateTime maximumValue)
        {
            var response = new SearchInteractionsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNull(minimumValue, "minimumValue");
                    Guard.ArgumentNotNull(maximumValue, "maximumValue");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Searching for interactions by date/time range between '{}' and '{}'.", minimumValue.ToString(CultureInfo.InvariantCulture), maximumValue.ToString(CultureInfo.InvariantCulture));
                    response = SearchInteractions(new DateTimeRangeFilter(minimumValue, maximumValue));
                    if (response.Success)
                        Trace.EasyIceServicesTopic.note("Successfully searched for interactions by date/time range between '{}' and '{}'. Got '{}' interaction snapshots.", minimumValue.ToString(CultureInfo.InvariantCulture), maximumValue.ToString(CultureInfo.InvariantCulture), response.InteractionSnapshots.Count);
                    else
                        throw new Exception(response.ErrorMessage);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SearchInteractionsResponse SearchInteractionsByDnis(string dnis)
        {
            var response = new SearchInteractionsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(dnis, "dnis");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Searching for interactions by DNIS '{}'.", dnis);
                    response = SearchInteractions(new DnisFilter(dnis));
                    if (response.Success)
                        Trace.EasyIceServicesTopic.note("Successfully searched for interactions by DNIS '{}'. Got '{}' interaction snapshots.", dnis, response.InteractionSnapshots.Count);
                    else
                        throw new Exception(response.ErrorMessage);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SearchInteractionsResponse SearchInteractionsByInteractionId(string interactionId)
        {
            var response = new SearchInteractionsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(interactionId, "interactionId");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Searching for interactions by Interaction Id '{}'.", interactionId);
                    response = SearchInteractions(new InteractionIdFilter(interactionId));
                    if (response.Success)
                        Trace.EasyIceServicesTopic.note("Successfully searched for interactions by Interaction Id '{}'. Got '{}' interaction snapshots.", interactionId, response.InteractionSnapshots.Count);
                    else
                        throw new Exception(response.ErrorMessage);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SearchInteractionsResponse SearchInteractionsByLastICUsername(string lastICUsername)
        {
            var response = new SearchInteractionsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(lastICUsername, "lastICUsername");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Searching for interactions by Last IC Username '{}'.", lastICUsername);
                    response = SearchInteractions(new LastICUserNameFilter(lastICUsername));
                    if (response.Success)
                        Trace.EasyIceServicesTopic.note("Successfully searched for interactions by Last IC Username '{}'. Got '{}' interaction snapshots.", lastICUsername, response.InteractionSnapshots.Count);
                    else
                        throw new Exception(response.ErrorMessage);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SearchInteractionsResponse SearchInteractionsByLastWorkgroup(string lastWorkgroup)
        {
            var response = new SearchInteractionsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(lastWorkgroup, "lastWorkgroup");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Searching for interactions by Last Workgroup '{}'.", lastWorkgroup);
                    response = SearchInteractions(new LastWorkgroupFilter(lastWorkgroup));
                    if (response.Success)
                        Trace.EasyIceServicesTopic.note("Successfully searched for interactions by Last Workgroup '{}'. Got '{}' interaction snapshots.", lastWorkgroup, response.InteractionSnapshots.Count);
                    else
                        throw new Exception(response.ErrorMessage);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SearchInteractionsResponse SearchInteractionsByMediaType(string mediaType)
        {
            var response = new SearchInteractionsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNull(mediaType, "mediaType");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Searching for interactions by Media Type '{}'.", mediaType);
                    InteractionSnapshotMediaType interactionSnapshotMediaType;
                    switch (mediaType.ToLowerInvariant())
                    {
                        case "call":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.Call;
                            break;
                        case "callback":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.Callback;
                            break;
                        case "chat":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.Chat;
                            break;
                        case "email":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.Email;
                            break;
                        case "fax":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.Fax;
                            break;
                        case "generic":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.Generic;
                            break;
                        case "instantquestion":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.InstantQuestion;
                            break;
                        case "invalid":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.Invalid;
                            break;
                        case "sms":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.Sms;
                            break;
                        case "webcollaboration":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.WebCollaboration;
                            break;
                        case "workflowitemobject":
                            interactionSnapshotMediaType = InteractionSnapshotMediaType.WorkflowItemObject;
                            break;
                        default:
                            throw new Exception(String.Format("Unknown media type: '{0}'.", mediaType));
                    }

                    response = SearchInteractions(new MediaTypeFilter(new[] {interactionSnapshotMediaType}));
                    if (response.Success)
                        Trace.EasyIceServicesTopic.note("Successfully searched for interactions by Media Type '{}'. Got '{}' interaction snapshots.", mediaType, response.InteractionSnapshots.Count);
                    else
                        throw new Exception(response.ErrorMessage);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public SearchInteractionsResponse SearchInteractionsBySiteId(string siteId)
        {
            var response = new SearchInteractionsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNull(siteId, "siteId");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Searching for interactions by Site Id '{}'.", siteId);
                    int iSiteId;
                    if (!int.TryParse(siteId, out iSiteId))
                    {
                        throw new Exception(String.Format("Failed to convert '{0}' to a valid integer. Only integers are allowed (0, 1, 2, 3, 4, 999, etc.).", siteId));
                    }
                    response = SearchInteractions(new SiteIdFilter(iSiteId));
                    if (response.Success)
                        Trace.EasyIceServicesTopic.note("Successfully searched for interactions by Site Id '{}'. Got '{}' interaction snapshots.", siteId, response.InteractionSnapshots.Count);
                    else
                        throw new Exception(response.ErrorMessage);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Directories
        /// <inheritdoc/>
        public GetUserDirectoryPhoneNumbersResponse GetUserDirectoryPhoneNumbers(string directoryName, string username)
        {
            var response = new GetUserDirectoryPhoneNumbersResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(username, "username");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting user directory phone numbers for '{}' in directory '{}'.", username, directoryName);
                    var directoriesManager = DirectoriesManager.GetInstance(_session);
                    var directoryConfiguration = new DirectoryConfiguration(directoriesManager);
                    directoryConfiguration.StartWatching();

                    foreach (var directoryMetaData in directoryConfiguration.GetList())
                    {
                        if (!directoryMetaData.Id.Equals(directoryName, StringComparison.InvariantCultureIgnoreCase))
                            continue;

                        Trace.EasyIceServicesTopic.note("Found directory name: '{}'.", directoryName);
                        var contactDirectory = new ContactDirectory(directoriesManager, directoryMetaData);
                        contactDirectory.StartWatching();
                        foreach (var contactEntry in contactDirectory.GetList().Where(contactEntry => contactEntry.UserId.Equals(username, StringComparison.InvariantCultureIgnoreCase) || contactEntry.DisplayName.Equals(username, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            Trace.EasyIceServicesTopic.note("Found contact: '{}'.", username);

                            response.Contact = new SerializedContact(contactEntry.Extension,
                                                                     !contactEntry.BusinessPhone.IsEmpty ? contactEntry.BusinessPhone.DialString : String.Empty,
                                                                     !contactEntry.BusinessPhone.IsEmpty ? contactEntry.BusinessPhone.DisplayString : String.Empty,
                                                                     !contactEntry.BusinessPhone.IsEmpty ? contactEntry.BusinessPhone.PhoneNumber : String.Empty,
                                                                     !contactEntry.BusinessPhone.IsEmpty ? contactEntry.BusinessPhone.Extension : String.Empty,
                                                                     !contactEntry.BusinessPhone2.IsEmpty ? contactEntry.BusinessPhone2.DialString : String.Empty,
                                                                     !contactEntry.BusinessPhone2.IsEmpty ? contactEntry.BusinessPhone2.DisplayString : String.Empty,
                                                                     !contactEntry.BusinessPhone2.IsEmpty ? contactEntry.BusinessPhone2.PhoneNumber : String.Empty,
                                                                     !contactEntry.BusinessPhone2.IsEmpty ? contactEntry.BusinessPhone2.Extension : String.Empty,
                                                                     !contactEntry.MobilePhone.IsEmpty ? contactEntry.MobilePhone.DialString : String.Empty,
                                                                     !contactEntry.MobilePhone.IsEmpty ? contactEntry.MobilePhone.DisplayString : String.Empty,
                                                                     !contactEntry.MobilePhone.IsEmpty ? contactEntry.MobilePhone.PhoneNumber : String.Empty,
                                                                     !contactEntry.MobilePhone.IsEmpty ? contactEntry.MobilePhone.Extension : String.Empty,
                                                                     !contactEntry.HomePhone.IsEmpty ? contactEntry.HomePhone.DialString : String.Empty,
                                                                     !contactEntry.HomePhone.IsEmpty ? contactEntry.HomePhone.DisplayString : String.Empty,
                                                                     !contactEntry.HomePhone.IsEmpty ? contactEntry.HomePhone.PhoneNumber : String.Empty,
                                                                     !contactEntry.HomePhone.IsEmpty ? contactEntry.HomePhone.Extension : String.Empty
                                );
                            Trace.EasyIceServicesTopic.note("Successfully got user directory phone numbers for '{}' in directory '{}'.", username, directoryName);
                            break;
                        }
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region IPA

        #region Private Methods
        private static XmlDocument CreateInputXmlDocument()
        {
            // Create a new XmlDocument to pass as the initial values for IPA process input variables
            var xmldoc = new XmlDocument();

            // Creates the XML declaration
            var xmlnode = xmldoc.CreateNode(XmlNodeType.XmlDeclaration, string.Empty, string.Empty);
            xmldoc.AppendChild(xmlnode);

            // The root element of all process input XML documents is "Process"
            var xmlelement = xmldoc.CreateElement("Process");
            xmlelement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

            // Add an element for each process input variable that should be set when the process starts.
            // You can set a process variable as an input in Interaction Process Automation
            // by changing the assessibility parameter of the variable to "Input."
            // The name of the element needs to match the variable name in Interaction Process
            // Automation exactly.
            AddXmlElement(xmldoc, xmlelement, "ExampleBoolean", "true");
            AddXmlElement(xmldoc, xmlelement, "ExampleDate", "2002-09-24");
            AddXmlElement(xmldoc, xmlelement, "ExampleDateTime", "2002-05-30T09:00:00");
            AddXmlElement(xmldoc, xmlelement, "ExampleDecimal", "127.00");
            AddXmlElement(xmldoc, xmlelement, "ExampleDuration", "P5DT15H");
            AddXmlElement(xmldoc, xmlelement, "ExampleInteger", "72");
            AddXmlElement(xmldoc, xmlelement, "ExampleMailbox", "Info@ININ.com");
            AddXmlElement(xmldoc, xmlelement, "ExampleSkill", "Spanish");
            AddXmlElement(xmldoc, xmlelement, "ExampleString", "Hello World!");
            AddXmlElement(xmldoc, xmlelement, "ExampleTime", "09:30:10");
            AddXmlElement(xmldoc, xmlelement, "ExampleUser", "admin");
            AddXmlElement(xmldoc, xmlelement, "ExampleUri", "http://www.testhost.com/test?hello%20there");
            AddXmlElement(xmldoc, xmlelement, "ExampleWorkgroup", "Support");
            AddXmlElement(xmldoc, xmlelement, "ExampleStringCollection", "Red");
            AddXmlElement(xmldoc, xmlelement, "ExampleStringCollection", "Green");
            AddXmlElement(xmldoc, xmlelement, "ExampleStringCollection", "Blue");

            // For Complex Data types, you can add elements to your complex element
            // for each property. The name of the complex property must match the
            // property name set in the definition as opposed to the property's label.
            var customerElement = xmldoc.CreateElement("ExampleCustomer");
            AddXmlElement(xmldoc, customerElement, "FirstName", "John");
            AddXmlElement(xmldoc, customerElement, "LastName", "Smythe");

            // If your complex data type has a complex property, create another
            // element and add its properties to it.
            var homeAddressElement = xmldoc.CreateElement("HomeAddress");
            AddXmlElement(xmldoc, homeAddressElement, "StreetName", "Main");
            AddXmlElement(xmldoc, homeAddressElement, "ZipCode", "46268");

            // Then, add any complex properties to their parent XmlElement
            // and add the whole complex data type to the "Process" element
            // in this case.
            customerElement.AppendChild(homeAddressElement);
            xmlelement.AppendChild(customerElement);

            var addressCollectionElement = xmldoc.CreateElement("ExampleAddressCollection");
            AddXmlElement(xmldoc, addressCollectionElement, "StreetName", "Calumet Avenue");
            AddXmlElement(xmldoc, addressCollectionElement, "ZipCode", "46321");
            xmlelement.AppendChild(addressCollectionElement);

            addressCollectionElement = xmldoc.CreateElement("ExampleAddressCollection");
            AddXmlElement(xmldoc, addressCollectionElement, "StreetName", "Lake Shore Drive");
            AddXmlElement(xmldoc, addressCollectionElement, "ZipCode", "60615");
            xmlelement.AppendChild(addressCollectionElement);

            // For the Currency data type, each variable has an amount and a currency 
            // code associated with it. The code is a three digit ISO 4217 code. Writing the xml
            // input is a bit like a complex type, except that "amount" and "code" are the
            // currency's well known properties. Here, I'm making a currency of twenty-three
            // US Dollars and seven cents.
            var currencyElement = xmldoc.CreateElement("ExampleCurrency");
            AddXmlElement(xmldoc, currencyElement, "amount", "23.07");
            AddXmlElement(xmldoc, currencyElement, "code", "USD");
            xmlelement.AppendChild(currencyElement);

            xmldoc.AppendChild(xmlelement);

            return xmldoc;
        }
        private static void AddXmlElement(XmlDocument doc, XmlNode parent, string name, string value)
        {
            if (string.IsNullOrEmpty(value)) return;

            var element = doc.CreateElement(name);
            element.AppendChild(doc.CreateTextNode(value));
            parent.AppendChild(element);
        }
        #endregion

        /// <inheritdoc/>
        public GetLaunchableIPAProcessesResponse GetLaunchableIPAProcesses()
        {
            var response = new GetLaunchableIPAProcessesResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting launchable IPA processes.");
                    var processManager = ProcessManager.GetInstance(_session);
                    var processDefinitions = processManager.GetLaunchableProcesses();
                    foreach (var processDefinition in processDefinitions)
                    {
                        response.IPAProcesses.Add(processDefinition.Name);
                    }

                    Trace.EasyIceServicesTopic.note("Successfully got '{}' launchable IPA processes.", response.IPAProcesses.Count);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public StartIPAProcessResponse StartIPAProcess(string processName)
        {
            var response = new StartIPAProcessResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(processName, "processName");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Starting IPA process '{}'.", processName);
                    var processManager = ProcessManager.GetInstance(_session);
                    var parameters = new ProcessLaunchParameters { InputDocument = CreateInputXmlDocument() };
                    //TODO How to pass parameters? Can REST accept list/dictionary of strings?
                    XmlSchema xmlSchema = null;
                    foreach (var processDefinition in processManager.GetLaunchableProcesses().Where(processDefinition => processDefinition.Name.Equals(processName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        xmlSchema = processDefinition.Inputs;
                    }

                    if (xmlSchema == null)
                    {
                        Trace.EasyIceServicesTopic.warning("Could not find process called '{}'.", processName);
                        response.ErrorMessage = String.Format("Could not find process called '{0}'.", processName);
                        //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                        return response;
                    }

                    var instance = processManager.LaunchProcessByName(processName, parameters);
                    if (instance == null)
                        throw new Exception(String.Format("Failed to start process '{0}'. LaunchProcessByName() returned a null instance.", processName));

                    response.ProcessId = instance.NumericId.ToString(CultureInfo.InvariantCulture);
                    Trace.EasyIceServicesTopic.note("Successfully started IPA process '{}' with process id '{}'.", processName, response.ProcessId);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public CancelIPAProcessResponse CancelIPAProcess(string processId, string cancelReason)
        {
            var response = new CancelIPAProcessResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(processId, "processId");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Cancelling IPA process '{}' with reason '{}'.", processId, cancelReason);
                    var processManager = ProcessManager.GetInstance(_session);
                    processManager.CancelProcessInstances(new[] { long.Parse(processId) }, new[] { cancelReason });

                    Trace.EasyIceServicesTopic.note("Successfully cancelled IPA process '{}' with reason '{}'.", processId, cancelReason);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        #region Dialer

        #region Private Methods
        private bool ConnectToDialer()
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    Guard.ArgumentNotNull(_session, "session");

                    if (!IsConnectedToCIC)
                        throw new Exception("Not connected to CIC. Can't connect to Dialer.");

                    Trace.EasyIceServicesTopic.always(
                        "Currently connected to '{}' (IC Release Version '{}', Session Manager Release Version '{}', Session Manager Version '{}') as user '{}'.",
                        _session.ICServer, _session.InteractionCenterReleaseVersion,
                        _session.SessionManagerReleaseVersion, _session.SessionManagerVersion, _session.UserId);

                    _dialingManager = new DialingManager(_session);

                    Trace.EasyIceServicesTopic.note("Getting DialerConfigurationManager.");
                    if (_dialerConfigurationManager != null) _dialerConfigurationManager.Dispose();
                    _dialerConfigurationManager = new DialerConfigurationManager(_session);

                    Trace.EasyIceServicesTopic.always("Connection to Dialer succeeded!");
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex, "Exception in ConnectToDialer().");
                    return false;
                }
            }
        }
        private CampaignConfiguration GetCampaignConfiguration(string campaignName)
        {
            CampaignConfigurationList campaignConfigurationList = null;
            ReadOnlyCollection<CampaignConfiguration> campaignConfigurations = null;
            try
            {
                Trace.EasyIceServicesTopic.note("Getting Campaign Configuration.");
                campaignConfigurationList = new CampaignConfigurationList(_dialerConfigurationManager.ConfigurationManager);
                var campaignQuerySettings = campaignConfigurationList.CreateQuerySettings();
                campaignQuerySettings.SetPropertiesToRetrieve(new[] { CampaignConfiguration.Property.ContactList });
                var campaignFilter = new BasicFilterDefinition<CampaignConfiguration, CampaignConfiguration.Property>(CampaignConfiguration.Property.DisplayName, campaignName, FilterMatchType.Exact);
                campaignQuerySettings.SetFilterDefinition(campaignFilter);
                Trace.EasyIceServicesTopic.note("Start caching on campaign configuration list.");
                campaignConfigurationList.StartCaching(campaignQuerySettings);
                Trace.EasyIceServicesTopic.note("Getting Contact List Configuration id.");
                campaignConfigurations = campaignConfigurationList.GetConfigurationList();
                if (campaignConfigurations == null || campaignConfigurations.Count == 0)
                {
                    throw new Exception(String.Format("Could not find campaign '{0}' configuration.", campaignName));
                }
                Trace.EasyIceServicesTopic.note("Got {} campaign configuration(s).", campaignConfigurations.Count);
            }
            catch (Exception ex)
            {
                Trace.EasyIceServicesTopic.exception(ex);
            }
            finally
            {
                if (campaignConfigurationList != null && campaignConfigurationList.IsCaching)
                    campaignConfigurationList.StopCaching();
            }
            return campaignConfigurations != null && campaignConfigurations.Count > 0 ? campaignConfigurations[0] : null;
        }
        private ContactListConfiguration GetContactListConfiguration(ConfigurationId contactListConfigurationId)
        {
            ContactListConfigurationList contactListConfigurationList = null;
            ContactListConfiguration contactListConfiguration = null;
            try
            {
                Trace.EasyIceServicesTopic.note("Start getting Contact List Configurations.");
                contactListConfigurationList = new ContactListConfigurationList(_dialerConfigurationManager.ConfigurationManager);
                var contactListQuerySettings = contactListConfigurationList.CreateQuerySettings();
                contactListQuerySettings.SetPropertiesToRetrieveToAll();
                Trace.EasyIceServicesTopic.note("Setting filter to search for '{}'.", contactListConfigurationId.DisplayName);
                var contactListFilter = new BasicFilterDefinition<ContactListConfiguration, ContactListConfiguration.Property>(ContactListConfiguration.Property.Id, contactListConfigurationId.DisplayName, FilterMatchType.Exact);
                contactListQuerySettings.SetFilterDefinition(contactListFilter);
                contactListConfigurationList.StartCaching(contactListQuerySettings);
                Trace.EasyIceServicesTopic.note("Getting Contact List Configurations.");
                var contactListConfigurations = contactListConfigurationList.GetConfigurationList();
                if (contactListConfigurations == null || contactListConfigurations.Count == 0)
                {
                    throw new Exception(String.Format("Could not find contact list '{0}' configuration.", contactListConfigurationId.DisplayName));
                }
                contactListConfiguration = contactListConfigurations[0];
                Trace.EasyIceServicesTopic.note("Got Contact List Configuration: '{}'.", contactListConfiguration.ConfigurationId.DisplayName);
                contactListConfigurationList.StopCaching();
            }
            catch (Exception ex)
            {
                Trace.EasyIceServicesTopic.exception(ex);
            }
            finally
            {
                if (contactListConfigurationList != null && contactListConfigurationList.IsCaching)
                    contactListConfigurationList.StopCaching();
            }
            return contactListConfiguration;
        }
        private ReadOnlyCollection<ContactColumnConfiguration> GetContactColumns(ContactListConfiguration contactListConfiguration)
        {
            ReadOnlyCollection<ContactColumnConfiguration> contactColumns = null;

            using (Trace.EasyIceServicesTopic.scope())
            {
                Guard.ArgumentNotNull(contactListConfiguration, "contactListConfiguration");

                try
                {
                    var contactColumnConfigurationList = new ContactColumnConfigurationList(_dialerConfigurationManager.ConfigurationManager);
                    var querySettings = contactColumnConfigurationList.CreateQuerySettings();
                    querySettings.SetPropertiesToRetrieve(ContactColumnConfiguration.Property.DisplayName);
                    var filters = contactListConfiguration.ContactColumns.Value.Select(contactColumn => new BasicFilterDefinition<ContactColumnConfiguration, ContactColumnConfiguration.Property>(ContactColumnConfiguration.Property.Id, contactColumn.DisplayName)).Cast<FilterDefinition<ContactColumnConfiguration, ContactColumnConfiguration.Property>>().ToList();
                    querySettings.SetFilterDefinition(new GroupFilterDefinition<ContactColumnConfiguration, ContactColumnConfiguration.Property>(filters));
                    contactColumnConfigurationList.StartCaching();
                    contactColumns = contactColumnConfigurationList.GetConfigurationList();
                    foreach (var contactColumn in contactColumns)
                    {
                        Trace.EasyIceServicesTopic.note("Got contact column: '{}'.", contactColumn.ConfigurationId.DisplayName);
                    }
                    contactColumnConfigurationList.StopCaching();
                    return contactColumns;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex, "Exception while getting contact columns from contact list '{}'.", contactListConfiguration.ConfigurationId.DisplayName);
                }
            }
            return contactColumns;
        }
        private IEnumerable<Dictionary<string, string>> GetContacts(ContactListConfiguration contactListConfiguration, Dictionary<string, string> lookupData, List<string> columnNamesToRetrieve)
        {
            var contactsToReturn = new List<Dictionary<string, string>>();

            using (Trace.EasyIceServicesTopic.scope())
            {
                Guard.ArgumentNotNull(contactListConfiguration, "contactListConfiguration");
                Guard.ArgumentNotNull(lookupData, "lookupData");

                try
                {
                    var select = new SelectCommand(contactListConfiguration);

                    //Include all data in where clause
                    foreach (var columnData in lookupData)
                    {
                        Trace.EasyIceServicesTopic.verbose("Adding column name '{}' with value '{}' to where clause.", columnData.Key, columnData.Value);
                        var expression = new BinaryExpression
                            (new ColumnExpression(contactListConfiguration.ColumnMap[columnData.Key]),
                             new ConstantExpression(columnData.Value, contactListConfiguration.ColumnMap[columnData.Key]),
                             BinaryOperationType.Equal);
                        select.Where = new BinaryExpression(expression, select.Where, BinaryOperationType.And);
                    }

                    Trace.EasyIceServicesTopic.note("Getting contacts.");
                    var contacts = contactListConfiguration.GetContacts(_dialerConfigurationManager.GetHttpRequestKey(contactListConfiguration.ConfigurationId), select);
                    if (contacts == null || contacts.Count == 0)
                    {
                        Trace.EasyIceServicesTopic.warning("No matches found in contact list '{}'.", contactListConfiguration.ConfigurationId.DisplayName);
                        return null;
                    }

                    Trace.EasyIceServicesTopic.note("Found {} match(es) in contact list '{}'.", contacts.Count, contactListConfiguration.ConfigurationId.DisplayName);
                    foreach (var contact in contacts)
                    {
                        var contactToReturn = new Dictionary<string, string>
                            {
                                {
                                    ContactListConfiguration.I3_Identity.Name,
                                    contact[ContactListConfiguration.I3_Identity.Name].ToString()
                                }
                            };
                        foreach (var columnName in columnNamesToRetrieve)
                        {
                            if (contact.ContainsKey(columnName))
                            {
                                if (!contactToReturn.ContainsKey(columnName))
                                {
                                    Trace.EasyIceServicesTopic.verbose("Contact column '{}' value: '{}'.", columnName, contact[columnName].ToString());
                                    if (!contactToReturn.ContainsKey(columnName))
                                        contactToReturn.Add(columnName, contact[columnName].ToString());
                                }
                            }
                            else
                            {
                                Trace.EasyIceServicesTopic.warning("Column '{}' does not exist in contact list '{}'.", columnName, contactListConfiguration.ConfigurationId.DisplayName);
                            }
                        }
                        contactsToReturn.Add(contactToReturn);
                    }

                    Trace.EasyIceServicesTopic.note("Finished populating all columns for all matches in contact list '{}'. Got {} contacts.", contactListConfiguration.ConfigurationId.DisplayName, contactsToReturn.Count);
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex, "Exception while getting contacts from contact list '{}'.", contactListConfiguration.ConfigurationId.DisplayName);
                }
            }
            return contactsToReturn;
        }
        private bool UpdateContact(ContactListConfiguration contactListConfiguration, string identity, Dictionary<string, string> updateData)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                Guard.ArgumentNotNull(contactListConfiguration, "contactListConfiguration");
                Guard.ArgumentNotNullOrEmptyString(identity, "identity");

                try
                {
                    var updateCommand = new UpdateCommand(contactListConfiguration, null)
                    {
                        Where = new BinaryExpression(new ColumnExpression(ContactListConfiguration.I3_Identity), new ConstantExpression(identity, ContactListConfiguration.I3_Identity), BinaryOperationType.Equal)
                    };

                    foreach (var updateDataItem in updateData)
                    {
                        Trace.EasyIceServicesTopic.note("UpdateCommand: Setting column '{}' to '{}'.", updateDataItem.Key, updateDataItem.Value);
                        updateCommand.UpdateData[contactListConfiguration.ColumnMap[updateDataItem.Key]] = updateDataItem.Value;
                    }

                    var transaction = new ContactListTransaction { updateCommand };
                    Trace.EasyIceServicesTopic.note("Running transaction for Update Command.");
                    var recordsAffected = contactListConfiguration.RunTransaction(transaction);
                    Trace.EasyIceServicesTopic.note("Finished running transaction. Number of records affected: '{}'.", recordsAffected);

                    Trace.EasyIceServicesTopic.note("Returning 'true'.");
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex, "Exception while updating a contact.");
                }
                Trace.EasyIceServicesTopic.note("Returning 'false'.");
                return false;
            }
        }
        private List<EasyResponses.DialerStatusMessage> GetCampaignMessages(string campaignName)
        {
            var statusMessages = new List<EasyResponses.DialerStatusMessage>();

            var dialerHealthMonitor = new DialerHealthMonitor(_session);

            dialerHealthMonitor.StartWatchingStatus();
            foreach (var currentDialerStatusMessage in dialerHealthMonitor.ActiveStatusMessages)
            {
                if (!String.IsNullOrEmpty(campaignName) && !currentDialerStatusMessage.Campaign.DisplayName.Equals(campaignName, StringComparison.InvariantCultureIgnoreCase))
                    continue;
                var dialerStatusMessage = new EasyResponses.DialerStatusMessage
                    {
                        Campaign = currentDialerStatusMessage.Campaign.DisplayName,
                        Content = currentDialerStatusMessage.Content,
                        CreatedDateTime = currentDialerStatusMessage.CreatedTime,
                        LastModifiedDateTime = currentDialerStatusMessage.LastModifiedTime,
                        MachineName = currentDialerStatusMessage.MachineName
                    };

                #region Message Type
                switch (currentDialerStatusMessage.MessageType)
                {
                    case DialerStatusMessageType.AgentTSFailure:
                        dialerStatusMessage.MessageType = "An agent has been placed on break because of a TS (Telephony) API call failed.";
                        break;
                    case DialerStatusMessageType.CallListDelayed:
                        dialerStatusMessage.MessageType = "Contact list updates are accumulating in queue.";
                        break;
                    case DialerStatusMessageType.CampaignAbandonRateExceeded:
                        dialerStatusMessage.MessageType = "Current abandonment rate exceeds the configured target rate.";
                        break;
                    case DialerStatusMessageType.CampaignAllZonesBlocked:
                        dialerStatusMessage.MessageType = "Configured Zone Set is has blocked all callable contacts.";
                        break;
                    case DialerStatusMessageType.CampaignFilterSizeEmpty:
                        dialerStatusMessage.MessageType = "No callable contacts left.";
                        break;
                    case DialerStatusMessageType.CampaignRetrievingContacts:
                        dialerStatusMessage.MessageType = "Contact cache has been depleted.";
                        break;
                    case DialerStatusMessageType.CampaignStrictAbandonRateExceeded:
                        dialerStatusMessage.MessageType = "Current abandonment rate exceeds the configured strict abandonment rate.";
                        break;
                    case DialerStatusMessageType.ContactCacheEmpty:
                        dialerStatusMessage.MessageType = "Contact cache has been depleted.";
                        break;
                    case DialerStatusMessageType.DNCScrubFailing:
                        dialerStatusMessage.MessageType = "Do Not Call scrubbing failed.";
                        break;
                    case DialerStatusMessageType.DialerConfigLoadError:
                        dialerStatusMessage.MessageType = "A problem occurred while attempting to load the configuration XML file.";
                        break;
                    case DialerStatusMessageType.DialerTranSlowToRespond:
                        dialerStatusMessage.MessageType = "Dialer TranServer is not responding.";
                        break;
                    case DialerStatusMessageType.FewAgentsForPredictive:
                        dialerStatusMessage.MessageType = "Campaign has fewer than six agents, potentially decreasing agent utilization and increasing abandon rate.";
                        break;
                    case DialerStatusMessageType.GlobalContactListUpdatesSlow:
                        dialerStatusMessage.MessageType = "Contact list updates are accumulating in queue.";
                        break;
                    case DialerStatusMessageType.GlobalHistoryInsertsSlow:
                        dialerStatusMessage.MessageType = "History updates (campaign stats, agent stats, call history) are accumulating in queue.";
                        break;
                    case DialerStatusMessageType.GlobalMaxCallsReached:
                        dialerStatusMessage.MessageType = "Dialing is being restricted because because the configured line capacity has been reached.";
                        break;
                    case DialerStatusMessageType.HighNonDialerReservation:
                        dialerStatusMessage.MessageType = "A campaign is reserving high percentage of active agents for non-Dialer calls.";
                        break;
                    case DialerStatusMessageType.HistoryDelayed:
                        dialerStatusMessage.MessageType = "History updates (campaign stats, agent stats, call history) are accumulating in queue.";
                        break;
                    case DialerStatusMessageType.IncorrectCollation:
                        dialerStatusMessage.MessageType = "Default database is not specified or UDL is not compatible with Dialer.";
                        break;
                    case DialerStatusMessageType.IncorrectProvider:
                        dialerStatusMessage.MessageType = "The database provider in use is not recommended for use with Dialer.";
                        break;
                    case DialerStatusMessageType.InvalidContactColumnBehavior:
                        dialerStatusMessage.MessageType = "Policy set contains one or more behaviors referencing contact columns not used by the campaign.";
                        break;
                    case DialerStatusMessageType.InvalidLinegroup:
                        dialerStatusMessage.MessageType = "Configured line group is invalid or contains no lines.";
                        break;
                    case DialerStatusMessageType.InvalidWorkgroup:
                        dialerStatusMessage.MessageType = "Configured workgroup is invalid or contains no agents.";
                        break;
                    case DialerStatusMessageType.MapDataSetFileUnavailable:
                        dialerStatusMessage.MessageType = "Automatic time zome mapping source data file is not accessible because the map data set is missing.";
                        break;
                    case DialerStatusMessageType.MaxDialerCallRateReached:
                        dialerStatusMessage.MessageType = "Dialing is being restricted by the configured maximum call rate setting.";
                        break;
                    case DialerStatusMessageType.NoAgentSkills:
                        dialerStatusMessage.MessageType = "Agent has none of the required skills for the campaign, so no calls will be placed for the agent.";
                        break;
                    case DialerStatusMessageType.NoDialPlanData:
                        dialerStatusMessage.MessageType = "Automatic time zome mapping source data file is not accessible because no Central Campaign Server (CCS) dial plan is available for phone number standardization.";
                        break;
                    case DialerStatusMessageType.NoInitialCatalog:
                        dialerStatusMessage.MessageType = "Default database is not specified or UDL is invalid.";
                        break;
                    case DialerStatusMessageType.NoPersistentConnectionWavFile:
                        dialerStatusMessage.MessageType = "An agent logged into a campaign with a persistent connection, but no persistent connection wav file is configured.";
                        break;
                    case DialerStatusMessageType.NoTimezoneMapDataSets:
                        dialerStatusMessage.MessageType = "Automatic time zome mapping source data file is not accessible because no time zone map data sets are configured for automatic time zone mapping.";
                        break;
                    case DialerStatusMessageType.NoTimezoneMappings:
                        dialerStatusMessage.MessageType = "Automatic time zome mapping is unavailable for all campaigns because no automatic time zone mappings are present.";
                        break;
                    case DialerStatusMessageType.None:
                        dialerStatusMessage.MessageType = "Invalid status message type.";
                        break;
                    case DialerStatusMessageType.RecycleInProgress:
                        dialerStatusMessage.MessageType = "A campaign is recycling its contact list.";
                        break;
                    case DialerStatusMessageType.TSDown:
                        dialerStatusMessage.MessageType = "Campaign is paused because TS (Telephony) API calls are failing, likely due to TS server not running on the CIC (ODS) server.";
                        break;
                    case DialerStatusMessageType.TimezoneMappingUnavailable:
                        dialerStatusMessage.MessageType = "Automatic time zome mapping is unavailable because the time zone source data could not be initialized.";
                        break;
                    default:
                        dialerStatusMessage.MessageType = "Unknown";
                        break;
                }
                #endregion

                dialerStatusMessage.ProcessName = currentDialerStatusMessage.ProcessName;

                #region Message Severity
                switch (currentDialerStatusMessage.Severity)
                {
                    case DialerStatusMessageSeverity.Error:
                        dialerStatusMessage.Severity = "Error";
                        break;
                    case DialerStatusMessageSeverity.Information:
                        dialerStatusMessage.Severity = "Information";
                        break;
                    case DialerStatusMessageSeverity.Unknown:
                        dialerStatusMessage.Severity = "Unknown";
                        break;
                    case DialerStatusMessageSeverity.Warning:
                        dialerStatusMessage.Severity = "Warning";
                        break;
                    default:
                        dialerStatusMessage.Severity = "Unknown";
                        break;
                }
                #endregion

                dialerStatusMessage.Title = currentDialerStatusMessage.Title;

                statusMessages.Add(dialerStatusMessage);
            }
            dialerHealthMonitor.StopWatchingStatus();
            return statusMessages;
        }
        private CampaignValidationResult GetCampaignValidationResult(string campaignName)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    var campaignValidateResult = new CampaignValidationResult();
                    var campaignConfiguration = GetCampaignConfiguration(campaignName);
                    var quickValidateResults = campaignConfiguration.QuickValidate();
                    campaignValidateResult.Campaign = campaignName;
                    campaignValidateResult.Success = quickValidateResults.Success;
                    campaignValidateResult.ErrorMessage = quickValidateResults.ErrorMessage;

                    return campaignValidateResult;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
                return null;
            }
        }
        private CampaignTestResult GetCampaignTestResult(string campaignName)
        {
            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    var campaignTestResult = new CampaignTestResult();
                    var campaignConfiguration = GetCampaignConfiguration(campaignName);
                    var campaignTestResults = campaignConfiguration.Test(20);

                    campaignTestResult.Campaign = campaignName;
                    campaignTestResult.CallableRecords = campaignTestResults.CallableRecords;
                    campaignTestResult.ContactListTable = campaignTestResults.ContactListTable;
                    campaignTestResult.ErrorMessage = campaignTestResults.ErrorMessage;
                    campaignTestResult.Filter = campaignTestResults.Filter;
                    campaignTestResult.FilteredRecords = campaignTestResults.FilteredRecords;
                    campaignTestResult.NoPhoneNumberRecords = campaignTestResults.NoPhoneNumberRecords;
                    campaignTestResult.NRRLib = campaignTestResults.NRRLib;
                    #region Sample Records
                    foreach (var sampleRecordColumn in campaignTestResults.SampleRecordColumns)
                    {
                        campaignTestResult.SampleRecordColumns.Add(sampleRecordColumn.Name);
                    }
                    foreach (var sampleRecord in campaignTestResults.SampleRecords)
                    {
                        campaignTestResult.SampleRecords.Add(new List<String>(sampleRecord));
                    }
                    #endregion
                    campaignTestResult.ScheduledCalls = campaignTestResults.ScheduledCalls;
                    #region Skill Combinations
                    foreach (var skillCombinationColumn in campaignTestResults.SkillCombinationColumns)
                    {
                        campaignTestResult.SkillCombinationColumns.Add(skillCombinationColumn);
                    }
                    foreach (var skillCombination in campaignTestResults.SkillCombinations)
                    {
                        campaignTestResult.SkillCombinations.Add(new SkillCombination(skillCombination.RecordCount, new List<String>(skillCombination.SkillValues)));
                    }
                    #endregion
                    campaignTestResult.Sort = campaignTestResults.Sort;
                    campaignTestResult.Success = campaignTestResults.Success;
                    campaignTestResult.TotalRecords = campaignTestResults.TotalRecords;
                    campaignTestResult.TotalUncallableRecords = campaignTestResults.UncallableRecords;

                    return campaignTestResult;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                }
                return null;
            }
        }
        #endregion

        /// <summary>
        /// True if connected to Dialer, false otherwise.
        /// </summary>
        public bool IsConnectedToDialer
        {
            get
            {
                var connectedToCIC = _session != null && _session.ConnectionState == ConnectionState.Up;
                var connectedToDialer = _dialingManager != null && _dialingManager.DialerSession != null;
                return connectedToCIC && connectedToDialer;
            }
        }
        /// <inheritdoc/>
        public GetAvailableCampaignsResponse GetAvailableCampaigns()
        {
            var response = new GetAvailableCampaignsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting available campaigns for user '{}'.", _session.UserId);

                    string[] campaigns = null;

                    if (_dialingManager != null)
                    {
                        Trace.EasyIceServicesTopic.verbose("Calling Dialing Manager GetAvailableCampaigns().");
                        campaigns = _dialingManager.GetAvailableCampaigns();
                    }
                    if (campaigns != null)
                    {
                        Trace.EasyIceServicesTopic.note("Found {} campaigns. Sorting them before returning them.", campaigns.Length);
                        var campaignsList = new List<String>(campaigns);
                        campaignsList.Sort();
                        response.AvailableCampaigns = campaignsList;
                    }

                    Trace.EasyIceServicesTopic.note("Retrieved {} campaigns for user '{}'.", response.AvailableCampaigns == null ? "0" : response.AvailableCampaigns.Count.ToString(CultureInfo.InvariantCulture), _session.UserId);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetContactsResponse GetContacts(string campaignName)
        {
            var response = new GetContactsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation

                    Guard.ArgumentNotNullOrEmptyString(campaignName, "campaignName");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting contacts for campaign '{}'.", campaignName);

                    #region Get Campaign Configuration
                    var campaignConfiguration = GetCampaignConfiguration(campaignName);
                    if (campaignConfiguration == null)
                    {
                        Trace.EasyIceServicesTopic.error("Campaign '{}' does not exist.", campaignName);
                        response.ErrorMessage = String.Format("Campaign '{0}' does not exist.", campaignName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    #region Get Campaign Contact List
                    ConfigurationId contactListConfigurationId;
                    if (campaignConfiguration.ContactList != null)
                    {
                        contactListConfigurationId = campaignConfiguration.ContactList.Value;
                        Trace.EasyIceServicesTopic.note("Got Contact List Configuration: '{}'.", contactListConfigurationId.DisplayName);
                    }
                    else
                    {
                        throw new Exception(String.Format("Contact list does not exist or is not configured for campaign '{0}'.", campaignName));
                    }
                    var contactListConfiguration = GetContactListConfiguration(contactListConfigurationId);
                    if (contactListConfigurationId == null)
                    {
                        throw new Exception(String.Format("Could not get contact list configuration for campaign '{0}'.", campaignName));
                    }
                    Trace.EasyIceServicesTopic.note("Got Contact List Configuration: '{}'.", contactListConfiguration.ConfigurationId.DisplayName);
                    #endregion

                    #region Get Contacts
                    var select = new SelectCommand(contactListConfiguration);
                    Trace.EasyIceServicesTopic.note("Getting contacts now.");
                    var records = contactListConfiguration.GetContacts(_dialerConfigurationManager.GetHttpRequestKey(contactListConfiguration.ConfigurationId), select);
                    Trace.EasyIceServicesTopic.note("Found '{}' records in contact list '{}'.", records == null ? "0" : records.Count.ToString(CultureInfo.InvariantCulture), contactListConfiguration.ConfigurationId.DisplayName);
                    #endregion

                    #region Format Records Output
                    if (records != null)
                    {
                        Trace.EasyIceServicesTopic.note("We have contacts!");
                        foreach (var record in records)
                        {
                            Trace.EasyIceServicesTopic.verbose("Creating new contact with Identity: '{}'.", record[ContactListConfiguration.I3_Identity.Name].ToString());
                            var contactInfo = new Dictionary<string, string>
                                {
                                    {
                                        ContactListConfiguration.I3_Identity.Name, 
                                        record[ContactListConfiguration.I3_Identity.Name].ToString()
                                    }
                                };
                            Trace.EasyIceServicesTopic.verbose("Adding column names and values for identity '{}'.", contactInfo[ContactListConfiguration.I3_Identity.Name]);
                            foreach (var columnName in contactListConfiguration.GetColumns().Where(columnName => !contactInfo.ContainsKey(columnName.Name)))
                            {
                                Trace.EasyIceServicesTopic.verbose("Adding column name '{}' and value '{}' for identity '{}'.", columnName.Name, record[columnName.Name].ToString(), contactInfo[ContactListConfiguration.I3_Identity.Name]);
                                contactInfo.Add(columnName.Name, record[columnName.Name].ToString());
                            }
                            Trace.EasyIceServicesTopic.note("Adding contact info to list.");
                            response.Contacts.Add(record[ContactListConfiguration.I3_Identity.Name].ToString(), contactInfo);
                        }
                        Trace.EasyIceServicesTopic.note("Finished looping through contacts.");
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Retrieved {} contacts for campaign '{}'.", response.Contacts.Count.ToString(CultureInfo.InvariantCulture), campaignName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public InsertContactResponse InsertContact(InsertContactData insertContactData)
        {
            var response = new InsertContactResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNull(insertContactData, "insertContactData");
                    Guard.ArgumentNotNullOrEmptyString(insertContactData.CampaignName, "campaignName");
                    Guard.ArgumentNotNull(insertContactData.InsertData, "insertData");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Inserting new contact.");

                    #region Get Campaign Configuration
                    var campaignConfiguration = GetCampaignConfiguration(insertContactData.CampaignName);
                    if (campaignConfiguration == null)
                    {
                        Trace.EasyIceServicesTopic.error("Campaign '{}' does not exist.", insertContactData.CampaignName);
                        response.ErrorMessage = String.Format("Campaign '{0}' does not exist.", insertContactData.CampaignName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    #region Get Campaign Contact List
                    ConfigurationId contactListConfigurationId;
                    if (campaignConfiguration.ContactList != null)
                    {
                        contactListConfigurationId = campaignConfiguration.ContactList.Value;
                        Trace.EasyIceServicesTopic.note("Got Contact List Configuration: '{}'.", contactListConfigurationId.DisplayName);
                    }
                    else
                    {
                        throw new Exception(String.Format("Contact list does not exist or is not configured for campaign '{0}'.", insertContactData.CampaignName));
                    }
                    var contactListConfiguration = GetContactListConfiguration(contactListConfigurationId);
                    if (contactListConfigurationId == null)
                    {
                        throw new Exception(String.Format("Could not get contact list configuration for campaign '{0}'.", insertContactData.CampaignName));
                    }
                    Trace.EasyIceServicesTopic.note("Got Contact List Configuration: '{}'.", contactListConfiguration.ConfigurationId.DisplayName);
                    #endregion

                    #region Insert Contact
                    var insertCommand = new InsertCommand(contactListConfiguration);
                    Trace.EasyIceServicesTopic.note("Inserting contact data.");
                    foreach (var dataItem in insertContactData.InsertData)
                    {
                        Trace.EasyIceServicesTopic.verbose("Inserting data. Column '{}', Value '{}'.", dataItem.Key, dataItem.Value);
                        insertCommand.Contact.Add(contactListConfiguration.ColumnMap[dataItem.Key], dataItem.Value);
                    }

                    if (insertContactData.Immediate)
                    {
                        Trace.EasyIceServicesTopic.note("Setting contact status to 'J' (immediate contact).");
                        insertCommand.Contact[ContactListConfiguration.Status] = "J";
                    }

                    insertCommand.Where = EmptyExpression.Instance;
                    var transaction = new ContactListTransaction { insertCommand };
                    Trace.EasyIceServicesTopic.note("Running transaction for Insert Command.");
                    var recordsAffected = contactListConfiguration.RunTransaction(transaction);
                    Trace.EasyIceServicesTopic.note("Finished running transaction. Number of records affected: '{}'.", recordsAffected);
                    #endregion

                    Trace.EasyIceServicesTopic.note("Successfully inserted contact into campaign '{}'.", insertContactData.CampaignName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public RecycleContactListResponse RecycleContactList(string campaignName)
        {
            var response = new RecycleContactListResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(campaignName, "campaignName");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Recycling contact list for campaign '{}'.", campaignName);

                    #region Get Campaign Configuration
                    var campaignConfiguration = GetCampaignConfiguration(campaignName);
                    if (campaignConfiguration == null)
                    {
                        Trace.EasyIceServicesTopic.error("Campaign '{}' does not exist.", campaignName);
                        response.ErrorMessage = String.Format("Campaign '{0}' does not exist.", campaignName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    #region Recycle Contact List
                    if (campaignConfiguration.ContactList != null)
                    {
                        Trace.EasyIceServicesTopic.note("Recycling contact list for campaign: '{}'.", campaignName);
                        if (campaignConfiguration.RecyclesRemaining.Value > 0)
                        {
                            var remainingRecords = campaignConfiguration.RecycleContactList();
                            response.Success = true;
                            Trace.EasyIceServicesTopic.note("Remaining records in the current recycle: '{}'.", remainingRecords);
                            response.RemainingRecords = remainingRecords;
                        }
                        else
                        {
                            response.Success = true;
                            Trace.EasyIceServicesTopic.warning("Ignoring recycle request. No recycles left to process.");
                            response.ErrorMessage = "Warning: Ignored recycle request. No recycles left to process.";
                        }
                    }
                    else
                    {
                        throw new Exception(String.Format("Contact list does not exist or is not configured for campaign id '{0}'.", campaignName));
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public ResetCampaignResponse ResetCampaign(string campaignName)
        {
            var response = new ResetCampaignResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(campaignName, "campaignName");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Resetting campaign '{}'.", campaignName);

                    #region Get Campaign Configuration
                    var campaignConfiguration = GetCampaignConfiguration(campaignName);
                    if (campaignConfiguration == null)
                    {
                        Trace.EasyIceServicesTopic.error("Campaign '{}' does not exist.", campaignName);
                        response.ErrorMessage = String.Format("Campaign '{0}' does not exist.", campaignName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    response.RemainingRecords = campaignConfiguration.ResetCampaign();

                    Trace.EasyIceServicesTopic.note("Campaign '{}' has been successfully resetted.", campaignName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetCampaignAgentsResponse GetCampaignAgents(string campaignName)
        {
            var response = new GetCampaignAgentsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(campaignName, "campaignName");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting Active and Eligible agents for campaign '{}'.", campaignName);

                    #region Get Campaign Configuration
                    var campaignConfiguration = GetCampaignConfiguration(campaignName);
                    if (campaignConfiguration == null)
                    {
                        Trace.EasyIceServicesTopic.error("Campaign '{}' does not exist.", campaignName);
                        response.ErrorMessage = String.Format("Campaign '{0}' does not exist.", campaignName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    var agentManager = new AgentManager(_session);
                    response.ActiveAgents = agentManager.GetActiveAgentsForCampaign(campaignConfiguration.ConfigurationId);
                    response.EligibleAgents = agentManager.GetEligibleAgentsForCampaign(campaignConfiguration.ConfigurationId);

                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetAgentCampaignsResponse GetAgentCampaigns(string agentName)
        {
            var response = new GetAgentCampaignsResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(agentName, "agentName");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting Active and Eligible campaigns for agent '{}'.", agentName);

                    #region Agent Exists?
                    if (!GetUser(agentName).Success)
                    {
                        Trace.EasyIceServicesTopic.error("Username '{}' does not exist.", agentName);
                        response.ErrorMessage = String.Format("Username '{0}' does not exist.", agentName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    var agentManager = new AgentManager(_session);
                    foreach (var campaignId in agentManager.GetActiveCampaignsForAgent(agentName))
                    {
                        response.ActiveCampaigns.Add(campaignId.Name);
                    }
                    foreach (var campaignId in agentManager.GetEligibleCampaignsForAgent(agentName))
                    {
                        response.EligibleCampaigns.Add(campaignId.Name);
                    }

                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public ExcludePhoneNumberFromCampaignResponse ExcludePhoneNumberFromCampaign(string campaignName, string phoneNumber)
        {
            var response = new ExcludePhoneNumberFromCampaignResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(campaignName, "campaignName");
                    Guard.ArgumentNotNull(phoneNumber, "phoneNumber");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Excluding phone number '{}' from campaign '{}'.", phoneNumber, campaignName);

                    #region Get Campaign Configuration
                    var campaignConfiguration = GetCampaignConfiguration(campaignName);
                    if (campaignConfiguration == null)
                    {
                        Trace.EasyIceServicesTopic.error("Campaign '{}' does not exist.", campaignName);
                        response.ErrorMessage = String.Format("Campaign '{0}' does not exist.", campaignName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    #region Get Campaign Contact List
                    ConfigurationId contactListConfigurationId;
                    if (campaignConfiguration.ContactList != null)
                    {
                        contactListConfigurationId = campaignConfiguration.ContactList.Value;
                        Trace.EasyIceServicesTopic.note("Got Contact List Configuration: '{}'.", contactListConfigurationId.DisplayName);
                    }
                    else
                    {
                        throw new Exception(String.Format("Contact list does not exist or is not configured for campaign '{0}'.", campaignName));
                    }
                    var contactListConfiguration = GetContactListConfiguration(contactListConfigurationId);
                    if (contactListConfigurationId == null)
                    {
                        throw new Exception(String.Format("Could not get contact list configuration for campaign '{0}'.", campaignName));
                    }
                    Trace.EasyIceServicesTopic.note("Got Contact List Configuration: '{}'.", contactListConfiguration.ConfigurationId.DisplayName);
                    #endregion

                    #region Get Contact Columns
                    var contactColumns = GetContactColumns(contactListConfiguration);
                    if (contactColumns == null || contactColumns.Count == 0)
                    {
                        throw new Exception(String.Format("Could not get contact columns for campaign '{0}'.", campaignName));
                    }
                    #endregion

                    #region Fill phone number in contact columns
                    var lookupData = new Dictionary<string, string>();
                    foreach (var contactColumnName in contactColumns)
                    {
                        Trace.EasyIceServicesTopic.note("Got contact column: '{}'.", contactColumnName.ConfigurationId.DisplayName);
                        if (!lookupData.ContainsKey(contactColumnName.ConfigurationId.DisplayName))
                            lookupData.Add(contactColumnName.ConfigurationId.DisplayName, phoneNumber);
                    }
                    #endregion

                    #region Get the list of contacts
                    var columnsToRetrieve = new List<string> { ContactListConfiguration.I3_Identity.Name };
                    var matchingContacts = GetContacts(contactListConfiguration, lookupData, columnsToRetrieve);
                    if (matchingContacts == null)
                    {
                        Trace.EasyIceServicesTopic.error("No contacts found with phoneNumber '{}' in campaign '{}'.", phoneNumber, campaignName);
                        response.ErrorMessage = String.Format("No contacts found with phoneNumber '{0}' in campaign '{1}'.", phoneNumber, campaignName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    #region Update Contacts Status
                    foreach (var contactToUpdate in matchingContacts)
                    {
                        if (!contactToUpdate.ContainsKey(ContactListConfiguration.I3_Identity.Name)) continue;

                        var updateData = new Dictionary<string, string>
                                {
                                    {ContactListConfiguration.Status.Name, "E"}
                                };
                        
                        if (UpdateContact(contactListConfiguration, contactToUpdate[ContactListConfiguration.I3_Identity.Name], updateData)) continue;
                        Trace.EasyIceServicesTopic.error("Failed to update contact with identity '{}'.", contactToUpdate[ContactListConfiguration.I3_Identity.Name]);
                        response.ErrorMessage += String.Format("Failed to update contact with identity '{0}'.", contactToUpdate[ContactListConfiguration.I3_Identity.Name]);
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Successfully excluded phone number '{}' from campaign '{}'.", phoneNumber, campaignName);
                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public ExcludePhoneNumberFromAllCampaigns ExcludePhoneNumberFromAllAvailableCampaigns(string phoneNumber)
        {
            var response = new ExcludePhoneNumberFromAllCampaigns();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNull(phoneNumber, "phoneNumber");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Excluding phone number '{}' from all campaigns.", phoneNumber);

                    var availableCampaignsResponse = GetAvailableCampaigns();

                    foreach (var campaign in availableCampaignsResponse.AvailableCampaigns)
                    {
                        var excludePhoneNumberFromCampaignResponse = ExcludePhoneNumberFromCampaign(campaign, phoneNumber);
                        if (!excludePhoneNumberFromCampaignResponse.Success)
                        {
                            response.ErrorMessage += String.Format("Failed to exclude phone number '{0}' from campaign '{1}'.\n\r", phoneNumber, campaign);
                        }
                    }
                    if (String.IsNullOrEmpty(response.ErrorMessage))
                        Trace.EasyIceServicesTopic.note("Successfully excluded phone number '{}' from all available campaigns.", phoneNumber);

                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetCampaignsStatusResponse GetCampaignsStatus()
        {
            var response = new GetCampaignsStatusResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting all campaigns status messages.");

                    response.StatusMessages = GetCampaignMessages(String.Empty);
                    Trace.EasyIceServicesTopic.note("Retrieved {} dialer campaigns statuses.", response.StatusMessages.Count);

                    var availableCampaignsResponse = GetAvailableCampaigns();
                    foreach (var campaign in availableCampaignsResponse.AvailableCampaigns)
                    {
                        response.CampaignValidationResults.Add(GetCampaignValidationResult(campaign));
                        Trace.EasyIceServicesTopic.note("Successfully executed the validation test for campaign '{}'.", campaign);
                        response.CampaignTestResults.Add(GetCampaignTestResult(campaign));
                        Trace.EasyIceServicesTopic.note("Successfully executed the quick test for campaign '{}'.", campaign);
                    }
                    Trace.EasyIceServicesTopic.note("Retrieved {} dialer validation test results.", response.CampaignValidationResults.Count);
                    Trace.EasyIceServicesTopic.note("Retrieved {} dialer quick test results.", response.CampaignValidationResults.Count);

                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        /// <inheritdoc/>
        public GetCampaignStatusResponse GetCampaignStatus(string campaignName)
        {
            var response = new GetCampaignStatusResponse();

            using (Trace.EasyIceServicesTopic.scope())
            {
                try
                {
                    LogClientInformation();

                    #region Validation
                    Guard.ArgumentNotNullOrEmptyString(campaignName, "campaignName");
                    if (!IsConnectedToCIC)
                    {
                        Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
                        response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
                        //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                        return response;
                    }

                    CheckDialerLicense();

                    if (!IsConnectedToDialer)
                    {
                        if (!ConnectToDialer())
                        {
                            Trace.EasyIceServicesTopic.error("Not connected to a Dialer server.");
                            response.ErrorMessage = "Not connected to a Dialer server.";
                            //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
                            return response;
                        }
                    }

                    CheckEasyIceServiceLicense();

                    if (GetCampaignConfiguration(campaignName) == null)
                    {
                        Trace.EasyIceServicesTopic.error("Campaign '{}' does not exist.", campaignName);
                        response.ErrorMessage = String.Format("Campaign '{0}' does not exist.", campaignName);
                        //SetResponseHttpStatus(HttpStatusCode.NotFound, response.ErrorMessage);
                        return response;
                    }
                    #endregion

                    Trace.EasyIceServicesTopic.note("Getting status messages for campaign '{}'.", campaignName);

                    response.StatusMessages = GetCampaignMessages(campaignName);
                    Trace.EasyIceServicesTopic.note("Retrieved {} status messages for campaign '{}'.", response.StatusMessages.Count, campaignName);

                    response.CampaignValidationResult = GetCampaignValidationResult(campaignName);
                    Trace.EasyIceServicesTopic.note("Successfully executed the quick validation for campaign '{}'.", campaignName);

                    response.CampaignTestResult = GetCampaignTestResult(campaignName);
                    Trace.EasyIceServicesTopic.note("Successfully executed the test for campaign '{}'.", campaignName);

                    response.Success = true;
                }
                catch (Exception ex)
                {
                    Trace.EasyIceServicesTopic.exception(ex);
                    response.ErrorMessage = ex.Message;
                    //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
                }
                Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
                return response;
            }
        }
        #endregion

        //#region eFaq
        ///// <inheritdoc/>
        //public QueryEFaqResponse QueryEFaq(string topic, string queryString, string maximumResults, string minimumGrade, string scoreThreshold)
        //{
        //    var response = new QueryEFaqResponse();
        //    EFaqServerList eFaqServerList = null;

        //    using (Trace.EasyIceServicesTopic.scope())
        //    {
        //        try
        //        {
        //            LogClientInformation();

        //            #region Validation
        //            Guard.ArgumentNotNullOrEmptyString(topic, "topic");
        //            Guard.ArgumentNotNullOrEmptyString(queryString, "queryString");
        //            if (String.IsNullOrEmpty(maximumResults))
        //                maximumResults = "20";
        //            if (String.IsNullOrEmpty(minimumGrade))
        //                minimumGrade = "50";
        //            if (String.IsNullOrEmpty(scoreThreshold))
        //                scoreThreshold = "50";

        //            if (!IsConnectedToCIC)
        //            {
        //                Trace.EasyIceServicesTopic.error("Not connected to a CIC server. Last login error message: '{}'.", _loginErrorMessage);
        //                response.ErrorMessage = String.Format("Not connected to a CIC server. Last login error message: '{0}'.", _loginErrorMessage);
        //                //SetResponseHttpStatus(HttpStatusCode.ServiceUnavailable, response.ErrorMessage);
        //                return response;
        //            }
        //            
        //            CheckEasyIceServiceLicense();
        //
        //#endregion

        //            Trace.EasyIceServicesTopic.note("Querying eFAQ with topic '{}', query string '{}', Maximum results '{}', Minimum grade '{}', Score threshold '{}'.", topic, queryString, maximumResults, minimumGrade, scoreThreshold);
        //            eFaqServerList = new EFaqServerList(_interactionsManager);
        //            eFaqServerList.StartWatching();
        //            var eFaqServers = eFaqServerList.GetServers();
        //            if (eFaqServers.Count == 0)
        //                throw new Exception("No eFaq servers found.");

        //            EFaqServer eFaqServerToUse = null;
        //            EFaqTopic eFaqTopicToUse = null;
        //            foreach (var eFaqServer in eFaqServers)
        //            {
        //                foreach (var eFaqTopic in eFaqServer.FaqTopics)
        //                {
        //                    if (eFaqTopic.Name.Equals(topic, StringComparison.InvariantCultureIgnoreCase))
        //                    {
        //                        eFaqServerToUse = eFaqServer;
        //                        eFaqTopicToUse = eFaqTopic;
        //                        break;
        //                    }
        //                }
        //            }
        //            if (eFaqServerToUse == null || eFaqTopicToUse == null)
        //            {
        //                Trace.EasyIceServicesTopic.warning("Failed to find an eFaq server with topic '{}'.", topic);
        //                response.ErrorMessage = String.Format("Failed to find an eFaq server with topic '{0}'.", topic);
        //                SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
        //                return response;
        //            }

        //            var eFaqResults = eFaqServerToUse.Query(queryString, new[] { eFaqTopicToUse }, new EFaqQueryOptions(Convert.ToInt32(maximumResults), Convert.ToDouble(minimumGrade), Convert.ToDouble(scoreThreshold)));
        //            foreach (var eFaqEntry in eFaqResults.Entries)
        //            {
        //                response.QueryResults.Add(new SerializedEFaqQueryResults(eFaqEntry));
        //            }
        //            Trace.EasyIceServicesTopic.note("Successfully queried eFAQ with topic '{}', query string '{}', Maximum results '{}', Minimum grade '{}', Score threshold '{}'. Got '{}' results.", topic, queryString, maximumResults, minimumGrade, scoreThreshold, response.QueryResults.Count);
        //        }
        //        catch (Exception ex)
        //        {
        //            Trace.EasyIceServicesTopic.exception(ex);
        //            response.ErrorMessage = ex.Message;
        //            //SetResponseHttpStatus(HttpStatusCode.InternalServerError, response.ErrorMessage);
        //        }
        //        finally
        //        {
        //            if (eFaqServerList != null && eFaqServerList.IsWatching())
        //                eFaqServerList.StopWatching();
        //        }
        //        Trace.EasyIceServicesTopic.note("Returning '{}'.", response);
        //        return response;
        //    }
        //}
        //#endregion

        #region Dispose
        private bool _disposed;

        /// <summary>
        /// Disposes this service. Do not use unless instructed to do so.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (IsConnectedToDialer)
                    {
                        _dialerConfigurationManager.Dispose();
                        _dialingManager.DialerSession = null;
                        _dialingManager = null;
                    }

                    if (IsConnectedToCIC)
                        Logout();

                    if (_session != null)
                    {
                        _session.Dispose();
                        _session = null;
                    }
                }
                _disposed = true;
            }
        }
        /// <summary>
        /// Disposes this service. Do not use unless instructed to do so.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Destroys this object. Do not use unless instructed to do so.
        /// </summary>
        ~EasyIceServiceImpl()
        {
            Dispose(false);
        }
        #endregion
    }
}
