﻿namespace WCFEasyIceService
{
    internal class EasyIceServiceTopic : TopicTracer
    {
        private readonly int _hdl = I3Trace.initialize_topic("EasyIceService", I3Trace.TraceLevel_Status);

        public override int get_handle()
        {
            return _hdl;
        }
    }
    internal static class Trace
    {
        public static EasyIceServiceTopic EasyIceServicesTopic = new EasyIceServiceTopic();
    }
    internal static class TraceContext
    {
        static TraceContext()
        {
            InteractionId = new ContextAttribute_UInt32("InteractionID", "Interaction ID", "{}");
        }
        public static ContextAttribute_UInt32 InteractionId { get; private set; }
    }
}
