﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using EasyResponses;
using ININ.IceLib.Connection;
using ININ.IceLib.Interactions;
using WCFExtras.Wsdl.Documentation;

// Before going live
//==================
//Obfuscate code ==> Use Crypto Obfuscator
//Re-build documentation
//Re-build Setup
//Send files to Dionne
//Share wordpress.com "About" page on Facebook/Twitter/LinkedIn

//Future Features:
//================
//Handle interaction events
//Check if each functionality is present/licensed before executing a method? 
//Send log method?
//Set server parameter via handler?
//Access structured parameters? http://devjira.i3domain.inin.com/browse/IC-55359
//Do interaction action via handler if interaction is in IVR?
//Add support for websockets (HTML5)? (https://github.com/kerryjiang/SuperWebSocket)
    //How to deserialize JSON in HTML5?
//More options in Add User/Add Workgroup/Add Station
//Check IceLib Tracker namespace ==> Any difference with directories?
//Get more information from directories?
//Reset to default tracing topic levels
//Get Voicemails (see ININ.IceLib.UnifiedMessaging.Voicemails and example in IceLib 4.0 help file under ININ.IceLib.UnifiedMessaging namespace)
//Add attachments to email interactions
//Multiple search parameters for reporting
//Alerts
//Memos
//SetUserStatus with more options
//Add GetLoggedInUsers, GetActivatedUsers, GetNonActivatedUsers, GetWorkgroupNotLoggedInUsers, etc
//Add eFaq (need to install it bridge from 4.0 to eFaq 2.3.1?)

namespace WCFEasyIceService
{
    /// <summary>
    /// Interface for <see cref="EasyIceServiceImpl"/>
    /// </summary>
    [XmlComments]
    [ServiceContract(Namespace = "http://easyiceservice.com")]
    public interface IEasyIceServiceImpl
    {
        #region CIC Login/Logout
        /// <summary>
        /// Occurs when a CIC connection state changes
        /// </summary>
        event EventHandler<ConnectionStateChangedEventArgs> CICConnectionStateChanged;

        /// <summary>
        /// Logs in to CIC.
        /// </summary>
        /// <returns>A <see cref="LoginResponse"/> object.</returns>
        LoginResponse Login();

        /// <summary>
        /// Call this method to logout from CIC.
        /// </summary>
        /// <returns>A <see cref="LogoutResponse"/> object.</returns>
        [OperationContract]
        [Description("Logs out of CIC."), Category("Connection")]
        [WebInvoke(Method = "PUT", UriTemplate = "/logout", ResponseFormat = WebMessageFormat.Json)]
        LogoutResponse Logout();

        #endregion

        #region User Status
        /// <summary>
        /// Gets a user status. If username is empty, gets the current logged in user status.
        /// </summary>
        /// <remarks>For connectivity reasons, the user status is still being watched after returning the status information. For this reason, the information will be returned quicker if another similar request comes in.</remarks>
        /// <param name="username">CIC username</param>
        /// <returns>A <see cref="GetUserStatusResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the status of a CIC user."), Category("UserStatus")]
        [WebGet(UriTemplate = "/User/{username}/Status")]
        GetUserStatusResponse GetUserStatus(string username);

        /// <summary>
        /// Changes a user status. If username is empty, sets the current logged in user status.
        /// </summary>
        /// <param name="username">CIC username</param>
        /// <param name="statusName">Status display name as specified in Interaction Administrator</param>
        /// <returns>A <see cref="SetUserStatusResponse"/> object.</returns>
        [OperationContract]
        [Description("Sets the status of a CIC user."), Category("UserStatus")]
        [WebInvoke(Method = "PUT", UriTemplate = "/User/{username}/Status/{statusName}", ResponseFormat = WebMessageFormat.Json)]
        SetUserStatusResponse SetUserStatus(string username, string statusName);
        #endregion

        #region Server Parameter
        /// <summary>
        /// Gets the value of a CIC server parameter.
        /// </summary>
        /// <param name="serverParameterName">Server parameter name as defined in Interaction Administrator</param>
        /// <returns>A <see cref="GetServerParameterResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the value of a server parameter."), Category("ServerParameters")]
        [WebGet(UriTemplate = "/ServerParameter/{serverparametername}", ResponseFormat = WebMessageFormat.Json)]
        GetServerParameterResponse GetServerParameter(string serverParameterName);
        #endregion

        #region Interactions

        #region Common to All Interaction Types
        /// <summary>
        /// Gets the value of an interaction attribute.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="attributeName">Name of the interaction attribute to retrieve.</param>
        /// <returns>A <see cref="GetInteractionAttributeResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the value of an interaction attribute."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/Attribute/{attributename}", ResponseFormat = WebMessageFormat.Json)]
        GetInteractionAttributeResponse GetInteractionAttribute(string interactionId, string attributeName);

        /// <summary>
        /// Sets the value of an interaction attribute.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="attributeName">Name of the interaction attribute to set.</param>
        /// <param name="setInteractionAttributeData">See <see cref="SetInteractionAttributeData"/></param>
        /// <returns>A <see cref="SetInteractionAttributeResponse"/> object.</returns>
        [OperationContract]
        [Description("Sets the value of an interaction attribute."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Attribute/{attributename}", ResponseFormat = WebMessageFormat.Json)]
        SetInteractionAttributeResponse SetInteractionAttribute(string interactionId, string attributeName, SetInteractionAttributeData setInteractionAttributeData);

        /// <summary>
        /// Sets the value of an interaction attribute.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="attributeName">Name of the interaction attribute to set.</param>
        /// <param name="attributeValue">Value of the interaction attribute.</param>
        /// <returns>A <see cref="SetInteractionAttributeResponse"/> object.</returns>
        [OperationContract]
        [Description("Sets the value of an interaction attribute."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/SetInteractionAttribute/?interactionid={interactionid}&attributename={attributename}&attributevalue={attributevalue}", ResponseFormat = WebMessageFormat.Json)]
        SetInteractionAttributeResponse SetInteractionAttributeExtended(string interactionId, string attributeName, string attributeValue);

        /// <summary>
        /// Transfers an interaction to a specified target.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="target">Phone number or Email address to transfer the interaction to, depending on its type.</param>
        /// <returns>A <see cref="TransferInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Transfers an interaction to a phone number."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Transfer/{target}", ResponseFormat = WebMessageFormat.Json)]
        TransferInteractionResponse TransferInteraction(string interactionId, string target);

        /// <summary>
        /// Transfers an interaction to a workgroup.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="workgroup">Workgroup to transfer the interaction to.</param>
        /// <returns>A <see cref="TransferInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Transfers an interaction to a workgroup."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/TransferToWorkgroup/{workgroup}", ResponseFormat = WebMessageFormat.Json)]
        TransferInteractionResponse TransferInteractionToWorkgroup(string interactionId, string workgroup);

        /// <summary>
        /// Disconnects an interaction, whatever its type (Call, Email, Generic, etc).
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="DisconnectInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Disconnects an interaction, whatever its type (Call, Email, Generic, etc)."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Disconnect", ResponseFormat = WebMessageFormat.Json)]
        DisconnectInteractionResponse DisconnectInteraction(string interactionId);

        /// <summary>
        /// Gets the interaction identifier of an interaction that has a specific attribute name and value in a workgroup. Useful to search for specific interactions with account numbers, ticket ids, etc.
        /// </summary>
        /// <param name="workgroup">Workgroup to use to search for the interaction.</param>
        /// <param name="interactionAttributeName">Name of the interaction attribute to look for.</param>
        /// <param name="interactionAttributeValue">Value of the interaction attribute to look for.</param>
        /// <returns>A <see cref="GetInteractionsWithAttributeValueResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets an interaction with a specific attribute value from a workgroup queue."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/Workgroup/{workgroup}/Attribute/{interactionattributename}/Value{interactionattributevalue}", ResponseFormat = WebMessageFormat.Json)]
        GetInteractionsWithAttributeValueResponse GetInteractionsWithAttributeValue(string workgroup, string interactionAttributeName, string interactionAttributeValue);

        /// <summary>
        /// Checks if an interaction is in a user queue
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="username">CIC user queue to check.</param>
        /// <returns>A <see cref="IsInteractionInUserQueueResponse"/> object.</returns>
        [OperationContract]
        [Description("Checks if an interaction exists in a user queue."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/User/{username}", ResponseFormat = WebMessageFormat.Json)]
        IsInteractionInUserQueueResponse IsInteractionInUserQueue(string interactionId, string username);

        /// <summary>
        /// Checks if an interaction is in a workgroup queue
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="workgroup">CIC workgroup queue to check.</param>
        /// <returns>A <see cref="IsInteractionInWorkgroupQueueResponse"/> object.</returns>
        [OperationContract]
        [Description("Checks if an interaction exists in a workgroup queue."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/Workgroup/{workgroup}", ResponseFormat = WebMessageFormat.Json)]
        IsInteractionInWorkgroupQueueResponse IsInteractionInWorkgroupQueue(string interactionId, string workgroup);
        #endregion

        #region Interaction Actions
        /// <summary>
        /// Appends a note to the interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="note">Note to append to the current interaction notes.</param>
        /// <returns>A <see cref="AppendInteractionNoteResponse"/> object.</returns>
        [OperationContract]
        [Description("Appends a note to the interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/AppendNote/{note}", ResponseFormat = WebMessageFormat.Json)]
        AppendInteractionNoteResponse AppendInteractionNote(string interactionId, string note);

        /// <summary>
        /// Picks up an interaction (pickup done on server side).
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="PickupInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Picks up an interaction (pickup done on server side)."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Pickup", ResponseFormat = WebMessageFormat.Json)]
        PickupInteractionResponse PickupInteraction(string interactionId);

        /// <summary>
        /// Holds an interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="HoldInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Holds an interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Hold", ResponseFormat = WebMessageFormat.Json)]
        HoldInteractionResponse HoldInteraction(string interactionId);

        /// <summary>
        /// Mutes an interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="MuteInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Mutes an interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Mute", ResponseFormat = WebMessageFormat.Json)]
        MuteInteractionResponse MuteInteraction(string interactionId);

        /// <summary>
        /// Unmutes an interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="MuteInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Unmutes an interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Unmute", ResponseFormat = WebMessageFormat.Json)]
        MuteInteractionResponse UnmuteInteraction(string interactionId);

        /// <summary>
        /// Makes an interaction private.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="PrivateInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Makes an interaction private."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Private", ResponseFormat = WebMessageFormat.Json)]
        PrivateInteractionResponse PrivateInteraction(string interactionId);

        /// <summary>
        /// Stops making an interaction private.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="PrivateInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Stops making an interaction private."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Unprivate", ResponseFormat = WebMessageFormat.Json)]
        PrivateInteractionResponse UnprivateInteraction(string interactionId);

        /// <summary>
        /// Parks an interaction on a specified user queue.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="username">Username to park this interaction on.</param>
        /// <returns>A <see cref="ParkInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Parks an interaction on a specified user queue."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Park", ResponseFormat = WebMessageFormat.Json)]
        ParkInteractionResponse ParkInteraction(string interactionId, string username);

        /// <summary>
        /// Records an interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="RecordInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Records an interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Record", ResponseFormat = WebMessageFormat.Json)]
        RecordInteractionResponse RecordInteraction(string interactionId);

        /// <summary>
        /// Stops recording an interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="RecordInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Stops recording an interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/StopRecord", ResponseFormat = WebMessageFormat.Json)]
        RecordInteractionResponse StopRecordInteraction(string interactionId);

        /// <summary>
        /// Pauses an interaction recording.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="PauseInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Pauses an interaction recording."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Pause", ResponseFormat = WebMessageFormat.Json)]
        PauseInteractionResponse PauseInteraction(string interactionId);

        /// <summary>
        /// Resumes an interaction recording.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="ResumeInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Resumes an interaction recording."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Resume", ResponseFormat = WebMessageFormat.Json)]
        ResumeInteractionResponse ResumeInteraction(string interactionId);

        /// <summary>
        /// Secure pauses an interaction recording.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="PauseInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Secure pauses an interaction recording."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/SecurePause", ResponseFormat = WebMessageFormat.Json)]
        PauseInteractionResponse SecurePauseInteraction(string interactionId);

        /// <summary>
        /// Secure resumes an interaction recording.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="PauseInteractionResponse"/> object.</returns>
        [OperationContract]
        [Description("Secure resumes an interaction recording."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/SecureResume", ResponseFormat = WebMessageFormat.Json)]
        PauseInteractionResponse SecureResumeInteraction(string interactionId);

        /// <summary>
        /// Updates deallocation time. This overwrites (as long as the interaction is not disconnected yet) the duration before the interaction attributes can no longer be updated.
        /// Setting the time to 0 will cause the interaction to dispose immediately after being disconnected.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="time">Number of seconds before the interaction gets deallocated.</param>
        /// <returns>A <see cref="UpdateInteractionDeallocationTimeResponse"/> object.</returns>
        [OperationContract]
        [Description("Updates deallocation time. This overwrites (as long as the interaction is not disconnected yet) the duration before the interaction attributes can no longer be updated."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/UpdateDeallocationTime", ResponseFormat = WebMessageFormat.Json)]
        UpdateInteractionDeallocationTimeResponse UpdateInteractionDeallocationTime(string interactionId, string time);

        /// <summary>
        /// Transfers an interaction to voicemail or to another user's voicemail.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="username">(optional) the user's voicemail to send the interaction to. If not set, will send to the current user's voicemail.</param>
        /// <returns>A <see cref="UpdateInteractionDeallocationTimeResponse"/> object.</returns>
        [OperationContract]
        [Description("Transfers an interaction to voicemail or to another user's voicemail."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/Voicemail/{username}", ResponseFormat = WebMessageFormat.Json)]
        TransferInteractionToVoicemailResponse TransferInteractionToVoicemail(string interactionId, string username);
        #endregion

        #region Interaction Properties
        /// <summary>
        /// Gets the interaction duration (in seconds).
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetInteractionDurationResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the interaction duration (in seconds)."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/Duration", ResponseFormat = WebMessageFormat.Json)]
        GetInteractionDurationResponse GetInteractionDuration(string interactionId);

        /// <summary>
        /// Gets the interaction initiation time (in CIC server timezone).
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetInteractionInitiationTimeResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the interaction initiation time (in CIC server timezone)."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/InitiationTime", ResponseFormat = WebMessageFormat.Json)]
        GetInteractionInitiationTimeResponse GetInteractionInitiationTime(string interactionId);

        /// <summary>
        /// Gets the interaction disconnection time (in CIC server timezone), if the interaction is disconnected.
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetInteractionDisconnectionTimeResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the interaction disconnection time (in CIC server timezone), if the interaction is disconnected."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/DisconnectionTime", ResponseFormat = WebMessageFormat.Json)]
        GetInteractionDisconnectionTimeResponse GetInteractionDisconnectionTime(string interactionId);

        /// <summary>
        /// Gets the interaction type (call, fax, email, generic, etc.).
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetInteractionTypeResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the interaction type (call, fax, email, generic, etc.)."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/Type", ResponseFormat = WebMessageFormat.Json)]
        GetInteractionTypeResponse GetInteractionType(string interactionId);

        /// <summary>
        /// Gets the interaction statuses (IsConnected, IsDisconnected, IsRecording, etc.).
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetInteractionStatusesResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the interaction statuses (IsConnected, IsDisconnected, IsRecording, etc.)."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/Statuses", ResponseFormat = WebMessageFormat.Json)]
        GetInteractionStatusesResponse GetInteractionStatuses(string interactionId);

        /// <summary>
        /// Gets the interaction last segment (with the (if available) workgroup, user, wrap up, etc.).
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetInteractionLastSegmentResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the interaction last segment (with the (if available) workgroup, user, wrap up, etc.)."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/LastSegment", ResponseFormat = WebMessageFormat.Json)]
        GetInteractionLastSegmentResponse GetInteractionLastSegment(string interactionId);

        /// <summary>
        /// Gets the account code id associated with an interaction.
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetAccountCodeIdResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the account code id associated with an interaction."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/AccountCodeId", ResponseFormat = WebMessageFormat.Json)]
        GetAccountCodeIdResponse GetAccountCodeId(string interactionId);

        /// <summary>
        /// Sets the account code id associated with an interaction.
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <param name="accountCodeId">Account Code Id value.</param>
        /// <returns>A <see cref="GetAccountCodeIdResponse"/> object.</returns>
        [OperationContract]
        [Description("Sets the account code id associated with an interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/AccountCodeId", ResponseFormat = WebMessageFormat.Json)]
        SetAccountCodeIdResponse SetAccountCodeId(string interactionId, string accountCodeId);

        /// <summary>
        /// Gets the interaction caller's name (if available).
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetRemoteNameResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the interaction caller's name (if available)."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/RemoteName", ResponseFormat = WebMessageFormat.Json)]
        GetRemoteNameResponse GetRemoteName(string interactionId);

        /// <summary>
        /// Sets the interaction's remote name.
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <param name="remoteName">Remote Name value.</param>
        /// <returns>A <see cref="SetRemoteNameResponse"/> object.</returns>
        [OperationContract]
        [Description("Sets the interaction's remote name."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/RemoteName", ResponseFormat = WebMessageFormat.Json)]
        SetRemoteNameResponse SetRemoteName(string interactionId, string remoteName);

        /// <summary>
        /// Gets the interaction wrap up code id (if available).
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetWrapUpCodeIdResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the interaction wrap up code id (if available)."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/WrapUpCodeId", ResponseFormat = WebMessageFormat.Json)]
        GetWrapUpCodeIdResponse GetWrapUpCodeId(string interactionId);

        /// <summary>
        /// Sets the interaction's wrap up code id.
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <param name="wrapUpCodeId">Wrap up code id (list available in Interaction Administrator).</param>
        /// <returns>A <see cref="SetWrapUpCodeIdResponse"/> object.</returns>
        [OperationContract]
        [Description("Sets the interaction's wrap up code id."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/{interactionid}/WrapUpCodeId", ResponseFormat = WebMessageFormat.Json)]
        SetWrapUpCodeIdResponse SetWrapUpCodeId(string interactionId, string wrapUpCodeId);

        /// <summary>
        /// Gets the duration (in seconds) of the interaction as of the time it joined a workgroup queue.
        /// </summary>
        /// <param name="interactionId">Interaction Identifier.</param>
        /// <returns>A <see cref="GetTimeInWorkgroupQueueResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets the duration of the interaction as of the time it joined a workgroup queue."), Category("Interactions")]
        [WebGet(UriTemplate = "/Interaction/{interactionid}/TimeInWorkgroupQueue", ResponseFormat = WebMessageFormat.Json)]
        GetTimeInWorkgroupQueueResponse GetTimeInWorkgroupQueue(string interactionId);
        #endregion

        #region Call Interactions
        /// <summary>
        /// Places a call from a workgroup (optional) with a specific account code id (optional)
        /// </summary>
        /// <param name="createCallInteractionData">See <see cref="CreateCallInteractionData"/></param>
        /// <returns>A <see cref="CreateCallInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new call interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Call", ResponseFormat = WebMessageFormat.Json)]
        CreateCallInteractionResponse CreateCallInteraction(CreateCallInteractionData createCallInteractionData);

        /// <summary>
        /// Places a call from a workgroup (optional) with a specific account code id (optional)
        /// </summary>
        /// <param name="phoneNumber">Phone number to dial.</param>
        /// <param name="fromWorkgroup">(optional) If calling from a workgroup, set this parameter to the workgroup name.</param>
        /// <param name="accountCodeId">(optional) Account code id to assign to this call.</param>
        /// <returns>A <see cref="CreateCallInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new call interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Call/?phonenumber={phonenumber}&fromworkgroup={fromworkgroup}&accountcodeid={accountcodeid}", ResponseFormat = WebMessageFormat.Json)]
        CreateCallInteractionResponse CreateCallInteractionExtended(string phoneNumber, string fromWorkgroup, string accountCodeId);

        /// <summary>
        /// Plays a wav file on the call interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="playWavFileData">See <see cref="PlayWavFileData"/></param>
        /// <returns>A <see cref="PlayWavFileResponse"/> object</returns>
        [OperationContract]
        [Description("Plays a wav file on a call interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/Call/{interactionid}/PlayWavFile", ResponseFormat = WebMessageFormat.Json)]
        PlayWavFileResponse PlayWavFile(string interactionId, PlayWavFileData playWavFileData);

        /// <summary>
        /// Plays a wav file on the call interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="wavFile">If located on the CIC server, must be the full path of the wav file.</param>
        /// <param name="remote">If set to true, file is a remote path.</param>
        /// <param name="enableDigits">If set to true, enables playing of digits to skip forward or backward. If false, will ignore digits.</param>
        /// <returns>A <see cref="PlayWavFileResponse"/> object</returns>
        [OperationContract]
        [Description("Plays a wav file on a call interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/Call/{interactionid}/PlayWavFile/?wavfile={wavfile}&remote={remote}&enabledigits={enabledigits}", ResponseFormat = WebMessageFormat.Json)]
        PlayWavFileResponse PlayWavFileExtended(string interactionId, string wavFile, bool remote, bool enableDigits);

        /// <summary>
        /// Plays digits on the call interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="playDigitsData">See <see cref="PlayDigitsData"/></param>
        /// <returns>A <see cref="PlayDigitsResponse"/> object</returns>
        [OperationContract]
        [Description("Plays digits on a call interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/Call/{interactionid}/PlayDigits", ResponseFormat = WebMessageFormat.Json)]
        PlayDigitsResponse PlayDigits(string interactionId, PlayDigitsData playDigitsData);

        /// <summary>
        /// Plays digits on the call interaction.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <param name="digits">Digits to play on the call interaction.</param>
        /// <returns>A <see cref="PlayDigitsResponse"/> object</returns>
        [OperationContract]
        [Description("Plays digits on a call interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/Call/{interactionid}/PlayDigits/{digits}", ResponseFormat = WebMessageFormat.Json)]
        PlayDigitsResponse PlayDigitsExtended(string interactionId, string digits);

        /// <summary>
        /// Stops all wave audio on this call.
        /// </summary>
        /// <param name="interactionId">Interaction identifier.</param>
        /// <returns>A <see cref="StopWaveAudioResponse"/> object</returns>
        [OperationContract]
        [Description("Stops all wave audio on a call interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/Call/{interactionid}/StopWaveAudio", ResponseFormat = WebMessageFormat.Json)]
        StopWaveAudioResponse StopWaveAudio(string interactionId);
        #endregion

        #region Chat Interactions
        /// <summary>
        /// Creates a new internal chat interaction.
        /// </summary>
        /// <param name="createChatInteractionData">See <see cref="CreateChatInteractionData"/></param>
        /// <returns>A <see cref="CreateChatInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new internal chat interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Chat", ResponseFormat = WebMessageFormat.Json)]
        CreateChatInteractionResponse CreateChatInteraction(CreateChatInteractionData createChatInteractionData);

        /// <summary>
        /// Creates a new internal chat interaction.
        /// </summary>
        /// <param name="username">User to chat with.</param>
        /// <returns>A <see cref="CreateChatInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new internal chat interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Chat/{username}", ResponseFormat = WebMessageFormat.Json)]
        CreateChatInteractionResponse CreateChatInteractionExtended(string username);

        /// <summary>
        /// Writes a message to a chat interaction.
        /// </summary>
        /// <param name="interactionId">Chat interaction identifier (will output an error if the interaction is not a chat interaction).</param>
        /// <param name="writeToChatInteractionData">See <see cref="WriteToChatInteractionData"/></param>
        /// <returns>A <see cref="WriteToChatInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Writes a message to a chat interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/Chat/{interactionid}/WriteMessage", ResponseFormat = WebMessageFormat.Json)]
        WriteToChatInteractionResponse WriteToChatInteraction(string interactionId, WriteToChatInteractionData writeToChatInteractionData);

        /// <summary>
        /// Writes a message to a chat interaction.
        /// </summary>
        /// <param name="interactionId">Chat interaction identifier (will output an error if the interaction is not a chat interaction).</param>
        /// <param name="message">Message to write inside the chat interaction.</param>
        /// <returns>A <see cref="WriteToChatInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Writes a message to a chat interaction."), Category("Interactions")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Interaction/Chat/{interactionid}/?message={message}", ResponseFormat = WebMessageFormat.Json)]
        WriteToChatInteractionResponse WriteToChatInteractionExtended(string interactionId, string message);
        #endregion

        #region Email Interactions
        /// <summary>
        /// Creates an outbound email interaction. Can not be used to create inbound email interactions.
        /// </summary>
        /// <param name="createEmailInteractionData">See <see cref="CreateEmailInteractionData"/></param>
        /// <returns>A <see cref="CreateEmailInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new email interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Email", ResponseFormat = WebMessageFormat.Json)]
        CreateEmailInteractionResponse CreateEmailInteraction(CreateEmailInteractionData createEmailInteractionData);

        /// <summary>
        /// Creates an outbound email interaction. Can not be used to create inbound email interactions.
        /// </summary>
        /// <param name="senderEmailAddress">Sender's email address.</param>
        /// <param name="senderDisplayName">Sender's display name</param>
        /// <param name="subject">Email subject.</param>
        /// <param name="body">Email body.</param>
        /// <param name="fromWorkgroup">(optional) If sending the email from a workgroup, the workgroup name.</param>
        /// <returns>A <see cref="CreateEmailInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new email interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Email/?senderemailaddress={senderemailaddress}&senderdisplayname={senderdisplayname}&subject={subject}&body={body}&fromworkgroup={fromworkgroup}", ResponseFormat = WebMessageFormat.Json)]
        CreateEmailInteractionResponse CreateEmailInteractionExtended(string senderEmailAddress, string senderDisplayName, string subject, string body, string fromWorkgroup);
        #endregion

        #region Fax Interactions
        /// <summary>
        /// Sends a fax.
        /// </summary>
        /// <returns>A <see cref="CreateEmailInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Sends a fax."), Category("Faxes")]
        [WebInvoke(Method = "POST", UriTemplate = "/Fax", ResponseFormat = WebMessageFormat.Json)]
        SendFaxResponse SendFax(SendFaxData sendFaxData);

        /// <summary>
        /// Sends a fax.
        /// </summary>
        /// <param name="faxNumber">Number to send the fax to.</param>
        /// <param name="image">Full path to the TIF image to send.</param>
        /// <param name="recipientName">(optional) Name of the person you are sending this fax to.</param>
        /// <param name="company">(optional) Company name you are sending this fax to.</param>
        /// <param name="phoneNumber">(optional) Phone number used to contact the person you are sending this fax to.</param>
        /// <param name="accountCodeId">(optional) Account code id to assign to this fax.</param>
        /// <param name="emailSuccess">Will send a notification email to this address if the fax is successfully sent.</param>
        /// <param name="emailFailure">Will send a notification email to this address if the fax send request failed.</param>
        /// <param name="faxPageSize">"A4", "Letter" (default) or "Legal"</param>
        /// <returns>A <see cref="CreateEmailInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Sends a fax."), Category("Faxes")]
        [WebInvoke(Method = "POST", UriTemplate = "/Fax/?faxnumber={faxnumber}&image={image}&recipientname={recipientname}&company={company}&phonenumber={phonenumber}&accountcodeid={accountcodeid}&emailsuccess={emailsuccess}&emailfailure={emailfailure}&faxpagesize={faxpagesize}", ResponseFormat = WebMessageFormat.Json)]
        SendFaxResponse SendFaxExtended(string faxNumber, string image, string recipientName, string company, string phoneNumber, string accountCodeId, string emailSuccess, string emailFailure, string faxPageSize);
        #endregion

        #region Generic Interactions
        /// <summary>
        /// Creates a generic interaction
        /// </summary>
        /// <param name="createGenericInteractionData">See <see cref="CreateGenericInteractionData"/></param>
        /// <returns>A <see cref="CreateGenericInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new generic interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Generic", ResponseFormat = WebMessageFormat.Json)]
        CreateGenericInteractionResponse CreateGenericInteraction(CreateGenericInteractionData createGenericInteractionData);

        /// <summary>
        /// Creates a generic interaction
        /// </summary>
        /// <param name="remoteName">Remote name (will be displayed in the Name field of the Interaction Client)</param>
        /// <param name="remoteId">Remote identifier (will be displayed in the Phone Number field of the Interaction Client)</param>
        /// <param name="workgroup">Workgroup in which the generic interaction will be created.</param>
        /// <param name="skillName">(optional) ACD skill to assign to the interaction.</param>
        /// <param name="interactionAttributeName">(optional) Interaction attribute name to set when the generic interaction is created.</param>
        /// <param name="interactionAttributeValue">(optional) Interaction attribute value to assign to the interaction attribute name.</param>
        /// <returns>A <see cref="CreateGenericInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new generic interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Generic/?remotename={remotename}&remoteid={remoteid}&workgroup={workgroup}&skillname={skillname}&interactionattributename={interactionattributename}&interactionattributevalue={interactionattributevalue}", ResponseFormat = WebMessageFormat.Json)]
        CreateGenericInteractionResponse CreateGenericInteractionExtended(string remoteName, string remoteId, string workgroup, string skillName, string interactionAttributeName, string interactionAttributeValue);
        #endregion

        #region Callback Interactions
        /// <summary>
        /// Creates a callback interaction
        /// </summary>
        /// <returns>A <see cref="CreateCallbackInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new callback interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Callback", ResponseFormat = WebMessageFormat.Json)]
        CreateCallbackInteractionResponse CreateCallbackInteraction(CreateCallbackInteractionData createCallbackInteractionData);

        /// <summary>
        /// Creates a callback interaction
        /// </summary>
        /// <param name="phoneNumber">Phone number to dial when the agent processes the callback.</param>
        /// <param name="name">Name which will be displayed in the Name field of the Interaction Client.</param>
        /// <param name="message">(optional) Message to insert in the callback form.</param>
        /// <param name="workgroup">Workgroup in which the callback interaction will be queued.</param>
        /// <param name="skillName">(optional) ACD skill to assign to the interaction.</param>
        /// <param name="acdPriority">(optional) ACD priority (if equals to 0 or -1, will be set to 50).</param>
        /// <param name="notes">(optional) Interaction notes.</param>
        /// <returns>A <see cref="CreateCallbackInteractionResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a new callback interaction."), Category("Interactions")]
        [WebInvoke(Method = "POST", UriTemplate = "/Interaction/Callback/?phonenumber={phonenumber}&name={name}&message={message}&workgroup={workgroup}&skillname={skillname}&acdpriority={acdpriority}&notes={notes}", ResponseFormat = WebMessageFormat.Json)]
        CreateCallbackInteractionResponse CreateCallbackInteractionExtended(string phoneNumber, string name, string message, string workgroup, string skillName, int acdPriority, string notes);
        #endregion

        #endregion

        #region Configuration

        #region Users
        /// <summary>
        /// Gets a CIC user configuration.
        /// </summary>
        /// <param name="username">CIC Username.</param>
        /// <returns>A <see cref="GetUserResponse"/> object</returns>
        [OperationContract]
        [Description("Gets a CIC user configuration."), Category("Users")]
        [WebGet(UriTemplate = "/User/{username}", ResponseFormat = WebMessageFormat.Json)]
        GetUserResponse GetUser(string username);

        /// <summary>
        /// Gets interactions from a CIC user queue.
        /// </summary>
        /// <param name="username">CIC username.</param>
        /// <returns>A <see cref="GetUserInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Gets interactions from a CIC user queue."), Category("Users")]
        [WebGet(UriTemplate = "/User/{username}/Interactions", ResponseFormat = WebMessageFormat.Json)]
        GetUserInteractionsResponse GetUserInteractions(string username);

        /// <summary>
        /// Adds a new user in CIC.
        /// </summary>
        /// <param name="username">CIC username.</param>
        /// <param name="createUserData"><see cref="CreateUserData"/></param>
        /// <returns>A <see cref="CreateUserResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a CIC user."), Category("Users")]
        [WebInvoke(Method = "POST", UriTemplate = "/User/{username}", ResponseFormat = WebMessageFormat.Json)]
        CreateUserResponse CreateUser(string username, CreateUserData createUserData);

        /// <summary>
        /// Adds a new user in CIC.
        /// </summary>
        /// <param name="username">CIC username.</param>
        /// <param name="password">CIC password.</param>
        /// <param name="extension">User extension.</param>
        /// <returns>A <see cref="CreateUserResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a CIC user."), Category("Users")]
        [WebInvoke(Method = "POST", UriTemplate = "/User/?username={username}&password={password}&extension={extension}", ResponseFormat = WebMessageFormat.Json)]
        CreateUserResponse CreateUserExtended(string username, string password, string extension);

        /// <summary>
        /// Deletes a user from CIC.
        /// </summary>
        /// <param name="username">CIC user name.</param>
        /// <returns>A <see cref="DeleteUserResponse"/> object</returns>
        [OperationContract]
        [Description("Deletes a CIC user."), Category("Users")]
        [WebInvoke(Method = "DELETE", UriTemplate = "/User/{username}", ResponseFormat = WebMessageFormat.Json)]
        DeleteUserResponse DeleteUser(string username);

        /// <summary>
        /// Retrieves the list of users from a particular workgroup.
        /// </summary>
        /// <param name="workgroup">CIC workgroup.</param>
        /// <returns>A <see cref="GetUsersFromWorkgroupResponse"/> object</returns>
        [OperationContract]
        [Description("Gets all CIC users from a workgroup."), Category("Users")]
        [WebGet(UriTemplate = "/User/Workgroup/{workgroup}", ResponseFormat = WebMessageFormat.Json)]
        GetUsersFromWorkgroupResponse GetUsersFromWorkgroup(string workgroup);

        /// <summary>
        /// Retrieves the list of active users from CIC.
        /// </summary>
        /// <returns>A <see cref="GetUsersResponse"/> object</returns>
        [OperationContract]
        [Description("Gets all active users."), Category("Workgroups")]
        [WebGet(UriTemplate = "/Users", ResponseFormat = WebMessageFormat.Json)]
        GetUsersResponse GetUsers();

        #endregion

        #region Workgroups
        /// <summary>
        /// Gets a CIC workgroup configuration.
        /// </summary>
        /// <param name="workgroupName">CIC Workgroup name.</param>
        /// <returns>A <see cref="GetWorkgroupResponse"/> object</returns>
        [OperationContract]
        [Description("Gets a CIC workgroup configuration."), Category("Workgroups")]
        [WebGet(UriTemplate = "/Workgroup/{workgroupName}", ResponseFormat = WebMessageFormat.Json)]
        GetWorkgroupResponse GetWorkgroup(string workgroupName);

        /// <summary>
        /// Gets interactions from a CIC workgroup queue.
        /// </summary>
        /// <param name="workgroupName">CIC Workgroup name.</param>
        /// <returns>A <see cref="GetWorkgroupInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Gets interactions from a CIC workgroup queue."), Category("Workgroups")]
        [WebGet(UriTemplate = "/Workgroup/{workgroupName}/Interactions", ResponseFormat = WebMessageFormat.Json)]
        GetWorkgroupInteractionsResponse GetWorkgroupInteractions(string workgroupName);

        /// <summary>
        /// Adds a new workgroup in CIC.
        /// </summary>
        /// <param name="workgroupName">CIC workgroup name.</param>
        /// <param name="createWorkgroupData">See <see cref="CreateWorkgroupData"/></param>
        /// <returns>A <see cref="CreateWorkgroupResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a CIC workgroup."), Category("Workgroups")]
        [WebInvoke(Method = "POST", UriTemplate = "/Workgroup/{workgroupName}", ResponseFormat = WebMessageFormat.Json)]
        CreateWorkgroupResponse CreateWorkgroup(string workgroupName, CreateWorkgroupData createWorkgroupData);

        /// <summary>
        /// Adds a new workgroup in CIC.
        /// </summary>
        /// <param name="workgroupName">CIC workgroup name.</param>
        /// <param name="extension">the workgroup's extension.</param>
        /// <returns>A <see cref="CreateWorkgroupResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a CIC workgroup."), Category("Workgroups")]
        [WebInvoke(Method = "POST", UriTemplate = "/Workgroup/?workgroupname={workgroupname}&extension={extension}", ResponseFormat = WebMessageFormat.Json)]
        CreateWorkgroupResponse CreateWorkgroupExtended(string workgroupName, string extension);

        /// <summary>
        /// Deletes a CIC workgroup.
        /// </summary>
        /// <param name="workgroupName">CIC workgroup name.</param>
        /// <returns>A <see cref="DeleteWorkgroupResponse"/> object</returns>
        [OperationContract]
        [Description("Deletes a CIC workgroup."), Category("Workgroups")]
        [WebInvoke(Method = "DELETE", UriTemplate = "/Workgroup/{workgroupName}", ResponseFormat = WebMessageFormat.Json)]
        DeleteWorkgroupResponse DeleteWorkgroup(string workgroupName);

        /// <summary>
        /// Retrieves the list of workgroups from CIC.
        /// </summary>
        /// <returns>A <see cref="GetWorkgroupsResponse"/> object</returns>
        [OperationContract]
        [Description("Gets all workgroups."), Category("Workgroups")]
        [WebGet(UriTemplate = "/Workgroups", ResponseFormat = WebMessageFormat.Json)]
        GetWorkgroupsResponse GetWorkgroups();
        #endregion

        #region Stations
        /// <summary>
        /// Gets a CIC station configuration.
        /// </summary>
        /// <param name="stationName">CIC station name.</param>
        /// <returns>A <see cref="GetStationResponse"/> object</returns>
        [OperationContract]
        [Description("Gets a CIC station configuration."), Category("Stations")]
        [WebGet(UriTemplate = "/Station/{stationname}", ResponseFormat = WebMessageFormat.Json)]
        GetStationResponse GetStation(string stationName);

        /// <summary>
        /// Gets interactions from a CIC station queue.
        /// </summary>
        /// <param name="stationName">CIC station.</param>
        /// <returns>A <see cref="GetStationInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Gets interactions from a CIC station queue."), Category("Stations")]
        [WebGet(UriTemplate = "/Station/{stationName}/Interactions", ResponseFormat = WebMessageFormat.Json)]
        GetStationInteractionsResponse GetStationInteractions(string stationName);

        /// <summary>
        /// Adds a new workstation in CIC.
        /// </summary>
        /// <param name="stationName">CIC workgroup name.</param>
        /// <param name="createStationData">See <see cref="CreateStationData"/></param>
        /// <returns>A <see cref="CreateStationResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a CIC station."), Category("Stations")]
        [WebInvoke(Method = "POST", UriTemplate = "/Station/{stationname}", ResponseFormat = WebMessageFormat.Json)]
        CreateStationResponse CreateStation(string stationName, CreateStationData createStationData);

        /// <summary>
        /// Adds a new workstation in CIC.
        /// </summary>
        /// <param name="stationName">CIC station name.</param>
        /// <param name="extension">The station's extension</param>
        /// <param name="userPortion">User portion of the station identification address. Usually the same as the station's extension.</param>
        /// <param name="hostPortion">(optional) Host portion of the station identification address. If empty, no host will be specified.</param>
        /// <param name="portPortion">(optional) Port portion of the station identification address. If empty, no port will be specified.</param>
        /// <returns>A <see cref="CreateStationResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a CIC station."), Category("Stations")]
        [WebInvoke(Method = "POST", UriTemplate = "/Station/?stationname={stationname}&extension={extension}&userportion={userportion}&hostportion={hostportion}&portportion={portportion}", ResponseFormat = WebMessageFormat.Json)]
        CreateStationResponse CreateStationExtended(string stationName, string extension, string userPortion, string hostPortion, string portPortion);

        /// <summary>
        /// Deletes a station from CIC.
        /// </summary>
        /// <param name="stationName">CIC Station name.</param>
        /// <returns>A <see cref="DeleteStationResponse"/> object</returns>
        [OperationContract]
        [Description("Deletes a CIC station."), Category("Stations")]
        [WebInvoke(Method = "DELETE", UriTemplate = "/Station/{stationname}", ResponseFormat = WebMessageFormat.Json)]
        DeleteStationResponse DeleteStation(string stationName);

        /// <summary>
        /// Retrieves the list of stations from CIC.
        /// </summary>
        /// <returns>A <see cref="GetStationsResponse"/> object</returns>
        [OperationContract]
        [Description("Gets all stations."), Category("Stations")]
        [WebGet(UriTemplate = "/Stations", ResponseFormat = WebMessageFormat.Json)]
        GetStationsResponse GetStations();

        #endregion

        #region Lines
        /// <summary>
        /// Gets a CIC line configuration.
        /// </summary>
        /// <param name="lineName">CIC Line name.</param>
        /// <returns>A <see cref="GetLineResponse"/> object</returns>
        [OperationContract]
        [Description("Gets a CIC line configuration."), Category("Lines")]
        [WebGet(UriTemplate = "/Line/{lineName}", ResponseFormat = WebMessageFormat.Json)]
        GetLineResponse GetLine(string lineName);

        /// <summary>
        /// Adds a new line in CIC.
        /// </summary>
        /// <param name="lineName">CIC line name.</param>
        /// <param name="createLineData">See <see cref="CreateLineData"/></param>
        /// <returns>A <see cref="CreateLineResponse"/> object</returns>
        [OperationContract]
        [Description("Creates a CIC line."), Category("Lines")]
        [WebInvoke(Method = "POST", UriTemplate = "/Line/{lineName}", ResponseFormat = WebMessageFormat.Json)]
        CreateLineResponse CreateLine(string lineName, CreateLineData createLineData);
        #endregion

        #endregion

        #region Recordings
        /// <summary>
        /// Extracts and decrypts (if encrypted) a recording based on its recording identifier.
        /// </summary>
        /// <param name="recordingid">Recording Identifier.</param>
        /// <returns>A <see cref="DownloadRecordingResponse"/> object.</returns>
        [OperationContract]
        [Description("Extracts and decrypts (if encrypted) a recording based on its recording identifier."), Category("Recordings")]
        [WebInvoke(Method = "GET", UriTemplate = "/Recording/RecordingId/{recordingid}", ResponseFormat = WebMessageFormat.Json)]
        DownloadRecordingResponse DownloadRecordingByRecordingId(string recordingid);

        /// <summary>
        /// Extracts and decrypts (if encrypted) a recording based on the interaction identifier. Requires CIC 4.0 SU4+.
        /// </summary>
        /// <param name="interactionid">Interaction identifier.</param>
        /// <returns>A <see cref="DownloadRecordingResponse"/> object.</returns>
        [OperationContract]
        [Description("Extracts and decrypts (if encrypted) a recording based on the interaction identifier. Note that this requires CIC 4.0 SU4+."), Category("Recordings")]
        [WebInvoke(Method = "GET", UriTemplate = "/Recording/InteractionId/{interactionid}", ResponseFormat = WebMessageFormat.Json)]
        DownloadRecordingResponse DownloadRecordingByInteractionId(string interactionid);

        /// <summary>
        /// Extracts and decrypts (if encrypted) a recording based on the interaction id key.
        /// </summary>
        /// <param name="interactionidkey">Interaction id key.</param>
        /// <returns>A <see cref="DownloadRecordingResponse"/> object.</returns>
        [OperationContract]
        [Description("Extracts and decrypts (if encrypted) a recording based on the interaction id key."), Category("Recordings")]
        [WebInvoke(Method = "GET", UriTemplate = "/Recording/InteractionIdKey/{interactionidkey}", ResponseFormat = WebMessageFormat.Json)]
        DownloadRecordingResponse DownloadRecordingByInteractionIdKey(string interactionidkey);
        #endregion

        #region Custom Notification
        /// <summary>
        /// Sends a custom notification.
        /// </summary>
        /// <param name="objectId">Object Identifier.</param>
        /// <param name="eventId">Event Identifier.</param>
        /// <param name="customData">(optional) Pipe (|) separated list of custom string data.</param>
        /// <returns>A <see cref="SendCustomNotificationResponse"/> object</returns>
        [OperationContract]
        [Description("Sends a custom notification."), Category("CustomNotifications")]
        [WebInvoke(Method = "POST", UriTemplate = "/CustomNotification/?objectid={objectid}&eventid={eventid}&customdata={customdata}", ResponseFormat = WebMessageFormat.Json)]
        SendCustomNotificationResponse SendCustomNotification(string objectId, string eventId, string customData);
        #endregion

        #region Statistics
        /// <summary>
        /// Gets all statistics ids from CIC's statistic catalog.
        /// </summary>
        /// <remarks>
        /// For more information about statistics, please refer to the CIC help files.
        /// </remarks>
        /// <returns>A <see cref="GetAllStatisticsResponse"/> object</returns>
        [OperationContract]
        [Description("Gets all available statistic names from CIC' statistics catalog. For more information about statistics, refer to the CIC help files."), Category("Statistics")]
        [WebGet(UriTemplate = "/Statistics", ResponseFormat = WebMessageFormat.Json)]
        GetAllStatisticsResponse GetAllStatistics();

        /// <summary>
        /// Gets a statistic.
        /// </summary>
        /// <param name="statisticId">The statistic identifier to retrieve.</param>
        /// <param name="parameters">Statistic parameters (separated by a '|' (pipe))</param>
        /// <param name="parameterTypeIds">Statistic parameter type ids (separated by a '|' (pipe))</param>
        /// <returns>A <see cref="GetStatisticResponse"/> object</returns>
        [OperationContract]
        [Description("Gets the value of a statistic."), Category("Statistics")]
        [WebGet(UriTemplate = "/Statistic/{statisticid}/{parameters}/{parametertypeids}", ResponseFormat = WebMessageFormat.Json)]
        GetStatisticResponse GetStatistic(string statisticId, string parameters, string parameterTypeIds);
        #endregion

        #region Reporting
        /// <summary>
        /// Searches for interactions based on their ANI (caller's number). Requires CIC 4.0 SU4+.
        /// </summary>
        /// <param name="ani">The caller's number (ANI, Automatic Number Identification).</param>
        /// <returns>A <see cref="SearchInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Searches for interactions by ANI (Caller's number). Requires CIC 4.0 SU4+."), Category("Reporting")]
        [WebGet(UriTemplate = "/Interactions/ANI/{ani}", ResponseFormat = WebMessageFormat.Json)]
        SearchInteractionsResponse SearchInteractionsByAni(string ani);

        /// <summary>
        /// Searches for interactions based on a date time range. Requires CIC 4.0 SU4+.
        /// </summary>
        /// <param name="minimumValue">Start of the date time range.</param>
        /// <param name="maximumValue">End of the date time range.</param>
        /// <returns>A <see cref="SearchInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Searches for interactions by date/time range. Requires CIC 4.0 SU4+."), Category("Reporting")]
        [WebGet(UriTemplate = "/Interactions/DateTimeRange/?minimumvalue={minimumvalue}&maximumvalue={maximumvalue}", ResponseFormat = WebMessageFormat.Json)]
        SearchInteractionsResponse SearchInteractionsByDateTimeRange(DateTime minimumValue, DateTime maximumValue);

        /// <summary>
        /// Searches for interactions based on their DNIS (number dialed). Requires CIC 4.0 SU4+.
        /// </summary>
        /// <param name="dnis">The number that was dialled (DNIS, Dialed Number Identification Service).</param>
        /// <returns>A <see cref="SearchInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Searches for interactions by DNIS (Number dialed by the caller). Requires CIC 4.0 SU4+."), Category("Reporting")]
        [WebGet(UriTemplate = "/Interactions/DNIS/{dnis}", ResponseFormat = WebMessageFormat.Json)]
        SearchInteractionsResponse SearchInteractionsByDnis(string dnis);

        /// <summary>
        /// Searches for interactions based on an interaction id. Requires CIC 4.0 SU4+.
        /// </summary>
        /// <param name="interactionId">The Interaction Id (can match multiple interactions but interaction id key is unique).</param>
        /// <returns>A <see cref="SearchInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Searches for an interaction by its interaction identifier. Requires CIC 4.0 SU4+."), Category("Reporting")]
        [WebGet(UriTemplate = "/Interactions/InteractionId/{interactionid}", ResponseFormat = WebMessageFormat.Json)]
        SearchInteractionsResponse SearchInteractionsByInteractionId(string interactionId);

        /// <summary>
        /// Searches for interactions based on the last IC username that was involved in the interaction. Requires CIC 4.0 SU4+.
        /// </summary>
        /// <param name="lastICUsername">IC username of the last agent that was involved in the interaction.</param>
        /// <returns>A <see cref="SearchInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Searches for interactions by the last user that was involved in the interaction. Requires CIC 4.0 SU4+."), Category("Reporting")]
        [WebGet(UriTemplate = "/Interactions/LastICUsername/{lasticusername}", ResponseFormat = WebMessageFormat.Json)]
        SearchInteractionsResponse SearchInteractionsByLastICUsername(string lastICUsername);

        /// <summary>
        /// Searches for interactions based on the last workgroup that was involved in the interaction. Requires CIC 4.0 SU4+.
        /// </summary>
        /// <param name="lastWorkgroup">Workgroup name.</param>
        /// <returns>A <see cref="SearchInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Searches for interactions by the last workgroup the interaction was on before it disconnected. Requires CIC 4.0 SU4+."), Category("Reporting")]
        [WebGet(UriTemplate = "/Interactions/LastWorkgroup/{lastworkgroup}", ResponseFormat = WebMessageFormat.Json)]
        SearchInteractionsResponse SearchInteractionsByLastWorkgroup(string lastWorkgroup);

        /// <summary>
        /// Searches for interactions based on the interaction media type. Requires CIC 4.0 SU4+.
        /// </summary>
        /// <param name="mediaType">"Call", "Callback", "Chat", "Email", "Fax", "Generic", "InstantQuestion", "Invalid", "Sms", "WebCollaboration", "WorkflowItemObject".</param>
        /// <returns>A <see cref="SearchInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Searches for interactions by media type. Requires CIC 4.0 SU4+."), Category("Reporting")]
        [WebGet(UriTemplate = "/Interactions/MediaType/{mediatype}", ResponseFormat = WebMessageFormat.Json)]
        SearchInteractionsResponse SearchInteractionsByMediaType(string mediaType);

        /// <summary>
        /// Searches for interactions based on the site identifier. Requires CIC 4.0 SU4+.
        /// </summary>
        /// <param name="siteId">Site Identifier (0 to 999).</param>
        /// <returns>A <see cref="SearchInteractionsResponse"/> object</returns>
        [OperationContract]
        [Description("Searches for interactions by site id. Requires CIC 4.0 SU4+."), Category("Reporting")]
        [WebGet(UriTemplate = "/Interactions/SiteId/{siteid}", ResponseFormat = WebMessageFormat.Json)]
        SearchInteractionsResponse SearchInteractionsBySiteId(string siteId);
        #endregion

        #region Directories
        /// <summary>
        /// Retrieves a user phone numbers (Business phone, business phone #2, home phone and mobile phone) from a directory.
        /// </summary>
        /// <param name="directoryName">Name of the directory to search (i.e. "Company Directory", "IC Private Contacts", "Workgroups and Profiles", etc.).</param>
        /// <param name="username">CIC Username or user display name.</param>
        /// <returns>A <see cref="GetUserDirectoryPhoneNumbersResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets a user phone numbers."), Category("Directories")]
        [WebGet(UriTemplate = "/User/{username}/Directory/{directoryname}/PhoneNumbers", ResponseFormat = WebMessageFormat.Json)]
        GetUserDirectoryPhoneNumbersResponse GetUserDirectoryPhoneNumbers(string directoryName, string username);
        #endregion

        #region IPA
        /// <summary>
        /// Gets all launchable IPA processes. You can use <see cref="StartIPAProcess"/> to start a process by name.
        /// </summary>
        /// <returns>A <see cref="GetLaunchableIPAProcessesResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets all launchable IPA processes."), Category("IPA")]
        [WebGet(UriTemplate = "/IPA", ResponseFormat = WebMessageFormat.Json)]
        GetLaunchableIPAProcessesResponse GetLaunchableIPAProcesses();

        /// <summary>
        /// Starts an IPA process.
        /// </summary>
        /// <param name="processName">IPA process name as configured in Interaction Administrator.</param>
        /// <returns>A <see cref="StartIPAProcessResponse"/> object.</returns>
        [OperationContract]
        [Description("Starts an IPA process."), Category("IPA")]
        [WebInvoke(Method = "PUT", UriTemplate = "/IPA/Process/{processname}", ResponseFormat = WebMessageFormat.Json)]
        StartIPAProcessResponse StartIPAProcess(string processName);

        /// <summary>
        /// Cancels an IPA process.
        /// </summary>
        /// <param name="processId">Process instance identifier.</param>
        /// <param name="cancelReason">String to indicate why the process is being cancelled.</param>
        /// <returns>A <see cref="CancelIPAProcessResponse"/> object.</returns>
        [OperationContract]
        [Description("Cancels an IPA process."), Category("IPA")]
        [WebInvoke(Method = "DELETE", UriTemplate = "/IPA/Process/{processid}/CancelReason/{cancelreason}", ResponseFormat = WebMessageFormat.Json)]
        CancelIPAProcessResponse CancelIPAProcess(string processId, string cancelReason);
        #endregion

        #region Dialer
        /// <summary>
        /// Gets all available campaigns for the CIC user connected in the Easy Ice Service.
        /// </summary>
        /// <returns>A <see cref="GetAvailableCampaignsResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets all available campaigns for the CIC user connected in the Easy Ice Service."), Category("Dialer")]
        [WebGet(UriTemplate = "/Dialer/Campaigns", ResponseFormat = WebMessageFormat.Json)]
        GetAvailableCampaignsResponse GetAvailableCampaigns();

        /// <summary>
        /// Gets all contacts for a specific Dialer campaign.
        /// </summary>
        /// <remarks>Can take a while depending on the number of contacts.</remarks>
        /// <returns>A <see cref="GetContactsResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets all contacts for a specific Dialer campaign. Can take a while depending on the number of contacts."), Category("Dialer")]
        [WebGet(UriTemplate = "/Dialer/Campaign/{campaignname}/Contacts", ResponseFormat = WebMessageFormat.Json)]
        GetContactsResponse GetContacts(string campaignName);

        /// <summary>
        /// Inserts a new contact to a specific campaign.
        /// </summary>
        /// <remarks>Can take a while depending on the number of contacts.</remarks>
        /// <returns>A <see cref="GetContactsResponse"/> object.</returns>
        [OperationContract]
        [Description("Inserts a new contact to a specific campaign."), Category("Dialer")]
        [WebInvoke(Method = "POST", UriTemplate = "/Dialer/Campaign/Contact", ResponseFormat = WebMessageFormat.Json)]
        InsertContactResponse InsertContact(InsertContactData insertContactData);

        /// <summary>
        /// Recycles the contact list of a Dialer campaign.
        /// </summary>
        /// <returns>A <see cref="RecycleContactListResponse"/> object.</returns>
        [OperationContract]
        [Description("Recycles the contact list of a Dialer campaign."), Category("Dialer")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Dialer/Campaign/{campaignname}/Recycle", ResponseFormat = WebMessageFormat.Json)]
        RecycleContactListResponse RecycleContactList(string campaignName);

        /// <summary>
        /// Resets a campaign by recycling the contact list, resetting the recycle count to 0 and picking up any saved configuration changes since the last time the campaign started.
        /// </summary>
        /// <returns>A <see cref="ResetCampaignResponse"/> object.</returns>
        [OperationContract]
        [Description("Resets a campaign by recycling the contact list, resetting the recycle count to 0 and picking up any saved configuration changes since the last time the campaign started."), Category("Dialer")]
        [WebInvoke(Method = "PUT", UriTemplate = "/Dialer/Campaign/{campaignname}/Reset", ResponseFormat = WebMessageFormat.Json)]
        ResetCampaignResponse ResetCampaign(string campaignName);

        /// <summary>
        /// Gets active and eligible agents for a Dialer campaign.
        /// </summary>
        /// <returns>A <see cref="GetCampaignAgentsResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets active and eligible agents for a Dialer campaign."), Category("Dialer")]
        [WebGet(UriTemplate = "/Dialer/Campaign/{campaignname}/Agents", ResponseFormat = WebMessageFormat.Json)]
        GetCampaignAgentsResponse GetCampaignAgents(string campaignName);

        /// <summary>
        /// Gets active and eligible campaigns for a Dialer agent.
        /// </summary>
        /// <returns>A <see cref="GetAgentCampaignsResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets active and eligible campaigns for a Dialer agent."), Category("Dialer")]
        [WebGet(UriTemplate = "/Dialer/Agent/{agentname}/Campaigns", ResponseFormat = WebMessageFormat.Json)]
        GetAgentCampaignsResponse GetAgentCampaigns(string agentName);

        /// <summary>
        /// Excludes a phone number from a campaign.
        /// </summary>
        /// <returns>A <see cref="ExcludePhoneNumberFromCampaignResponse"/> object.</returns>
        [OperationContract]
        [Description("Excludes a phone number from a campaign."), Category("Dialer")]
        [WebInvoke(Method = "DELETE", UriTemplate = "/Dialer/Campaign/{campaignName}/PhoneNumber/{phonenumber}", ResponseFormat = WebMessageFormat.Json)]
        ExcludePhoneNumberFromCampaignResponse ExcludePhoneNumberFromCampaign(string campaignName, string phoneNumber);

        /// <summary>
        /// Excludes a phone number from all available campaigns.
        /// </summary>
        /// <returns>A <see cref="ExcludePhoneNumberFromAllCampaigns"/> object.</returns>
        [OperationContract]
        [Description("Excludes a phone number from all available campaigns."), Category("Dialer")]
        [WebInvoke(Method = "DELETE", UriTemplate = "/Dialer/PhoneNumber/{phonenumber}", ResponseFormat = WebMessageFormat.Json)]
        ExcludePhoneNumberFromAllCampaigns ExcludePhoneNumberFromAllAvailableCampaigns(string phoneNumber);

        /// <summary>
        /// Gets all campaigns status messages, performs a campaign validation test and retrieves several records for all available campaigns. Useful for troubleshooting campaign-related issues.
        /// </summary>
        /// <remarks>
        /// WARNING: Can be resource intensive if a lot of campaigns are being tested.
        /// </remarks>
        /// <returns>A <see cref="GetCampaignsStatusResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets all campaigns status messages, performs a campaign validation test and retrieves several records for all available campaigns. Useful for troubleshooting campaign-related issues. WARNING: Can be resource intensive."), Category("Dialer")]
        [WebGet(UriTemplate = "/Dialer/Campaigns/Status", ResponseFormat = WebMessageFormat.Json)]
        GetCampaignsStatusResponse GetCampaignsStatus();

        /// <summary>
        /// Gets status messages, performs a campaign validation test and retrieves several records for a specific campaign.
        /// </summary>
        /// <returns>A <see cref="GetCampaignStatusResponse"/> object.</returns>
        [OperationContract]
        [Description("Gets status messages, performs a campaign validation test and retrieves several records for a specific campaign."), Category("Dialer")]
        [WebGet(UriTemplate = "/Dialer/Campaign/{campaignname}/Status", ResponseFormat = WebMessageFormat.Json)]
        GetCampaignStatusResponse GetCampaignStatus(string campaignName);
        #endregion

        //#region eFaq
        ///// <summary>
        ///// Queries eFaq and returns query results.
        ///// </summary>
        ///// <param name="topic">eFaq Topic.</param>
        ///// <param name="queryString">eFaq Query String.</param>
        ///// <param name="maximumResults">(optional) Maximum number of results. Default is 20.</param>
        ///// <param name="minimumGrade">(optional) Minimum grade for query results. Default is 50.</param>
        ///// <param name="scoreThreshold">(optional) Score Threshold. Default is 50.</param>
        ///// <returns>A <see cref="QueryEFaqResponse"/> object.</returns>
        //[OperationContract]
        //[Description("Queries eFaq for the answer to a specific question."), Category("eFAQ")]
        //[WebGet(UriTemplate = "/eFAQ/?topic={topic}&querystring={querystring}&maximumresults={maximumresults}&minimumgrade={minimumgrade}&scorethreshold={scorethreshold}", ResponseFormat = WebMessageFormat.Json)]
        //QueryEFaqResponse QueryEFaq(string topic, string queryString, string maximumResults, string minimumGrade, string scoreThreshold);
        //#endregion
    }

    #region Data
    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.CreateUser"/> and <see cref="EasyIceServiceImpl.CreateUserExtended"/> methods
    /// </summary>
    public class CreateUserData
    {
        /// <summary>
        /// User's password (bypasses password policies)
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// User's extension
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateUserData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="password">User's password (bypasses password policies)</param>
        /// <param name="extension">User's extension</param>
        public CreateUserData(string password, string extension)
        {
            Password = password;
            Extension = extension;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.CreateLine"/> method
    /// </summary>
    public class CreateLineData
    {
        /// <summary>
        /// Workgroup's extension
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateLineData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="extension">Workgroup's extension</param>
        public CreateLineData(string extension)
        {
            Extension = extension;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.CreateWorkgroup"/> method
    /// </summary>
    public class CreateWorkgroupData
    {
        /// <summary>
        /// Workgroup's extension
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateWorkgroupData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="extension">Workgroup's extension</param>
        public CreateWorkgroupData(string extension)
        {
            Extension = extension;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.CreateStation"/> method
    /// </summary>
    public class CreateStationData
    {
        /// <summary>
        /// Station's extension
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// User portion of the station identification address.
        /// </summary>
        public string UserPortion { get; set; }

        /// <summary>
        /// Host portion of the station identification address. If empty, no host will be specified.
        /// </summary>
        public string HostPortion { get; set; }

        /// <summary>
        /// Port portion of the station identification address. If empty, no port will be specified.
        /// </summary>
        public string PortPortion { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateStationData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="extension">Station's extension.</param>
        /// <param name="userPortion">User portion of the station identification address. Usually the same as the station's extension.</param>
        /// <param name="hostPortion">(optional) Host portion of the station identification address. If empty, no host will be specified.</param>
        /// <param name="portPortion">(optional) Port portion of the station identification address. If empty, no port will be specified.</param>
        public CreateStationData(string extension, string userPortion, string hostPortion, string portPortion)
        {
            Extension = extension;
            UserPortion = userPortion;
            HostPortion = hostPortion;
            PortPortion = portPortion;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.SetInteractionAttribute"/> method
    /// </summary>
    public class SetInteractionAttributeData
    {
        /// <summary>
        /// Interaction Attribute Value
        /// </summary>
        public string InteractionAttributeValue { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public SetInteractionAttributeData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="interactionAttributeValue">Interaction Attribute Value</param>
        public SetInteractionAttributeData(string interactionAttributeValue)
        {
            InteractionAttributeValue = interactionAttributeValue;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.CreateCallInteraction"/> method
    /// </summary>
    public class CreateCallInteractionData
    {
        /// <summary>
        /// Phone number to call
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// If set, calls from the specified workgroup
        /// </summary>
        public string FromWorkgroup { get; set; }

        /// <summary>
        /// If set, sets the specified account code id
        /// </summary>
        public string AccountCodeId { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateCallInteractionData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="phoneNumber">Phone number to call</param>
        /// <param name="fromWorkgroup">(optional) if set, calls from the specified workgroup</param>
        /// <param name="accountCodeId">(optional) if set, sets the specified account code id</param>
        public CreateCallInteractionData(string phoneNumber, string fromWorkgroup, string accountCodeId)
        {
            PhoneNumber = phoneNumber;
            FromWorkgroup = fromWorkgroup;
            AccountCodeId = accountCodeId;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.CreateChatInteraction"/> method
    /// </summary>
    public class CreateChatInteractionData
    {
        /// <summary>
        /// Target username (chat receiver)
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateChatInteractionData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="username">Target username (chat receiver)</param>
        public CreateChatInteractionData(string username)
        {
            Username = username;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.WriteToChatInteraction"/> method
    /// </summary>
    public class WriteToChatInteractionData
    {
        /// <summary>
        /// Message to write to the chat interaction.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public WriteToChatInteractionData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="message">Message to write to the chat interaction.</param>
        public WriteToChatInteractionData(string message)
        {
            Message = message;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.CreateEmailInteraction"/> method
    /// </summary>
    public class CreateEmailInteractionData
    {
        /// <summary>
        /// Sender's email address
        /// </summary>
        public string SenderEmailAddress { get; set; }

        /// <summary>
        /// Sender's display name
        /// </summary>
        public string SenderDisplayName { get; set; }

        /// <summary>
        /// Email subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Email body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// If set, emails from the specified workgroup. Make sure the workgroup has its routing configured to handle email.
        /// </summary>
        public string FromWorkgroup { get; set; }

        /// <summary>
        /// Only use to save the interaction in a list and re-create the object in case of a switchover.
        /// </summary>
        public InteractionId InteractionId { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateEmailInteractionData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="senderEmailAddress">Sender's email address</param>
        /// <param name="senderDisplayName">Sender's display name</param>
        /// <param name="subject">Email subject</param>
        /// <param name="body">Email body</param>
        /// <param name="fromWorkgroup">(optional) if set, emails from the specified workgroup</param>
        public CreateEmailInteractionData(string senderEmailAddress, string senderDisplayName, string subject, string body, string fromWorkgroup)
        {
            SenderEmailAddress = senderEmailAddress;
            SenderDisplayName = senderDisplayName;
            Subject = subject;
            Body = body;
            FromWorkgroup = fromWorkgroup;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.CreateGenericInteraction"/> method
    /// </summary>
    public class CreateGenericInteractionData
    {
        /// <summary>
        /// Remote name (will be displayed in the Name field of the Interaction Client)
        /// </summary>
        public string RemoteName { get; set; }

        /// <summary>
        /// Remote identifier (will be displayed in the Phone Number field of the Interaction Client)
        /// </summary>
        public string RemoteId { get; set; }

        /// <summary>
        /// Workgroup in which the generic interaction will be created.
        /// </summary>
        public string Workgroup { get; set; }

        /// <summary>
        /// (optional) ACD skill to assign to the interaction.
        /// </summary>
        public string SkillName { get; set; }

        /// <summary>
        /// (optional) Interaction attribute name to set when the generic interaction is created.
        /// </summary>
        public string InteractionAttributeName { get; set; }

        /// <summary>
        /// (optional) Interaction attribute value to assign to the interaction attribute name.
        /// </summary>
        public string InteractionAttributeValue { get; set; }

        /// <summary>
        /// Only use to save the interaction in a list and re-create the object in case of a switchover.
        /// </summary>
        public InteractionId InteractionId { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateGenericInteractionData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="remoteName">Remote name (will be displayed in the Name field of the Interaction Client)</param>
        /// <param name="remoteId">Remote identifier (will be displayed in the Phone Number field of the Interaction Client)</param>
        /// <param name="workgroup">Workgroup in which the generic interaction will be created.</param>
        /// <param name="skillName">(optional) ACD skill to assign to the interaction.</param>
        /// <param name="interactionAttributeName">(optional) Interaction attribute name to set when the generic interaction is created.</param>
        /// <param name="interactionAttributeValue">(optional) Interaction attribute value to assign to the interaction attribute name.</param>
        public CreateGenericInteractionData(string remoteName, string remoteId, string workgroup, string skillName, string interactionAttributeName, string interactionAttributeValue)
        {
            RemoteName = remoteName;
            RemoteId = remoteId;
            Workgroup = workgroup;
            SkillName = skillName;
            InteractionAttributeName = interactionAttributeName;
            InteractionAttributeValue = interactionAttributeValue;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.CreateCallbackInteraction"/> method
    /// </summary>
    public class CreateCallbackInteractionData
    {
        /// <summary>
        /// Phone number to dial when the agent processes the callback.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Name which will be displayed in the Name field of the Interaction Client.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// (optional) Message to insert in the callback form.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Workgroup in which the callback interaction will be queued.
        /// </summary>
        public string Workgroup { get; set; }

        /// <summary>
        /// (optional) ACD skill to assign to the interaction.
        /// </summary>
        public string SkillName { get; set; }

        /// <summary>
        /// (optional) ACD priority (if equals to 0 or -1, will be set to 50).
        /// </summary>
        public int AcdPriority { get; set; }

        /// <summary>
        /// (optional) Interaction notes.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Only use to save the interaction in a list and re-create the object in case of a switchover.
        /// </summary>
        public InteractionId InteractionId { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CreateCallbackInteractionData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="phoneNumber">Phone number to dial when the agent processes the callback.</param>
        /// <param name="name">Name which will be displayed in the Name field of the Interaction Client.</param>
        /// <param name="message">(optional) Message to insert in the callback form.</param>
        /// <param name="workgroup">Workgroup in which the callback interaction will be queued.</param>
        /// <param name="skillName">(optional) ACD skill to assign to the interaction.</param>
        /// <param name="acdPriority">(optional) ACD priority (if equals to 0 or -1, will be set to 50).</param>
        /// <param name="notes">(optional) Interaction notes.</param>
        public CreateCallbackInteractionData(string phoneNumber, string name, string message, string workgroup, string skillName, int acdPriority, string notes)
        {
            PhoneNumber = phoneNumber;
            Name = name;
            Workgroup = workgroup;
            Message = message;
            SkillName = skillName;
            AcdPriority = acdPriority;
            Notes = notes;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.SendFax"/> and <see cref="EasyIceServiceImpl.SendFaxExtended"/> method
    /// </summary>
    public class SendFaxData
    {
        /// <summary>
        /// Number to send the fax to.
        /// </summary>
        public string FaxNumber { get; set; }

        /// <summary>
        /// Full path to the TIF image to send.
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// (optional) Name of the person you are sending this fax to.
        /// </summary>
        public string RecipientName { get; set; }

        /// <summary>
        /// (optional) Company name you are sending this fax to.
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// (optional) Phone number used to contact the person you are sending this fax to.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// (optional) Account code id to assign to this fax.
        /// </summary>
        public string AccountCodeId { get; set; }

        /// <summary>
        /// Will send a notification email to this address if the fax is successfully sent.
        /// </summary>
        public string EmailSuccess { get; set; }

        /// <summary>
        /// Will send a notification email to this address if the fax send request failed.
        /// </summary>
        public string EmailFailure { get; set; }

        /// <summary>
        /// "A4", "Letter" (default) or "Legal"
        /// </summary>
        public string FaxPageSize { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public SendFaxData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="faxNumber">Number to send the fax to.</param>
        /// <param name="image">Full path to the TIF image to send.</param>
        /// <param name="recipientName">(optional) Name of the person you are sending this fax to.</param>
        /// <param name="company">(optional) Company name you are sending this fax to.</param>
        /// <param name="phoneNumber">(optional) Phone number used to contact the person you are sending this fax to.</param>
        /// <param name="accountCodeId">(optional) Account code id to assign to this fax.</param>
        /// <param name="emailSuccess">Will send a notification email to this address if the fax is successfully sent.</param>
        /// <param name="emailFailure">Will send a notification email to this address if the fax send request failed.</param>
        /// <param name="faxPageSize">"A4", "Letter" (default) or "Legal"</param>
        public SendFaxData(string faxNumber, string image, string recipientName, string company, string phoneNumber, string accountCodeId, string emailSuccess, string emailFailure, string faxPageSize)
        {
            FaxNumber = faxNumber;
            Image = image;
            RecipientName = recipientName;
            Company = company;
            PhoneNumber = phoneNumber;
            AccountCodeId = accountCodeId;
            EmailSuccess = emailSuccess;
            EmailFailure = emailFailure;
            FaxPageSize = faxPageSize;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.PlayWavFile"/> method
    /// </summary>
    public class PlayWavFileData
    {
        /// <summary>
        /// If located on the CIC server, must be the full path of the wav file.
        /// </summary>
        public string WavFile { get; set; }

        /// <summary>
        /// If set to true, file is a remote path.
        /// </summary>
        public bool Remote { get; set; }

        /// <summary>
        /// If set to true, enables playing of digits to skip forward or backward or to skip the prompt entirely. If false, will ignore any digits and the file will be played entirely.
        /// </summary>
        public bool EnableDigits { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public PlayWavFileData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="wavFile">If located on the CIC server, must be the full path of the wav file.</param>
        /// <param name="remote">If set to true, file is a remote path.</param>
        /// <param name="enableDigits">If set to true, enables playing of digits to skip forward or backward or to skip the prompt entirely. If false, will ignore any digits and the file will be played entirely.</param>
        public PlayWavFileData(string wavFile, bool remote, bool enableDigits)
        {
            WavFile = wavFile;
            Remote = remote;
            EnableDigits = enableDigits;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.PlayDigits"/> method.
    /// </summary>
    public class PlayDigitsData
    {
        /// <summary>
        /// Digits to play on the call interaction.
        /// </summary>
        public string Digits { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public PlayDigitsData() { }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="digits">Digits to play on the call interaction.</param>
        public PlayDigitsData(string digits)
        {
            Digits = digits;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.DownloadRecording"/> method.
    /// </summary>
    public class DownloadRecordingData
    {
        /// <summary>
        /// Interaction Id. If Interaction Id Key and Recording Id are not specified, this parameter is required.
        /// </summary>
        public string InteractionId { get; set; }

        /// <summary>
        /// Interaction Id Key. If Interaction Id and Recording Id are not specified, this parameter is required.
        /// </summary>
        public string InteractionIdKey { get; set; }

        /// <summary>
        /// Recording Identifier. If the interaction id key and interaction id parameters are not specified, this parameter is required.
        /// </summary>
        public string RecordingId { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public DownloadRecordingData() { }

        /// <summary>
        /// Overloaded Constructor. Only one of the parameters (either InteractionId or RecordingId) is required.
        /// </summary>
        /// <param name="interactionIdKey">Interaction Id Key of the interaction that was recorded.</param>
        /// <param name="interactionId">Interaction Id of the interaction that was recorded.</param>
        /// <param name="recordingId">Recording identifier of the recording.</param>
        public DownloadRecordingData(string interactionIdKey, string interactionId, string recordingId)
        {
            InteractionIdKey = interactionIdKey;
            InteractionId = interactionId;
            RecordingId = recordingId;
        }
    }

    /// <summary>
    /// Data to submit to the <see cref="EasyIceServiceImpl.InsertContact"/> method.
    /// </summary>
    public class InsertContactData
    {
        /// <summary>
        /// Contact's data.
        /// </summary>
        public Dictionary<String, object> InsertData { get; set; }

        /// <summary>
        /// Campaign Name.
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        /// If true, contact will be dialled as soon as possible. If not, contact will be queued.
        /// </summary>
        public bool Immediate { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InsertContactData() { }

        /// <summary>
        /// Overloaded Constructor.
        /// </summary>
        /// <param name="insertData">Contact's data.</param>
        /// <param name="campaignName">Campaign Name.</param>
        /// <param name="immediate">If true, contact will be dialled as soon as possible. If not, contact will be queued.</param>
        public InsertContactData(Dictionary<String, object> insertData, string campaignName, bool immediate)
        {
            InsertData = insertData;
            CampaignName = campaignName;
            Immediate = immediate;
        }
    }
    #endregion

    #region Error Handler
    /// <summary>
    /// Applies the <see cref="FaultErrorHandler"/> logic to all channels
    /// </summary>
    internal class ErrorHandlerAttribute : Attribute, IServiceBehavior
    {
        #region IServiceBehavior Members
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                dispatcher.ErrorHandlers.Add(new FaultErrorHandler());
            }
        }
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
        #endregion
    }
    /// <summary>
    /// Used to catch uncaught exceptions and prevent channels from going into faulted state.
    /// </summary>
    internal class FaultErrorHandler : IErrorHandler
    {
        #region IErrorHandler Members
        public bool HandleError(Exception error)
        {
            var f = error as FaultException;
            if (f != null)
            {
                Trace.EasyIceServicesTopic.exception(f);
                return true;
            }
            return false;
        }
        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            if (fault != null) return;
            var f = new FaultException(error.Message, FaultCode.CreateSenderFaultCode(null));
            var mf = f.CreateMessageFault();
            fault = Message.CreateMessage(version, mf, f.Action);
        }
        #endregion
    }
    #endregion
}
