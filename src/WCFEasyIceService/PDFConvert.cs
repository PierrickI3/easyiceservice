﻿using System;
using System.Diagnostics;
using System.IO;

namespace WCFEasyIceService
{
    internal static class PDFConvert
    {
        internal static bool ConvertPDFToTiff(string pdfFilename, string workingDirectory)
        {
            if (!File.Exists(workingDirectory + "\\convert.exe"))
                return false;

            var conversionProcess = new Process
                {
                    StartInfo =
                        {
                            UseShellExecute = true,
                            CreateNoWindow = true,
                            FileName = "convert.exe",
                            WorkingDirectory = workingDirectory,
                            Arguments = String.Format(
                                "-density 600x600 -colorspace gray -ordered-dither 1x1 -compress group4 \"{0}\" \"{1}\"",
                                pdfFilename, pdfFilename.Replace(".pdf", ".tif")),
                            WindowStyle = ProcessWindowStyle.Hidden
                        }
                };
            bool success = conversionProcess.Start();
            if (conversionProcess.HasExited)
            {
                return false;
            }
            conversionProcess.PriorityClass = ProcessPriorityClass.Idle;
            conversionProcess.ProcessorAffinity = new IntPtr(1);
            conversionProcess.WaitForExit();
            return success;
        }
    }
}
