﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TestConsoleApp.EasyIceServiceReference;

namespace TestConsoleApp
{
    class Program
    {
        static void Main()
        {
            EasyIceServiceImplClient proxy = null;

            try
            {
                Console.WriteLine("Opening proxy...");
                proxy = new EasyIceServiceImplClient("basicHttp");
                proxy.Open();

                var insertContactData = new InsertContactData {CampaignName = "TestCampaign", Immediate = false, InsertData = new Dictionary<string, object> {{"PHONENUMBER", "3178723000"}}};
                var insertContactResponse = proxy.InsertContact(insertContactData);
                Console.WriteLine("Insert Contact Reponse: {0}.", insertContactResponse.Success);
                Console.WriteLine("Insert Contact Error Message: {0}.", insertContactResponse.ErrorMessage);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (proxy != null && proxy.ChannelFactory.State != CommunicationState.Closed)
                    proxy.Close();

                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
        }
    }
}
