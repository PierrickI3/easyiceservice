﻿using System;
using Microsoft.Deployment.WindowsInstaller;
using ININ.IceLib.Connection;

namespace CAValidateCICCredentials
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult ValidateCICCredentials(Microsoft.Deployment.WindowsInstaller.Session session)
        {
            session["LOGGING"] = "Begin ValidateCICCredentials";
            session["VALIDCICCREDENTIALS"] = "FALSE";
            session["LOGGING"] = "Starting CIC credentials validation"; //Using this as CA called using "DoAction" will prevent log messages from being logged.

            ININ.IceLib.Connection.Session iceSession = null;

            try
            {
                var cicServer = session["CICSERVER"];
                var cicUsername = session["CICUSERNAME"];
                var cicPassword = session["CICPASSWORD"];

                session["LOGGING"] = "Creating session settings";
                var sessionSettings = new SessionSettings
                    {
                        MachineName = Environment.MachineName,
                        ClassOfService = ClassOfService.General,
                        ApplicationName = "CAValidateCICCredentials"
                    };

                session["LOGGING"] = "Creating host settings";
                var hostSettings = new HostSettings(new HostEndpoint(cicServer));

                session["LOGGING"] = "Creating authentication settings";
                var authSettings = new ICAuthSettings(cicUsername, cicPassword);

                session["LOGGING"] = "Creating station settings";
                var stationSettings = new StationlessSettings();

                iceSession = new ININ.IceLib.Connection.Session();
                session["LOGGING"] = String.Format("Connecting to CIC now with username: {0}, password: {1}, server {2}.", cicUsername, cicPassword, cicServer);
                iceSession.Connect(sessionSettings, hostSettings, authSettings, stationSettings);
                session["LOGGING"] = String.Format("Session connection state: {0}", iceSession.ConnectionState);

                if (iceSession.ConnectionState == ConnectionState.Up)
                {
                    session["VALIDCICCREDENTIALS"] = "TRUE";
                    var encryptor = new Encryptor();
                    session["CICPASSWORD"] = encryptor.EncryptToBase64String(cicPassword, "3#sy!c3S3rv!c3");
                }
                else
                {
                    var record = new Record(2);
                    record[0] = "[1]";
                    record[1] = "Invalid CIC credentials";
                    session.Message(InstallMessage.Error, record);
                }
            }
            catch (Exception ex)
            {
                session["LOGGING"] = String.Format("Exception: {0}", ex.Message);
                var record = new Record(2);
                record[0] = "[1]";
                record[1] = String.Format("Exception: {0}", ex.Message);
                session.Message(InstallMessage.Error, record);
            }
            finally
            {
                if (iceSession != null && iceSession.ConnectionState == ConnectionState.Up)
                    iceSession.DisconnectAsync(null, null);
            }
            return ActionResult.Success;
        }
    }
}
