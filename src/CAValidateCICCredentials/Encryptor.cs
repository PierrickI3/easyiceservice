﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CAValidateCICCredentials
{
    internal class Encryptor
    {
        private byte[] _key = { };
        private readonly byte[] _iv = { 18, 52, 86, 120, 144, 171, 205, 239 };

        internal string DecryptFromBase64String(string stringToDecrypt, string sEncryptionKey)
        {
            try
            {
                _key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                var des = new DESCryptoServiceProvider();
                var inputByteArray = Convert.FromBase64String(stringToDecrypt);
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, des.CreateDecryptor(_key, _iv), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                var encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        internal string EncryptToBase64String(string stringToEncrypt, string sEncryptionKey)
        {
            try
            {
                _key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                var des = new DESCryptoServiceProvider();
                var inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                var ms = new MemoryStream();
                var cs = new CryptoStream(ms, des.CreateEncryptor(_key, _iv), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        } 
    }
}