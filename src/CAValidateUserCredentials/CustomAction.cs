﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Windows.Forms;
using Microsoft.Deployment.WindowsInstaller;

namespace CAValidateUserCredentials
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult ValidateUserCredentials(Session session)
        {
            session.Log("Begin ValidateUserCredentials");
            session["VALIDUSERCREDENTIALS"] = "FALSE";

            try
            {
                //MessageBox.Show("Please attach a debugger to rundll32.exe.", "Attach");

                var serviceUsername = session["SERVICEUSERNAME"];
                var servicePassword = session["SERVICEPASSWORD"];
                session.Log("Service Username: " + serviceUsername);

                bool valid;
                session.Log("Checking against machine...");
                using (var context = new PrincipalContext(ContextType.Machine))
                {
                    valid = context.ValidateCredentials(serviceUsername, servicePassword);
                }
                session.Log("Machine credentials valid? " + valid);

                if (!valid)
                {
                    session.Log("Checking against domain...");
                    using (var context = new PrincipalContext(ContextType.Domain))
                    {
                        valid = context.ValidateCredentials(serviceUsername, servicePassword);
                    }
                    session.Log("Domain credentials valid? " + valid);
                }

                if (valid)
                {
                    session.Log("Setting VALIDUSERCREDENTIALS property to true.");
                    session["VALIDUSERCREDENTIALS"] = "TRUE";
                }
                else
                {
                    session.Log("Returning error.");
                    var record = new Record(2);
                    record[0] = "[1]";
                    record[1] = "Invalid Machine/Domain user credentials";
                    session.Message(InstallMessage.Error, record);
                }
            }
            catch (Exception ex)
            {
                session.Log(String.Format("Exception: {0}", ex));
                var record = new Record(2);
                record[0] = "[1]";
                record[1] = String.Format("Exception: {0}", ex);
                session.Message(InstallMessage.Error, record);
            }
            return ActionResult.Success;
        }
    }
}
