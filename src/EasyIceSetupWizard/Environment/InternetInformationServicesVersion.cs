// ---------------------------------------------------------------------------
// Campari Software
//
// InternetInformationServicesVersion.cs
//
//
// ---------------------------------------------------------------------------
// Copyright (C) 2006-2007 Campari Software
// All rights reserved.
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR
// FITNESS FOR A PARTICULAR PURPOSE.
// ---------------------------------------------------------------------------
using System;

namespace EasyIceSetupWizard
{
    #region enum InternetInformationServicesVersion
    /// <summary>
    /// Specifies the Internet Information Services (IIS) versions
    /// </summary>
    public enum InternetInformationServicesVersion
    {
        /// <summary>
        /// Internet Information Services 6
        /// </summary>
        /// <remarks>Shipped in Windows Server 2003</remarks>
        IIS6,

        /// <summary>
        /// Internet Information Services 7
        /// </summary>
        /// <remarks>Shipped in Windows Vista</remarks>
        IIS7,

        /// <summary>
        /// Internet Information Services 7.5
        /// </summary>
        /// <remarks>Shipped in Windows Server 2008</remarks>
        IIS75,
    }
    #endregion
}
