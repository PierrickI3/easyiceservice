﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EasyIceSetupWizard
{
    public partial class UninstallForm : Form
    {
        public UninstallForm()
        {
            InitializeComponent();
        }
        private void UninstallForm_Load(object sender, EventArgs e)
        {
            progressBar1.Maximum = 10;
            if (ServiceInstaller.ServiceIsInstalled("Easy Ice Service"))
            {
                lblUninstallInfo.Text = "Uninstall Windows Service...";
                progressBar1.Value++;
                ServiceInstaller.Uninstall("Easy Ice Service");
                progressBar1.Value++;
            }

            lblUninstallInfo.Text = "Uninstalling IIS web site...";
            //TODO Check if IIS web site is created, stop it and remove it
            //IISHelper.DeleteWebSite("localhost", "Easy Ice Service");

            Application.Exit(new CancelEventArgs(false));
        }
    }
}
