﻿using System;
using System.DirectoryServices;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Windows.Forms;
using System.ServiceProcess;
using ININ.IceLib.Connection;
using System.Text;
using System.Configuration;
using System.Diagnostics;

namespace EasyIceSetupWizard
{
    public partial class MainForm : Form
    {
        private const string EASYICESERVICE_APP_POOL_NAME = "EasyIceService";

        #region External Calls
        // API declaration for validating user credentials
        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, out IntPtr phToken);
        //API to close the credential token
        [DllImport("kernel32", EntryPoint = "CloseHandle")]
        public static extern long CloseHandle(IntPtr hObject);
        // Use NTLM security provider to check 
        public const int LOGON32_PROVIDER_DEFAULT = 0x0;
        // To validate the account
        public const int LOGON32_LOGON_NETWORK = 0x3;
        #endregion

        #region Form Methods
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.fbdHostInformationIISPath = new System.Windows.Forms.FolderBrowserDialog();
            this.wizardPages = new EasyIceSetupWizard.WizardPages();
            this.tpWelcome = new System.Windows.Forms.TabPage();
            this.pnlWelcomePageInformation = new System.Windows.Forms.Panel();
            this.lblWelcomePageTitle = new System.Windows.Forms.Label();
            this.pnlWelcomeContent = new System.Windows.Forms.Panel();
            this.btnWelcomeNext = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblWelcomePageDetails = new System.Windows.Forms.Label();
            this.pnlWelcomeSideLogo = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tpStopServices = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnStopServiceBack = new System.Windows.Forms.Button();
            this.wizardPageStopService = new EasyIceSetupWizard.WizardPages();
            this.tpStopServiceWindowsService = new System.Windows.Forms.TabPage();
            this.btnStopServiceWindowsService = new System.Windows.Forms.Button();
            this.lblStopServiceWindowsServiceDetails2 = new System.Windows.Forms.Label();
            this.lblStopServiceWindowsServiceDetails1 = new System.Windows.Forms.Label();
            this.tpStopServiceIIS = new System.Windows.Forms.TabPage();
            this.btnStopServiceIISStop = new System.Windows.Forms.Button();
            this.lblStopServiceIISDetails2 = new System.Windows.Forms.Label();
            this.lblStopServiceIISDetails1 = new System.Windows.Forms.Label();
            this.btnStopServiceNext = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblStopServicesTitle = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tpHostInformation = new System.Windows.Forms.TabPage();
            this.pnlHostInformationPageInformation = new System.Windows.Forms.Panel();
            this.lblHostInformationPageTitle = new System.Windows.Forms.Label();
            this.pnlHostInformationContent = new System.Windows.Forms.Panel();
            this.wizardPageHostType = new EasyIceSetupWizard.WizardPages();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tpHostInformationWindowsService = new System.Windows.Forms.TabPage();
            this.lblHostInformationWindowsServiceURL = new System.Windows.Forms.LinkLabel();
            this.lblHostInformationWindowsServiceAccessInfo = new System.Windows.Forms.Label();
            this.txtHostInformationWindowsServiceDomain = new System.Windows.Forms.TextBox();
            this.lblHostInformationWindowsServiceDomain = new System.Windows.Forms.Label();
            this.nudHostInformationWindowsServicePort = new System.Windows.Forms.NumericUpDown();
            this.lblHostInformationWindowsServicePort = new System.Windows.Forms.Label();
            this.txtHostInformationWindowsServicePassword = new System.Windows.Forms.TextBox();
            this.lblHostInformationWindowsServicePassword = new System.Windows.Forms.Label();
            this.lblHostInformationWindowsServiceInfo = new System.Windows.Forms.Label();
            this.txtHostInformationWindowsServiceUsername = new System.Windows.Forms.TextBox();
            this.lblHostInformationWindowsServiceUsername = new System.Windows.Forms.Label();
            this.tpHostInformationIIS = new System.Windows.Forms.TabPage();
            this.lblHostInformationIISMachineURL = new System.Windows.Forms.LinkLabel();
            this.txtHostInformationIISDomain = new System.Windows.Forms.TextBox();
            this.lblHostInformationIISDomain = new System.Windows.Forms.Label();
            this.txtHostInformationIISPassword = new System.Windows.Forms.TextBox();
            this.lblHostInformationIISPassword = new System.Windows.Forms.Label();
            this.txtHostInformationIISUsername = new System.Windows.Forms.TextBox();
            this.lblHostInformationIISUsername = new System.Windows.Forms.Label();
            this.lblHostInformationIISIPAddress = new System.Windows.Forms.Label();
            this.cbHostInformationIISIPAddress = new System.Windows.Forms.ComboBox();
            this.btnHostInformationIISBrowsePath = new System.Windows.Forms.Button();
            this.lblHostInformationIISURL = new System.Windows.Forms.LinkLabel();
            this.lblHostInformationIISServiceAccessInfo = new System.Windows.Forms.Label();
            this.txtHostInformationIISWebSiteName = new System.Windows.Forms.TextBox();
            this.lblHostInformationIISWebSiteName = new System.Windows.Forms.Label();
            this.nudHostInformationIISPort = new System.Windows.Forms.NumericUpDown();
            this.lblHostInformationIISPort = new System.Windows.Forms.Label();
            this.lblHostInformationIISInfo = new System.Windows.Forms.Label();
            this.txtHostInformationIISPath = new System.Windows.Forms.TextBox();
            this.lblHostInformationIISPath = new System.Windows.Forms.Label();
            this.txtHostInformationServerTypeInfo = new System.Windows.Forms.RichTextBox();
            this.rbHostInformationIIS = new System.Windows.Forms.RadioButton();
            this.rbHostInformationWindowsService = new System.Windows.Forms.RadioButton();
            this.lblHostInformationServerHostType = new System.Windows.Forms.Label();
            this.btnHostInformationBack = new System.Windows.Forms.Button();
            this.btnHostInformationNext = new System.Windows.Forms.Button();
            this.pnlHostInformationSideLogo = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tpCICCredentials = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblCICCredentialsTitle = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lblCICCredentialsDetails = new System.Windows.Forms.Label();
            this.txtCICCredentialsCICPassword = new System.Windows.Forms.TextBox();
            this.txtCICCredentialsCICUsername = new System.Windows.Forms.TextBox();
            this.txtCICCredentialsCICServer = new System.Windows.Forms.TextBox();
            this.lblCICCredentialsCICServer = new System.Windows.Forms.Label();
            this.lblCICCredentialsCICPassword = new System.Windows.Forms.Label();
            this.lblCICCredentialsCICUsername = new System.Windows.Forms.Label();
            this.btnCICCredentialsBack = new System.Windows.Forms.Button();
            this.btnCICCredentialsNext = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.tpSummary = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblSummaryTitle = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pbSummary = new System.Windows.Forms.ProgressBar();
            this.txtSummaryDetails = new System.Windows.Forms.RichTextBox();
            this.btnSummaryBack = new System.Windows.Forms.Button();
            this.btnSummaryCommit = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.tpFinish = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblFinishTitle = new System.Windows.Forms.Label();
            this.pnlFinishContents = new System.Windows.Forms.Panel();
            this.llblFinishDoc = new System.Windows.Forms.LinkLabel();
            this.lblFinishDocPage = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.llblFinishWSDL = new System.Windows.Forms.LinkLabel();
            this.lblFinishWSDLPage = new System.Windows.Forms.Label();
            this.lblFinishHelpPage = new System.Windows.Forms.Label();
            this.llblFinishHelpPage = new System.Windows.Forms.LinkLabel();
            this.txtFinishDetails = new System.Windows.Forms.RichTextBox();
            this.btnFinishBack = new System.Windows.Forms.Button();
            this.btnFinishClose = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.wizardPages.SuspendLayout();
            this.tpWelcome.SuspendLayout();
            this.pnlWelcomePageInformation.SuspendLayout();
            this.pnlWelcomeContent.SuspendLayout();
            this.pnlWelcomeSideLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tpStopServices.SuspendLayout();
            this.panel1.SuspendLayout();
            this.wizardPageStopService.SuspendLayout();
            this.tpStopServiceWindowsService.SuspendLayout();
            this.tpStopServiceIIS.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tpHostInformation.SuspendLayout();
            this.pnlHostInformationPageInformation.SuspendLayout();
            this.pnlHostInformationContent.SuspendLayout();
            this.wizardPageHostType.SuspendLayout();
            this.tpHostInformationWindowsService.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHostInformationWindowsServicePort)).BeginInit();
            this.tpHostInformationIIS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHostInformationIISPort)).BeginInit();
            this.pnlHostInformationSideLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tpCICCredentials.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.tpSummary.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.tpFinish.SuspendLayout();
            this.panel7.SuspendLayout();
            this.pnlFinishContents.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // wizardPages
            // 
            this.wizardPages.Controls.Add(this.tpWelcome);
            this.wizardPages.Controls.Add(this.tpStopServices);
            this.wizardPages.Controls.Add(this.tpHostInformation);
            this.wizardPages.Controls.Add(this.tpCICCredentials);
            this.wizardPages.Controls.Add(this.tpSummary);
            this.wizardPages.Controls.Add(this.tpFinish);
            this.wizardPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizardPages.Location = new System.Drawing.Point(0, 0);
            this.wizardPages.Name = "wizardPages";
            this.wizardPages.SelectedIndex = 0;
            this.wizardPages.Size = new System.Drawing.Size(681, 500);
            this.wizardPages.TabIndex = 0;
            this.wizardPages.TabStop = false;
            // 
            // tpWelcome
            // 
            this.tpWelcome.Controls.Add(this.pnlWelcomePageInformation);
            this.tpWelcome.Controls.Add(this.pnlWelcomeContent);
            this.tpWelcome.Controls.Add(this.pnlWelcomeSideLogo);
            this.tpWelcome.Location = new System.Drawing.Point(4, 22);
            this.tpWelcome.Name = "tpWelcome";
            this.tpWelcome.Padding = new System.Windows.Forms.Padding(3);
            this.tpWelcome.Size = new System.Drawing.Size(673, 474);
            this.tpWelcome.TabIndex = 0;
            this.tpWelcome.Text = "Welcome!";
            this.tpWelcome.UseVisualStyleBackColor = true;
            // 
            // pnlWelcomePageInformation
            // 
            this.pnlWelcomePageInformation.BackColor = System.Drawing.Color.Linen;
            this.pnlWelcomePageInformation.Controls.Add(this.lblWelcomePageTitle);
            this.pnlWelcomePageInformation.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlWelcomePageInformation.Location = new System.Drawing.Point(217, 3);
            this.pnlWelcomePageInformation.Name = "pnlWelcomePageInformation";
            this.pnlWelcomePageInformation.Size = new System.Drawing.Size(453, 69);
            this.pnlWelcomePageInformation.TabIndex = 2;
            // 
            // lblWelcomePageTitle
            // 
            this.lblWelcomePageTitle.AutoSize = true;
            this.lblWelcomePageTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcomePageTitle.Location = new System.Drawing.Point(10, 7);
            this.lblWelcomePageTitle.Name = "lblWelcomePageTitle";
            this.lblWelcomePageTitle.Size = new System.Drawing.Size(59, 13);
            this.lblWelcomePageTitle.TabIndex = 0;
            this.lblWelcomePageTitle.Text = "Welcome";
            // 
            // pnlWelcomeContent
            // 
            this.pnlWelcomeContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlWelcomeContent.Controls.Add(this.btnWelcomeNext);
            this.pnlWelcomeContent.Controls.Add(this.label1);
            this.pnlWelcomeContent.Controls.Add(this.lblWelcomePageDetails);
            this.pnlWelcomeContent.Location = new System.Drawing.Point(217, 78);
            this.pnlWelcomeContent.Name = "pnlWelcomeContent";
            this.pnlWelcomeContent.Size = new System.Drawing.Size(453, 393);
            this.pnlWelcomeContent.TabIndex = 2;
            // 
            // btnWelcomeNext
            // 
            this.btnWelcomeNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWelcomeNext.Location = new System.Drawing.Point(373, 365);
            this.btnWelcomeNext.Name = "btnWelcomeNext";
            this.btnWelcomeNext.Size = new System.Drawing.Size(75, 23);
            this.btnWelcomeNext.TabIndex = 2;
            this.btnWelcomeNext.Text = "Next >";
            this.btnWelcomeNext.UseVisualStyleBackColor = true;
            this.btnWelcomeNext.Click += new System.EventHandler(this.btnWelcomeNext_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Click on \"Next >\" to configure your service.";
            // 
            // lblWelcomePageDetails
            // 
            this.lblWelcomePageDetails.AutoSize = true;
            this.lblWelcomePageDetails.Location = new System.Drawing.Point(13, 8);
            this.lblWelcomePageDetails.Name = "lblWelcomePageDetails";
            this.lblWelcomePageDetails.Size = new System.Drawing.Size(235, 13);
            this.lblWelcomePageDetails.TabIndex = 0;
            this.lblWelcomePageDetails.Text = "Welcome to the Easy Ice Service Setup Wizard.";
            // 
            // pnlWelcomeSideLogo
            // 
            this.pnlWelcomeSideLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlWelcomeSideLogo.Controls.Add(this.pictureBox1);
            this.pnlWelcomeSideLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlWelcomeSideLogo.Location = new System.Drawing.Point(3, 3);
            this.pnlWelcomeSideLogo.Name = "pnlWelcomeSideLogo";
            this.pnlWelcomeSideLogo.Size = new System.Drawing.Size(214, 468);
            this.pnlWelcomeSideLogo.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::EasyIceSetupWizard.Properties.Resources.EasyIceLogo;
            this.pictureBox1.Location = new System.Drawing.Point(0, 101);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(213, 250);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tpStopServices
            // 
            this.tpStopServices.Controls.Add(this.panel1);
            this.tpStopServices.Controls.Add(this.panel2);
            this.tpStopServices.Controls.Add(this.panel3);
            this.tpStopServices.Location = new System.Drawing.Point(4, 22);
            this.tpStopServices.Name = "tpStopServices";
            this.tpStopServices.Size = new System.Drawing.Size(673, 474);
            this.tpStopServices.TabIndex = 5;
            this.tpStopServices.Text = "Stop Services";
            this.tpStopServices.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnStopServiceBack);
            this.panel1.Controls.Add(this.wizardPageStopService);
            this.panel1.Controls.Add(this.btnStopServiceNext);
            this.panel1.Location = new System.Drawing.Point(217, 78);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(453, 393);
            this.panel1.TabIndex = 4;
            // 
            // btnStopServiceBack
            // 
            this.btnStopServiceBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStopServiceBack.Location = new System.Drawing.Point(292, 365);
            this.btnStopServiceBack.Name = "btnStopServiceBack";
            this.btnStopServiceBack.Size = new System.Drawing.Size(75, 23);
            this.btnStopServiceBack.TabIndex = 4;
            this.btnStopServiceBack.Text = "< Back";
            this.btnStopServiceBack.UseVisualStyleBackColor = true;
            this.btnStopServiceBack.Click += new System.EventHandler(this.btnStopServiceBack_Click);
            // 
            // wizardPageStopService
            // 
            this.wizardPageStopService.Controls.Add(this.tpStopServiceWindowsService);
            this.wizardPageStopService.Controls.Add(this.tpStopServiceIIS);
            this.wizardPageStopService.Location = new System.Drawing.Point(4, 3);
            this.wizardPageStopService.Name = "wizardPageStopService";
            this.wizardPageStopService.SelectedIndex = 0;
            this.wizardPageStopService.Size = new System.Drawing.Size(452, 356);
            this.wizardPageStopService.TabIndex = 3;
            // 
            // tpStopServiceWindowsService
            // 
            this.tpStopServiceWindowsService.Controls.Add(this.btnStopServiceWindowsService);
            this.tpStopServiceWindowsService.Controls.Add(this.lblStopServiceWindowsServiceDetails2);
            this.tpStopServiceWindowsService.Controls.Add(this.lblStopServiceWindowsServiceDetails1);
            this.tpStopServiceWindowsService.Location = new System.Drawing.Point(4, 22);
            this.tpStopServiceWindowsService.Name = "tpStopServiceWindowsService";
            this.tpStopServiceWindowsService.Padding = new System.Windows.Forms.Padding(3);
            this.tpStopServiceWindowsService.Size = new System.Drawing.Size(444, 330);
            this.tpStopServiceWindowsService.TabIndex = 0;
            this.tpStopServiceWindowsService.Text = "Stop Windows Service";
            this.tpStopServiceWindowsService.UseVisualStyleBackColor = true;
            // 
            // btnStopServiceWindowsService
            // 
            this.btnStopServiceWindowsService.Location = new System.Drawing.Point(102, 126);
            this.btnStopServiceWindowsService.Name = "btnStopServiceWindowsService";
            this.btnStopServiceWindowsService.Size = new System.Drawing.Size(247, 23);
            this.btnStopServiceWindowsService.TabIndex = 2;
            this.btnStopServiceWindowsService.Text = "Stop \"Easy Ice Service\" Service";
            this.btnStopServiceWindowsService.UseVisualStyleBackColor = true;
            this.btnStopServiceWindowsService.Click += new System.EventHandler(this.btnStopServiceWindowsService_Click);
            // 
            // lblStopServiceWindowsServiceDetails2
            // 
            this.lblStopServiceWindowsServiceDetails2.AutoSize = true;
            this.lblStopServiceWindowsServiceDetails2.Location = new System.Drawing.Point(98, 93);
            this.lblStopServiceWindowsServiceDetails2.Name = "lblStopServiceWindowsServiceDetails2";
            this.lblStopServiceWindowsServiceDetails2.Size = new System.Drawing.Size(252, 13);
            this.lblStopServiceWindowsServiceDetails2.TabIndex = 1;
            this.lblStopServiceWindowsServiceDetails2.Text = "This service needs to be stopped before continuing.";
            // 
            // lblStopServiceWindowsServiceDetails1
            // 
            this.lblStopServiceWindowsServiceDetails1.AutoSize = true;
            this.lblStopServiceWindowsServiceDetails1.Location = new System.Drawing.Point(82, 71);
            this.lblStopServiceWindowsServiceDetails1.Name = "lblStopServiceWindowsServiceDetails1";
            this.lblStopServiceWindowsServiceDetails1.Size = new System.Drawing.Size(294, 13);
            this.lblStopServiceWindowsServiceDetails1.TabIndex = 0;
            this.lblStopServiceWindowsServiceDetails1.Text = "The windows service \"Easy Ice Service\" is currently running.";
            // 
            // tpStopServiceIIS
            // 
            this.tpStopServiceIIS.Controls.Add(this.btnStopServiceIISStop);
            this.tpStopServiceIIS.Controls.Add(this.lblStopServiceIISDetails2);
            this.tpStopServiceIIS.Controls.Add(this.lblStopServiceIISDetails1);
            this.tpStopServiceIIS.Location = new System.Drawing.Point(4, 22);
            this.tpStopServiceIIS.Name = "tpStopServiceIIS";
            this.tpStopServiceIIS.Padding = new System.Windows.Forms.Padding(3);
            this.tpStopServiceIIS.Size = new System.Drawing.Size(444, 330);
            this.tpStopServiceIIS.TabIndex = 1;
            this.tpStopServiceIIS.Text = "Stop IIS";
            this.tpStopServiceIIS.UseVisualStyleBackColor = true;
            // 
            // btnStopServiceIISStop
            // 
            this.btnStopServiceIISStop.Location = new System.Drawing.Point(96, 126);
            this.btnStopServiceIISStop.Name = "btnStopServiceIISStop";
            this.btnStopServiceIISStop.Size = new System.Drawing.Size(247, 23);
            this.btnStopServiceIISStop.TabIndex = 5;
            this.btnStopServiceIISStop.Text = "Stop EasyIceService web site";
            this.btnStopServiceIISStop.UseVisualStyleBackColor = true;
            this.btnStopServiceIISStop.Click += new System.EventHandler(this.btnStopServiceIISStop_Click);
            // 
            // lblStopServiceIISDetails2
            // 
            this.lblStopServiceIISDetails2.AutoSize = true;
            this.lblStopServiceIISDetails2.Location = new System.Drawing.Point(93, 93);
            this.lblStopServiceIISDetails2.Name = "lblStopServiceIISDetails2";
            this.lblStopServiceIISDetails2.Size = new System.Drawing.Size(256, 13);
            this.lblStopServiceIISDetails2.TabIndex = 4;
            this.lblStopServiceIISDetails2.Text = "The web site needs to be stopped before continuing.";
            // 
            // lblStopServiceIISDetails1
            // 
            this.lblStopServiceIISDetails1.AutoSize = true;
            this.lblStopServiceIISDetails1.Location = new System.Drawing.Point(99, 68);
            this.lblStopServiceIISDetails1.Name = "lblStopServiceIISDetails1";
            this.lblStopServiceIISDetails1.Size = new System.Drawing.Size(239, 13);
            this.lblStopServiceIISDetails1.TabIndex = 3;
            this.lblStopServiceIISDetails1.Text = "The EasyIceService web site is currently running.";
            // 
            // btnStopServiceNext
            // 
            this.btnStopServiceNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStopServiceNext.Enabled = false;
            this.btnStopServiceNext.Location = new System.Drawing.Point(373, 365);
            this.btnStopServiceNext.Name = "btnStopServiceNext";
            this.btnStopServiceNext.Size = new System.Drawing.Size(75, 23);
            this.btnStopServiceNext.TabIndex = 2;
            this.btnStopServiceNext.Text = "Next >";
            this.btnStopServiceNext.UseVisualStyleBackColor = true;
            this.btnStopServiceNext.Click += new System.EventHandler(this.btnStopServiceNext_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Linen;
            this.panel2.Controls.Add(this.lblStopServicesTitle);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(214, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(459, 69);
            this.panel2.TabIndex = 5;
            // 
            // lblStopServicesTitle
            // 
            this.lblStopServicesTitle.AutoSize = true;
            this.lblStopServicesTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStopServicesTitle.Location = new System.Drawing.Point(10, 7);
            this.lblStopServicesTitle.Name = "lblStopServicesTitle";
            this.lblStopServicesTitle.Size = new System.Drawing.Size(80, 13);
            this.lblStopServicesTitle.TabIndex = 0;
            this.lblStopServicesTitle.Text = "Stop Service";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.pictureBox3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(214, 474);
            this.panel3.TabIndex = 3;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::EasyIceSetupWizard.Properties.Resources.EasyIceLogo;
            this.pictureBox3.Location = new System.Drawing.Point(0, 101);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(213, 250);
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // tpHostInformation
            // 
            this.tpHostInformation.Controls.Add(this.pnlHostInformationPageInformation);
            this.tpHostInformation.Controls.Add(this.pnlHostInformationContent);
            this.tpHostInformation.Controls.Add(this.pnlHostInformationSideLogo);
            this.tpHostInformation.Location = new System.Drawing.Point(4, 22);
            this.tpHostInformation.Name = "tpHostInformation";
            this.tpHostInformation.Padding = new System.Windows.Forms.Padding(3);
            this.tpHostInformation.Size = new System.Drawing.Size(673, 474);
            this.tpHostInformation.TabIndex = 1;
            this.tpHostInformation.Text = "Host Information";
            this.tpHostInformation.UseVisualStyleBackColor = true;
            // 
            // pnlHostInformationPageInformation
            // 
            this.pnlHostInformationPageInformation.BackColor = System.Drawing.Color.Linen;
            this.pnlHostInformationPageInformation.Controls.Add(this.lblHostInformationPageTitle);
            this.pnlHostInformationPageInformation.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHostInformationPageInformation.Location = new System.Drawing.Point(217, 3);
            this.pnlHostInformationPageInformation.Name = "pnlHostInformationPageInformation";
            this.pnlHostInformationPageInformation.Size = new System.Drawing.Size(453, 28);
            this.pnlHostInformationPageInformation.TabIndex = 4;
            // 
            // lblHostInformationPageTitle
            // 
            this.lblHostInformationPageTitle.AutoSize = true;
            this.lblHostInformationPageTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHostInformationPageTitle.Location = new System.Drawing.Point(10, 7);
            this.lblHostInformationPageTitle.Name = "lblHostInformationPageTitle";
            this.lblHostInformationPageTitle.Size = new System.Drawing.Size(100, 13);
            this.lblHostInformationPageTitle.TabIndex = 0;
            this.lblHostInformationPageTitle.Text = "Host Information";
            // 
            // pnlHostInformationContent
            // 
            this.pnlHostInformationContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHostInformationContent.Controls.Add(this.wizardPageHostType);
            this.pnlHostInformationContent.Controls.Add(this.txtHostInformationServerTypeInfo);
            this.pnlHostInformationContent.Controls.Add(this.rbHostInformationIIS);
            this.pnlHostInformationContent.Controls.Add(this.rbHostInformationWindowsService);
            this.pnlHostInformationContent.Controls.Add(this.lblHostInformationServerHostType);
            this.pnlHostInformationContent.Controls.Add(this.btnHostInformationBack);
            this.pnlHostInformationContent.Controls.Add(this.btnHostInformationNext);
            this.pnlHostInformationContent.Location = new System.Drawing.Point(217, 37);
            this.pnlHostInformationContent.Name = "pnlHostInformationContent";
            this.pnlHostInformationContent.Size = new System.Drawing.Size(453, 434);
            this.pnlHostInformationContent.TabIndex = 5;
            // 
            // wizardPageHostType
            // 
            this.wizardPageHostType.Controls.Add(this.tabPage1);
            this.wizardPageHostType.Controls.Add(this.tpHostInformationWindowsService);
            this.wizardPageHostType.Controls.Add(this.tpHostInformationIIS);
            this.wizardPageHostType.Location = new System.Drawing.Point(7, 93);
            this.wizardPageHostType.Name = "wizardPageHostType";
            this.wizardPageHostType.SelectedIndex = 0;
            this.wizardPageHostType.Size = new System.Drawing.Size(440, 307);
            this.wizardPageHostType.TabIndex = 8;
            this.wizardPageHostType.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(432, 281);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tpHostInformationWindowsService
            // 
            this.tpHostInformationWindowsService.Controls.Add(this.lblHostInformationWindowsServiceURL);
            this.tpHostInformationWindowsService.Controls.Add(this.lblHostInformationWindowsServiceAccessInfo);
            this.tpHostInformationWindowsService.Controls.Add(this.txtHostInformationWindowsServiceDomain);
            this.tpHostInformationWindowsService.Controls.Add(this.lblHostInformationWindowsServiceDomain);
            this.tpHostInformationWindowsService.Controls.Add(this.nudHostInformationWindowsServicePort);
            this.tpHostInformationWindowsService.Controls.Add(this.lblHostInformationWindowsServicePort);
            this.tpHostInformationWindowsService.Controls.Add(this.txtHostInformationWindowsServicePassword);
            this.tpHostInformationWindowsService.Controls.Add(this.lblHostInformationWindowsServicePassword);
            this.tpHostInformationWindowsService.Controls.Add(this.lblHostInformationWindowsServiceInfo);
            this.tpHostInformationWindowsService.Controls.Add(this.txtHostInformationWindowsServiceUsername);
            this.tpHostInformationWindowsService.Controls.Add(this.lblHostInformationWindowsServiceUsername);
            this.tpHostInformationWindowsService.Location = new System.Drawing.Point(4, 22);
            this.tpHostInformationWindowsService.Name = "tpHostInformationWindowsService";
            this.tpHostInformationWindowsService.Padding = new System.Windows.Forms.Padding(3);
            this.tpHostInformationWindowsService.Size = new System.Drawing.Size(432, 281);
            this.tpHostInformationWindowsService.TabIndex = 0;
            this.tpHostInformationWindowsService.Text = "Windows Service";
            this.tpHostInformationWindowsService.UseVisualStyleBackColor = true;
            // 
            // lblHostInformationWindowsServiceURL
            // 
            this.lblHostInformationWindowsServiceURL.AutoSize = true;
            this.lblHostInformationWindowsServiceURL.Location = new System.Drawing.Point(8, 161);
            this.lblHostInformationWindowsServiceURL.Name = "lblHostInformationWindowsServiceURL";
            this.lblHostInformationWindowsServiceURL.Size = new System.Drawing.Size(294, 13);
            this.lblHostInformationWindowsServiceURL.TabIndex = 10;
            this.lblHostInformationWindowsServiceURL.TabStop = true;
            this.lblHostInformationWindowsServiceURL.Text = "http://localhost:22859/EasyIceService/RestServiceImpl.svc";
            this.lblHostInformationWindowsServiceURL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblHostInformationWindowsServiceURL_LinkClicked);
            // 
            // lblHostInformationWindowsServiceAccessInfo
            // 
            this.lblHostInformationWindowsServiceAccessInfo.AutoSize = true;
            this.lblHostInformationWindowsServiceAccessInfo.Location = new System.Drawing.Point(8, 148);
            this.lblHostInformationWindowsServiceAccessInfo.Name = "lblHostInformationWindowsServiceAccessInfo";
            this.lblHostInformationWindowsServiceAccessInfo.Size = new System.Drawing.Size(135, 13);
            this.lblHostInformationWindowsServiceAccessInfo.TabIndex = 9;
            this.lblHostInformationWindowsServiceAccessInfo.Text = "Service will be available at:";
            // 
            // txtHostInformationWindowsServiceDomain
            // 
            this.txtHostInformationWindowsServiceDomain.Location = new System.Drawing.Point(79, 40);
            this.txtHostInformationWindowsServiceDomain.Name = "txtHostInformationWindowsServiceDomain";
            this.txtHostInformationWindowsServiceDomain.Size = new System.Drawing.Size(178, 20);
            this.txtHostInformationWindowsServiceDomain.TabIndex = 2;
            this.txtHostInformationWindowsServiceDomain.Text = "INFERNO";
            this.txtHostInformationWindowsServiceDomain.TextChanged += new System.EventHandler(this.txtHostInformationWindowsServiceDomain_TextChanged);
            // 
            // lblHostInformationWindowsServiceDomain
            // 
            this.lblHostInformationWindowsServiceDomain.AutoSize = true;
            this.lblHostInformationWindowsServiceDomain.Location = new System.Drawing.Point(27, 43);
            this.lblHostInformationWindowsServiceDomain.Name = "lblHostInformationWindowsServiceDomain";
            this.lblHostInformationWindowsServiceDomain.Size = new System.Drawing.Size(46, 13);
            this.lblHostInformationWindowsServiceDomain.TabIndex = 8;
            this.lblHostInformationWindowsServiceDomain.Text = "Domain:";
            // 
            // nudHostInformationWindowsServicePort
            // 
            this.nudHostInformationWindowsServicePort.Location = new System.Drawing.Point(79, 118);
            this.nudHostInformationWindowsServicePort.Maximum = new decimal(new int[] {
            65534,
            0,
            0,
            0});
            this.nudHostInformationWindowsServicePort.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nudHostInformationWindowsServicePort.Name = "nudHostInformationWindowsServicePort";
            this.nudHostInformationWindowsServicePort.Size = new System.Drawing.Size(54, 20);
            this.nudHostInformationWindowsServicePort.TabIndex = 5;
            this.nudHostInformationWindowsServicePort.Value = new decimal(new int[] {
            22859,
            0,
            0,
            0});
            this.nudHostInformationWindowsServicePort.ValueChanged += new System.EventHandler(this.nudHostInformationWebServicesPort_ValueChanged);
            // 
            // lblHostInformationWindowsServicePort
            // 
            this.lblHostInformationWindowsServicePort.AutoSize = true;
            this.lblHostInformationWindowsServicePort.Location = new System.Drawing.Point(5, 120);
            this.lblHostInformationWindowsServicePort.Name = "lblHostInformationWindowsServicePort";
            this.lblHostInformationWindowsServicePort.Size = new System.Drawing.Size(68, 13);
            this.lblHostInformationWindowsServicePort.TabIndex = 6;
            this.lblHostInformationWindowsServicePort.Text = "Service Port:";
            // 
            // txtHostInformationWindowsServicePassword
            // 
            this.txtHostInformationWindowsServicePassword.Location = new System.Drawing.Point(79, 92);
            this.txtHostInformationWindowsServicePassword.Name = "txtHostInformationWindowsServicePassword";
            this.txtHostInformationWindowsServicePassword.PasswordChar = '*';
            this.txtHostInformationWindowsServicePassword.Size = new System.Drawing.Size(178, 20);
            this.txtHostInformationWindowsServicePassword.TabIndex = 4;
            this.txtHostInformationWindowsServicePassword.Text = "Vero052412";
            this.txtHostInformationWindowsServicePassword.TextChanged += new System.EventHandler(this.txtHostInformationWindowsServicePassword_TextChanged);
            // 
            // lblHostInformationWindowsServicePassword
            // 
            this.lblHostInformationWindowsServicePassword.AutoSize = true;
            this.lblHostInformationWindowsServicePassword.Location = new System.Drawing.Point(17, 95);
            this.lblHostInformationWindowsServicePassword.Name = "lblHostInformationWindowsServicePassword";
            this.lblHostInformationWindowsServicePassword.Size = new System.Drawing.Size(56, 13);
            this.lblHostInformationWindowsServicePassword.TabIndex = 3;
            this.lblHostInformationWindowsServicePassword.Text = "Password:";
            // 
            // lblHostInformationWindowsServiceInfo
            // 
            this.lblHostInformationWindowsServiceInfo.AutoSize = true;
            this.lblHostInformationWindowsServiceInfo.Location = new System.Drawing.Point(6, 24);
            this.lblHostInformationWindowsServiceInfo.Name = "lblHostInformationWindowsServiceInfo";
            this.lblHostInformationWindowsServiceInfo.Size = new System.Drawing.Size(400, 13);
            this.lblHostInformationWindowsServiceInfo.TabIndex = 2;
            this.lblHostInformationWindowsServiceInfo.Text = "Enter the credentials of the user who will run the service (needs to be a local a" +
    "dmin)";
            // 
            // txtHostInformationWindowsServiceUsername
            // 
            this.txtHostInformationWindowsServiceUsername.Location = new System.Drawing.Point(79, 66);
            this.txtHostInformationWindowsServiceUsername.Name = "txtHostInformationWindowsServiceUsername";
            this.txtHostInformationWindowsServiceUsername.Size = new System.Drawing.Size(178, 20);
            this.txtHostInformationWindowsServiceUsername.TabIndex = 3;
            this.txtHostInformationWindowsServiceUsername.Text = "Pierrick";
            this.txtHostInformationWindowsServiceUsername.TextChanged += new System.EventHandler(this.txtHostInformationWindowsServiceUsername_TextChanged);
            // 
            // lblHostInformationWindowsServiceUsername
            // 
            this.lblHostInformationWindowsServiceUsername.AutoSize = true;
            this.lblHostInformationWindowsServiceUsername.Location = new System.Drawing.Point(15, 69);
            this.lblHostInformationWindowsServiceUsername.Name = "lblHostInformationWindowsServiceUsername";
            this.lblHostInformationWindowsServiceUsername.Size = new System.Drawing.Size(58, 13);
            this.lblHostInformationWindowsServiceUsername.TabIndex = 0;
            this.lblHostInformationWindowsServiceUsername.Text = "Username:";
            // 
            // tpHostInformationIIS
            // 
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISMachineURL);
            this.tpHostInformationIIS.Controls.Add(this.txtHostInformationIISDomain);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISDomain);
            this.tpHostInformationIIS.Controls.Add(this.txtHostInformationIISPassword);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISPassword);
            this.tpHostInformationIIS.Controls.Add(this.txtHostInformationIISUsername);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISUsername);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISIPAddress);
            this.tpHostInformationIIS.Controls.Add(this.cbHostInformationIISIPAddress);
            this.tpHostInformationIIS.Controls.Add(this.btnHostInformationIISBrowsePath);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISURL);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISServiceAccessInfo);
            this.tpHostInformationIIS.Controls.Add(this.txtHostInformationIISWebSiteName);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISWebSiteName);
            this.tpHostInformationIIS.Controls.Add(this.nudHostInformationIISPort);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISPort);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISInfo);
            this.tpHostInformationIIS.Controls.Add(this.txtHostInformationIISPath);
            this.tpHostInformationIIS.Controls.Add(this.lblHostInformationIISPath);
            this.tpHostInformationIIS.Location = new System.Drawing.Point(4, 22);
            this.tpHostInformationIIS.Name = "tpHostInformationIIS";
            this.tpHostInformationIIS.Padding = new System.Windows.Forms.Padding(3);
            this.tpHostInformationIIS.Size = new System.Drawing.Size(432, 281);
            this.tpHostInformationIIS.TabIndex = 1;
            this.tpHostInformationIIS.Text = "IIS";
            this.tpHostInformationIIS.UseVisualStyleBackColor = true;
            // 
            // lblHostInformationIISMachineURL
            // 
            this.lblHostInformationIISMachineURL.AutoSize = true;
            this.lblHostInformationIISMachineURL.Location = new System.Drawing.Point(6, 251);
            this.lblHostInformationIISMachineURL.Name = "lblHostInformationIISMachineURL";
            this.lblHostInformationIISMachineURL.Size = new System.Drawing.Size(333, 13);
            this.lblHostInformationIISMachineURL.TabIndex = 32;
            this.lblHostInformationIISMachineURL.TabStop = true;
            this.lblHostInformationIISMachineURL.Text = "http://<MachineName>:22859/EasyIceService/RestServiceImpl.svc";
            // 
            // txtHostInformationIISDomain
            // 
            this.txtHostInformationIISDomain.Location = new System.Drawing.Point(97, 146);
            this.txtHostInformationIISDomain.Name = "txtHostInformationIISDomain";
            this.txtHostInformationIISDomain.Size = new System.Drawing.Size(178, 20);
            this.txtHostInformationIISDomain.TabIndex = 26;
            this.txtHostInformationIISDomain.TextChanged += new System.EventHandler(this.txtHostInformationIISDomain_TextChanged);
            // 
            // lblHostInformationIISDomain
            // 
            this.lblHostInformationIISDomain.AutoSize = true;
            this.lblHostInformationIISDomain.Location = new System.Drawing.Point(45, 149);
            this.lblHostInformationIISDomain.Name = "lblHostInformationIISDomain";
            this.lblHostInformationIISDomain.Size = new System.Drawing.Size(46, 13);
            this.lblHostInformationIISDomain.TabIndex = 30;
            this.lblHostInformationIISDomain.Text = "Domain:";
            // 
            // txtHostInformationIISPassword
            // 
            this.txtHostInformationIISPassword.Location = new System.Drawing.Point(97, 198);
            this.txtHostInformationIISPassword.Name = "txtHostInformationIISPassword";
            this.txtHostInformationIISPassword.PasswordChar = '*';
            this.txtHostInformationIISPassword.Size = new System.Drawing.Size(178, 20);
            this.txtHostInformationIISPassword.TabIndex = 29;
            this.txtHostInformationIISPassword.TextChanged += new System.EventHandler(this.txtHostInformationIISPassword_TextChanged);
            // 
            // lblHostInformationIISPassword
            // 
            this.lblHostInformationIISPassword.AutoSize = true;
            this.lblHostInformationIISPassword.Location = new System.Drawing.Point(35, 201);
            this.lblHostInformationIISPassword.Name = "lblHostInformationIISPassword";
            this.lblHostInformationIISPassword.Size = new System.Drawing.Size(56, 13);
            this.lblHostInformationIISPassword.TabIndex = 28;
            this.lblHostInformationIISPassword.Text = "Password:";
            // 
            // txtHostInformationIISUsername
            // 
            this.txtHostInformationIISUsername.Location = new System.Drawing.Point(97, 172);
            this.txtHostInformationIISUsername.Name = "txtHostInformationIISUsername";
            this.txtHostInformationIISUsername.Size = new System.Drawing.Size(178, 20);
            this.txtHostInformationIISUsername.TabIndex = 27;
            this.txtHostInformationIISUsername.TextChanged += new System.EventHandler(this.txtHostInformationIISUsername_TextChanged);
            // 
            // lblHostInformationIISUsername
            // 
            this.lblHostInformationIISUsername.AutoSize = true;
            this.lblHostInformationIISUsername.Location = new System.Drawing.Point(33, 175);
            this.lblHostInformationIISUsername.Name = "lblHostInformationIISUsername";
            this.lblHostInformationIISUsername.Size = new System.Drawing.Size(58, 13);
            this.lblHostInformationIISUsername.TabIndex = 25;
            this.lblHostInformationIISUsername.Text = "Username:";
            // 
            // lblHostInformationIISIPAddress
            // 
            this.lblHostInformationIISIPAddress.AutoSize = true;
            this.lblHostInformationIISIPAddress.Location = new System.Drawing.Point(30, 70);
            this.lblHostInformationIISIPAddress.Name = "lblHostInformationIISIPAddress";
            this.lblHostInformationIISIPAddress.Size = new System.Drawing.Size(61, 13);
            this.lblHostInformationIISIPAddress.TabIndex = 24;
            this.lblHostInformationIISIPAddress.Text = "IP Address:";
            // 
            // cbHostInformationIISIPAddress
            // 
            this.cbHostInformationIISIPAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHostInformationIISIPAddress.FormattingEnabled = true;
            this.cbHostInformationIISIPAddress.Location = new System.Drawing.Point(97, 67);
            this.cbHostInformationIISIPAddress.Name = "cbHostInformationIISIPAddress";
            this.cbHostInformationIISIPAddress.Size = new System.Drawing.Size(296, 21);
            this.cbHostInformationIISIPAddress.TabIndex = 23;
            // 
            // btnHostInformationIISBrowsePath
            // 
            this.btnHostInformationIISBrowsePath.Location = new System.Drawing.Point(399, 118);
            this.btnHostInformationIISBrowsePath.Name = "btnHostInformationIISBrowsePath";
            this.btnHostInformationIISBrowsePath.Size = new System.Drawing.Size(27, 23);
            this.btnHostInformationIISBrowsePath.TabIndex = 22;
            this.btnHostInformationIISBrowsePath.Text = "...";
            this.btnHostInformationIISBrowsePath.UseVisualStyleBackColor = true;
            this.btnHostInformationIISBrowsePath.Click += new System.EventHandler(this.btnHostInformationIISBrowsePath_Click);
            // 
            // lblHostInformationIISURL
            // 
            this.lblHostInformationIISURL.AutoSize = true;
            this.lblHostInformationIISURL.Location = new System.Drawing.Point(6, 238);
            this.lblHostInformationIISURL.Name = "lblHostInformationIISURL";
            this.lblHostInformationIISURL.Size = new System.Drawing.Size(294, 13);
            this.lblHostInformationIISURL.TabIndex = 21;
            this.lblHostInformationIISURL.TabStop = true;
            this.lblHostInformationIISURL.Text = "http://localhost:22859/EasyIceService/RestServiceImpl.svc";
            this.lblHostInformationIISURL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblHostInformationIISURL_LinkClicked);
            // 
            // lblHostInformationIISServiceAccessInfo
            // 
            this.lblHostInformationIISServiceAccessInfo.AutoSize = true;
            this.lblHostInformationIISServiceAccessInfo.Location = new System.Drawing.Point(6, 225);
            this.lblHostInformationIISServiceAccessInfo.Name = "lblHostInformationIISServiceAccessInfo";
            this.lblHostInformationIISServiceAccessInfo.Size = new System.Drawing.Size(248, 13);
            this.lblHostInformationIISServiceAccessInfo.TabIndex = 20;
            this.lblHostInformationIISServiceAccessInfo.Text = "Service will be available at the following addresses:";
            // 
            // txtHostInformationIISWebSiteName
            // 
            this.txtHostInformationIISWebSiteName.Location = new System.Drawing.Point(97, 40);
            this.txtHostInformationIISWebSiteName.Name = "txtHostInformationIISWebSiteName";
            this.txtHostInformationIISWebSiteName.Size = new System.Drawing.Size(296, 20);
            this.txtHostInformationIISWebSiteName.TabIndex = 13;
            this.txtHostInformationIISWebSiteName.Text = "EasyIceService";
            this.txtHostInformationIISWebSiteName.TextChanged += new System.EventHandler(this.txtHostInformationIISWebSiteName_TextChanged);
            // 
            // lblHostInformationIISWebSiteName
            // 
            this.lblHostInformationIISWebSiteName.AutoSize = true;
            this.lblHostInformationIISWebSiteName.Location = new System.Drawing.Point(6, 43);
            this.lblHostInformationIISWebSiteName.Name = "lblHostInformationIISWebSiteName";
            this.lblHostInformationIISWebSiteName.Size = new System.Drawing.Size(85, 13);
            this.lblHostInformationIISWebSiteName.TabIndex = 19;
            this.lblHostInformationIISWebSiteName.Text = "Web Site Name:";
            // 
            // nudHostInformationIISPort
            // 
            this.nudHostInformationIISPort.Location = new System.Drawing.Point(97, 94);
            this.nudHostInformationIISPort.Maximum = new decimal(new int[] {
            65534,
            0,
            0,
            0});
            this.nudHostInformationIISPort.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nudHostInformationIISPort.Name = "nudHostInformationIISPort";
            this.nudHostInformationIISPort.Size = new System.Drawing.Size(54, 20);
            this.nudHostInformationIISPort.TabIndex = 17;
            this.nudHostInformationIISPort.Value = new decimal(new int[] {
            22859,
            0,
            0,
            0});
            this.nudHostInformationIISPort.ValueChanged += new System.EventHandler(this.nudHostInformationIISPort_ValueChanged);
            // 
            // lblHostInformationIISPort
            // 
            this.lblHostInformationIISPort.AutoSize = true;
            this.lblHostInformationIISPort.Location = new System.Drawing.Point(62, 96);
            this.lblHostInformationIISPort.Name = "lblHostInformationIISPort";
            this.lblHostInformationIISPort.Size = new System.Drawing.Size(29, 13);
            this.lblHostInformationIISPort.TabIndex = 18;
            this.lblHostInformationIISPort.Text = "Port:";
            // 
            // lblHostInformationIISInfo
            // 
            this.lblHostInformationIISInfo.AutoSize = true;
            this.lblHostInformationIISInfo.Location = new System.Drawing.Point(6, 24);
            this.lblHostInformationIISInfo.Name = "lblHostInformationIISInfo";
            this.lblHostInformationIISInfo.Size = new System.Drawing.Size(102, 13);
            this.lblHostInformationIISInfo.TabIndex = 12;
            this.lblHostInformationIISInfo.Text = "Enter the IIS details:";
            // 
            // txtHostInformationIISPath
            // 
            this.txtHostInformationIISPath.Location = new System.Drawing.Point(97, 120);
            this.txtHostInformationIISPath.Name = "txtHostInformationIISPath";
            this.txtHostInformationIISPath.Size = new System.Drawing.Size(296, 20);
            this.txtHostInformationIISPath.TabIndex = 15;
            this.txtHostInformationIISPath.TextChanged += new System.EventHandler(this.txtHostInformationIISPath_TextChanged);
            // 
            // lblHostInformationIISPath
            // 
            this.lblHostInformationIISPath.AutoSize = true;
            this.lblHostInformationIISPath.Location = new System.Drawing.Point(59, 123);
            this.lblHostInformationIISPath.Name = "lblHostInformationIISPath";
            this.lblHostInformationIISPath.Size = new System.Drawing.Size(32, 13);
            this.lblHostInformationIISPath.TabIndex = 11;
            this.lblHostInformationIISPath.Text = "Path:";
            // 
            // txtHostInformationServerTypeInfo
            // 
            this.txtHostInformationServerTypeInfo.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtHostInformationServerTypeInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtHostInformationServerTypeInfo.Location = new System.Drawing.Point(6, 30);
            this.txtHostInformationServerTypeInfo.Name = "txtHostInformationServerTypeInfo";
            this.txtHostInformationServerTypeInfo.ReadOnly = true;
            this.txtHostInformationServerTypeInfo.Size = new System.Drawing.Size(441, 57);
            this.txtHostInformationServerTypeInfo.TabIndex = 7;
            this.txtHostInformationServerTypeInfo.Text = "";
            // 
            // rbHostInformationIIS
            // 
            this.rbHostInformationIIS.AutoSize = true;
            this.rbHostInformationIIS.Location = new System.Drawing.Point(230, 7);
            this.rbHostInformationIIS.Name = "rbHostInformationIIS";
            this.rbHostInformationIIS.Size = new System.Drawing.Size(38, 17);
            this.rbHostInformationIIS.TabIndex = 1;
            this.rbHostInformationIIS.TabStop = true;
            this.rbHostInformationIIS.Text = "IIS";
            this.rbHostInformationIIS.UseVisualStyleBackColor = true;
            this.rbHostInformationIIS.CheckedChanged += new System.EventHandler(this.rbHostInformationIIS_CheckedChanged);
            // 
            // rbHostInformationWindowsService
            // 
            this.rbHostInformationWindowsService.AutoSize = true;
            this.rbHostInformationWindowsService.Location = new System.Drawing.Point(116, 7);
            this.rbHostInformationWindowsService.Name = "rbHostInformationWindowsService";
            this.rbHostInformationWindowsService.Size = new System.Drawing.Size(108, 17);
            this.rbHostInformationWindowsService.TabIndex = 0;
            this.rbHostInformationWindowsService.TabStop = true;
            this.rbHostInformationWindowsService.Text = "Windows Service";
            this.rbHostInformationWindowsService.UseVisualStyleBackColor = true;
            this.rbHostInformationWindowsService.CheckedChanged += new System.EventHandler(this.rbHostInformationWindowsService_CheckedChanged);
            // 
            // lblHostInformationServerHostType
            // 
            this.lblHostInformationServerHostType.AutoSize = true;
            this.lblHostInformationServerHostType.Location = new System.Drawing.Point(17, 9);
            this.lblHostInformationServerHostType.Name = "lblHostInformationServerHostType";
            this.lblHostInformationServerHostType.Size = new System.Drawing.Size(93, 13);
            this.lblHostInformationServerHostType.TabIndex = 4;
            this.lblHostInformationServerHostType.Text = "Server Host Type:";
            // 
            // btnHostInformationBack
            // 
            this.btnHostInformationBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHostInformationBack.Location = new System.Drawing.Point(292, 406);
            this.btnHostInformationBack.Name = "btnHostInformationBack";
            this.btnHostInformationBack.Size = new System.Drawing.Size(75, 23);
            this.btnHostInformationBack.TabIndex = 3;
            this.btnHostInformationBack.Text = "< Back";
            this.btnHostInformationBack.UseVisualStyleBackColor = true;
            this.btnHostInformationBack.Click += new System.EventHandler(this.btnHostInformationBack_Click);
            // 
            // btnHostInformationNext
            // 
            this.btnHostInformationNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHostInformationNext.Enabled = false;
            this.btnHostInformationNext.Location = new System.Drawing.Point(373, 406);
            this.btnHostInformationNext.Name = "btnHostInformationNext";
            this.btnHostInformationNext.Size = new System.Drawing.Size(75, 23);
            this.btnHostInformationNext.TabIndex = 2;
            this.btnHostInformationNext.Text = "Next >";
            this.btnHostInformationNext.UseVisualStyleBackColor = true;
            this.btnHostInformationNext.Click += new System.EventHandler(this.btnHostInformationNext_Click);
            // 
            // pnlHostInformationSideLogo
            // 
            this.pnlHostInformationSideLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlHostInformationSideLogo.Controls.Add(this.pictureBox2);
            this.pnlHostInformationSideLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlHostInformationSideLogo.Location = new System.Drawing.Point(3, 3);
            this.pnlHostInformationSideLogo.Name = "pnlHostInformationSideLogo";
            this.pnlHostInformationSideLogo.Size = new System.Drawing.Size(214, 468);
            this.pnlHostInformationSideLogo.TabIndex = 3;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::EasyIceSetupWizard.Properties.Resources.EasyIceLogo;
            this.pictureBox2.Location = new System.Drawing.Point(0, 101);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(213, 250);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // tpCICCredentials
            // 
            this.tpCICCredentials.Controls.Add(this.panel8);
            this.tpCICCredentials.Controls.Add(this.panel10);
            this.tpCICCredentials.Controls.Add(this.panel11);
            this.tpCICCredentials.Location = new System.Drawing.Point(4, 22);
            this.tpCICCredentials.Name = "tpCICCredentials";
            this.tpCICCredentials.Padding = new System.Windows.Forms.Padding(3);
            this.tpCICCredentials.Size = new System.Drawing.Size(673, 474);
            this.tpCICCredentials.TabIndex = 2;
            this.tpCICCredentials.Text = "CIC Credentials";
            this.tpCICCredentials.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Linen;
            this.panel8.Controls.Add(this.lblCICCredentialsTitle);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(217, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(453, 28);
            this.panel8.TabIndex = 7;
            // 
            // lblCICCredentialsTitle
            // 
            this.lblCICCredentialsTitle.AutoSize = true;
            this.lblCICCredentialsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCICCredentialsTitle.Location = new System.Drawing.Point(10, 7);
            this.lblCICCredentialsTitle.Name = "lblCICCredentialsTitle";
            this.lblCICCredentialsTitle.Size = new System.Drawing.Size(94, 13);
            this.lblCICCredentialsTitle.TabIndex = 0;
            this.lblCICCredentialsTitle.Text = "CIC Credentials";
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel10.Controls.Add(this.lblCICCredentialsDetails);
            this.panel10.Controls.Add(this.txtCICCredentialsCICPassword);
            this.panel10.Controls.Add(this.txtCICCredentialsCICUsername);
            this.panel10.Controls.Add(this.txtCICCredentialsCICServer);
            this.panel10.Controls.Add(this.lblCICCredentialsCICServer);
            this.panel10.Controls.Add(this.lblCICCredentialsCICPassword);
            this.panel10.Controls.Add(this.lblCICCredentialsCICUsername);
            this.panel10.Controls.Add(this.btnCICCredentialsBack);
            this.panel10.Controls.Add(this.btnCICCredentialsNext);
            this.panel10.Location = new System.Drawing.Point(217, 37);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(453, 434);
            this.panel10.TabIndex = 8;
            // 
            // lblCICCredentialsDetails
            // 
            this.lblCICCredentialsDetails.AutoSize = true;
            this.lblCICCredentialsDetails.Location = new System.Drawing.Point(32, 15);
            this.lblCICCredentialsDetails.Name = "lblCICCredentialsDetails";
            this.lblCICCredentialsDetails.Size = new System.Drawing.Size(271, 13);
            this.lblCICCredentialsDetails.TabIndex = 10;
            this.lblCICCredentialsDetails.Text = "Please enter the credentials to access the CIC server(s):";
            // 
            // txtCICCredentialsCICPassword
            // 
            this.txtCICCredentialsCICPassword.Location = new System.Drawing.Point(113, 96);
            this.txtCICCredentialsCICPassword.Name = "txtCICCredentialsCICPassword";
            this.txtCICCredentialsCICPassword.PasswordChar = '*';
            this.txtCICCredentialsCICPassword.Size = new System.Drawing.Size(185, 20);
            this.txtCICCredentialsCICPassword.TabIndex = 9;
            this.txtCICCredentialsCICPassword.Text = "1234";
            this.txtCICCredentialsCICPassword.TextChanged += new System.EventHandler(this.txtCICCredentialsCICPassword_TextChanged);
            // 
            // txtCICCredentialsCICUsername
            // 
            this.txtCICCredentialsCICUsername.Location = new System.Drawing.Point(113, 70);
            this.txtCICCredentialsCICUsername.Name = "txtCICCredentialsCICUsername";
            this.txtCICCredentialsCICUsername.Size = new System.Drawing.Size(185, 20);
            this.txtCICCredentialsCICUsername.TabIndex = 8;
            this.txtCICCredentialsCICUsername.Text = "devlab_user";
            this.txtCICCredentialsCICUsername.TextChanged += new System.EventHandler(this.txtCICCredentialsCICUsername_TextChanged);
            // 
            // txtCICCredentialsCICServer
            // 
            this.txtCICCredentialsCICServer.Location = new System.Drawing.Point(113, 44);
            this.txtCICCredentialsCICServer.Name = "txtCICCredentialsCICServer";
            this.txtCICCredentialsCICServer.Size = new System.Drawing.Size(185, 20);
            this.txtCICCredentialsCICServer.TabIndex = 7;
            this.txtCICCredentialsCICServer.Text = "pierre-ezscript";
            this.txtCICCredentialsCICServer.TextChanged += new System.EventHandler(this.txtCICCredentialsCICServer_TextChanged);
            // 
            // lblCICCredentialsCICServer
            // 
            this.lblCICCredentialsCICServer.AutoSize = true;
            this.lblCICCredentialsCICServer.Location = new System.Drawing.Point(46, 47);
            this.lblCICCredentialsCICServer.Name = "lblCICCredentialsCICServer";
            this.lblCICCredentialsCICServer.Size = new System.Drawing.Size(61, 13);
            this.lblCICCredentialsCICServer.TabIndex = 6;
            this.lblCICCredentialsCICServer.Text = "CIC Server:";
            // 
            // lblCICCredentialsCICPassword
            // 
            this.lblCICCredentialsCICPassword.AutoSize = true;
            this.lblCICCredentialsCICPassword.Location = new System.Drawing.Point(31, 99);
            this.lblCICCredentialsCICPassword.Name = "lblCICCredentialsCICPassword";
            this.lblCICCredentialsCICPassword.Size = new System.Drawing.Size(76, 13);
            this.lblCICCredentialsCICPassword.TabIndex = 5;
            this.lblCICCredentialsCICPassword.Text = "CIC Password:";
            // 
            // lblCICCredentialsCICUsername
            // 
            this.lblCICCredentialsCICUsername.AutoSize = true;
            this.lblCICCredentialsCICUsername.Location = new System.Drawing.Point(29, 73);
            this.lblCICCredentialsCICUsername.Name = "lblCICCredentialsCICUsername";
            this.lblCICCredentialsCICUsername.Size = new System.Drawing.Size(78, 13);
            this.lblCICCredentialsCICUsername.TabIndex = 4;
            this.lblCICCredentialsCICUsername.Text = "CIC Username:";
            // 
            // btnCICCredentialsBack
            // 
            this.btnCICCredentialsBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCICCredentialsBack.Location = new System.Drawing.Point(292, 406);
            this.btnCICCredentialsBack.Name = "btnCICCredentialsBack";
            this.btnCICCredentialsBack.Size = new System.Drawing.Size(75, 23);
            this.btnCICCredentialsBack.TabIndex = 3;
            this.btnCICCredentialsBack.Text = "< Back";
            this.btnCICCredentialsBack.UseVisualStyleBackColor = true;
            this.btnCICCredentialsBack.Click += new System.EventHandler(this.btnCICCredentialsBack_Click);
            // 
            // btnCICCredentialsNext
            // 
            this.btnCICCredentialsNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCICCredentialsNext.Enabled = false;
            this.btnCICCredentialsNext.Location = new System.Drawing.Point(373, 406);
            this.btnCICCredentialsNext.Name = "btnCICCredentialsNext";
            this.btnCICCredentialsNext.Size = new System.Drawing.Size(75, 23);
            this.btnCICCredentialsNext.TabIndex = 2;
            this.btnCICCredentialsNext.Text = "Next >";
            this.btnCICCredentialsNext.UseVisualStyleBackColor = true;
            this.btnCICCredentialsNext.Click += new System.EventHandler(this.btnCICCredentialsNext_Click);
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.pictureBox6);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(3, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(214, 468);
            this.panel11.TabIndex = 6;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::EasyIceSetupWizard.Properties.Resources.EasyIceLogo;
            this.pictureBox6.Location = new System.Drawing.Point(0, 101);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(213, 250);
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // tpSummary
            // 
            this.tpSummary.Controls.Add(this.panel4);
            this.tpSummary.Controls.Add(this.panel5);
            this.tpSummary.Controls.Add(this.panel6);
            this.tpSummary.Location = new System.Drawing.Point(4, 22);
            this.tpSummary.Name = "tpSummary";
            this.tpSummary.Size = new System.Drawing.Size(673, 474);
            this.tpSummary.TabIndex = 3;
            this.tpSummary.Text = "Summary";
            this.tpSummary.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Linen;
            this.panel4.Controls.Add(this.lblSummaryTitle);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(214, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(459, 69);
            this.panel4.TabIndex = 7;
            // 
            // lblSummaryTitle
            // 
            this.lblSummaryTitle.AutoSize = true;
            this.lblSummaryTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSummaryTitle.Location = new System.Drawing.Point(10, 7);
            this.lblSummaryTitle.Name = "lblSummaryTitle";
            this.lblSummaryTitle.Size = new System.Drawing.Size(57, 13);
            this.lblSummaryTitle.TabIndex = 0;
            this.lblSummaryTitle.Text = "Summary";
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.pbSummary);
            this.panel5.Controls.Add(this.txtSummaryDetails);
            this.panel5.Controls.Add(this.btnSummaryBack);
            this.panel5.Controls.Add(this.btnSummaryCommit);
            this.panel5.Location = new System.Drawing.Point(217, 78);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(453, 393);
            this.panel5.TabIndex = 8;
            // 
            // pbSummary
            // 
            this.pbSummary.Location = new System.Drawing.Point(3, 336);
            this.pbSummary.Name = "pbSummary";
            this.pbSummary.Size = new System.Drawing.Size(445, 23);
            this.pbSummary.TabIndex = 8;
            this.pbSummary.Visible = false;
            // 
            // txtSummaryDetails
            // 
            this.txtSummaryDetails.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtSummaryDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSummaryDetails.Location = new System.Drawing.Point(4, 3);
            this.txtSummaryDetails.Name = "txtSummaryDetails";
            this.txtSummaryDetails.ReadOnly = true;
            this.txtSummaryDetails.Size = new System.Drawing.Size(444, 332);
            this.txtSummaryDetails.TabIndex = 7;
            this.txtSummaryDetails.Text = "";
            // 
            // btnSummaryBack
            // 
            this.btnSummaryBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSummaryBack.Location = new System.Drawing.Point(294, 365);
            this.btnSummaryBack.Name = "btnSummaryBack";
            this.btnSummaryBack.Size = new System.Drawing.Size(75, 23);
            this.btnSummaryBack.TabIndex = 3;
            this.btnSummaryBack.Text = "< Back";
            this.btnSummaryBack.UseVisualStyleBackColor = true;
            this.btnSummaryBack.Click += new System.EventHandler(this.btnSummaryBack_Click);
            // 
            // btnSummaryCommit
            // 
            this.btnSummaryCommit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSummaryCommit.Location = new System.Drawing.Point(375, 365);
            this.btnSummaryCommit.Name = "btnSummaryCommit";
            this.btnSummaryCommit.Size = new System.Drawing.Size(75, 23);
            this.btnSummaryCommit.TabIndex = 2;
            this.btnSummaryCommit.Text = "Commit";
            this.btnSummaryCommit.UseVisualStyleBackColor = true;
            this.btnSummaryCommit.Click += new System.EventHandler(this.btnSummaryCommit_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.pictureBox4);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(214, 474);
            this.panel6.TabIndex = 6;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::EasyIceSetupWizard.Properties.Resources.EasyIceLogo;
            this.pictureBox4.Location = new System.Drawing.Point(0, 101);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(213, 250);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // tpFinish
            // 
            this.tpFinish.Controls.Add(this.panel7);
            this.tpFinish.Controls.Add(this.pnlFinishContents);
            this.tpFinish.Controls.Add(this.panel9);
            this.tpFinish.Location = new System.Drawing.Point(4, 22);
            this.tpFinish.Name = "tpFinish";
            this.tpFinish.Size = new System.Drawing.Size(673, 474);
            this.tpFinish.TabIndex = 4;
            this.tpFinish.Text = "Finish";
            this.tpFinish.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Linen;
            this.panel7.Controls.Add(this.lblFinishTitle);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(214, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(459, 69);
            this.panel7.TabIndex = 10;
            // 
            // lblFinishTitle
            // 
            this.lblFinishTitle.AutoSize = true;
            this.lblFinishTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinishTitle.Location = new System.Drawing.Point(10, 7);
            this.lblFinishTitle.Name = "lblFinishTitle";
            this.lblFinishTitle.Size = new System.Drawing.Size(57, 13);
            this.lblFinishTitle.TabIndex = 0;
            this.lblFinishTitle.Text = "Summary";
            // 
            // pnlFinishContents
            // 
            this.pnlFinishContents.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFinishContents.Controls.Add(this.llblFinishDoc);
            this.pnlFinishContents.Controls.Add(this.lblFinishDocPage);
            this.pnlFinishContents.Controls.Add(this.textBox1);
            this.pnlFinishContents.Controls.Add(this.llblFinishWSDL);
            this.pnlFinishContents.Controls.Add(this.lblFinishWSDLPage);
            this.pnlFinishContents.Controls.Add(this.lblFinishHelpPage);
            this.pnlFinishContents.Controls.Add(this.llblFinishHelpPage);
            this.pnlFinishContents.Controls.Add(this.txtFinishDetails);
            this.pnlFinishContents.Controls.Add(this.btnFinishBack);
            this.pnlFinishContents.Controls.Add(this.btnFinishClose);
            this.pnlFinishContents.Location = new System.Drawing.Point(217, 78);
            this.pnlFinishContents.Name = "pnlFinishContents";
            this.pnlFinishContents.Size = new System.Drawing.Size(453, 393);
            this.pnlFinishContents.TabIndex = 11;
            // 
            // llblFinishDoc
            // 
            this.llblFinishDoc.AutoSize = true;
            this.llblFinishDoc.Location = new System.Drawing.Point(92, 239);
            this.llblFinishDoc.Name = "llblFinishDoc";
            this.llblFinishDoc.Size = new System.Drawing.Size(182, 13);
            this.llblFinishDoc.TabIndex = 15;
            this.llblFinishDoc.TabStop = true;
            this.llblFinishDoc.Text = "http://easyiceservice.wordpress.com";
            this.llblFinishDoc.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llblFinishDoc_LinkClicked);
            // 
            // lblFinishDocPage
            // 
            this.lblFinishDocPage.AutoSize = true;
            this.lblFinishDocPage.Location = new System.Drawing.Point(4, 239);
            this.lblFinishDocPage.Name = "lblFinishDocPage";
            this.lblFinishDocPage.Size = new System.Drawing.Size(82, 13);
            this.lblFinishDocPage.TabIndex = 14;
            this.lblFinishDocPage.Text = "Documentation:";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(10, 292);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(438, 67);
            this.textBox1.TabIndex = 13;
            this.textBox1.Text = "Please note that the first attempt to connect to the service can take a few secon" +
    "ds while it logs in to CIC. Subsequent calls will be faster.";
            // 
            // llblFinishWSDL
            // 
            this.llblFinishWSDL.AutoSize = true;
            this.llblFinishWSDL.Location = new System.Drawing.Point(92, 226);
            this.llblFinishWSDL.Name = "llblFinishWSDL";
            this.llblFinishWSDL.Size = new System.Drawing.Size(321, 13);
            this.llblFinishWSDL.TabIndex = 11;
            this.llblFinishWSDL.TabStop = true;
            this.llblFinishWSDL.Text = "http://localhost:23589/EasyIceService/RestServiceImpl.svc?wsdl";
            this.llblFinishWSDL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llblFinishWSDL_LinkClicked);
            // 
            // lblFinishWSDLPage
            // 
            this.lblFinishWSDLPage.AutoSize = true;
            this.lblFinishWSDLPage.Location = new System.Drawing.Point(44, 226);
            this.lblFinishWSDLPage.Name = "lblFinishWSDLPage";
            this.lblFinishWSDLPage.Size = new System.Drawing.Size(42, 13);
            this.lblFinishWSDLPage.TabIndex = 10;
            this.lblFinishWSDLPage.Text = "WSDL:";
            // 
            // lblFinishHelpPage
            // 
            this.lblFinishHelpPage.AutoSize = true;
            this.lblFinishHelpPage.Location = new System.Drawing.Point(26, 213);
            this.lblFinishHelpPage.Name = "lblFinishHelpPage";
            this.lblFinishHelpPage.Size = new System.Drawing.Size(60, 13);
            this.lblFinishHelpPage.TabIndex = 9;
            this.lblFinishHelpPage.Text = "Help Page:";
            // 
            // llblFinishHelpPage
            // 
            this.llblFinishHelpPage.AutoSize = true;
            this.llblFinishHelpPage.Location = new System.Drawing.Point(92, 213);
            this.llblFinishHelpPage.Name = "llblFinishHelpPage";
            this.llblFinishHelpPage.Size = new System.Drawing.Size(319, 13);
            this.llblFinishHelpPage.TabIndex = 8;
            this.llblFinishHelpPage.TabStop = true;
            this.llblFinishHelpPage.Text = "http://localhost:23589/EasyIceService/RestServiceImpl.svc/help";
            this.llblFinishHelpPage.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llblFinishHelpPage_LinkClicked);
            // 
            // txtFinishDetails
            // 
            this.txtFinishDetails.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtFinishDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFinishDetails.Location = new System.Drawing.Point(0, 3);
            this.txtFinishDetails.Name = "txtFinishDetails";
            this.txtFinishDetails.ReadOnly = true;
            this.txtFinishDetails.Size = new System.Drawing.Size(453, 207);
            this.txtFinishDetails.TabIndex = 7;
            this.txtFinishDetails.Text = "";
            // 
            // btnFinishBack
            // 
            this.btnFinishBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinishBack.Location = new System.Drawing.Point(292, 365);
            this.btnFinishBack.Name = "btnFinishBack";
            this.btnFinishBack.Size = new System.Drawing.Size(75, 23);
            this.btnFinishBack.TabIndex = 3;
            this.btnFinishBack.Text = "< Back";
            this.btnFinishBack.UseVisualStyleBackColor = true;
            this.btnFinishBack.Click += new System.EventHandler(this.btnFinishBack_Click);
            // 
            // btnFinishClose
            // 
            this.btnFinishClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinishClose.Location = new System.Drawing.Point(373, 365);
            this.btnFinishClose.Name = "btnFinishClose";
            this.btnFinishClose.Size = new System.Drawing.Size(75, 23);
            this.btnFinishClose.TabIndex = 2;
            this.btnFinishClose.Text = "Close";
            this.btnFinishClose.UseVisualStyleBackColor = true;
            this.btnFinishClose.Click += new System.EventHandler(this.btnFinishClose_Click);
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.pictureBox5);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(214, 474);
            this.panel9.TabIndex = 9;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::EasyIceSetupWizard.Properties.Resources.EasyIceLogo;
            this.pictureBox5.Location = new System.Drawing.Point(0, 101);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(213, 250);
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 500);
            this.Controls.Add(this.wizardPages);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "EasyIceService Setup Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.wizardPages.ResumeLayout(false);
            this.tpWelcome.ResumeLayout(false);
            this.pnlWelcomePageInformation.ResumeLayout(false);
            this.pnlWelcomePageInformation.PerformLayout();
            this.pnlWelcomeContent.ResumeLayout(false);
            this.pnlWelcomeContent.PerformLayout();
            this.pnlWelcomeSideLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tpStopServices.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.wizardPageStopService.ResumeLayout(false);
            this.tpStopServiceWindowsService.ResumeLayout(false);
            this.tpStopServiceWindowsService.PerformLayout();
            this.tpStopServiceIIS.ResumeLayout(false);
            this.tpStopServiceIIS.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tpHostInformation.ResumeLayout(false);
            this.pnlHostInformationPageInformation.ResumeLayout(false);
            this.pnlHostInformationPageInformation.PerformLayout();
            this.pnlHostInformationContent.ResumeLayout(false);
            this.pnlHostInformationContent.PerformLayout();
            this.wizardPageHostType.ResumeLayout(false);
            this.tpHostInformationWindowsService.ResumeLayout(false);
            this.tpHostInformationWindowsService.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHostInformationWindowsServicePort)).EndInit();
            this.tpHostInformationIIS.ResumeLayout(false);
            this.tpHostInformationIIS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHostInformationIISPort)).EndInit();
            this.pnlHostInformationSideLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tpCICCredentials.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.tpSummary.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.tpFinish.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.pnlFinishContents.ResumeLayout(false);
            this.pnlFinishContents.PerformLayout();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }
        public MainForm()
        {
            InitializeComponent();
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show(this, "Are you sure you want to exit?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }

            }
        }
        #endregion

        #region Welcome Tab
        private void GoToWelcomeTab()
        {
            wizardPages.SelectTab(tpWelcome);
        }
        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            if (IsWindowsServiceInstalled() && (GetWindowsServiceStatus() == ServiceState.Run || GetWindowsServiceStatus() == ServiceState.Starting))
            {
                wizardPageStopService.SelectTab(tpStopServiceWindowsService);
                GoToStopServicesTab();
            }
            else if (IsIISInstalled() && IsWebSiteStarted())
            {
                wizardPageStopService.SelectTab(tpStopServiceIIS);
                GoToStopServicesTab();
            }
            else
            {
                GoToHostInformationTab();
            }
        }
        #endregion

        #region Stop Services Tab
        private void GoToStopServicesTab()
        {
            wizardPages.SelectTab(tpStopServices);
        }
        private void btnStopServiceBack_Click(object sender, EventArgs e)
        {
            GoToWelcomeTab();
        }
        private void btnStopServiceWindowsService_Click(object sender, EventArgs e)
        {
            if (StopWindowsService())
            {
                btnStopServiceNext.Enabled = true;
                GoToHostInformationTab();
            }
            else
            {
                MessageBox.Show(this, "Failed to stop the Easy Ice Windows service.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnStopServiceNext.Enabled = false;
            }
        }
        private void btnStopServiceIISStop_Click(object sender, EventArgs e)
        {

        }
        private bool IsWebSiteStarted()
        {
            //TODO Check if IIS is installed
            //TODO Check if web site is started
            return false;
        }
        private void btnStopServiceNext_Click(object sender, EventArgs e)
        {
            GoToHostInformationTab();
        }
        #endregion

        #region Host Information Tab
        private void GoToHostInformationTab()
        {
            wizardPages.SelectTab(tpHostInformation);
            ValidateWindowsServiceControls(); //In case controls are pre-filled for debugging purposes
            ValidateIISControls(); //In case controls are pre-filled for debugging purposes
        }
        private void btnHostInformationBack_Click(object sender, EventArgs e)
        {
            GoToWelcomeTab();
        }
        private void btnHostInformationNext_Click(object sender, EventArgs e)
        {
            if (wizardPageHostType.SelectedTab == tpHostInformationWindowsService)
            {
                if (!IsUserALocalAdmin(txtHostInformationWindowsServiceDomain.Text, txtHostInformationWindowsServiceUsername.Text, txtHostInformationWindowsServicePassword.Text))
                {
                    MessageBox.Show(this, "User is not a local administrator", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (!IsPortAvailable((int)nudHostInformationWindowsServicePort.Value))
                {
                    MessageBox.Show(this, String.Format("Port {0} is not available.", (int)nudHostInformationWindowsServicePort.Value));
                    return;
                }
                GoToCICCredentialsTab();
            }
            else if (wizardPageHostType.SelectedTab == tpHostInformationIIS)
            {
                if (!IsUserALocalAdmin(txtHostInformationIISDomain.Text, txtHostInformationIISUsername.Text, txtHostInformationIISPassword.Text))
                {
                    MessageBox.Show(this, "User is not a local administrator", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (!IsPortAvailable((int)nudHostInformationIISPort.Value))
                {
                    MessageBox.Show(this, String.Format("Port {0} is not available.", (int)nudHostInformationIISPort.Value));
                    return;
                }
                if (DoesWebSiteExist(txtHostInformationIISWebSiteName.Text, String.Format("{0}:{1}:", cbHostInformationIISIPAddress.SelectedItem.ToString(), (int)nudHostInformationIISPort.Value), txtHostInformationIISPath.Text))
                {
                    if (MessageBox.Show(this, String.Format("Web site {0} already exists. If you continue, the existing web site will be deleted.", txtHostInformationIISWebSiteName.Text), "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.Cancel)
                    {
                        return;
                    }
                }
                GoToCICCredentialsTab();
            }
            else
            {
                MessageBox.Show(this, "Please select a host type first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }
        private void rbHostInformationWindowsService_CheckedChanged(object sender, EventArgs e)
        {
            if (rbHostInformationWindowsService.Checked)
            {
                txtHostInformationServerTypeInfo.Text = "Does not require IIS.";
                wizardPageHostType.SelectTab(tpHostInformationWindowsService);
                UpdateWindowsServiceLink();
                txtHostInformationWindowsServiceDomain.Focus();
            }
        }
        private void rbHostInformationIIS_CheckedChanged(object sender, EventArgs e)
        {
            if (rbHostInformationIIS.Checked)
            {
                if (!IsIISInstalled())
                {
                    MessageBox.Show(this, "IIS 6, 7 or 7.5 was not found. IIS Host Type will not be supported.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    rbHostInformationWindowsService.Checked = true;
                    return;
                }
                txtHostInformationServerTypeInfo.Text = "If using IIS 6.0, specific steps are required to enable .Net 4.0. Check http://johan.driessen.se/posts/getting-an-asp.net-4-application-to-work-on-iis6. \n\rFor more information on which OS is supported, check http://www.microsoft.com/en-us/download/details.aspx?id=17851.";
                wizardPageHostType.SelectTab(tpHostInformationIIS);
                UpdateIISLink();
                PopulateIPAddresses();
                txtHostInformationIISWebSiteName.Focus();
            }
        }
        private bool IsUserALocalAdmin(string domain, string username, string password)
        {
            bool bReturn = false;
            try
            {
                IntPtr token = IntPtr.Zero;
                bool ret = LogonUser(username,
                    domain,
                    password,
                    LOGON32_LOGON_NETWORK,
                    LOGON32_PROVIDER_DEFAULT,
                    out token);

                if (!ret) return false;
                using (WindowsIdentity id = new WindowsIdentity(token))
                {
                    WindowsPrincipal principal = new WindowsPrincipal(id);
                    bReturn = principal.IsInRole(WindowsBuiltInRole.Administrator);
                    CloseHandle(token);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return bReturn;
        }
        private bool IsPortAvailable(int port)
        {
            bool isAvailable = true;

            try
            {
                var ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
                var tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

                foreach (var tcpConnectionInformation in tcpConnInfoArray)
                {
                    if (tcpConnectionInformation.LocalEndPoint.Port.Equals(port))
                    {
                        isAvailable = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return isAvailable;
        }
        private void PopulateIPAddresses()
        {
            object oldSelection = null;
            if (cbHostInformationIISIPAddress.SelectedItem != null)
            {
                oldSelection = cbHostInformationIISIPAddress.SelectedItem;
            }

            cbHostInformationIISIPAddress.Items.Clear();
            cbHostInformationIISIPAddress.Text = "(All Unassigned)";
            cbHostInformationIISIPAddress.Items.Add("(All Unassigned)");
            var hostEntry = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress currentAddress in hostEntry.AddressList)
            {
                cbHostInformationIISIPAddress.Items.Add(currentAddress.ToString());
            }

            if (oldSelection != null && cbHostInformationIISIPAddress.Items.Contains(oldSelection))
            {
                cbHostInformationIISIPAddress.SelectedItem = oldSelection;
            }
            else
            {
                cbHostInformationIISIPAddress.SelectedIndex = 0;
            }
        }

        #region Windows Service Host Type
        private void txtHostInformationWindowsServiceUsername_TextChanged(object sender, EventArgs e)
        {
            ValidateWindowsServiceControls();
        }
        private void txtHostInformationWindowsServicePassword_TextChanged(object sender, EventArgs e)
        {
            ValidateWindowsServiceControls();
        }
        private void txtHostInformationWindowsServiceDomain_TextChanged(object sender, EventArgs e)
        {
            ValidateWindowsServiceControls();
        }
        private void ValidateWindowsServiceControls()
        {
            if (txtHostInformationWindowsServiceDomain.Text.Length > 0 && txtHostInformationWindowsServiceUsername.Text.Length > 0 && txtHostInformationWindowsServicePassword.Text.Length > 0)
            {
                btnHostInformationNext.Enabled = true;
            }
            else
            {
                btnHostInformationNext.Enabled = false;
            }
        }
        private void nudHostInformationWebServicesPort_ValueChanged(object sender, EventArgs e)
        {
            UpdateWindowsServiceLink();
        }
        private void UpdateWindowsServiceLink()
        {
            lblHostInformationWindowsServiceURL.Text = String.Format("http://{0}:{1}/EasyIceService/RestServiceImpl.svc", Environment.MachineName, (int)nudHostInformationWindowsServicePort.Value);
        }
        private void lblHostInformationWindowsServiceURL_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(lblHostInformationWindowsServiceURL.Text);
        }
        #endregion

        #region IIS Host Type
        private void btnHostInformationIISBrowsePath_Click(object sender, EventArgs e)
        {
            fbdHostInformationIISPath.SelectedPath = txtHostInformationIISPath.Text;
            if (fbdHostInformationIISPath.ShowDialog() == DialogResult.OK)
            {
                txtHostInformationIISPath.Text = fbdHostInformationIISPath.SelectedPath;
            }
        }
        private void txtHostInformationIISWebSiteName_TextChanged(object sender, EventArgs e)
        {
            ValidateIISControls();
        }
        private void txtHostInformationIISPath_TextChanged(object sender, EventArgs e)
        {
            ValidateIISControls();
        }
        private void txtHostInformationIISDomain_TextChanged(object sender, EventArgs e)
        {
            ValidateIISControls();
        }
        private void txtHostInformationIISUsername_TextChanged(object sender, EventArgs e)
        {
            ValidateIISControls();
        }
        private void txtHostInformationIISPassword_TextChanged(object sender, EventArgs e)
        {
            ValidateIISControls();
        }
        private void ValidateIISControls()
        {
            if (txtHostInformationIISWebSiteName.Text.Length > 0 && txtHostInformationIISPath.Text.Length > 0)
            {
                btnHostInformationNext.Enabled = true;
            }
            else
            {
                btnHostInformationNext.Enabled = false;
            }
        }
        private void nudHostInformationIISPort_ValueChanged(object sender, EventArgs e)
        {
            UpdateIISLink();
        }
        private void UpdateIISLink()
        {
            lblHostInformationIISURL.Text = String.Format("http://localhost:{0}/EasyIceService/RestServiceImpl.svc", (int)nudHostInformationIISPort.Value);
            lblHostInformationIISMachineURL.Text = String.Format("http://{0}:{1}/EasyIceService/RestServiceImpl.svc", Environment.MachineName, (int)nudHostInformationIISPort.Value);
        }
        private void lblHostInformationIISURL_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(lblHostInformationIISURL.Text);
        }
        #endregion

        #endregion

        #region CIC Credentials Tab
        private void GoToCICCredentialsTab()
        {
            wizardPages.SelectTab(tpCICCredentials);
            txtCICCredentialsCICServer.Focus();
            ValidateCICCredentialsControls(); //In case controls are pre-filled for debugging purposes
        }
        private void txtCICCredentialsCICServer_TextChanged(object sender, EventArgs e)
        {
            ValidateCICCredentialsControls();
        }
        private void txtCICCredentialsCICUsername_TextChanged(object sender, EventArgs e)
        {
            ValidateCICCredentialsControls();
        }
        private void txtCICCredentialsCICPassword_TextChanged(object sender, EventArgs e)
        {
            ValidateCICCredentialsControls();
        }
        private void ValidateCICCredentialsControls()
        {
            if (txtCICCredentialsCICServer.Text.Length > 0 && txtCICCredentialsCICUsername.Text.Length > 0 && txtCICCredentialsCICPassword.Text.Length > 0)
            {
                btnCICCredentialsNext.Enabled = true;
            }
            else
            {
                btnCICCredentialsNext.Enabled = false;
            }
        }

        private void btnCICCredentialsBack_Click(object sender, EventArgs e)
        {
            GoToHostInformationTab();
        }
        private void btnCICCredentialsNext_Click(object sender, EventArgs e)
        {
            if (!AreCICCredentialsValid())
            {
                //TODO What if certificates are required?
                MessageBox.Show(String.Format("Failed to connect with these credentials. Make sure the CIC server, username and password are correct.\n\rAlso make sure connecting to {0} on port 2633 is allowed.", txtCICCredentialsCICServer.Text));
                return;
            }
            GoToSummaryTab();
        }
        #endregion

        #region Summary Tab
        private void GoToSummaryTab()
        {
            wizardPages.SelectTab(tpSummary);
            var sb = new StringBuilder();
            sb.AppendLine("Summary for Easy Ice Service:");
            sb.AppendLine();
            sb.AppendLine("Host Information:");
            if (rbHostInformationWindowsService.Checked)
            {
                sb.AppendLine("Host Type: Windows Service");
                sb.AppendFormat("Username: {0}\\{1}{2}", txtHostInformationWindowsServiceDomain.Text, txtHostInformationWindowsServiceUsername.Text, Environment.NewLine);
                sb.AppendFormat("Service Port: {0}{1}", (int)nudHostInformationWindowsServicePort.Value, Environment.NewLine);
            }
            else if (rbHostInformationIIS.Checked)
            {
                sb.AppendLine("Host Type: IIS");
                sb.AppendFormat("Web Site Name: {0}{1}", txtHostInformationIISWebSiteName.Text, Environment.NewLine);
                sb.AppendFormat("Web Site Path: {0}{1}", txtHostInformationIISPath.Text, Environment.NewLine);
                sb.AppendFormat("Username: {0}\\{1}{2}", txtHostInformationIISDomain.Text, txtHostInformationIISUsername.Text, Environment.NewLine);
                sb.AppendFormat("Service Port: {0}{1}", (int)nudHostInformationIISPort.Value, Environment.NewLine);
            }
            sb.AppendLine();
            sb.AppendLine("CIC Credentials:");
            sb.AppendFormat("CIC Server: {0}{1}", txtCICCredentialsCICServer.Text, Environment.NewLine);
            sb.AppendFormat("CIC Username: {0}{1}", txtCICCredentialsCICUsername.Text, Environment.NewLine);
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("Click on the '{0}' button below to proceed with the configuration.", btnSummaryCommit.Text);

            txtSummaryDetails.Text = sb.ToString();
        }
        private void btnSummaryBack_Click(object sender, EventArgs e)
        {
            GoToCICCredentialsTab();
        }
        private void btnSummaryCommit_Click(object sender, EventArgs e)
        {
            pbSummary.Visible = true;
            pbSummary.Value = 0;

            if (rbHostInformationWindowsService.Checked)
            {
                if (!InstallWindowsService())
                {
                    pbSummary.Visible = false;
                    return;
                }
                GoToFinishTab();
            }
            else if (rbHostInformationIIS.Checked)
            {
                if (!InstallIISService())
                {
                    pbSummary.Visible = false;
                    return;
                }
                GoToFinishTab();
            }
        }
        private bool InstallWindowsService()
        {
            bool success = false;
            try
            {
                pbSummary.Maximum = 10;
                pbSummary.Value++;

                //Change App.Config settings
                if (!UpdateWindowsServiceAppConfigValues())
                {
                    MessageBox.Show("Failed to update configuration values.\n\rThere are 3 settings to update (CICServer, CICUsername and CICPassword).\n\rAlso, the WCFEasyIceService.config need to have its port set (i.e. 9000).");
                    return false;
                }
                pbSummary.Value++;

                // Remove existing service
                if (ServiceInstaller.ServiceIsInstalled("Easy Ice Service"))
                {
                    pbSummary.Value++;
                    if (!ServiceInstaller.Uninstall("Easy Ice Service"))
                    {
                        //TODO Add tips/tricks
                        MessageBox.Show("Failed to uninstall the existing Easy Ice Service windows service.");
                        return false;
                    }
                }
                pbSummary.Value++;

                // Create new Windows Service
                if (!ServiceInstaller.InstallAndStart("Easy Ice Service", "Easy Ice Service", String.Format("{0}\\EasyIceWindowsService.exe", Application.StartupPath)))
                {
                    //TODO Add tips/tricks
                    MessageBox.Show("Failed to install the Easy Ice Windows Service.");
                    return false;
                }
                pbSummary.Value++;

                txtFinishDetails.Text = "Windows service has been successfully installed!";
                success = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return success;
        }
        private bool InstallIISService()
        {
            bool success = false;
            try
            {
                pbSummary.Maximum = 10;
                pbSummary.Value++;

                //Change Web.Config settings
                if (!UpdateWindowsServiceAppConfigValues())
                {
                    //TODO Add tips/tricks
                    MessageBox.Show("Failed to update configuration values.");
                    return false;
                }
                pbSummary.Value++;

                //Check if web site exists, remove it, then create new one.
                //if (!RemoveWebSite(txtHostInformationIISPath.Text))
                //{
                //    return;
                //}

                if (ServiceInstaller.ServiceIsInstalled("Easy Ice Service"))
                {
                    pbSummary.Value++;
                    if (!ServiceInstaller.Uninstall("Easy Ice Service"))
                    {
                        //TODO Add tips/tricks
                        MessageBox.Show("Failed to uninstall the existing Easy Ice Service windows service.");
                        return false;
                    }
                }
                pbSummary.Value++;

                // Setup Windows Service
                if (!ServiceInstaller.InstallAndStart("Easy Ice Service", "Easy Ice Service", String.Format("{0}\\Easy Ice Windows Service.exe", Application.StartupPath)))
                {
                    //TODO Add tips/tricks
                    MessageBox.Show("Failed to install the Easy Ice Windows Service.");
                    return false;
                }
                pbSummary.Value++;
                txtFinishDetails.Text = "Windows service has been successfully installed!";
                success = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return success;
        }
        #endregion

        #region Finish Tab
        private void GoToFinishTab()
        {
            wizardPages.SelectTab(tpFinish);
        }
        private void btnFinishBack_Click(object sender, EventArgs e)
        {
            GoToCICCredentialsTab();
        }
        private void btnFinishClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void llblFinishHelpPage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(llblFinishHelpPage.Text);
        }
        private void llblFinishWSDL_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(llblFinishWSDL.Text);
        }
        private void llblFinishDoc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(llblFinishDoc.Text);
        }
        #endregion

        #region App.Config Methods
        private bool UpdateWindowsServiceAppConfigValues()
        {
            bool success = false;

            try
            {
                var configuration = ConfigurationManager.OpenExeConfiguration("EasyIceWindowsService.exe");
                configuration.AppSettings.Settings["CICServer"].Value = txtCICCredentialsCICServer.Text;
                configuration.AppSettings.Settings["CICUsername"].Value = txtCICCredentialsCICUsername.Text;
                var encryptor = new Encryptor();
                configuration.AppSettings.Settings["CICPassword"].Value = encryptor.EncryptToBase64String(txtCICCredentialsCICPassword.Text, "3#sy!c3S3rv!c3");

                //TODO Update Port


                configuration.Save(ConfigurationSaveMode.Full);
                ConfigurationManager.RefreshSection("appSettings");
                success = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return success;
        }
        #endregion

        #region CIC Methods
        private bool AreCICCredentialsValid()
        {
            bool success = false;
            try
            {
                var _session = new Session();

                // Session settings
                var sessionSettings = new SessionSettings();
                //sessionSettings.CustomSettings.Add("identitydetail", "bonjour");
                sessionSettings.MachineName = Environment.MachineName;
                sessionSettings.ApplicationName = "Easy Ice Service";
                sessionSettings.DeviceType = DeviceType.Web;

                // Host settings
                var hostSettings = new HostSettings(new HostEndpoint(txtCICCredentialsCICServer.Text));

                // Authentication settings
                var authSettings = new ICAuthSettings(txtCICCredentialsCICUsername.Text, txtCICCredentialsCICPassword.Text);

                // Station settings
                var stationSettings = new StationlessSettings();

                // Connect!
                _session.Connect(sessionSettings, hostSettings, authSettings, stationSettings);
                if (_session.ConnectionState == ConnectionState.Up)
                {
                    success = true;
                }
                _session.DisconnectAsync(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return success;
        }
        #endregion

        //TODO Create StopWebSite method based on http://www.soft-bin.com/html/2010/07/29/use-csharp-to-manage-iis.html or InvokeMethod in ApplicationPool.cs?
        #region IIS Methods
        private bool IsIISInstalled()
        {
            return InternetInformationServicesDetection.IsInstalled(InternetInformationServicesVersion.IIS6)
                || InternetInformationServicesDetection.IsInstalled(InternetInformationServicesVersion.IIS7)
                || InternetInformationServicesDetection.IsInstalled(InternetInformationServicesVersion.IIS75);
        }
        private bool CreateWebSite()
        {
            //Create web site with NT Authentication
            //int siteID;
            //if (setupUtil.CreateWebSite(txtWebSiteName.Text,
            //    txtWebSiteLocalAddress.Text,
            //    IP,
            //    System.Convert.ToInt32(txtWebSitePort.Text),
            //    new string[] { "splash.aspx" }, //DefaultDocuments 
            //    iAuth, //Security level
            //    String.Format(@"{0}\{1}", txtDomainDomain.Text, txtDomainUser.Text), //App Pool Username
            //    txtDomainPassword.Text, //App Pool Password
            //    out siteID,
            //    out returnString))
            //{
            //    setupUtil.SetCustomHttpHeader(txtWebSiteName.Text, CustomHttpHeaders.COMPATIBILITY_VIEW_MODE);
            //}
            return true;
        }
        private bool DoesWebSiteExist(string webSiteName, string webAddress, string webLocalPath)
        {
            var root = new DirectoryEntry("IIS://localhost/W3SVC");
            
            try
            {
                foreach (DirectoryEntry e in root.Children)
                {
                    if (e.SchemaClassName == "IIsWebServer")
                    {
                        string WebSiteName = e.Properties["ServerComment"].Value.ToString();
                        string Address = e.Properties["ServerBindings"].Value.ToString();
                        //    ":80:"
                        var siteVDir = e.Children.Find("Root", "IISWebVirtualDir");
                        string LocalPath = siteVDir.Properties["Path"].Value.ToString();
                        return (WebSiteName == webSiteName) && (Address == webAddress) && (LocalPath == webLocalPath);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Is IIS installed?\n\r{0}.", ex.Message));
                return false;
            }
            return false;
        }
        private bool RemoveWebSite(string path)
        {
            var root = new DirectoryEntry("IIS://localhost/W3SVC");

            foreach (DirectoryEntry e in root.Children)
            {
                if (e.SchemaClassName == "IIsWebServer")
                {
                    var siteVDir = e.Children.Find("Root", "IISWebVirtualDir");
                    var s2 = siteVDir.Properties;
                    string LocalPath = s2["Path"].Value.ToString();

                    try
                    {
                        if (path.ToLower() == LocalPath.ToLower())
                        {
                            e.DeleteTree();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(String.Format("Failed to remove existing website: {0}.", ex.Message));
                        return false;
                    }
                }
            }
            return true;
        }
        private bool SetAppPoolIdentity(string username, string password)
        {
            try
            {
                ApplicationPool appPool = null;
                if (ApplicationPool.TryFind(EASYICESERVICE_APP_POOL_NAME, out appPool))
                {
                    appPool.SetIdentity(username, password);

                    appPool.Restart();
                }
                else
                {
                    //"Application pool'{}' does not exist", EASYICESERVICE_APP_POOL_NAME
                    return false;
                }
            }
            catch
            {
                //"Exception setting app pool user.
                return false;
            }

            return true;
        }
        private bool CreateAppPool(DirectoryEntry virtualDirectory, string appPoolName, string sUsername, string sPassword, bool bRestart)
        {
            //"Creating application pool named {} Username:{}", appPoolName, sUsername
            ApplicationPool appPool = null;

            try
            {
                if (ApplicationPool.TryFind(appPoolName, out appPool))
                {
                    appPool.SetIdentity(sUsername, sPassword);
                }
                else
                {
                    appPool = ApplicationPool.Create(appPoolName, sUsername, sPassword);
                }
            }
            catch (Exception ex)
            {
                //"Failed to create application pool."
                MessageBox.Show(ex.Message);
                return false;
            }

            try
            {
                string className = virtualDirectory.SchemaClassName.ToString();

                if (className.EndsWith("VirtualDir"))
                {
                    //"Assigning application {} to the new application pool named {}:", virtualDirectory.Path, appPoolName
                    //1 = Create out of process
                    object[] param = { 1, appPoolName, true };
                    virtualDirectory.Invoke("AppCreate3", param);
                    //1 = out of process.
                    virtualDirectory.Properties["AppPoolId"][0] = appPoolName;
                    virtualDirectory.Properties["AppIsolated"][0] = "1";
                    virtualDirectory.CommitChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

            if (bRestart)
            {
                try
                {
                    appPool.Restart();

                    //"Restarting AspApp for virtual directory {}", virtualDirectory.SchemaClassName.ToString();
                    virtualDirectory.Invoke("AspAppRestart", null);
                    System.Threading.Thread.Sleep(1000);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return true;
        }
        #endregion

        #region Windows Service Methods
        private bool InstallAndStartWindowsService()
        {
            return ServiceInstaller.InstallAndStart("EasyIceService", "Easy Ice Service", @".\EasyIceWindowsService.exe");
        }
        private bool RemoveWindowsService()
        {
            return ServiceInstaller.Uninstall("Easy Ice Service");
        }
        private ServiceState GetWindowsServiceStatus()
        {
            return ServiceInstaller.GetServiceStatus("Easy Ice Service");
        }
        private bool StartWindowsService()
        {
            return ServiceInstaller.StartService("Easy Ice Service");
        }
        private bool StopWindowsService()
        {
            return ServiceInstaller.StopService("Easy Ice Service");
        }
        private bool IsWindowsServiceInstalled()
        {
            return ServiceInstaller.ServiceIsInstalled("Easy Ice Service");
        }
        #endregion
    }
}