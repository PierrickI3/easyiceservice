﻿namespace EasyIceSetupWizard
{
    partial class MainForm
    {

        #region Windows Form Designer generated code


        #endregion

        private WizardPages wizardPages;
        private System.Windows.Forms.TabPage tpWelcome;
        private System.Windows.Forms.TabPage tpHostInformation;
        private System.Windows.Forms.TabPage tpCICCredentials;
        private System.Windows.Forms.TabPage tpSummary;
        private System.Windows.Forms.TabPage tpFinish;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnlWelcomeSideLogo;
        private System.Windows.Forms.Panel pnlWelcomePageInformation;
        private System.Windows.Forms.Label lblWelcomePageTitle;
        private System.Windows.Forms.Panel pnlWelcomeContent;
        private System.Windows.Forms.Label lblWelcomePageDetails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnWelcomeNext;
        private System.Windows.Forms.Panel pnlHostInformationPageInformation;
        private System.Windows.Forms.Label lblHostInformationPageTitle;
        private System.Windows.Forms.Panel pnlHostInformationContent;
        private System.Windows.Forms.Button btnHostInformationNext;
        private System.Windows.Forms.Panel pnlHostInformationSideLogo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnHostInformationBack;
        private System.Windows.Forms.RadioButton rbHostInformationIIS;
        private System.Windows.Forms.RadioButton rbHostInformationWindowsService;
        private System.Windows.Forms.Label lblHostInformationServerHostType;
        private System.Windows.Forms.RichTextBox txtHostInformationServerTypeInfo;
        private WizardPages wizardPageHostType;
        private System.Windows.Forms.TabPage tpHostInformationWindowsService;
        private System.Windows.Forms.TabPage tpHostInformationIIS;
        private System.Windows.Forms.TextBox txtHostInformationWindowsServiceUsername;
        private System.Windows.Forms.Label lblHostInformationWindowsServiceUsername;
        private System.Windows.Forms.TextBox txtHostInformationWindowsServicePassword;
        private System.Windows.Forms.Label lblHostInformationWindowsServicePassword;
        private System.Windows.Forms.Label lblHostInformationWindowsServiceInfo;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.NumericUpDown nudHostInformationWindowsServicePort;
        private System.Windows.Forms.Label lblHostInformationWindowsServicePort;
        private System.Windows.Forms.TextBox txtHostInformationWindowsServiceDomain;
        private System.Windows.Forms.Label lblHostInformationWindowsServiceDomain;
        private System.Windows.Forms.LinkLabel lblHostInformationWindowsServiceURL;
        private System.Windows.Forms.Label lblHostInformationWindowsServiceAccessInfo;
        private System.Windows.Forms.LinkLabel lblHostInformationIISURL;
        private System.Windows.Forms.Label lblHostInformationIISServiceAccessInfo;
        private System.Windows.Forms.TextBox txtHostInformationIISWebSiteName;
        private System.Windows.Forms.Label lblHostInformationIISWebSiteName;
        private System.Windows.Forms.NumericUpDown nudHostInformationIISPort;
        private System.Windows.Forms.Label lblHostInformationIISPort;
        private System.Windows.Forms.Label lblHostInformationIISInfo;
        private System.Windows.Forms.TextBox txtHostInformationIISPath;
        private System.Windows.Forms.Label lblHostInformationIISPath;
        private System.Windows.Forms.Button btnHostInformationIISBrowsePath;
        private System.Windows.Forms.FolderBrowserDialog fbdHostInformationIISPath;
        private System.Windows.Forms.Label lblHostInformationIISIPAddress;
        private System.Windows.Forms.ComboBox cbHostInformationIISIPAddress;
        private System.Windows.Forms.TabPage tpStopServices;
        private System.Windows.Forms.Panel panel1;
        private WizardPages wizardPageStopService;
        private System.Windows.Forms.TabPage tpStopServiceWindowsService;
        private System.Windows.Forms.TabPage tpStopServiceIIS;
        private System.Windows.Forms.Button btnStopServiceNext;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblStopServicesTitle;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnStopServiceWindowsService;
        private System.Windows.Forms.Label lblStopServiceWindowsServiceDetails2;
        private System.Windows.Forms.Label lblStopServiceWindowsServiceDetails1;
        private System.Windows.Forms.Button btnStopServiceIISStop;
        private System.Windows.Forms.Label lblStopServiceIISDetails2;
        private System.Windows.Forms.Label lblStopServiceIISDetails1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblSummaryTitle;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RichTextBox txtSummaryDetails;
        private System.Windows.Forms.Button btnSummaryBack;
        private System.Windows.Forms.Button btnSummaryCommit;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button btnStopServiceBack;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblFinishTitle;
        private System.Windows.Forms.Panel pnlFinishContents;
        private System.Windows.Forms.RichTextBox txtFinishDetails;
        private System.Windows.Forms.Button btnFinishBack;
        private System.Windows.Forms.Button btnFinishClose;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.TextBox txtHostInformationIISDomain;
        private System.Windows.Forms.Label lblHostInformationIISDomain;
        private System.Windows.Forms.TextBox txtHostInformationIISPassword;
        private System.Windows.Forms.Label lblHostInformationIISPassword;
        private System.Windows.Forms.TextBox txtHostInformationIISUsername;
        private System.Windows.Forms.Label lblHostInformationIISUsername;
        private System.Windows.Forms.LinkLabel lblHostInformationIISMachineURL;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblCICCredentialsTitle;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnCICCredentialsBack;
        private System.Windows.Forms.Button btnCICCredentialsNext;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblCICCredentialsCICPassword;
        private System.Windows.Forms.Label lblCICCredentialsCICUsername;
        private System.Windows.Forms.TextBox txtCICCredentialsCICServer;
        private System.Windows.Forms.Label lblCICCredentialsCICServer;
        private System.Windows.Forms.TextBox txtCICCredentialsCICPassword;
        private System.Windows.Forms.TextBox txtCICCredentialsCICUsername;
        private System.Windows.Forms.Label lblCICCredentialsDetails;
        private System.Windows.Forms.ProgressBar pbSummary;
        private System.Windows.Forms.Label lblFinishHelpPage;
        private System.Windows.Forms.LinkLabel llblFinishHelpPage;
        private System.Windows.Forms.LinkLabel llblFinishWSDL;
        private System.Windows.Forms.Label lblFinishWSDLPage;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.LinkLabel llblFinishDoc;
        private System.Windows.Forms.Label lblFinishDocPage;

    }
}

