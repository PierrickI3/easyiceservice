﻿using System;
using System.Collections.Generic;
using System.Text;
using System.DirectoryServices;
using System.IO;

namespace EasyIceSetupWizard
{
    class ApplicationPool
    {
        private const string APP_POOLS_METABASE_PATH = "IIS://Localhost/W3SVC/AppPools";

        private static class AppPoolKeys
        {
            public const string IdentityType = "AppPoolIdentityType";
            public const string Username = "WAMUserName";
            public const string Password = "WAMUserPass";
        }
        private static class AppPoolMethods
        {
            public const string SetInfo = "SetInfo";
            public const string Start = "Start";
            public const string Stop = "Stop";
        }
        private enum AppPoolIdentityTypes
        {
            username_password = 3
        }
        private static DirectoryEntry s_AppPoolsRoot;
        private DirectoryEntry _AppPoolEntry;
        static ApplicationPool()
        {
            s_AppPoolsRoot = new DirectoryEntry(APP_POOLS_METABASE_PATH);
        }
        private ApplicationPool(DirectoryEntry appPoolEntry)
        {
            if (appPoolEntry == null)
            {
                throw new ArgumentNullException("appPoolEntry");
            }

            _AppPoolEntry = appPoolEntry;
        }
        public string Name
        {
            get
            {
                return _AppPoolEntry.Name;
            }
        }

        #region Static Methods

        /// <summary>
        /// Locate the application pool with the specified name.
        /// </summary>
        /// <param name="name">Name of the application pool to find.</param>
        /// <param name="appPool">Contains an <see cref="ApplicationPool"/> object if one was found
        /// (i.e. if the this method returns True</param>
        /// <returns>True if the Application Pool was found. False otherwise.</returns>
        /// <exception cref="ApplicationPoolOperationException"></exception>
        public static bool TryFind(string name, out ApplicationPool appPool)
        {
            DirectoryEntry appPoolEntry = null;

            try
            {
                appPoolEntry = s_AppPoolsRoot.Children.Find(name, "IIsApplicationPool");
            }
            catch
            {
                // Don't bother to log this exception. It's expected if the app pool doesn't exist.
                //Exception (expected when the app pool does not exist) occurred while trying to find IIsApplicationPool application pool.
                appPoolEntry = null;
            }

            if (appPoolEntry != null)
            {
                appPool = new ApplicationPool(appPoolEntry);
                return true;
            }
            else
            {
                appPool = null;
                return false;
            }
        }

        /// <summary>
        /// Creates a new IIS Application Pool with the specified name and set the identity
        /// to the username/password provided.
        /// </summary>
        /// <param name="name">Name of the application pool to find or create.</param>
        /// <param name="username">NT domain\Username of the Application Pool identity.</param>
        /// <param name="password">NT password for the provided username.</param>
        /// <returns>Returns an <see cref="ApplicationPool"/> object representing the IIS Application Pool
        /// that was created.</returns>
        /// <exception cref="ApplicationPoolOperationException"></exception>
        public static ApplicationPool Create(string name, string username, string password)
        {
            DirectoryEntry appPoolEntry = null;
            try
            {
                //"Adding new application pool named {}/{}", APP_POOLS_METABASE_PATH, name
                appPoolEntry = s_AppPoolsRoot.Children.Add(name, "IIsApplicationPool");
            }
            catch (Exception ex)
            {
                throw new ApplicationPoolOperationException("Error creating application pool.", ex);
            }

            if (appPoolEntry != null)
            {
                ApplicationPool newPool = new ApplicationPool(appPoolEntry);
                newPool.SetIdentity(username, password);
                return newPool;
            }
            else
            {
                throw new ApplicationPoolOperationException("Could not create application pool. Directory Entry was null.");
            }
        }

        #endregion Static Methods

        #region Instance Methods

        /// <summary>
        /// Sets the identity of the Application Pool to the credentials provided.
        /// </summary>
        /// <param name="username">NT domain\Username of the Application Pool identity.</param>
        /// <param name="password">NT password for the provided username.</param>
        /// <exception cref="ApplicationPoolOperationException"></exception>
        public void SetIdentity(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ApplicationPoolOperationException("Application Pool Username cannot be empty.");
            }

            GrantUserMetabaseAccess(username);

            AddUserToIisWpgWorkgroup(username);

            SetAppPoolProperty(AppPoolKeys.IdentityType, AppPoolIdentityTypes.username_password);
            SetAppPoolProperty(AppPoolKeys.Username, username);
            SetAppPoolProperty(AppPoolKeys.Password, password);

            InvokeMethod(AppPoolMethods.SetInfo);

            _AppPoolEntry.CommitChanges();

            System.Threading.Thread.Sleep(1000);
        }

        /// <summary>
        /// Restart the Application Pool
        /// </summary>
        /// <exception cref="ApplicationPoolOperationException"></exception>
        public void Restart()
        {
            try
            {
                //"Stopping ApplicationPool {}", this.Name
                InvokeMethod(AppPoolMethods.Stop);
            }
            catch { }

            System.Threading.Thread.Sleep(1000);

            try
            {
                //"Start ApplicationPool {}", this.Name
                InvokeMethod(AppPoolMethods.Start);
            }
            catch (Exception ex)
            {
                throw new ApplicationPoolOperationException("Could not start application pool.", ex);
            }

            System.Threading.Thread.Sleep(1000);

            //"App pool '{}' was restarted.", this.Name
        }

        #endregion Instance Methods

        #region Private Helper Methods
        private void SetAppPoolProperty(string propertyKey, params object[] parameters)
        {
            _AppPoolEntry.InvokeSet(propertyKey, parameters);
        }
        private void InvokeMethod(string methodName, params object[] parameters)
        {
            _AppPoolEntry.Invoke(methodName, parameters);
        }
        private void GrantUserMetabaseAccess(string username)
        {
            try
            {
                //First thing to do is setup the allow access for the user to the server’s IIS metabase
                //-ga <user> Grant the specified user or group access to the IIS metabase and other directories used by ASP.NET.
                string sRegIISPath = Path.Combine(
                    System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory(),
                    @"aspnet_regiis.exe");

                //"registering user:{} with IIS Metabase", username
                System.Diagnostics.ProcessStartInfo inUtilProcessInfo =
                    new System.Diagnostics.ProcessStartInfo(
                        sRegIISPath,
                        String.Format("-ga {0}", username));
                inUtilProcessInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                System.Diagnostics.Process inUtilProcess = System.Diagnostics.Process.Start(inUtilProcessInfo);
                inUtilProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                throw new ApplicationPoolOperationException("Failed to grant user IIS metabase access.", ex);
            }
        }
        private void AddUserToIisWpgWorkgroup(string username)
        {
            try
            {
                //now adding the user to IIS_WPG group
                DirectoryEntry localMachine = new DirectoryEntry(String.Format("WinNT://{0},Computer", Environment.MachineName));
                //"locating machine local group:IIS_WPG"
                DirectoryEntry admGroup = localMachine.Children.Find("IIS_WPG", "group");
                if (admGroup != null)
                {
                    //"finding user to add:{}", username
                    DirectoryEntry userAdding = new DirectoryEntry(String.Format("WinNT://{0}", username.Replace('\\', '/')));
                    if (userAdding != null)
                    {
                        //"adding user to group:{}", username
                        admGroup.Invoke("Add", userAdding.Path);
                    }
                }
            }
            catch (Exception ex)
            {
                // Don't rethrow the exception if it "already exists" exception.
                if (ex.InnerException == null ||
                    !ex.InnerException.Message.ToLowerInvariant().Contains("already a member"))
                {
                    throw new ApplicationPoolOperationException("Failure adding user to IIS_WPG workgroup", ex);
                }
            }
        }
        #endregion
    }
    internal class ApplicationPoolOperationException : Exception
    {
        public ApplicationPoolOperationException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public ApplicationPoolOperationException(string message)
            : base(message)
        { }
    }
}