﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.DirectoryServices;

/// <summary>
/// IISHelper
/// This class contains methods to handle few functions of IIS programmatically.
/// Included Methods are
/// Get List Of all the Directories.
/// Create Virtual Directory.
/// Add alias entry in Host Header of wesite.
/// </summary>
public static class IISHelper
{
    #region Enum
    public enum IISVersion
    {
        IIS5 = 1,
        IIS6 = 2,
        IIS7 = 3,
        IIS75 = 4
    }
    #endregion

    #region Constants
    /// <summary>
    /// Constant for IISWebServer directory only. 
    /// FOR IIS 6.0
    /// </summary>
    static string IISWEBSERVER = "iiswebserver";

    /// <summary>
    /// Constant for IISWebServer directory only.
    /// FOR IIS 5.1
    /// </summary>
    static string IISWEBVIRTUALDIRECTORY = "IIsWebVirtualDir";

    /// <summary>
    /// Default Virtual path 
    /// </summary>
    static string DEFAULTVIRTUALPATH = "/1/Root";

    #endregion

    #region Public Methods
    /// <summary>
    /// Get List of Directories and their properties.
    /// </summary>
    /// <param name="ServerName">Server Name</param>
    /// <param name="DirectoryName">Virtual Directory or Website name</param>
    /// <param name="IISVersionNumber">IIS  Version Number</param>
    /// <returns></returns>
    public static List<WebSiteInfo> GetListOfDirectories(string ServerName, string DirectoryName, IISVersion IISVersionNumber)
    {
        string strIISPath;

        #region Set Path according to the Virsion of IIS.

        // Get Directory entry point depending on IIS 5 and IIS 6
        if (IISVersionNumber == IISVersion.IIS5)
        {
            // Set Path for IIS 5.0
            strIISPath = WebSiteInfo.IIS + ServerName + WebSiteInfo.WEBSERVICE + DEFAULTVIRTUALPATH + (string.IsNullOrEmpty(DirectoryName) ? string.Empty : "/" + DirectoryName);
        }
        else
        {
            // Set Path for IIS 6.0
            if (string.IsNullOrEmpty(DirectoryName))
                strIISPath = WebSiteInfo.IIS + ServerName + WebSiteInfo.WEBSERVICE;
            else
            {
                // For IIS 6.0 we require Site ID, Call Method to Get Site ID
                DirectoryName = GetSiteID(WebSiteInfo.IIS + ServerName + WebSiteInfo.WEBSERVICE, DirectoryName);

                // Generate Path.
                strIISPath = WebSiteInfo.IIS + ServerName + WebSiteInfo.WEBSERVICE + (string.IsNullOrEmpty(DirectoryName) ? string.Empty : "/" + DirectoryName);
            }
        }
        #endregion

        // Check if path points to any existing directory
        if (IsExist(strIISPath))
        {
            // Generate Entry point
            DirectoryEntry objDirectoryEntry = new DirectoryEntry(strIISPath);

            try
            {
                List<WebSiteInfo> lstWebSiteInfo = FillWeSiteInfo(objDirectoryEntry);

                return lstWebSiteInfo;
            }
            finally
            {
                objDirectoryEntry.Close();
            }
        }
        else
            return null;
    }

    /// <summary>
    /// Creates Virtual Directory for IIS 5 and IIS 6.0,
    /// Set Parent Website value for IIS 6.0 or later,
    /// Parent Directory indicates parent virtual directory not website.
    /// </summary>
    /// <param name="WebSiteInformation"></param>
    /// <param name="IISVersionNumber">IIS Version Number</param>
    /// <returns></returns>
    public static bool CreateVirtualDirectory(WebSiteInfo WebSiteInformation, IISVersion IISVersionNumber)
    {
        string strIISPath;

        #region Set Path according to the Virsion of IIS.
        // Generate Entry point for IIS 5 and IIS 6
        if (IISVersionNumber == IISVersion.IIS5)
        {
            // Generate Path
            strIISPath = WebSiteInfo.IIS + WebSiteInformation.ServerName + WebSiteInfo.WEBSERVICE + DEFAULTVIRTUALPATH + (string.IsNullOrEmpty(WebSiteInformation.ParentDirectoryName) ? string.Empty : "/" + WebSiteInformation.ParentDirectoryName);
        }
        else
        {
            if (string.IsNullOrEmpty(WebSiteInformation.ParentWebSiteName.Trim()))
            {
                strIISPath = WebSiteInfo.IIS + WebSiteInformation.ServerName + WebSiteInfo.WEBSERVICE + DEFAULTVIRTUALPATH + (string.IsNullOrEmpty(WebSiteInformation.ParentDirectoryName) ? string.Empty : "/" + WebSiteInformation.ParentDirectoryName);
            }
            else
            {
                // Generate Path
                string strID = GetSiteID(WebSiteInfo.IIS + WebSiteInformation.ServerName + WebSiteInfo.WEBSERVICE, WebSiteInformation.ParentWebSiteName);
                strIISPath = WebSiteInfo.IIS + WebSiteInformation.ServerName + WebSiteInfo.WEBSERVICE + "/" + strID + (string.IsNullOrEmpty(WebSiteInformation.ParentDirectoryName) ? string.Empty : "/" + WebSiteInformation.ParentDirectoryName);
            }
        }
        #endregion

        // Check for valid path
        if (IsExist(strIISPath))
        {
            // Create Root Entry
            var rootDirectoryEntry = new DirectoryEntry(strIISPath);

            try
            {
                // Refresh the schema
                rootDirectoryEntry.RefreshCache();

                // Create New child in root directory
                var directoryEntry = rootDirectoryEntry.Children.Add(WebSiteInformation.DirectoryName, IISWEBVIRTUALDIRECTORY);

                // Set physical path for newly created Directory.
                directoryEntry.Properties["Path"].Insert(0, WebSiteInformation.PhysicalPath);

                // Create Virstual directory
                directoryEntry.Invoke("AppCreate", true);

                // Assign property to Virtual directory
                directoryEntry.Properties["EnableDirBrowsing"][0] = WebSiteInformation.HasBrowseAccess;
                directoryEntry.Properties["AccessExecute"][0] = WebSiteInformation.HasExecuteAccess;
                directoryEntry.Properties["AccessRead"][0] = WebSiteInformation.HasReadAccess;
                directoryEntry.Properties["AccessWrite"][0] = WebSiteInformation.HasWriteAccess;
                directoryEntry.Properties["AuthAnonymous"][0] = WebSiteInformation.IsAnonymousAccessAllow;
                directoryEntry.Properties["AuthBasic"][0] = WebSiteInformation.IsBasicAuthenticationSet;
                directoryEntry.Properties["AuthNTLM"][0] = WebSiteInformation.IsNTLMAuthenticationSet;

                // Save Changes
                directoryEntry.CommitChanges();
                rootDirectoryEntry.CommitChanges();

                // Close both directory object.
                directoryEntry.Close();
                rootDirectoryEntry.Close();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
            return false;
    }

    /// <summary>
    /// Add alias value in Host Header, this method is specific to IIS 6.0, Method is not supported by any virsion previous to IIS 6.0
    /// </summary>
    /// <param name="WebSiteInformation"></param>
    /// <returns></returns>
    public static bool AddValueToHostHeader(WebSiteInfo WebSiteInformation)
    {
        string strIISPath = WebSiteInfo.IIS + WebSiteInformation.ServerName + WebSiteInfo.WEBSERVICE + (string.IsNullOrEmpty(WebSiteInformation.ParentDirectoryName) ? string.Empty : "/" + WebSiteInformation.ParentDirectoryName);

        // Check if path points to any existing directory
        if (IsExist(strIISPath))
        {
            try
            {
                // Get WebSite ID.
                string strSiteID = GetSiteID(WebSiteInformation);

                // Check for valid Website ID
                if (string.IsNullOrEmpty(strSiteID))
                    return false;

                // Generate Entry point
                DirectoryEntry objDirectoryEntry = new DirectoryEntry(strIISPath + "/" + strSiteID);

                // Add Host Header
                objDirectoryEntry.Properties["ServerBindings"].Add(WebSiteInformation.HostHeader);

                // Commit changes
                objDirectoryEntry.CommitChanges();

                // Close
                objDirectoryEntry.Close();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
            return false;
    }

    /// <summary>
    /// Check that directory is exist or not
    /// </summary>
    /// <param name="IISPath"></param>
    /// <returns></returns>
    public static bool IsExist(string IISPath)
    {
        return DirectoryEntry.Exists(IISPath);
    }

    /// <summary>
    /// This method creates new website in IIS 6.0 or later
    /// Method is not supported by any virsion previous to IIS 6.0
    /// </summary>
    /// <param name="WebsiteInformation"></param>
    /// <returns></returns>
    public static bool CreateNewWebsite(WebSiteInfo WebsiteInformation)
    {
        string strIISPath = WebSiteInfo.IIS + WebsiteInformation.ServerName + WebSiteInfo.WEBSERVICE;

        // Check if path points to any existing directory
        if (IsExist(strIISPath))
        {
            // Create Directory entry at root level
            DirectoryEntry objDirectoryEntry = new DirectoryEntry(strIISPath);

            // Create object for site specification
            object[] objNewSite = new object[] { WebsiteInformation.SiteName, new object[] { WebsiteInformation.HostHeader }, WebsiteInformation.PhysicalPath };

            // Invoke create site method.
            objDirectoryEntry.Invoke("CreateNewSite", objNewSite);

            // Save newly created directory.
            objDirectoryEntry.CommitChanges();

            // close directory entry.
            objDirectoryEntry.Close();

            // Create entry point new website
            objDirectoryEntry = new DirectoryEntry(strIISPath + "/" + GetSiteID(strIISPath, WebsiteInformation.SiteName));

            // Assign property to Website
            objDirectoryEntry.Properties["EnableDirBrowsing"][0] = WebsiteInformation.HasBrowseAccess;
            objDirectoryEntry.Properties["AccessExecute"][0] = WebsiteInformation.HasExecuteAccess;
            objDirectoryEntry.Properties["AccessRead"][0] = WebsiteInformation.HasReadAccess;
            objDirectoryEntry.Properties["AccessWrite"][0] = WebsiteInformation.HasWriteAccess;
            objDirectoryEntry.Properties["AuthAnonymous"][0] = WebsiteInformation.IsAnonymousAccessAllow;
            objDirectoryEntry.Properties["AuthBasic"][0] = WebsiteInformation.IsBasicAuthenticationSet;
            objDirectoryEntry.Properties["AuthNTLM"][0] = WebsiteInformation.IsNTLMAuthenticationSet;

            // Save Changes
            objDirectoryEntry.CommitChanges();

            // Close both directory object.
            objDirectoryEntry.Close();

            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// Delete Virtual Directory
    /// </summary>
    /// <param name="ServerName">Server Name</param>
    /// <param name="DirectoryName">Directory to be deleted</param>
    /// <param name="ParentDirectoryName">Parent Virtual Directory, if any</param>
    /// <param name="ParentWebSiteName">Parent Website, Only for IIS 6.0</param>
    /// <param name="IISVersionNumber">IIS Version Number</param>
    /// <returns></returns>  
    public static bool DeleteVirtualDirectory(string ServerName, string DirectoryName, string ParentDirectoryName, string ParentWebSiteName, IISVersion IISVersionNumber)
    {
        string strIISPath;

        #region Set Path according to the Version of IIS.
        // Set path fpr IIS 5.0
        if (IISVersionNumber == IISVersion.IIS5)
            strIISPath = WebSiteInfo.IIS + ServerName + WebSiteInfo.WEBSERVICE + DEFAULTVIRTUALPATH + (string.IsNullOrEmpty(ParentDirectoryName) ? string.Empty : "/" + ParentDirectoryName);
        else
        {
            // Set path fpr IIS 6.0
            if (string.IsNullOrEmpty(ParentDirectoryName.Trim()))
                strIISPath = WebSiteInfo.IIS + ServerName + WebSiteInfo.WEBSERVICE + DEFAULTVIRTUALPATH + (string.IsNullOrEmpty(ParentDirectoryName) ? string.Empty : "/" + ParentDirectoryName);
            else
            {
                // Get Parent Website
                ParentWebSiteName = GetSiteID(WebSiteInfo.IIS + ServerName + WebSiteInfo.WEBSERVICE, ParentWebSiteName);

                strIISPath = WebSiteInfo.IIS + ServerName + WebSiteInfo.WEBSERVICE + "/" + ParentWebSiteName + (string.IsNullOrEmpty(ParentDirectoryName) ? string.Empty : "/" + ParentDirectoryName);
            }
        }
        #endregion

        // Check if path points to any existing directory
        if (IsExist(strIISPath))
        {
            // Create Entry point
            DirectoryEntry objDirectoryEntry = new DirectoryEntry(strIISPath);

            objDirectoryEntry.Invoke("Delete", new string[] { IISWEBVIRTUALDIRECTORY, DirectoryName });

            // Commit changes
            objDirectoryEntry.CommitChanges();

            // Close Directory
            objDirectoryEntry.Close();

            return true;
        }
        return false;
    }

    /// <summary>
    /// Delete Website. Only for IIS 6.0 or later.
    /// </summary>
    /// <param name="ServerName">Server Name</param>
    /// <param name="WebSiteName">Website Name</param>
    /// <returns></returns>
    public static bool DeleteWebSite(string ServerName, string WebSiteName)
    {
        string strIISPath = WebSiteInfo.IIS + ServerName + WebSiteInfo.WEBSERVICE;

        if (IsExist(strIISPath))
        {
            string strWebSiteID = GetSiteID(strIISPath, WebSiteName);

            if (string.IsNullOrEmpty(strWebSiteID))
                throw new Exception("Website doest not exist.");

            // Create Entry point
            DirectoryEntry objDirectoryEntry = new DirectoryEntry(strIISPath);

            // Invoke dlete method.
            objDirectoryEntry.Invoke("Delete", new string[] { IISWEBSERVER, strWebSiteID });

            // Commit changes
            objDirectoryEntry.CommitChanges();

            // Close the Directory
            objDirectoryEntry.Close();

            return true;
        }
        return false;
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Get all directories.
    /// </summary>
    /// <param name="objDirectoryEntry"></param>
    /// <returns>List<WebSiteInfo></returns>
    private static List<WebSiteInfo> FillWeSiteInfo(DirectoryEntry objDirectoryEntry)
    {
        // Create List of WebSiteInfo
        List<WebSiteInfo> lstWebSiteInfo = new List<WebSiteInfo>();

        // Get Children of perticular directory
        DirectoryEntries objDirectoryEntries = objDirectoryEntry.Children;

        // Add entry in List
        AddEntryInList(objDirectoryEntry, lstWebSiteInfo);

        // Check if the Entry has childrens
        if (objDirectoryEntries != null)
        {
            // Loop through each children and get all the properties
            foreach (DirectoryEntry tempDirectoryEntry in objDirectoryEntries)
            {
                // Add entry in List
                AddEntryInList(tempDirectoryEntry, lstWebSiteInfo);
            }
            return lstWebSiteInfo;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Add entry in List
    /// </summary>
    /// <param name="tempDirectoryEntry"></param>
    /// <param name="lstWebSiteInfo"></param>
    private static void AddEntryInList(DirectoryEntry tempDirectoryEntry, List<WebSiteInfo> lstWebSiteInfo)
    {
        // You can get following property of on IISWebServer Shema class.
        // For all other schema class you do not have this property 
        if (tempDirectoryEntry.SchemaClassName.ToLower() == IISWEBSERVER || tempDirectoryEntry.SchemaClassName.ToLower() == IISWEBVIRTUALDIRECTORY.ToLower())
        {
            WebSiteInfo objWebSiteInfo = new WebSiteInfo();

            objWebSiteInfo.IISPath = tempDirectoryEntry.Path;
            objWebSiteInfo.SiteName = Convert.ToString(tempDirectoryEntry.Properties["ServerComment"].Value);
            objWebSiteInfo.HasBrowseAccess = Convert.ToBoolean(tempDirectoryEntry.Properties["EnableDirBrowsing"].Value);
            objWebSiteInfo.HasExecuteAccess = Convert.ToBoolean(tempDirectoryEntry.Properties["AccessExecute"].Value);
            objWebSiteInfo.HasReadAccess = Convert.ToBoolean(tempDirectoryEntry.Properties["AccessRead"].Value);
            objWebSiteInfo.HasWriteAccess = Convert.ToBoolean(tempDirectoryEntry.Properties["AccessWrite"].Value);
            objWebSiteInfo.IsAnonymousAccessAllow = Convert.ToBoolean(tempDirectoryEntry.Properties["AuthAnonymous"].Value);
            objWebSiteInfo.IsBasicAuthenticationSet = Convert.ToBoolean(tempDirectoryEntry.Properties["AuthBasic"].Value);
            objWebSiteInfo.IsNTLMAuthenticationSet = Convert.ToBoolean(tempDirectoryEntry.Properties["AuthNTLM"].Value);
            lstWebSiteInfo.Add(objWebSiteInfo);
        }
    }

    /// <summary>
    /// THis is for II 6.0 Only.
    /// Get Website ID for Site.
    /// IIS assign ID to each website.
    /// </summary>
    /// <param name="WebSiteInformation"></param>
    /// <returns>website id as string.</returns>
    private static string GetSiteID(WebSiteInfo WebSiteInformation)
    {
        string strSiteID = string.Empty;
        string strIISPath = WebSiteInfo.IIS + WebSiteInformation.ServerName + WebSiteInfo.WEBSERVICE + (string.IsNullOrEmpty(WebSiteInformation.ParentDirectoryName) ? string.Empty : "/" + WebSiteInformation.ParentDirectoryName);
        strSiteID = GetSiteID(strIISPath, WebSiteInformation.SiteName);

        return strSiteID;
    }

    /// <summary>
    /// THis is for II 6.0 Only.
    /// Get Website ID for Site.
    /// IIS assign ID to each website.
    /// </summary>
    /// <param name="WebSiteInformation"></param>
    /// <returns>website id as string.</returns>
    private static string GetSiteID(string strIISPath, string strDirectoryName)
    {
        string strSiteID = string.Empty;

        // Check if path points to any existing directory
        if (IsExist(strIISPath))
        {
            // Generate Entry point
            DirectoryEntry objDirectoryEntry = new DirectoryEntry(strIISPath);

            foreach (DirectoryEntry tempDirectoryEntry in objDirectoryEntry.Children)
            {
                if (Convert.ToString(tempDirectoryEntry.Properties["ServerComment"].Value).ToLower() == strDirectoryName.ToLower())
                {
                    strSiteID = tempDirectoryEntry.Name;
                    break;
                }
            }
        }
        return strSiteID;
    }
    #endregion
}

public class WebSiteInfo
{
    #region Constants
    public const string IIS = "IIS://";
    public const string WEBSERVICE = "/W3SVC";
    public const string FTPSERVICE = "/MSFTPSVC";
    #endregion

    #region Variables
    private string strIISPath = string.Empty;
    private string strSiteName = string.Empty;
    private string strServerName = string.Empty;
    private string strDirectoryName = string.Empty;
    private string strParentDirectoryName = string.Empty;
    private string strParentWebSiteName = string.Empty;
    private string strPhysicalPath = string.Empty;
    private string strHostHeader = string.Empty;
    private bool blnHasReadAccess = true;
    private bool blnHasBrowseAccess = false;
    private bool blnHasWriteAccess = false;
    private bool blnHasExecuteAccess = false;
    private bool blnIsAnonymousAccessAllow = false;
    private bool blnIsBasicAuthenticationSet = false;
    private bool blnIsNTLMAuthenticationSet = false;
    #endregion

    #region Constructors
    public WebSiteInfo()
    {
    }
    #endregion

    #region Public Properties
    public string SiteName
    {
        get { return strSiteName; }
        set { strSiteName = value; }
    }

    /// <summary>
    /// Gets or Sets Virtual Path
    /// </summary>
    public string IISPath
    {
        get { return strIISPath; }
        set { strIISPath = value; }
    }

    /// <summary>
    /// Gets or Sets Virtual Directory/Website name.
    /// </summary>
    public string DirectoryName
    {
        get { return strDirectoryName; }
        set { strDirectoryName = value; }
    }

    /// <summary>
    /// Get or Set parent virtual directory name
    /// </summary>
    public string ParentDirectoryName
    {
        get { return strParentDirectoryName; }
        set { strParentDirectoryName = value; }
    }

    /// <summary>
    /// Get or Set parent website name, Only for IIS 6.0 or later
    /// </summary>
    public string ParentWebSiteName
    {
        get { return strParentWebSiteName; }
        set { strParentWebSiteName = value; }
    }

    /// <summary>
    /// Physical path for Website or Virtual Directory
    /// </summary>
    public string PhysicalPath
    {
        get { return strPhysicalPath; }
        set { strPhysicalPath = value; }
    }

    /// <summary>
    /// Server name where directorry or website exist or wnat to create.
    /// </summary>
    public string ServerName
    {
        get { return strServerName; }
        set { strServerName = value; }
    }

    /// <summary>
    /// Specify host heade as ":80:Testing.com", 
    /// user must specify ":" as shown.
    /// </summary>
    public string HostHeader
    {
        get { return strHostHeader; }
        set { strHostHeader = value; }
    }

    /// <summary>
    /// Get or Set NTLM Authentication, Default is false.
    /// </summary>
    public bool IsNTLMAuthenticationSet
    {
        get { return blnIsNTLMAuthenticationSet; }
        set { blnIsNTLMAuthenticationSet = value; }
    }

    /// <summary>
    /// Get or Set Basic Authentication, Default is false.
    /// </summary>
    public bool IsBasicAuthenticationSet
    {
        get { return blnIsBasicAuthenticationSet; }
        set { blnIsBasicAuthenticationSet = value; }
    }

    /// <summary>
    /// Get or Set Anonymous Authentication, Default is false.
    /// </summary>
    public bool IsAnonymousAccessAllow
    {
        get { return blnIsAnonymousAccessAllow; }
        set { blnIsAnonymousAccessAllow = value; }
    }

    /// <summary>
    /// Get or Set Execute right, Default is false.
    /// </summary>
    public bool HasExecuteAccess
    {
        get { return blnHasExecuteAccess; }
        set { blnHasExecuteAccess = value; }
    }

    /// <summary>
    /// Get or Set Write right, Default is false.
    /// </summary>
    public bool HasWriteAccess
    {
        get { return blnHasWriteAccess; }
        set { blnHasWriteAccess = value; }
    }

    /// <summary>
    /// Get or Set Browse access, Default is false.
    /// </summary>
    public bool HasBrowseAccess
    {
        get { return blnHasBrowseAccess; }
        set { blnHasBrowseAccess = value; }
    }

    /// <summary>
    /// Get or Set Read right, Default is true.
    /// </summary>
    public bool HasReadAccess
    {
        get { return blnHasReadAccess; }
        set { blnHasReadAccess = value; }
    }

    #endregion
}