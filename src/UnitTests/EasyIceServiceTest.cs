﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using WCFEasyIceService;
using System.IO;
using EasyResponses;

namespace RESTInteractionsUnitTesting
{
    /// <summary>
    ///This is a test class for EasyIceServiceTest and is intended
    ///to contain all EasyIceServiceTest Unit Tests
    ///</summary>
    [TestClass]
    public class EasyIceServiceTest
    {
        // Test Preparation
        // ================
        //
        // . Make sure a client is started with the "esadmin" user and station "PIERRICKSTATION".
        // . Make sure the "PIERRICKSTATION" phone is turned on.

        static readonly EasyIceServiceImpl EasyIceServiceImpl = new EasyIceServiceImpl(); // to avoid logging in for each test.
        
        private const string TESTUSER = "devlab_user";
        
        private const string TESTSERVERPARAMETERNAME = "I3tables Path";
        private const string TESTSERVERPARAMETERVALUE = @"C:\I3\IC\Server\I3Tables\";
        
        private const string TESTPHONENUMBER = "3178723000";
        
        private const string TESTFAXNUMBER = "3177158162";
        private const string TESTFAXTIFFILE = @"C:\Users\Pierrick\Documents\Visual Studio 2010\Projects\Easy Ice Service\trunk\Deploy\test.tif";
        private const string TESTFAXPDFFILE = @"C:\Users\Pierrick\Documents\Visual Studio 2010\Projects\Easy Ice Service\trunk\Deploy\test.pdf";

        private const string TESTRECORDINGID = "6166c012-55c0-d013-8041-bce3194c0001";
        private const string TESTRECORDINGINTERACTIONID = "1001385798";
        private const string TESTRECORDINGINTERACTIONIDKEY = "100138579830121025";

        private const string TESTSTATION = "TestStation7778";
        private const string TESTSTATIONEXTENSION = "7778";

        private const string TESTWORKGROUP = "TestWorkgroup";
        private const string TESTWORKGROUP2 = "TestWorkgroup2";

        private const string TESTSUBSYSTEMNAME = "outlook";
        private const string TESTTOPICNAME = "VoicemailAddIn";

        private const string TESTCAMPAIGNNAME = "TestCampaign";
        private const string TESTINSERTCONTACTCOLUMNNAME = "PHONENUMBER";
        private const string TESTINSERTCONTACTCOLUMNVALUE = "3178723000";
        private const string TESTPHONENUMBERTOEXCLUDE = "3178723001";

        #region Test Stuff

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion

        #region User Status
        [TestMethod]
        public void UserStatus_get_user_status()
        {
            var expected = new GetUsersResponse {Success = true};

            var actual = EasyIceServiceImpl.GetUserStatus(TESTUSER);

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.AreEqual(expected.Success, actual.Success, String.Format("Success is not '{0}' as expected.", expected.Success));
        }
        [TestMethod]
        public void UserStatus_set_user_status_to_DND_then_get_status_then_set_it_to_available()
        {
            // Set user status to DND
            var expectedSet = new SetUserStatusResponse {Success = true};
            var actualSet = EasyIceServiceImpl.SetUserStatus(TESTUSER, "Do Not Disturb");
            Assert.IsTrue(String.IsNullOrEmpty(actualSet.ErrorMessage), String.Format("Got error message: '{0}'.", actualSet.ErrorMessage));
            Assert.AreEqual(expectedSet.Success, actualSet.Success, String.Format("Success is not '{0}' as expected.", expectedSet.Success));

            // Is is set to DND?
            var expectedGet = new GetUsersResponse {Success = true};
            var actualGet = EasyIceServiceImpl.GetUserStatus(TESTUSER);

            Assert.IsTrue(String.IsNullOrEmpty(actualGet.ErrorMessage), String.Format("Got error message: '{0}'.", actualGet.ErrorMessage));
            Assert.AreEqual(expectedGet.Success, actualGet.Success, String.Format("Success is not '{0}' as expected.", expectedGet.Success));
            Assert.IsTrue(actualGet.Id.Equals("Do Not Disturb", StringComparison.InvariantCultureIgnoreCase), "Status is not set to DND.");

            // Set user status to Available
            var expectedSetAvailable = new SetUserStatusResponse {Success = true};
            var actualSetAvailable = EasyIceServiceImpl.SetUserStatus(TESTUSER, "Available");
            Assert.IsTrue(String.IsNullOrEmpty(actualSetAvailable.ErrorMessage), String.Format("Got error message: '{0}'.", actualSetAvailable.ErrorMessage));
            Assert.AreEqual(expectedSetAvailable.Success, actualSetAvailable.Success, String.Format("Success is not '{0}' as expected.", expectedSetAvailable.Success));
        }
        #endregion

        #region Server Parameter
        [TestMethod]
        public void ServerParameter_get_server_parameter()
        {
            var expected = new GetServerParameterResponse {Success = true};

            var actual = EasyIceServiceImpl.GetServerParameter(TESTSERVERPARAMETERNAME);

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.AreEqual(expected.Success, actual.Success, String.Format("Success is not '{0}' as expected.", expected.Success));
            Assert.IsTrue(actual.ServerParameterValue.Equals(TESTSERVERPARAMETERVALUE, StringComparison.InvariantCultureIgnoreCase), "Server parameter value is not correct.");
        }

        #endregion

        #region Interactions

        #region Call Interactions
        [TestMethod]
        public void Call_call_i3_and_get_interaction_state_and_disconnect_it()
        {
            var actual = EasyIceServiceImpl.CreateCallInteraction(new CreateCallInteractionData(TESTPHONENUMBER, String.Empty, String.Empty));

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Generic interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Generic interaction id key is null or empty");

            var checkActual = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(checkActual.ErrorMessage), String.Format("Got error message: '{0}'.", checkActual.ErrorMessage));
            Assert.IsTrue(checkActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(checkActual.IsConnected, "Call interaction is NOT connected.");
            Assert.IsFalse(checkActual.IsDisconnected, "Call interaction is disconnected.");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");

            var checkActual2 = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(checkActual2.ErrorMessage), String.Format("Got error message: '{0}'.", checkActual2.ErrorMessage));
            Assert.IsTrue(checkActual2.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(checkActual2.IsConnected, "Call interaction is connected.");
            Assert.IsTrue(checkActual2.IsDisconnected, "Call interaction is NOT disconnected.");
        }
        #endregion

        #region Fax Interactions
        [TestMethod]
        public void Fax_send_myself_a_tif_fax()
        {
            var actual = EasyIceServiceImpl.SendFax(new SendFaxData(TESTFAXNUMBER, TESTFAXTIFFILE, "Pierrick", "I3", TESTPHONENUMBER, String.Empty, "pierrick.lozach@inin.com", "pierrick.lozach@inin.com", "Letter"));
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
        }
        [TestMethod]
        public void Fax_send_myself_a_pdf_fax()
        {
            var actual = EasyIceServiceImpl.SendFax(new SendFaxData(TESTFAXNUMBER, TESTFAXPDFFILE, "Pierrick", "I3", TESTPHONENUMBER, String.Empty, "pierrick.lozach@inin.com", "pierrick.lozach@inin.com", "Letter"));
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
        }
        #endregion

        #region Generic Interactions
        [TestMethod]
        public void Generic_create_an_interaction_and_disconnect_it()
        {
            var actual = EasyIceServiceImpl.CreateGenericInteraction(new CreateGenericInteractionData("Remote Name Test", "Remote Id Test", TESTWORKGROUP, String.Empty, String.Empty, String.Empty));
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Generic interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Generic interaction id key is null or empty");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");
        }
        [TestMethod]
        public void Generic_create_an_interaction_transfer_it_and_disconnect_it()
        {
            var actual = EasyIceServiceImpl.CreateGenericInteraction(new CreateGenericInteractionData("Remote Name Test", "Remote Id Test", TESTWORKGROUP, String.Empty, String.Empty, String.Empty));

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Generic interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Generic interaction id key is null or empty");

            var transferActual = EasyIceServiceImpl.TransferInteractionToWorkgroup(actual.InteractionId, "TestWorkgroup2");
            Assert.IsTrue(String.IsNullOrEmpty(transferActual.ErrorMessage), String.Format("Got error message: '{0}'.", transferActual.ErrorMessage));
            Assert.IsTrue(transferActual.Success, "Success is not 'true' as expected.");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");
        }
        [TestMethod]
        public void Generic_create_an_interaction_then_use_all_available_interaction_functionalities()
        {
            var actual = EasyIceServiceImpl.CreateGenericInteraction(new CreateGenericInteractionData("Remote Name Test", "Remote Id Test", TESTWORKGROUP, String.Empty, "Custom_TestAttribute", "Custom_TestValue"));

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Generic interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Generic interaction id key is null or empty");

            var getAttributeCall = EasyIceServiceImpl.GetInteractionAttribute(actual.InteractionId, "Custom_TestAttribute");
            Assert.IsTrue(String.IsNullOrEmpty(getAttributeCall.ErrorMessage), String.Format("Got error message: '{0}'.", getAttributeCall.ErrorMessage));
            Assert.IsTrue(getAttributeCall.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getAttributeCall.AttributeValue.Equals("Custom_TestValue"));

            var setAttributeCall = EasyIceServiceImpl.SetInteractionAttribute(actual.InteractionId, "Custom_TestAttribute", new SetInteractionAttributeData("Custom_TestValueOverwrite"));
            Assert.IsTrue(String.IsNullOrEmpty(setAttributeCall.ErrorMessage), String.Format("Got error message: '{0}'.", setAttributeCall.ErrorMessage));
            Assert.IsTrue(setAttributeCall.Success, "Success is not 'true' as expected.");

            var transferActual = EasyIceServiceImpl.TransferInteractionToWorkgroup(actual.InteractionId, TESTWORKGROUP2);
            Assert.IsTrue(String.IsNullOrEmpty(transferActual.ErrorMessage), String.Format("Got error message: '{0}'.", transferActual.ErrorMessage));
            Assert.IsTrue(transferActual.Success, "Success is not 'true' as expected.");

            Thread.Sleep(1000);
            var checkWorkgroupActual = EasyIceServiceImpl.IsInteractionInWorkgroupQueue(actual.InteractionId, TESTWORKGROUP2);
            Assert.IsTrue(String.IsNullOrEmpty(checkWorkgroupActual.ErrorMessage), String.Format("Got error message: '{0}'.", checkWorkgroupActual.ErrorMessage));
            Assert.IsTrue(checkWorkgroupActual.Exists, "Interaction does not exist in workgroup queue.");

            //Should be false
            var checkUserActual = EasyIceServiceImpl.IsInteractionInUserQueue(actual.InteractionId, TESTWORKGROUP2);
            Assert.IsTrue(String.IsNullOrEmpty(checkUserActual.ErrorMessage), String.Format("Got error message: '{0}'.", checkUserActual.ErrorMessage));
            Assert.IsFalse(checkUserActual.Exists, "Interaction does not exist in user queue.");

            var getDurationActual = EasyIceServiceImpl.GetInteractionDuration(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getDurationActual.ErrorMessage), String.Format("Got error message: '{0}'.", getDurationActual.ErrorMessage));
            Assert.IsTrue(getDurationActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getDurationActual.Duration > 0, "Interaction duration is 0.");

            var getInitiationTimeActual = EasyIceServiceImpl.GetInteractionInitiationTime(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getInitiationTimeActual.ErrorMessage), String.Format("Got error message: '{0}'.", getInitiationTimeActual.ErrorMessage));
            Assert.IsTrue(getInitiationTimeActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getInitiationTimeActual.InitiationTime < DateTime.Now, "Interaction initiation time is not before current time.");

            var getDisconnectionTimeActual = EasyIceServiceImpl.GetInteractionDisconnectionTime(actual.InteractionId);
            Assert.IsFalse(String.IsNullOrEmpty(getDisconnectionTimeActual.ErrorMessage), String.Format("Got error message: '{0}'.", getDisconnectionTimeActual.ErrorMessage));
            Assert.IsFalse(getDisconnectionTimeActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getDisconnectionTimeActual.DisconnectionTime < DateTime.Now, "Interaction disconnection time is not before current time.");

            var getTypeActual = EasyIceServiceImpl.GetInteractionType(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getTypeActual.ErrorMessage), String.Format("Got error message: '{0}'.", getTypeActual.ErrorMessage));
            Assert.IsTrue(getTypeActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getTypeActual.Type.Equals("Generic", StringComparison.InvariantCultureIgnoreCase), "Interaction is NOT a generic interaction.");

            var getStatusesActual = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getStatusesActual.ErrorMessage), String.Format("Got error message: '{0}'.", getStatusesActual.ErrorMessage));
            Assert.IsTrue(getStatusesActual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(getStatusesActual.IsConnected, "Interaction is connected.");
            Assert.IsFalse(getStatusesActual.IsDisconnected, "Interaction is disconnected.");
            Assert.IsFalse(getStatusesActual.IsHeld, "Interaction is held.");
            Assert.IsFalse(getStatusesActual.IsMonitored, "Interaction is monitored.");
            Assert.IsFalse(getStatusesActual.IsMuted, "Interaction is muted.");
            Assert.IsFalse(getStatusesActual.IsPaused, "Interaction recording is paused.");
            Assert.IsFalse(getStatusesActual.IsPrivate, "Interaction is private.");

            // Hold (should fail since the interaction is waiting for an agent)
            var holdInteractionActual = EasyIceServiceImpl.HoldInteraction(actual.InteractionId);
            Assert.IsFalse(String.IsNullOrEmpty(holdInteractionActual.ErrorMessage), String.Format("Got error message: '{0}'.", holdInteractionActual.ErrorMessage));
            Assert.IsFalse(holdInteractionActual.Success, "Success is not 'false' as expected.");
            var getHoldActual = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getHoldActual.ErrorMessage), String.Format("Got error message: '{0}'.", getHoldActual.ErrorMessage));
            Assert.IsTrue(getHoldActual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(getHoldActual.IsHeld, "Interaction is held ??!!");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");

            var getDisconnectedActual = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getDisconnectedActual.ErrorMessage), String.Format("Got error message: '{0}'.", getDisconnectedActual.ErrorMessage));
            Assert.IsTrue(getDisconnectedActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getDisconnectedActual.IsDisconnected, "Interaction is not disconnected.");

            var getDisconnectionTime2Actual = EasyIceServiceImpl.GetInteractionDisconnectionTime(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getDisconnectionTime2Actual.ErrorMessage), String.Format("Got error message: '{0}'.", getDisconnectionTime2Actual.ErrorMessage));
            Assert.IsTrue(getDisconnectionTime2Actual.Success, "Success is not 'true' as expected.");
            Thread.Sleep(5000);
            Assert.IsTrue(getDisconnectionTime2Actual.DisconnectionTime < DateTime.Now, String.Format("Interaction disconnection time ({0}) is past current time ({1}).", getDisconnectionTime2Actual.DisconnectionTime, DateTime.Now));
        }
        #endregion

        #region Email Interactions
        [TestMethod]
        public void Email_create_an_interaction_and_disconnect_it()
        {
            var actual = EasyIceServiceImpl.CreateEmailInteraction(new CreateEmailInteractionData("bla@bla.com", "Bla sender", "Test Subject", "Test Body", String.Empty));

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Email interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Email interaction id key is null or empty");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");
        }
        [TestMethod]
        public void Email_create_an_interaction_transfer_it_and_disconnect_it()
        {
            var actual = EasyIceServiceImpl.CreateEmailInteraction(new CreateEmailInteractionData("bla@bla.com", "Bla sender", "Test Subject", "Test Body", String.Empty));

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Email interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Email interaction id key is null or empty");

            var transferActual = EasyIceServiceImpl.TransferInteractionToWorkgroup(actual.InteractionId, "TestWorkgroup2");
            Assert.IsTrue(String.IsNullOrEmpty(transferActual.ErrorMessage), String.Format("Got error message: '{0}'.", transferActual.ErrorMessage));
            Assert.IsTrue(transferActual.Success, "Success is not 'true' as expected.");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");
        }
        [TestMethod]
        public void Email_create_an_interaction_then_use_all_available_interaction_functionalities()
        {
            var actual = EasyIceServiceImpl.CreateEmailInteraction(new CreateEmailInteractionData("bla@bla.com", "Bla sender", "Test Subject", "Test Body", TESTWORKGROUP));

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Email interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Email interaction id key is null or empty");

            //Should be false
            var getAttributeCall = EasyIceServiceImpl.GetInteractionAttribute(actual.InteractionId, "Custom_TestAttribute");
            Assert.IsTrue(String.IsNullOrEmpty(getAttributeCall.ErrorMessage), String.Format("Got error message: '{0}'.", getAttributeCall.ErrorMessage));
            Assert.IsTrue(getAttributeCall.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(getAttributeCall.AttributeValue.Equals("Custom_TestValue"));

            var setAttributeCall = EasyIceServiceImpl.SetInteractionAttribute(actual.InteractionId, "Custom_TestAttribute", new SetInteractionAttributeData("Custom_TestValueOverwrite"));
            Assert.IsTrue(String.IsNullOrEmpty(setAttributeCall.ErrorMessage), String.Format("Got error message: '{0}'.", setAttributeCall.ErrorMessage));
            Assert.IsTrue(setAttributeCall.Success, "Success is not 'true' as expected.");

            var transferActual = EasyIceServiceImpl.TransferInteractionToWorkgroup(actual.InteractionId, "TestWorkgroup2");
            Assert.IsTrue(String.IsNullOrEmpty(transferActual.ErrorMessage), String.Format("Got error message: '{0}'.", transferActual.ErrorMessage));
            Assert.IsTrue(transferActual.Success, "Success is not 'true' as expected.");

            Thread.Sleep(5000);

            //var checkWorkgroupActual = EasyIceServiceImpl.IsInteractionInWorkgroupQueue(actual.InteractionId, "TestWorkgroup2");
            //Assert.IsTrue(String.IsNullOrEmpty(checkWorkgroupActual.ErrorMessage), String.Format("Got error message: '{0}'.", checkWorkgroupActual.ErrorMessage));
            //Assert.IsTrue(checkWorkgroupActual.Exists, "Interaction does not exist in workgroup queue.");

            //Should be false
            var checkUserActual = EasyIceServiceImpl.IsInteractionInUserQueue(actual.InteractionId, "TestWorkgroup2");
            Assert.IsTrue(String.IsNullOrEmpty(checkUserActual.ErrorMessage), String.Format("Got error message: '{0}'.", checkUserActual.ErrorMessage));
            Assert.IsFalse(checkUserActual.Exists, "Interaction does not exist in user queue.");

            var getDurationActual = EasyIceServiceImpl.GetInteractionDuration(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getDurationActual.ErrorMessage), String.Format("Got error message: '{0}'.", getDurationActual.ErrorMessage));
            Assert.IsTrue(getDurationActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getDurationActual.Duration > 0, "Interaction duration is 0.");

            var getInitiationTimeActual = EasyIceServiceImpl.GetInteractionInitiationTime(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getInitiationTimeActual.ErrorMessage), String.Format("Got error message: '{0}'.", getInitiationTimeActual.ErrorMessage));
            Assert.IsTrue(getInitiationTimeActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getInitiationTimeActual.InitiationTime < DateTime.Now, "Interaction initiation time is not before current time.");

            var getDisconnectionTimeActual = EasyIceServiceImpl.GetInteractionDisconnectionTime(actual.InteractionId);
            Assert.IsFalse(String.IsNullOrEmpty(getDisconnectionTimeActual.ErrorMessage), String.Format("Got error message: '{0}'.", getDisconnectionTimeActual.ErrorMessage));
            Assert.IsFalse(getDisconnectionTimeActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getDisconnectionTimeActual.DisconnectionTime < DateTime.Now, "Interaction disconnection time is not before current time.");

            var getTypeActual = EasyIceServiceImpl.GetInteractionType(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getTypeActual.ErrorMessage), String.Format("Got error message: '{0}'.", getTypeActual.ErrorMessage));
            Assert.IsTrue(getTypeActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getTypeActual.Type.Equals("Email", StringComparison.InvariantCultureIgnoreCase), "Interaction is NOT an email interaction.");

            var getStatusesActual = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getStatusesActual.ErrorMessage), String.Format("Got error message: '{0}'.", getStatusesActual.ErrorMessage));
            Assert.IsTrue(getStatusesActual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(getStatusesActual.IsConnected, "Interaction is connected.");
            Assert.IsFalse(getStatusesActual.IsDisconnected, "Interaction is disconnected.");
            Assert.IsFalse(getStatusesActual.IsHeld, "Interaction is held.");
            Assert.IsFalse(getStatusesActual.IsMonitored, "Interaction is monitored.");
            Assert.IsFalse(getStatusesActual.IsMuted, "Interaction is muted.");
            Assert.IsFalse(getStatusesActual.IsPaused, "Interaction recording is paused.");
            Assert.IsFalse(getStatusesActual.IsPrivate, "Interaction is private.");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");

            var getDisconnectedActual = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getDisconnectedActual.ErrorMessage), String.Format("Got error message: '{0}'.", getDisconnectedActual.ErrorMessage));
            Assert.IsTrue(getDisconnectedActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getDisconnectedActual.IsDisconnected, "Interaction is not disconnected.");

            var getDisconnectionTime2Actual = EasyIceServiceImpl.GetInteractionDisconnectionTime(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getDisconnectionTime2Actual.ErrorMessage), String.Format("Got error message: '{0}'.", getDisconnectionTime2Actual.ErrorMessage));
            Assert.IsTrue(getDisconnectionTime2Actual.Success, "Success is not 'true' as expected.");
            Thread.Sleep(5000);
            Assert.IsTrue(getDisconnectionTime2Actual.DisconnectionTime < DateTime.Now, String.Format("Interaction disconnection time ({0}) is past current time ({1}).", getDisconnectionTime2Actual.DisconnectionTime, DateTime.Now));
        }
        #endregion

        #region Callback Interactions
        [TestMethod]
        public void Callback_create_an_interaction_and_disconnect_it()
        {
            var actual = EasyIceServiceImpl.CreateCallbackInteraction(new CreateCallbackInteractionData("123456", "Customer to callback", "Test Callback", TESTWORKGROUP, String.Empty, 0, "Test Notes"));

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Callback interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Callback interaction id key is null or empty");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");
        }
        [TestMethod]
        public void Callback_create_an_interaction_transfer_it_and_disconnect_it()
        {
            var actual = EasyIceServiceImpl.CreateCallbackInteraction(new CreateCallbackInteractionData("123456", "Customer to callback", "Test Callback", TESTWORKGROUP, String.Empty, 0, "Test Notes"));

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Callback interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Callback interaction id key is null or empty");

            var transferActual = EasyIceServiceImpl.TransferInteractionToWorkgroup(actual.InteractionId, TESTWORKGROUP2);
            Assert.IsTrue(String.IsNullOrEmpty(transferActual.ErrorMessage), String.Format("Got error message: '{0}'.", transferActual.ErrorMessage));
            Assert.IsTrue(transferActual.Success, "Success is not 'true' as expected.");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");
        }
        [TestMethod]
        public void Callback_create_an_interaction_then_use_all_available_interaction_functionalities()
        {
            var actual = EasyIceServiceImpl.CreateCallbackInteraction(new CreateCallbackInteractionData("123456", "Customer to callback", "Test Callback", TESTWORKGROUP, String.Empty, 0, "Test Notes"));

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Callback interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Callback interaction id key is null or empty");

            //Should be false
            var getAttributeCall = EasyIceServiceImpl.GetInteractionAttribute(actual.InteractionId, "Custom_TestAttribute");
            Assert.IsTrue(String.IsNullOrEmpty(getAttributeCall.ErrorMessage), String.Format("Got error message: '{0}'.", getAttributeCall.ErrorMessage));
            Assert.IsTrue(getAttributeCall.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(getAttributeCall.AttributeValue.Equals("Custom_TestValue"));

            var setAttributeCall = EasyIceServiceImpl.SetInteractionAttribute(actual.InteractionId, "Custom_TestAttribute", new SetInteractionAttributeData("Custom_TestValueOverwrite"));
            Assert.IsTrue(String.IsNullOrEmpty(setAttributeCall.ErrorMessage), String.Format("Got error message: '{0}'.", setAttributeCall.ErrorMessage));
            Assert.IsTrue(setAttributeCall.Success, "Success is not 'true' as expected.");

            var transferActual = EasyIceServiceImpl.TransferInteractionToWorkgroup(actual.InteractionId, TESTWORKGROUP2);
            Assert.IsTrue(String.IsNullOrEmpty(transferActual.ErrorMessage), String.Format("Got error message: '{0}'.", transferActual.ErrorMessage));
            Assert.IsTrue(transferActual.Success, "Success is not 'true' as expected.");

            Thread.Sleep(5000);
            var checkWorkgroupActual = EasyIceServiceImpl.IsInteractionInWorkgroupQueue(actual.InteractionId, TESTWORKGROUP2);
            Assert.IsTrue(String.IsNullOrEmpty(checkWorkgroupActual.ErrorMessage), String.Format("Got error message: '{0}'.", checkWorkgroupActual.ErrorMessage));
            Assert.IsTrue(checkWorkgroupActual.Exists, "Interaction does not exist in workgroup queue.");

            //Should be false
            var checkUserActual = EasyIceServiceImpl.IsInteractionInUserQueue(actual.InteractionId, TESTWORKGROUP2);
            Assert.IsTrue(String.IsNullOrEmpty(checkUserActual.ErrorMessage), String.Format("Got error message: '{0}'.", checkUserActual.ErrorMessage));
            Assert.IsFalse(checkUserActual.Exists, "Interaction does not exist in user queue.");

            var getDurationActual = EasyIceServiceImpl.GetInteractionDuration(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getDurationActual.ErrorMessage), String.Format("Got error message: '{0}'.", getDurationActual.ErrorMessage));
            Assert.IsTrue(getDurationActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getDurationActual.Duration > 0, "Interaction duration is 0.");

            var getInitiationTimeActual = EasyIceServiceImpl.GetInteractionInitiationTime(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getInitiationTimeActual.ErrorMessage), String.Format("Got error message: '{0}'.", getInitiationTimeActual.ErrorMessage));
            Assert.IsTrue(getInitiationTimeActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getInitiationTimeActual.InitiationTime < DateTime.Now, "Interaction initiation time is not before current time.");

            var getDisconnectionTimeActual = EasyIceServiceImpl.GetInteractionDisconnectionTime(actual.InteractionId);
            Assert.IsFalse(String.IsNullOrEmpty(getDisconnectionTimeActual.ErrorMessage), String.Format("Got error message: '{0}'.", getDisconnectionTimeActual.ErrorMessage));
            Assert.IsFalse(getDisconnectionTimeActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getDisconnectionTimeActual.DisconnectionTime < DateTime.Now, "Interaction disconnection time is not before current time.");

            var getTypeActual = EasyIceServiceImpl.GetInteractionType(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getTypeActual.ErrorMessage), String.Format("Got error message: '{0}'.", getTypeActual.ErrorMessage));
            Assert.IsTrue(getTypeActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getTypeActual.Type.Equals("Callback", StringComparison.InvariantCultureIgnoreCase), "Interaction is NOT a callback interaction.");

            var getStatusesActual = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getStatusesActual.ErrorMessage), String.Format("Got error message: '{0}'.", getStatusesActual.ErrorMessage));
            Assert.IsTrue(getStatusesActual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(getStatusesActual.IsConnected, "Interaction is connected.");
            Assert.IsFalse(getStatusesActual.IsDisconnected, "Interaction is disconnected.");
            Assert.IsFalse(getStatusesActual.IsHeld, "Interaction is held.");
            Assert.IsFalse(getStatusesActual.IsMonitored, "Interaction is monitored.");
            Assert.IsFalse(getStatusesActual.IsMuted, "Interaction is muted.");
            Assert.IsFalse(getStatusesActual.IsPaused, "Interaction recording is paused.");
            Assert.IsFalse(getStatusesActual.IsPrivate, "Interaction is private.");

            // Hold
            var holdInteractionActual = EasyIceServiceImpl.HoldInteraction(actual.InteractionId);
            Assert.IsFalse(String.IsNullOrEmpty(holdInteractionActual.ErrorMessage), String.Format("Got error message: '{0}'.", holdInteractionActual.ErrorMessage));
            Assert.IsFalse(holdInteractionActual.Success, "Success is not 'true' as expected.");
            var getHoldActual = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getHoldActual.ErrorMessage), String.Format("Got error message: '{0}'.", getHoldActual.ErrorMessage));
            Assert.IsTrue(getHoldActual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(getHoldActual.IsHeld, "Interaction is held.");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");

            var getDisconnectedActual = EasyIceServiceImpl.GetInteractionStatuses(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getDisconnectedActual.ErrorMessage), String.Format("Got error message: '{0}'.", getDisconnectedActual.ErrorMessage));
            Assert.IsTrue(getDisconnectedActual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(getDisconnectedActual.IsDisconnected, "Interaction is not disconnected.");

            var getDisconnectionTime2Actual = EasyIceServiceImpl.GetInteractionDisconnectionTime(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(getDisconnectionTime2Actual.ErrorMessage), String.Format("Got error message: '{0}'.", getDisconnectionTime2Actual.ErrorMessage));
            Assert.IsTrue(getDisconnectionTime2Actual.Success, "Success is not 'true' as expected.");
            Thread.Sleep(5000);
            Assert.IsTrue(getDisconnectionTime2Actual.DisconnectionTime < DateTime.Now, String.Format("Interaction disconnection time ({0}) is past current time ({1}).", getDisconnectionTime2Actual.DisconnectionTime, DateTime.Now));
        }
        #endregion

        #region Chat Interactions
        [TestMethod]
        public void Chat_create_an_interaction_and_add_message_and_disconnect_it()
        {
            var actual = EasyIceServiceImpl.CreateChatInteraction(new CreateChatInteractionData(TESTUSER));
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Chat interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Chat interaction id key is null or empty");

            var addMessageActual = EasyIceServiceImpl.WriteToChatInteraction(actual.InteractionId, new WriteToChatInteractionData("Bonjour!!"));
            Assert.IsTrue(String.IsNullOrEmpty(addMessageActual.ErrorMessage), String.Format("Got error message: '{0}'.", addMessageActual.ErrorMessage));
            Assert.IsTrue(addMessageActual.Success, "Success is not 'true' as expected.");

            //Wait until it is picked up to check the message
            Thread.Sleep(10000);

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");
        }
        #endregion

        #endregion

        #region Configuration

        #region Users
        [TestMethod]
        public void Users_get_all_users_and_check_there_is_at_least_one()
        {
            var expected = new GetUsersResponse {Success = true};

            var actual = EasyIceServiceImpl.GetUsers();

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.AreEqual(expected.Success, actual.Success, String.Format("Success is not '{0}' as expected.", expected.Success));
            Assert.IsTrue(actual.Users.Count > 0, "No users found on CIC.");
        }

        [TestMethod]
        public void Users_create_a_user_then_get_its_configuration_then_delete_it()
        {
            if (EasyIceServiceImpl.GetUser("Test User").Success)
            {
                var actualDelete = EasyIceServiceImpl.DeleteUser("Test User");
                Assert.IsTrue(actualDelete.Success);
            }
            var actualCreate = EasyIceServiceImpl.CreateUser("Test User", new CreateUserData("1234", "5555"));
            Assert.IsTrue(String.IsNullOrEmpty(actualCreate.ErrorMessage), String.Format("Got error message: '{0}'.", actualCreate.ErrorMessage));
            Assert.IsTrue(actualCreate.Success);

            var actualGetNotExistingUserConfiguration = EasyIceServiceImpl.GetUser("YadiYada");
            Assert.IsFalse(String.IsNullOrEmpty(actualGetNotExistingUserConfiguration.ErrorMessage), "Didn't get an error message?");
            Assert.IsFalse(actualGetNotExistingUserConfiguration.Success);

            var actualGetUserConfiguration = EasyIceServiceImpl.GetUser("Test User");
            Assert.IsTrue(String.IsNullOrEmpty(actualGetUserConfiguration.ErrorMessage), String.Format("Got error message: '{0}'.", actualGetUserConfiguration.ErrorMessage));
            Assert.IsTrue(actualGetUserConfiguration.Success);
            Assert.IsNotNull(actualGetUserConfiguration.UserConfiguration);
            Assert.IsTrue(actualGetUserConfiguration.UserConfiguration.Extension.Equals("5555"), "Newly created user extension does not match.");
            Assert.IsTrue(actualGetUserConfiguration.UserConfiguration.Id.Equals("Test User"), "Newly created user id does not match.");
            Assert.IsTrue(String.IsNullOrEmpty(actualGetUserConfiguration.UserConfiguration.DefaultWorkstation), "Newly created user should not have a default workstation set.");

            var actualGetUsers = EasyIceServiceImpl.GetUsers();
            Assert.IsTrue(String.IsNullOrEmpty(actualGetUsers.ErrorMessage), String.Format("Got error message: '{0}'.", actualGetUsers.ErrorMessage));
            Assert.IsTrue(actualGetUsers.Success);
            Assert.IsTrue(actualGetUsers.Users.Contains("Test User"), "Failed to find newly created user.");

            //Should be false
            var actualIsInWorkgroup = EasyIceServiceImpl.GetUsersFromWorkgroup("TestWorkgroup");
            Assert.IsTrue(String.IsNullOrEmpty(actualIsInWorkgroup.ErrorMessage), String.Format("Got error message: '{0}'.", actualIsInWorkgroup.ErrorMessage));
            Assert.IsFalse(actualIsInWorkgroup.Users.Contains("Test User"), "Newly created user shouldn't be in a workgroup.");

            var actualDeleteUser = EasyIceServiceImpl.DeleteUser("Test User");
            Assert.IsTrue(String.IsNullOrEmpty(actualDeleteUser.ErrorMessage), String.Format("Got error message: '{0}'.", actualDeleteUser.ErrorMessage));
            Assert.IsTrue(actualDeleteUser.Success);
        }
        #endregion

        #region Workgroups
        [TestMethod]
        public void Workgroups_get_all_workgroups_and_check_there_is_at_least_one()
        {
            var actual = EasyIceServiceImpl.GetWorkgroups();

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(actual.Workgroups.Count > 0, "No workgroups found on CIC.");
        }

        [TestMethod]
        public void Workgroups_create_a_workgroup_then_get_its_configuration_then_delete_it()
        {
            var actualCreate = EasyIceServiceImpl.CreateWorkgroup("Test Workgroup", new CreateWorkgroupData("6666"));
            Assert.IsTrue(String.IsNullOrEmpty(actualCreate.ErrorMessage), String.Format("Got error message: '{0}'.", actualCreate.ErrorMessage));
            Assert.IsTrue(actualCreate.Success);

            var actualGetNotExistingWorkgroupConfiguration = EasyIceServiceImpl.GetWorkgroup("YadiYada");
            Assert.IsFalse(String.IsNullOrEmpty(actualGetNotExistingWorkgroupConfiguration.ErrorMessage), "Didn't get an error message?");
            Assert.IsFalse(actualGetNotExistingWorkgroupConfiguration.Success);

            var actualGetWorkgroupConfiguration = EasyIceServiceImpl.GetWorkgroup("Test Workgroup");
            Assert.IsTrue(String.IsNullOrEmpty(actualGetWorkgroupConfiguration.ErrorMessage), String.Format("Got error message: '{0}'.", actualGetWorkgroupConfiguration.ErrorMessage));
            Assert.IsTrue(actualGetWorkgroupConfiguration.Success);
            Assert.IsNotNull(actualGetWorkgroupConfiguration.WorkgroupConfiguration);
            Assert.IsTrue(actualGetWorkgroupConfiguration.WorkgroupConfiguration.Extension.Equals("6666"), "Newly created workgroup extension does not match.");
            Assert.IsTrue(actualGetWorkgroupConfiguration.WorkgroupConfiguration.Id.Equals("Test Workgroup"), "Newly created workgroup id does not match.");

            var actualGetWorkgroups = EasyIceServiceImpl.GetWorkgroups();
            Assert.IsTrue(String.IsNullOrEmpty(actualGetWorkgroups.ErrorMessage), String.Format("Got error message: '{0}'.", actualGetWorkgroups.ErrorMessage));
            Assert.IsTrue(actualGetWorkgroups.Success);
            Assert.IsTrue(actualGetWorkgroups.Workgroups.Contains("Test Workgroup"), "Failed to find newly created workgroup.");

            //Should be false
            var actualIsInWorkgroup = EasyIceServiceImpl.GetUsersFromWorkgroup("Test Workgroup");
            Assert.IsTrue(String.IsNullOrEmpty(actualIsInWorkgroup.ErrorMessage), String.Format("Got error message: '{0}'.", actualIsInWorkgroup.ErrorMessage));
            Assert.IsFalse(actualIsInWorkgroup.Users.Count > 0, "Newly created workgroup shouldn't have members.");

            var actualDeleteWorkgroup = EasyIceServiceImpl.DeleteWorkgroup("Test Workgroup");
            Assert.IsTrue(String.IsNullOrEmpty(actualDeleteWorkgroup.ErrorMessage), String.Format("Got error message: '{0}'.", actualDeleteWorkgroup.ErrorMessage));
            Assert.IsTrue(actualDeleteWorkgroup.Success);
        }
        #endregion

        #region Stations
        [TestMethod]
        public void Stations_get_all_stations_and_check_there_is_at_least_one()
        {
            var actual = EasyIceServiceImpl.GetStations();

            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsTrue(actual.Stations.Count > 0, "No Stations found on CIC.");
        }

        [TestMethod]
        public void Stations_create_a_station_then_get_its_configuration_then_delete_it()
        {
            var actualCreate = EasyIceServiceImpl.CreateStation(TESTSTATION, new CreateStationData(TESTSTATIONEXTENSION, TESTSTATIONEXTENSION, String.Empty, String.Empty));
            Assert.IsTrue(String.IsNullOrEmpty(actualCreate.ErrorMessage), String.Format("Got error message: '{0}'.", actualCreate.ErrorMessage));
            Assert.IsTrue(actualCreate.Success);

            var actualGetNotExistingStationConfiguration = EasyIceServiceImpl.GetStation("YadiYada");
            Assert.IsFalse(String.IsNullOrEmpty(actualGetNotExistingStationConfiguration.ErrorMessage), "Didn't get an error message?");
            Assert.IsFalse(actualGetNotExistingStationConfiguration.Success);

            var actualGetStationConfiguration = EasyIceServiceImpl.GetStation(TESTSTATION);
            Assert.IsTrue(String.IsNullOrEmpty(actualGetStationConfiguration.ErrorMessage), String.Format("Got error message: '{0}'.", actualGetStationConfiguration.ErrorMessage));
            Assert.IsTrue(actualGetStationConfiguration.Success);
            Assert.IsNotNull(actualGetStationConfiguration.StationConfiguration);
            Assert.IsTrue(actualGetStationConfiguration.StationConfiguration.Extension.Equals(TESTSTATIONEXTENSION), "Newly created station extension does not match.");
            Assert.IsTrue(actualGetStationConfiguration.StationConfiguration.Id.Equals(TESTSTATION), "Newly created station id does not match.");

            var actualGetStations = EasyIceServiceImpl.GetStations();
            Assert.IsTrue(String.IsNullOrEmpty(actualGetStations.ErrorMessage), String.Format("Got error message: '{0}'.", actualGetStations.ErrorMessage));
            Assert.IsTrue(actualGetStations.Success);
            Assert.IsTrue(actualGetStations.Stations.Contains(TESTSTATION), "Failed to find newly created station.");

            var actualDeleteStation = EasyIceServiceImpl.DeleteStation(TESTSTATION);
            Assert.IsTrue(String.IsNullOrEmpty(actualDeleteStation.ErrorMessage), String.Format("Got error message: '{0}'.", actualDeleteStation.ErrorMessage));
            Assert.IsTrue(actualDeleteStation.Success);
        }
        #endregion

        #endregion

        #region Recordings
        [TestMethod]
        public void Recordings_download_by_recording_id()
        {
            var actual = EasyIceServiceImpl.DownloadRecordingByRecordingId(TESTRECORDINGID);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success);
            Assert.IsTrue(actual.Streams.Count > 0, "No streams returned.");
            using (var localFile = File.Create("C:\\Users\\Pierrick\\Desktop\\Test.wav"))
                using (actual.Streams[0])
                    actual.Streams[0].CopyTo(localFile);
        }

        [TestMethod]
        public void Recordings_download_by_interaction_id()
        {
            var actual = EasyIceServiceImpl.DownloadRecordingByInteractionId(TESTRECORDINGINTERACTIONID);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success);
            Assert.IsTrue(actual.Streams.Count > 0, "No streams returned.");
            using (var localFile = File.Create("C:\\Users\\Pierrick\\Desktop\\Test.wav"))
            using (actual.Streams[0])
                actual.Streams[0].CopyTo(localFile);
        }

        [TestMethod]
        public void Recordings_download_by_interaction_id_key()
        {
            var actual = EasyIceServiceImpl.DownloadRecordingByInteractionIdKey(TESTRECORDINGINTERACTIONIDKEY);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success);
            Assert.IsTrue(actual.Streams.Count > 0, "No streams returned.");
            using (var localFile = File.Create("C:\\Users\\Pierrick\\Desktop\\Test.wav"))
            using (actual.Streams[0])
                actual.Streams[0].CopyTo(localFile);
        }
        #endregion

        #region Custom Notification
        [TestMethod]
        public void CustomNotification_send_custom_notification()
        {
            var actual = EasyIceServiceImpl.SendCustomNotification("test", "test", "test1|test2");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to send custom notification.");
        }
        #endregion

        #region Statistics
        [TestMethod]
        public void Statistics_get_all_statistics()
        {
            var actual = EasyIceServiceImpl.GetAllStatistics();
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get statistics.");
            Assert.IsTrue(actual.Statistics.Count > 0, "Failed to get statistics.");
        }

        [TestMethod]
        public void Statistics_should_have_a_lot_of_statistics()
        {
            var actual = EasyIceServiceImpl.GetAllStatistics();
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get statistics.");
            Assert.IsTrue(actual.Statistics.Count > 10, String.Format("Got {0} statistics", actual.Statistics.Count));
        }

        [TestMethod]
        public void Statistics_query_a_statistics_with_parameters()
        {
            var actualGetStatisticWithParameters = EasyIceServiceImpl.GetStatistic("ININ.Queue:InteractionCount", "User|devlab_user", "ININ.People.AgentStats:Workgroup|ININ.People.AgentStats:User");
            Assert.IsTrue(String.IsNullOrEmpty(actualGetStatisticWithParameters.ErrorMessage), String.Format("Got error message: '{0}'.", actualGetStatisticWithParameters.ErrorMessage));
            Assert.IsTrue(actualGetStatisticWithParameters.Success, "Failed to get Agent interactions count.");
            Assert.IsFalse(String.IsNullOrEmpty(actualGetStatisticWithParameters.StatisticValue), "Statistic (with parameters) value is null or empty.");
        }

        [TestMethod]
        public void Statistics_query_a_workgroup_statistics_with_parameters()
        {
            var actual = EasyIceServiceImpl.CreateGenericInteraction(new CreateGenericInteractionData("Remote Name Test", "Remote Id Test", TESTWORKGROUP, String.Empty, String.Empty, String.Empty));
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Success is not 'true' as expected.");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionId), "Generic interaction id is null or empty");
            Assert.IsFalse(String.IsNullOrEmpty(actual.InteractionIdKey), "Generic interaction id key is null or empty");

            var actualGetStatisticWithParameters = EasyIceServiceImpl.GetStatistic("ININ.Queue:InteractionCount", "Workgroup|TestWorkgroup", "ININ.Queue:Type|ININ.Queue:Name");
            Assert.IsTrue(String.IsNullOrEmpty(actualGetStatisticWithParameters.ErrorMessage), String.Format("Got error message: '{0}'.", actualGetStatisticWithParameters.ErrorMessage));
            Assert.IsTrue(actualGetStatisticWithParameters.Success, "Failed to get Workgroup interactions count.");
            Assert.IsFalse(String.IsNullOrEmpty(actualGetStatisticWithParameters.StatisticValue), "Statistic (with parameters) value is null or empty.");
            Assert.IsFalse(String.Equals(actualGetStatisticWithParameters.StatisticValue, "N/A"), "Interactions count is N/A");
            Assert.IsTrue(Convert.ToInt32(actualGetStatisticWithParameters.StatisticValue) > 0, "Interaction count is 0");

            var disconnectActual = EasyIceServiceImpl.DisconnectInteraction(actual.InteractionId);
            Assert.IsTrue(String.IsNullOrEmpty(disconnectActual.ErrorMessage), String.Format("Got error message: '{0}'.", disconnectActual.ErrorMessage));
            Assert.IsTrue(disconnectActual.Success, "Success is not 'true' as expected.");
        }

        [TestMethod]
        public void Statistics_query_a_workgroup_statistics_that_returns_na()
        {
            var actualGetStatisticWithParameters = EasyIceServiceImpl.GetStatistic("ININ.Workgroup:LongestOutboundACDInteraction", "TestWorkgroup", "ININ.People.WorkgroupStats:Workgroup");
            Assert.IsTrue(String.IsNullOrEmpty(actualGetStatisticWithParameters.ErrorMessage), String.Format("Got error message: '{0}'.", actualGetStatisticWithParameters.ErrorMessage));
            Assert.IsTrue(actualGetStatisticWithParameters.Success, "Failed to get Workgroup longest outbound ACD interaction.");
            Assert.IsFalse(String.IsNullOrEmpty(actualGetStatisticWithParameters.StatisticValue), "Statistic (with parameters) value is null or empty.");
            Assert.IsTrue(String.Equals(actualGetStatisticWithParameters.StatisticValue, "N/A"), "Statistic value is NOT N/A");
        }
        #endregion

        #region Reporting
        [TestMethod]
        public void Reporting_search_interactions_by_ani()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByAni("1234");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get interactions by ANI.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No interactions found.");
        }

        [TestMethod]
        public void Reporting_search_interactions_by_date_time_range()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByDateTimeRange(DateTime.Now.AddDays(-10), DateTime.Now);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get interactions by Date Time range.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No interactions found.");
        }

        [TestMethod]
        public void Reporting_search_interactions_by_dnis()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByDnis("1234");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get interactions by DNIS.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No interactions found.");
        }

        [TestMethod]
        public void Reporting_search_interactions_by_interaction_id()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByInteractionId("1001385668"); // Key = 100138566830121016
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get interactions by Interaction Id.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No interactions found.");
        }

        [TestMethod]
        public void Reporting_search_interactions_by_last_user()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByLastICUsername("devlab_user");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get interactions by Last IC Username.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No interactions found.");
        }

        [TestMethod]
        public void Reporting_search_interactions_by_last_workgroup()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByLastWorkgroup("TestWorkgroup2");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get interactions by last workgroup.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No interactions found.");
        }

        [TestMethod]
        public void Reporting_search_call_interactions()
        {
            var actualCall = EasyIceServiceImpl.SearchInteractionsByMediaType("Call");
            Assert.IsTrue(String.IsNullOrEmpty(actualCall.ErrorMessage), String.Format("Got error message: '{0}'.", actualCall.ErrorMessage));
            Assert.IsTrue(actualCall.Success, "Failed to get Call interactions.");
            Assert.IsTrue(actualCall.InteractionSnapshots.Count > 0, "No Call interactions found.");
        }

        [TestMethod]
        public void Reporting_search_generic_interactions()
        {
            var actualGeneric = EasyIceServiceImpl.SearchInteractionsByMediaType("Generic");
            Assert.IsTrue(String.IsNullOrEmpty(actualGeneric.ErrorMessage), String.Format("Got error message: '{0}'.", actualGeneric.ErrorMessage));
            Assert.IsTrue(actualGeneric.Success, "Failed to get Generic interactions.");
            Assert.IsTrue(actualGeneric.InteractionSnapshots.Count > 0, "No Generic interactions found.");
        }

        [TestMethod]
        public void Reporting_search_callback_interactions()
        {
            var actualCallback = EasyIceServiceImpl.SearchInteractionsByMediaType("Callback");
            Assert.IsTrue(String.IsNullOrEmpty(actualCallback.ErrorMessage), String.Format("Got error message: '{0}'.", actualCallback.ErrorMessage));
            Assert.IsTrue(actualCallback.Success, "Failed to get Callback interactions.");
            Assert.IsTrue(actualCallback.InteractionSnapshots.Count > 0, "No Callback interactions found.");
        }

        [TestMethod]
        public void Reporting_search_chat_interactions()
        {
            var actualChat = EasyIceServiceImpl.SearchInteractionsByMediaType("Chat");
            Assert.IsTrue(String.IsNullOrEmpty(actualChat.ErrorMessage), String.Format("Got error message: '{0}'.", actualChat.ErrorMessage));
            Assert.IsTrue(actualChat.Success, "Failed to get Chat interactions.");
            Assert.IsTrue(actualChat.InteractionSnapshots.Count > 0, "No Chat interactions found.");

            var actualEmail = EasyIceServiceImpl.SearchInteractionsByMediaType("Email");
            Assert.IsTrue(String.IsNullOrEmpty(actualEmail.ErrorMessage), String.Format("Got error message: '{0}'.", actualEmail.ErrorMessage));
            Assert.IsTrue(actualEmail.Success, "Failed to get Email interactions.");
            Assert.IsTrue(actualEmail.InteractionSnapshots.Count > 0, "No Email interactions found.");

            var actualFax = EasyIceServiceImpl.SearchInteractionsByMediaType("Fax");
            Assert.IsTrue(String.IsNullOrEmpty(actualFax.ErrorMessage), String.Format("Got error message: '{0}'.", actualFax.ErrorMessage));
            Assert.IsTrue(actualFax.Success, "Failed to get Fax interactions.");
            Assert.IsTrue(actualFax.InteractionSnapshots.Count > 0, "No Fax interactions found.");

            var actualInstantQuestion = EasyIceServiceImpl.SearchInteractionsByMediaType("InstantQuestion");
            Assert.IsTrue(String.IsNullOrEmpty(actualInstantQuestion.ErrorMessage), String.Format("Got error message: '{0}'.", actualInstantQuestion.ErrorMessage));
            Assert.IsTrue(actualInstantQuestion.Success, "Failed to get InstantQuestion interactions.");
            Assert.IsTrue(actualInstantQuestion.InteractionSnapshots.Count > 0, "No InstantQuestion interactions found.");
        }

        [TestMethod]
        public void Reporting_search_email_interactions()
        {
            var actualEmail = EasyIceServiceImpl.SearchInteractionsByMediaType("Email");
            Assert.IsTrue(String.IsNullOrEmpty(actualEmail.ErrorMessage), String.Format("Got error message: '{0}'.", actualEmail.ErrorMessage));
            Assert.IsTrue(actualEmail.Success, "Failed to get Email interactions.");
            Assert.IsTrue(actualEmail.InteractionSnapshots.Count > 0, "No Email interactions found.");
        }

        [TestMethod]
        public void Reporting_search_fax_interactions()
        {
            var actualFax = EasyIceServiceImpl.SearchInteractionsByMediaType("Fax");
            Assert.IsTrue(String.IsNullOrEmpty(actualFax.ErrorMessage), String.Format("Got error message: '{0}'.", actualFax.ErrorMessage));
            Assert.IsTrue(actualFax.Success, "Failed to get Fax interactions.");
            Assert.IsTrue(actualFax.InteractionSnapshots.Count > 0, "No Fax interactions found.");
        }

        [TestMethod]
        public void Reporting_search_instantquestion_interactions()
        {
            var actualInstantQuestion = EasyIceServiceImpl.SearchInteractionsByMediaType("InstantQuestion");
            Assert.IsTrue(String.IsNullOrEmpty(actualInstantQuestion.ErrorMessage), String.Format("Got error message: '{0}'.", actualInstantQuestion.ErrorMessage));
            Assert.IsTrue(actualInstantQuestion.Success, "Failed to get InstantQuestion interactions.");
            Assert.IsTrue(actualInstantQuestion.InteractionSnapshots.Count > 0, "No InstantQuestion interactions found.");
        }

        [TestMethod]
        public void Reporting_search_invalid_interactions()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByMediaType("Invalid");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get Invalid interactions.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No Invalid interactions found.");
        }

        [TestMethod]
        public void Reporting_search_sms_interactions()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByMediaType("SMS");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get SMS interactions.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No SMS interactions found.");
        }

        [TestMethod]
        public void Reporting_search_webcollaboration_interactions()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByMediaType("WebCollaboration");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get WebCollaboration interactions.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No WebCollaboration interactions found.");
        }

        [TestMethod]
        public void Reporting_search_workflowitemobject_interactions()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsByMediaType("WorkflowItemObject");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get WorkflowItemObject interactions.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No WorkflowItemObject interactions found.");
        }

        [TestMethod]
        public void Reporting_search_interactions_by_site_id()
        {
            var actual = EasyIceServiceImpl.SearchInteractionsBySiteId("1");
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get interactions by Site Id.");
            Assert.IsTrue(actual.InteractionSnapshots.Count > 0, "No interactions found by Site Id.");
        }
        #endregion

        #region Directories
        [TestMethod]
        public void Directories_get_user_directory_phone_number()
        {
            var actual = EasyIceServiceImpl.GetUserDirectoryPhoneNumbers("Company Directory", TESTUSER);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsNotNull(actual.Contact, "Failed to get contact information.");
        }
        #endregion

        #region IPA
        [TestMethod]
        public void IPA_get_launchable_ipa_processes()
        {
            var actual = EasyIceServiceImpl.GetLaunchableIPAProcesses();
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get IPA launchable processes.");
            Assert.IsTrue(actual.IPAProcesses.Count > 0, "No launchable IPA processes found.");
        }

        [TestMethod]
        public void IPA_get_launchable_ipa_processes_and_start_the_first_one_then_cancel_it()
        {
            var actual = EasyIceServiceImpl.GetLaunchableIPAProcesses();
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get IPA launchable processes.");
            Assert.IsTrue(actual.IPAProcesses.Count > 0, "No launchable IPA processes found.");

            // Should fail
            var actualStart = EasyIceServiceImpl.StartIPAProcess("YadiYada");
            Assert.IsFalse(String.IsNullOrEmpty(actualStart.ErrorMessage), String.Format("Got error message: '{0}'.", actualStart.ErrorMessage));
            Assert.IsFalse(actualStart.Success, "Succeeded in starting non-existent IPA process 'YadiYada' ???");

            var actualStart2 = EasyIceServiceImpl.StartIPAProcess(actual.IPAProcesses[0]);
            Assert.IsTrue(String.IsNullOrEmpty(actualStart2.ErrorMessage), String.Format("Got error message: '{0}'.", actualStart2.ErrorMessage));
            Assert.IsTrue(actualStart2.Success, String.Format("Failed to start IPA process '{0}'.", actual.IPAProcesses[0]));

            var actualCancel = EasyIceServiceImpl.CancelIPAProcess(actual.IPAProcesses[0], "Unit testing");
            Assert.IsTrue(String.IsNullOrEmpty(actualCancel.ErrorMessage), String.Format("Got error message: '{0}'.", actualCancel.ErrorMessage));
            Assert.IsTrue(actualCancel.Success, String.Format("Failed to cancel IPA process '{0}'.", actual.IPAProcesses[0]));
        }
        #endregion

        //#region eFAQ
        //[TestMethod]
        //public void query_eFaq()
        //{
        //    var actual = easyIceServiceImpl.QueryEFaq("Stuff", "Hello there", "100", "50", "50");
        //    Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
        //    Assert.IsTrue(actual.QueryResults.Count > 0, "Failed to query eFaq.");
        //}
        //#endregion

        #region Dialer
        [TestMethod]
        public void Dialer_get_available_campaigns()
        {
            var actual = EasyIceServiceImpl.GetAvailableCampaigns();
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get available campaigns.");
            Assert.IsTrue(actual.AvailableCampaigns.Count > 0, "No available campaigns found.");
        }

        [TestMethod]
        public void Dialer_get_contacts_from_a_campaign()
        {
            var actual = EasyIceServiceImpl.GetContacts(TESTCAMPAIGNNAME);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get contacts.");
            Assert.IsTrue(actual.Contacts.Count > 0, "No contacts found.");
        }

        [TestMethod]
        public void Dialer_get_contacts_from_a_non_existent_campaign()
        {
            var actual = EasyIceServiceImpl.GetContacts("blabla_campaign");
            Assert.IsTrue(!String.IsNullOrEmpty(actual.ErrorMessage), "Didn't get an error message. Does blabla_campaign exist?");
            Assert.IsFalse(actual.Success, "Succeeded in getting contacts from a non existent campaign!");
            Assert.IsTrue(actual.Contacts.Count == 0, "Contacts found!");
        }

        [TestMethod]
        public void Dialer_insert_new_contact()
        {
            var insertContactData = new Dictionary<String, object> {{TESTINSERTCONTACTCOLUMNNAME, TESTINSERTCONTACTCOLUMNVALUE}};
            var actual = EasyIceServiceImpl.InsertContact(new InsertContactData(insertContactData, TESTCAMPAIGNNAME, false));
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to insert contact.");
        }

        [TestMethod]
        public void Dialer_recycle_contact_list()
        {
            var actual = EasyIceServiceImpl.RecycleContactList(TESTCAMPAIGNNAME);
            Assert.IsTrue(actual.Success, "Failed to insert contact.");
            if (!actual.Success)
                Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
        }

        [TestMethod]
        public void Dialer_recycle_contact_list_for_a_non_existent_campaign()
        {
            var actual = EasyIceServiceImpl.RecycleContactList("blabla_campaign");
            Assert.IsFalse(actual.Success, "Contact inserted? This shouldn't work.");
            Assert.IsTrue(!String.IsNullOrEmpty(actual.ErrorMessage), "Does blabla_campaign really exist?");
        }

        [TestMethod]
        public void Dialer_get_active_and_eligible_agents_for_campaign()
        {
            var actual = EasyIceServiceImpl.GetCampaignAgents(TESTCAMPAIGNNAME);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get campaign agents.");
        }

        [TestMethod]
        public void Dialer_get_active_and_eligible_agents_for_non_existent_campaign()
        {
            var actual = EasyIceServiceImpl.GetCampaignAgents("blabla_campaign");
            Assert.IsTrue(!String.IsNullOrEmpty(actual.ErrorMessage), "Didn't get an error message? Campaign should not exist.");
            Assert.IsFalse(actual.Success, "Does campaign blabla_campaign really exist?");
        }

        [TestMethod]
        public void Dialer_get_active_and_eligible_campaigns_for_agent()
        {
            var actual = EasyIceServiceImpl.GetAgentCampaigns(TESTUSER);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "Failed to get campaign agents.");
        }

        [TestMethod]
        public void Dialer_get_active_and_eligible_campaigns_for_non_existent_agent()
        {
            var actual = EasyIceServiceImpl.GetAgentCampaigns("blabla_user");
            Assert.IsTrue(!String.IsNullOrEmpty(actual.ErrorMessage), "Didn't get error message. User should not exist.");
            Assert.IsFalse(actual.Success, "Does user blabla_user really exist?");
        }

        [TestMethod]
        public void Dialer_exclude_phone_number_from_a_campaign()
        {
            var actual = EasyIceServiceImpl.ExcludePhoneNumberFromCampaign(TESTCAMPAIGNNAME, TESTPHONENUMBERTOEXCLUDE);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "An error occurred.");
        }

        [TestMethod]
        public void Dialer_exclude_phone_number_from_all_available_campaigns()
        {
            var actual = EasyIceServiceImpl.ExcludePhoneNumberFromAllAvailableCampaigns(TESTPHONENUMBERTOEXCLUDE);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "An error occurred.");
        }

        [TestMethod]
        public void Dialer_get_campaign_status_for_a_campaign_that_exists()
        {
            var actual = EasyIceServiceImpl.GetCampaignStatus(TESTCAMPAIGNNAME);
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "A general error occurred.");
            Assert.IsTrue(actual.CampaignValidationResult.Success, "An error occurred while running the validation test.");
            Assert.IsTrue(actual.CampaignTestResult.Success, "An error occurred while running the quick test.");
        }

        [TestMethod]
        public void Dialer_get_campaign_status_for_all_available_campaigns()
        {
            var actual = EasyIceServiceImpl.GetCampaignsStatus();
            Assert.IsTrue(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Got error message: '{0}'.", actual.ErrorMessage));
            Assert.IsTrue(actual.Success, "A general error occurred.");
            foreach (var campaignValidationResult in actual.CampaignValidationResults)
            {
                Assert.IsTrue(campaignValidationResult.Success, String.Format("An error occurred while running the validation test for campaign '{0}'.", campaignValidationResult.Campaign));
            }
            foreach (var campaignTestResult in actual.CampaignTestResults)
            {
                Assert.IsTrue(campaignTestResult.Success, String.Format("An error occurred while running the quick test for campaign '{0}'.", campaignTestResult.Campaign));
            }
        }

        [TestMethod]
        public void Dialer_get_campaign_status_for_a_nonexistent_campaign()
        {
            var actual = EasyIceServiceImpl.GetCampaignStatus("Yadiyada");
            Assert.IsFalse(String.IsNullOrEmpty(actual.ErrorMessage), String.Format("Didn't get an error message???"));
            Assert.IsFalse(actual.Success, "Should be false.");
        }
        #endregion
    }
}
