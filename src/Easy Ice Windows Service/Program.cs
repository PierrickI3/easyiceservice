﻿using System.ServiceProcess;

namespace EasyIceWindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var servicesToRun = new ServiceBase[] 
                { 
                    new EasyIceService1() 
                };
            ServiceBase.Run(servicesToRun);
        }
    }
}
