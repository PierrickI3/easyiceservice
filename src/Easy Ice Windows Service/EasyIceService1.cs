﻿using System.Diagnostics;
using System.ServiceModel;
using System.ServiceProcess;

namespace EasyIceWindowsService
{
    public partial class EasyIceService1 : ServiceBase
    {
        ServiceHost _serviceHost;

        public EasyIceService1()
        {
            InitializeComponent();
        }

        #region Service Start/Stop
        protected override void OnStart(string[] args)
        {
            if (_serviceHost != null)
                _serviceHost.Close();

            _serviceHost = new ServiceHost(typeof(WCFEasyIceService.EasyIceServiceImpl));
            _serviceHost.Opening += serviceHost_Opening;
            _serviceHost.Opened += serviceHost_Opened;
            _serviceHost.Closing += serviceHost_Closing;
            _serviceHost.Closed += serviceHost_Closed;
            _serviceHost.Faulted += serviceHost_Faulted;
            _serviceHost.Open();
        }
        protected override void OnStop()
        {
            if (_serviceHost == null) return;

            _serviceHost.Close();
            _serviceHost = null;
        }
        #endregion

        #region Service Host Events
        void serviceHost_Faulted(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Error, "Service is in a faulted state. Please restart the 'Easy Ice Service' Windows service.");
        }
        void serviceHost_Closed(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Information, "Easy Ice Service is now closed.");
        }
        void serviceHost_Closing(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Information, "Easy Ice Service is closing.");
        }
        void serviceHost_Opened(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Information, "Easy Ice Service is now opened.");
        }
        void serviceHost_Opening(object sender, System.EventArgs e)
        {
            LogMessage(EventLogEntryType.Information, "Easy Ice Service is opening.");
        }
        #endregion

        private void LogMessage(EventLogEntryType entryType, string message)
        {
            const string eventSource = "Easy Ice Service";
            const string eventLog = "Application";

            if (!EventLog.SourceExists(eventSource))
                EventLog.CreateEventSource(eventSource, eventLog);

            EventLog.WriteEntry(eventSource, message, entryType);
        }
    }
}
