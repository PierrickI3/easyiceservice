﻿using System;

namespace EncryptionConsole
{
    class Program
    {
        private const string KEY = "3#sy!c3S3rv!c3"; // Easy Ice Service key
        //private const string key = "GpSkOg+S"; // EasyScripter key
        static void Main()
        {
            Console.Write("Enter the password to decrypt:");
            string password = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine("Calling Encryptor...");
            var encryptor = new Encryptor();
            string decryptedPassword = encryptor.DecryptFromBase64String(password, KEY);
            Console.WriteLine("Decrypted password: " + decryptedPassword);
            Console.WriteLine("Press any key to exit.");
            Console.ReadLine();


            //Console.Write("Enter the password to encrypt:");
            //string password = Console.ReadLine();
            //Console.WriteLine();
            //Console.WriteLine("Calling Encryptor...");
            //Encryptor encryptor = new Encryptor();
            //string encryptedPassword = encryptor.EncryptToBase64String(password, key);
            //Console.WriteLine("Encrypted password: " + encryptedPassword);
            //Console.WriteLine();
            //Console.WriteLine("Press any key to exit.");
            //Console.ReadLine();
        }
    }
}
