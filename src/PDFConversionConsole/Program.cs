﻿using System;
using System.Diagnostics;
using System.ComponentModel;

namespace PDFConversionConsole
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Converting test.pdf");
            var backgroundWorker = new BackgroundWorker();

            backgroundWorker.DoWork += backgroundWorker_DoWork;
            backgroundWorker.RunWorkerAsync("test.pdf");

            Console.WriteLine("Background worker started.");
            Console.WriteLine();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        static void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ConvertPDFToTiff((string)e.Argument);
        }
        public static bool ConvertPDFToTiff(string pdfFilename)
        {
            var success = false;
            try
            {
                var conversionProcess = new Process
                    {
                        StartInfo =
                            {
                                UseShellExecute = true,
                                CreateNoWindow = true,
                                FileName = "convert",
                                WorkingDirectory = Environment.CurrentDirectory + "\\Conversion",
                                Arguments =
                                    String.Format(
                                        "-density 600x600 -colorspace gray -ordered-dither 1x1 -compress group4 ..\\{0} ..\\{1}.tif",
                                        pdfFilename, pdfFilename.Substring(0, pdfFilename.LastIndexOf(".pdf", StringComparison.Ordinal)))
                            }
                    };
                success = conversionProcess.Start();
                if (conversionProcess.HasExited)
                {
                    return false;
                }
                conversionProcess.PriorityClass = ProcessPriorityClass.Idle;
                conversionProcess.ProcessorAffinity = new IntPtr(1);
                conversionProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return success;
        }
    }
}
