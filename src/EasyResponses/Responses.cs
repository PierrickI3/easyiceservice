﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;

namespace EasyResponses
{
    #region Login/Logout
    /// <summary>
    /// Response passed back when calling Login()
    /// </summary>
    [DataContract]
    public class LoginResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success?: '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back when calling Logout()
    /// </summary>
    [DataContract]
    public class LogoutResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success?: '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Class helper for holding CIC credentials.
    /// </summary>
    [DataContract]
    public class CICSettings
    {
        /// <summary>
        /// CIC Server name or IP address.
        /// </summary>
        public string CICServer { get; set; }
        /// <summary>
        /// CIC Username.
        /// </summary>
        public string CICUsername { get; set; }
        /// <summary>
        /// CIC Password.
        /// </summary>
        public string CICPassword { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public CICSettings()
        {
        }
        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="cicServer">CIC Server name or IP address.</param>
        /// <param name="cicUsername">CIC username.</param>
        /// <param name="cicPassword">CIC password.</param>
        public CICSettings(string cicServer, string cicUsername, string cicPassword)
        {
            CICServer = cicServer;
            CICUsername = cicUsername;
            CICPassword = cicPassword;
        }
        /// <summary>
        /// Checks if all settings are correct.
        /// </summary>
        /// <returns>True if valid, false otherwise.</returns>
        public bool IsValid()
        {
            return !String.IsNullOrEmpty(CICServer) && !String.IsNullOrEmpty(CICUsername) && !String.IsNullOrEmpty(CICPassword);
        }
        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("CIC Server: '{0}', CIC Username: '{1}', CIC Password: '****'.", CICServer, CICUsername);
        }
    }
    #endregion

    #region User Status
    /// <summary>
    /// Response passed back after calling GetUserStatus()
    /// </summary>
    [DataContract]
    public class GetUserStatusResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Status localized message text (i.e. Available, Disponible, etc).
        /// </summary>
        [DataMember]
        public string MessageText { get; set; }

        /// <summary>
        /// Http link to get the icon filename.
        /// </summary>
        [DataMember]
        public string IconFilename { get; set; }

        /// <summary>
        /// If a forward status is selected, contains the forward number incoming call interactions will be transferred to.
        /// </summary>
        [DataMember]
        public string ForwardNumber { get; set; }

        /// <summary>
        /// If true, the agent is logged in. False otherwise.
        /// </summary>
        [DataMember]
        public string LoggedIn { get; set; }

        /// <summary>
        /// Status notes (optional).
        /// </summary>
        [DataMember]
        public string Notes { get; set; }

        /// <summary>
        /// If true, agent is on the phone. False otherwise.
        /// </summary>
        [DataMember]
        public string OnPhone { get; set; }

        /// <summary>
        /// Contains the date/time when the <see cref="OnPhone"/> flag was set.
        /// </summary>
        [DataMember]
        public string OnPhoneChanged { get; set; }

        /// <summary>
        /// Contains the date/time when the status was last changed.
        /// </summary>
        [DataMember]
        public string StatusChanged { get; set; }

        /// <summary>
        /// If available, contains the "Until Date" section of the status.
        /// </summary>
        [DataMember]
        public string UntilDate { get; set; }

        /// <summary>
        /// If available, contains the "Until Time" section of the status.
        /// </summary>
        [DataMember]
        public string UntilTime { get; set; }

        /// <summary>
        /// Status id as specified in Administrator (i.e. Available, DoNotDisturb, etc).
        /// </summary>
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// Set to "True" if the status is an after call work (or follow up) status.
        /// </summary>
        [DataMember]
        public string IsAfterCallWork { get; set; }

        /// <summary>
        /// Set to "True" if the status allows ACD interactions.
        /// </summary>
        [DataMember]
        public string IsAcd { get; set; }

        /// <summary>
        /// Set to "True" if the status allows follow up statuses to be selected after interactions disconnect.
        /// </summary>
        [DataMember]
        public string IsAllowFollowUp { get; set; }

        /// <summary>
        /// Set to "True" if the status is a DND status.
        /// </summary>
        [DataMember]
        public string IsDoNotDisturb { get; set; }

        /// <summary>
        /// Set to "True" if the status allows forward information to be set.
        /// </summary>
        [DataMember]
        public string IsForward { get; set; }

        /// <summary>
        /// Set to "True" if the status remains after the agent logs out.
        /// </summary>
        [DataMember]
        public string IsPersistent { get; set; }

        /// <summary>
        /// Set to "True" if the status can be selected by the agent.
        /// </summary>
        [DataMember]
        public string IsSelectable { get; set; }

        /// <summary>
        /// Set to "True" if the status is a valid status.
        /// </summary>
        [DataMember]
        public string IsValid { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Status Message Text: '{2}'.", Success, ErrorMessage, MessageText);
        }
    }

    /// <summary>
    /// Response passed back after calling SetUserStatus()
    /// </summary>
    [DataContract]
    public class SetUserStatusResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }
    #endregion

    #region Server Parameter
    /// <summary>
    /// Response passed back after calling GetServerParameter()
    /// </summary>
    [DataContract]
    public class GetServerParameterResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Value of the server parameter if found.
        /// </summary>
        [DataMember]
        public string ServerParameterValue { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Server Parameter Value: '{2}'.", Success, ErrorMessage, ServerParameterValue);
        }
    }
    #endregion

    #region Interactions

    #region Common to All Interactions
    /// <summary>
    /// Response passed back after calling TransferInteraction()
    /// </summary>
    [DataContract]
    public class TransferInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling DisconnectInteraction()
    /// </summary>
    [DataContract]
    public class DisconnectInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling EasyIceServiceImpl.GetInteractionAttribute()
    /// </summary>
    [DataContract]
    public class GetInteractionAttributeResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Interaction Attribute value.
        /// </summary>
        [DataMember]
        public string AttributeValue { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Attribute Value: '{2}'.", Success, ErrorMessage, AttributeValue);
        }
    }

    /// <summary>
    /// Response passed back after calling SetInteractionAttribute()
    /// </summary>
    [DataContract]
    public class SetInteractionAttributeResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetInteractionsWithAttributeValue()
    /// </summary>
    [DataContract]
    public class GetInteractionsWithAttributeValueResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _interactionsId;
        /// <summary>
        /// List of interactions with the attribute name set to the specified value.
        /// </summary>
        [DataMember]
        public List<String> InteractionsId
        {
            get { return _interactionsId ?? (_interactionsId = new List<String>()); }
            set { _interactionsId = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of interactions: '{2}'.", Success, ErrorMessage, InteractionsId.Count);
        }
    }

    /// <summary>
    /// Response passed back after calling IsInteractionInUserQueue()
    /// </summary>
    [DataContract]
    public class IsInteractionInUserQueueResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Exists"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item exists. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Exists { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Exists? '{0}', Error Message: '{1}'.", Exists, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling IsInteractionInWorkgroupQueue()
    /// </summary>
    [DataContract]
    public class IsInteractionInWorkgroupQueueResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Exists"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item exists. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Exists { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Exists? '{0}', Error Message: '{1}'.", Exists, ErrorMessage);
        }
    }
    #endregion

    #region Interaction Actions
    /// <summary>
    /// Response passed back after calling AppendInteractionNote()
    /// </summary>
    [DataContract]
    public class AppendInteractionNoteResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling PickupInteraction()
    /// </summary>
    [DataContract]
    public class PickupInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling HoldInteraction()
    /// </summary>
    [DataContract]
    public class HoldInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling MuteInteraction() and UnmuteInteraction()
    /// </summary>
    [DataContract]
    public class MuteInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling ParkInteraction()
    /// </summary>
    [DataContract]
    public class ParkInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling PrivateInteraction() and UnprivateInteraction()
    /// </summary>
    [DataContract]
    public class PrivateInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling RecordInteraction() and StopRecordInteraction()
    /// </summary>
    [DataContract]
    public class RecordInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling PauseInteraction(), ResumeInteraction(), SecurePauseInteraction() and SecureResumeInteraction()
    /// </summary>
    [DataContract]
    public class PauseInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling ResumeInteraction()
    /// </summary>
    [DataContract]
    public class ResumeInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling UpdateInteractionDeallocationTime()
    /// </summary>
    [DataContract]
    public class UpdateInteractionDeallocationTimeResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling TransferInteractionToVoicemail()
    /// </summary>
    [DataContract]
    public class TransferInteractionToVoicemailResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }
    #endregion

    #region Interaction Properties
    /// <summary>
    /// Response passed back after calling GetInteractionDuration()
    /// </summary>
    [DataContract]
    public class GetInteractionDurationResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// The interaction's duration in seconds.
        /// </summary>
        [DataMember]
        public double Duration { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Duration: '{2}'.", Success, ErrorMessage, Duration);
        }
    }

    /// <summary>
    /// Response passed back after calling GetInteractionInitiationTime()
    /// </summary>
    [DataContract]
    public class GetInteractionInitiationTimeResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// The interaction's initiation time (in CIC server timezone).
        /// </summary>
        [DataMember]
        public DateTime InitiationTime { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Initiation Time: '{2}'.", Success, ErrorMessage, InitiationTime.ToString(CultureInfo.InvariantCulture));
        }
    }

    /// <summary>
    /// Response passed back after calling GetInteractionDisconnectionTime()
    /// </summary>
    [DataContract]
    public class GetInteractionDisconnectionTimeResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// The interaction's disconnection time (in CIC server timezone), if the interaction is disconnected.
        /// </summary>
        [DataMember]
        public DateTime DisconnectionTime { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Disconnection Time: '{2}'.", Success, ErrorMessage, DisconnectionTime.ToString(CultureInfo.InvariantCulture));
        }
    }

    /// <summary>
    /// Response passed back after calling GetInteractionType()
    /// </summary>
    [DataContract]
    public class GetInteractionTypeResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// The interaction's type (call, fax, email, generic, etc.).
        /// </summary>
        [DataMember]
        public string Type { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Type: '{2}'.", Success, ErrorMessage, Type);
        }
    }

    /// <summary>
    /// Response passed back after calling GetInteractionStatuses()
    /// </summary>
    [DataContract]
    public class GetInteractionStatusesResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// True if the interaction is connected to an agent, false otherwise.
        /// </summary>
        [DataMember]
        public bool IsConnected { get; set; }

        /// <summary>
        /// True if the interaction is disconnected to an agent, false otherwise.
        /// </summary>
        [DataMember]
        public bool IsDisconnected { get; set; }

        /// <summary>
        /// True if the interaction is held, false otherwise.
        /// </summary>
        [DataMember]
        public bool IsHeld { get; set; }

        /// <summary>
        /// True if the interaction is being monitored by the session user (configured in the configuration file), false otherwise.
        /// </summary>
        [DataMember]
        public bool IsMonitored { get; set; }

        /// <summary>
        /// True if the interaction is muted, false otherwise.
        /// </summary>
        [DataMember]
        public bool IsMuted { get; set; }

        /// <summary>
        /// True if the interaction recording is paused, false otherwise.
        /// </summary>
        [DataMember]
        public bool IsPaused { get; set; }

        /// <summary>
        /// True if the interaction is private, false otherwise.
        /// </summary>
        [DataMember]
        public bool IsPrivate { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', IsConnected: '{2}', IsDisconnected: '{3}', IsHeld: '{4}', IsMonitored: '{5}', IsMuted: '{6}', IsPaused: '{7}', IsPrivate: '{8}'.", Success, ErrorMessage, IsConnected, IsDisconnected, IsHeld, IsMonitored, IsMuted, IsPaused, IsPrivate);
        }
    }

    /// <summary>
    /// Response passed back after calling GetInteractionLastSegment()
    /// </summary>
    [DataContract]
    public class GetInteractionLastSegmentResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Queue Id for the interaction (name of the workgroup/distribution queue).
        /// </summary>
        [DataMember]
        public string QueueId { get; set; }

        /// <summary>
        /// Segment id.
        /// </summary>
        [DataMember]
        public int SegmentId { get; set; }

        /// <summary>
        /// User identifier.
        /// </summary>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// If true, segment requires a wrap up code.
        /// </summary>
        [DataMember]
        public bool WrapUpRequired { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Segment Id: '{2}', Queue Id: '{3}', User Id: '{4}', Wrap Up Required: '{5}'.", Success, ErrorMessage, SegmentId, QueueId, UserId, WrapUpRequired);
        }
    }

    /// <summary>
    /// Response passed back after calling GetAccountCodeId()
    /// </summary>
    [DataContract]
    public class GetAccountCodeIdResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Account Code Id associated with the interaction.
        /// </summary>
        [DataMember]
        public string AccountCodeId { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Account Code Id: '{2}'.", Success, ErrorMessage, AccountCodeId);
        }
    }

    /// <summary>
    /// Response passed back after calling SetAccountCodeId()
    /// </summary>
    [DataContract]
    public class SetAccountCodeIdResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetRemoteName()
    /// </summary>
    [DataContract]
    public class GetRemoteNameResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Caller's Name.
        /// </summary>
        [DataMember]
        public string RemoteName { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Remote Name: '{2}'.", Success, ErrorMessage, RemoteName);
        }
    }

    /// <summary>
    /// Response passed back after calling SetRemoteName()
    /// </summary>
    [DataContract]
    public class SetRemoteNameResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetWrapUpCodeId()
    /// </summary>
    [DataContract]
    public class GetWrapUpCodeIdResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Wrap Up Code Id.
        /// </summary>
        [DataMember]
        public string WrapUpCodeId { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Wrap Up Code Id: '{2}'.", Success, ErrorMessage, WrapUpCodeId);
        }
    }

    /// <summary>
    /// Response passed back after calling SetWrapUpCodeId()
    /// </summary>
    [DataContract]
    public class SetWrapUpCodeIdResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetTimeInWorkgroupQueue()
    /// </summary>
    [DataContract]
    public class GetTimeInWorkgroupQueueResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Duration (in seconds) of the interaction as of the time it joined a workgroup queue.
        /// </summary>
        [DataMember]
        public double TimeInWorkgroupQueue { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Time in workgroup queue (in seconds): '{2}'.", Success, ErrorMessage, TimeInWorkgroupQueue);
        }
    }
    #endregion

    #region Call Interactions
    /// <summary>
    /// Response passed back after calling CreateCallInteraction()
    /// </summary>
    [DataContract]
    public class CreateCallInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Interaction identifier of the call that was placed.
        /// </summary>
        [DataMember]
        public string InteractionId { get; set; }

        /// <summary>
        /// Interaction id key of the call that was placed.
        /// </summary>
        [DataMember]
        public string InteractionIdKey { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Interaction Id: '{2}', Interaction Id Key: '{3}'.", Success, ErrorMessage, InteractionId, InteractionIdKey);
        }
    }

    /// <summary>
    /// Response passed back after calling PlayWavFile()
    /// </summary>
    [DataContract]
    public class PlayWavFileResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling PlayDigits()
    /// </summary>
    [DataContract]
    public class PlayDigitsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling StopWaveAudio()
    /// </summary>
    [DataContract]
    public class StopWaveAudioResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }
    #endregion

    #region Chat Interactions
    /// <summary>
    /// Response passed back after calling CreateChatInteraction()
    /// </summary>
    [DataContract]
    public class CreateChatInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Interaction identifier of the chat that was created.
        /// </summary>
        [DataMember]
        public string InteractionId { get; set; }

        /// <summary>
        /// Interaction id key of the chat that was created.
        /// </summary>
        [DataMember]
        public string InteractionIdKey { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Interaction Id: '{2}', Interaction Id Key: '{3}'.", Success, ErrorMessage, InteractionId, InteractionIdKey);
        }
    }

    /// <summary>
    /// Response passed back after calling WriteToChatInteraction()
    /// </summary>
    [DataContract]
    public class WriteToChatInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }
    #endregion

    #region Email Interactions
    /// <summary>
    /// Response passed back after calling CreateEmailInteraction()
    /// </summary>
    [DataContract]
    public class CreateEmailInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Interaction identifier of the call that was placed.
        /// </summary>
        [DataMember]
        public string InteractionId { get; set; }

        /// <summary>
        /// Interaction id key of the email that was created.
        /// </summary>
        [DataMember]
        public string InteractionIdKey { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Interaction Id: '{2}', Interaction Id Key: '{3}'.", Success, ErrorMessage, InteractionId, InteractionIdKey);
        }
    }
    #endregion

    #region Fax Interactions
    /// <summary>
    /// Response passed back after calling SendFax().
    /// </summary>
    [DataContract]
    public class SendFaxResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, fax request has been successfully processed and fax will be sent soon. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }
    #endregion

    #region Generic Interactions
    /// <summary>
    /// Response passed back after calling CreateGenericInteraction()
    /// </summary>
    [DataContract]
    public class CreateGenericInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Interaction identifier of the call that was placed.
        /// </summary>
        [DataMember]
        public string InteractionId { get; set; }

        /// <summary>
        /// Interaction id key of the generic interaction that was created.
        /// </summary>
        [DataMember]
        public string InteractionIdKey { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Interaction Id: '{2}', Interaction Id Key: '{3}'.", Success, ErrorMessage, InteractionId, InteractionIdKey);
        }
    }
    #endregion

    #region Callback Interactions
    /// <summary>
    /// Response passed back after calling CreateCallbackInteraction()
    /// </summary>
    [DataContract]
    public class CreateCallbackInteractionResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Interaction identifier of the call that was placed.
        /// </summary>
        [DataMember]
        public string InteractionId { get; set; }

        /// <summary>
        /// Interaction id key of the callback interaction that was created.
        /// </summary>
        [DataMember]
        public string InteractionIdKey { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Interaction Id: '{2}', Interaction Id Key: '{3}'.", Success, ErrorMessage, InteractionId, InteractionIdKey);
        }
    }
    #endregion

    #endregion

    #region Recordings
    /// <summary>
    /// Response passed back after calling DownloadRecording()
    /// </summary>
    [DataContract]
    public class DownloadRecordingResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, operation was successful. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// File streams to download the recording(s). One interaction can have more than one recordings.
        /// </summary>
        [DataMember]
        public List<Stream> Streams { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of wav files: '{2}'.", Success, ErrorMessage, Streams != null ? Streams.Count : 0);
        }
    }
    #endregion

    #region Configuration

    #region Users
    /// <summary>
    /// Response passed back after calling GetUsers()
    /// </summary>
    [DataContract]
    public class GetUsersResponse
    {
        private string _errorMessage;
        /// <summary>
        /// Contains an error message if an error occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item were retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _users;
        /// <summary>
        /// Contains the list of all active users.
        /// </summary>
        [DataMember]
        public List<String> Users
        {
            get { return _users ?? (_users = new List<string>()); }
            set { _users = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Number of active users: '{1}', Error Message: '{2}'.", Success, Users.Count, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling CreateUser() or CreateUserExtended()
    /// </summary>
    [DataContract]
    public class CreateUserResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred (i.e. user already exists).
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, user was successfully created. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetUser()
    /// </summary>
    [DataContract]
    public class GetUserResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private SerializedUserConfiguration _userConfiguration;
        /// <summary>
        /// Contains the user configuration.
        /// </summary>
        [DataMember]
        public SerializedUserConfiguration UserConfiguration
        {
            get { return _userConfiguration ?? (_userConfiguration = new SerializedUserConfiguration()); }
            set { _userConfiguration = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', User Configuration: '{1}', Error Message: '{2}'.", Success, UserConfiguration, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetUserInteractions()
    /// </summary>
    [DataContract]
    public class GetUserInteractionsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _interactionIds;
        /// <summary>
        /// Contains the list of interaction ids.
        /// </summary>
        [DataMember]
        public List<String> InteractionIds
        {
            get { return _interactionIds ?? (_interactionIds = new List<String>()); }
            set { _interactionIds = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of interaction ids: '{2}'.", Success, ErrorMessage, InteractionIds.Count);
        }
    }

    /// <summary>
    /// Represents a user configuration.
    /// </summary>
    [DataContract]
    public class SerializedUserConfiguration
    {
        private List<String> _workgroups;
        /// <summary>
        /// List of workgroups this user is member of.
        /// </summary>
        [DataMember]
        public List<String> Workgroups
        {
            get { return _workgroups ?? (_workgroups = new List<String>()); }
            set { _workgroups = value; }
        }

        private List<String> _skills;
        /// <summary>
        /// List of skills this user is assigned to.
        /// </summary>
        [DataMember]
        public List<String> Skills
        {
            get { return _skills ?? (_skills = new List<String>()); }
            set { _skills = value; }
        }

        private List<String> _roles;
        /// <summary>
        /// List of roles this user is assigned to.
        /// </summary>
        [DataMember]
        public List<String> Roles
        {
            get { return _roles ?? (_roles = new List<String>()); }
            set { _roles = value; }
        }

        /// <summary>
        /// Preferred Language (if available)
        /// </summary>
        [DataMember]
        public string PreferredLanguage { get; set; }

        /// <summary>
        /// User's title (if available)
        /// </summary>
        [DataMember]
        public string Title { get; set; }

        /// <summary>
        /// User's surname (if available)
        /// </summary>
        [DataMember]
        public string Surname { get; set; }

        /// <summary>
        /// User's street address (if available)
        /// </summary>
        [DataMember]
        public string StreetAddress { get; set; }

        /// <summary>
        /// User's state or province (if available).
        /// </summary>
        [DataMember]
        public string StateOrProvince { get; set; }

        /// <summary>
        /// User's postal code (if available).
        /// </summary>
        [DataMember]
        public string PostalCode { get; set; }

        /// <summary>
        /// User's given name (if available).
        /// </summary>
        [DataMember]
        public string GivenName { get; set; }

        /// <summary>
        /// User's email address (if available).
        /// </summary>
        [DataMember]
        public string EmailAddress { get; set; }

        /// <summary>
        /// User's department name (if available).
        /// </summary>
        [DataMember]
        public string DepartmentName { get; set; }

        /// <summary>
        /// User's country (if available).
        /// </summary>
        [DataMember]
        public string Country { get; set; }

        /// <summary>
        /// User's company name (if available).
        /// </summary>
        [DataMember]
        public string CompanyName { get; set; }

        /// <summary>
        /// User's city (if available).
        /// </summary>
        [DataMember]
        public string City { get; set; }

        /// <summary>
        /// User's domain username (if available).
        /// </summary>
        [DataMember]
        public string NtDomainUser { get; set; }

        /// <summary>
        /// User id.
        /// </summary>
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// User's CIC home site.
        /// </summary>
        [DataMember]
        public string HomeSite { get; set; }

        /// <summary>
        /// User's CIC extension (if available).
        /// </summary>
        [DataMember]
        public string Extension { get; set; }

        /// <summary>
        /// User's display name (if available).
        /// </summary>
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// User's default workstation (if available).
        /// </summary>
        [DataMember]
        public string DefaultWorkstation { get; set; }

        /// <summary>
        /// User's alias (if available).
        /// </summary>
        [DataMember]
        public string Alias { get; set; }

        /// <summary>
        /// If true, ACD interactions will be auto-answered.
        /// </summary>
        [DataMember]
        public string AutoAnswerAcdInteractions { get; set; }

        /// <summary>
        /// If true, non-ACD interactions will be auto-answered.
        /// </summary>
        [DataMember]
        public string AutoAnswerNonAcdInteractions { get; set; }

        /// <summary>
        /// If true, user will not be shown in the Company Directory
        /// </summary>
        [DataMember]
        public string ExcludeFromDirectory { get; set; }

        /// <summary>
        /// Outbound call timeout (in seconds)
        /// </summary>
        [DataMember]
        public string ExtCallTimeout { get; set; }

        /// <summary>
        /// Timeout for incoming interactions for this user.
        /// </summary>
        [DataMember]
        public string InteractionOfferingTimeout { get; set; }

        /// <summary>
        /// Outbound phone number displayed to external parties when the user places an outbound call.
        /// </summary>
        [DataMember]
        public string OutboundAni { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Id: '{0}', Number of workgroups: '{1}', Number of skills: '{2}', etc.", Id, Workgroups.Count, Skills.Count);
        }
    }

    /// <summary>
    /// Response passed back after calling GetUsersFromWorkgroup()
    /// </summary>
    [DataContract]
    public class GetUsersFromWorkgroupResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Users"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item were retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _users;
        /// <summary>
        /// Contains the list of users of the specified workgroup.
        /// </summary>
        [DataMember]
        public List<String> Users
        {
            get { return _users ?? (_users = new List<string>()); }
            set { _users = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Number of users: '{1}', Error Message: '{2}'.", Success, Users.Count, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling DeleteUser()
    /// </summary>
    [DataContract]
    public class DeleteUserResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was deleted successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }
    #endregion

    #region Workgroups
    /// <summary>
    /// Response passed back after calling GetWorkgroup()
    /// </summary>
    [DataContract]
    public class GetWorkgroupResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private SerializedWorkgroupConfiguration _workgroupConfiguration;
        /// <summary>
        /// Contains the workgroup configuration.
        /// </summary>
        [DataMember]
        public SerializedWorkgroupConfiguration WorkgroupConfiguration
        {
            get { return _workgroupConfiguration ?? (_workgroupConfiguration = new SerializedWorkgroupConfiguration()); }
            set { _workgroupConfiguration = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Workgroup Configuration: '{1}', Error Message: '{2}'.", Success, WorkgroupConfiguration, ErrorMessage);
        }
    }

    /// <summary>
    /// Represents a workgroup configuration.
    /// </summary>
    [DataContract]
    public class SerializedWorkgroupConfiguration
    {
        /// <summary>
        /// If set to "true", has a queue (check <see cref="QueueType"/> to get the workgroup's queue type (ACD, round robin, etc.)). False otherwise.
        /// </summary>
        [DataMember]
        public string HasQueue { get; set; }

        /// <summary>
        /// Default interaction offering timeout.
        /// </summary>
        [DataMember]
        public string InteractionOfferingTimeout { get; set; }

        /// <summary>
        /// If true, workgroup is active. False otherwise.
        /// </summary>
        [DataMember]
        public string IsActive { get; set; }

        /// <summary>
        /// If true, wrap up is active on this workgroup.
        /// </summary>
        [DataMember]
        public string IsWrapUpActive { get; set; }

        private List<String> _monitoredMailboxes;
        /// <summary>
        /// List of monitored mailboxes (if available).
        /// </summary>
        [DataMember]
        public List<String> MonitoredMailboxes
        {
            get { return _monitoredMailboxes ?? (_monitoredMailboxes = new List<String>()); }
            set { _monitoredMailboxes = value; }
        }

        /// <summary>
        /// Status the agents are set to when not answering.
        /// </summary>
        [DataMember]
        public string NoAnswerStatus { get; set; }

        /// <summary>
        /// Status the agents are set to when on call.
        /// </summary>
        [DataMember]
        public string OnCallStatus { get; set; }

        /// <summary>
        /// Workgroup's on hold message (if available).
        /// </summary>
        [DataMember]
        public string OnHoldMessage { get; set; }

        /// <summary>
        /// Workgroup's on hold music.
        /// </summary>
        [DataMember]
        public string OnHoldMusic { get; set; }

        /// <summary>
        /// Workgroup's queue type (ACD, Round Robin, etc.).
        /// </summary>
        [DataMember]
        public string QueueType { get; set; }

        private List<String> _supervisors;
        /// <summary>
        /// List of the workgroup' supervisors.
        /// </summary>
        [DataMember]
        public List<String> Supervisors
        {
            get { return _supervisors ?? (_supervisors = new List<String>()); }
            set { _supervisors = value; }
        }

        private List<String> _wrapUpCodes;
        /// <summary>
        /// List of the workgroup' wrap up codes.
        /// </summary>
        [DataMember]
        public List<String> WrapUpCodes
        {
            get { return _wrapUpCodes ?? (_wrapUpCodes = new List<String>()); }
            set { _wrapUpCodes = value; }
        }

        /// <summary>
        /// Status the agents are set to when in wrap up (or follow up) mode.
        /// </summary>
        [DataMember]
        public string WrapUpStatus { get; set; }

        /// <summary>
        /// Maximum number of seconds before the agent' status is changed back to available when in wrap up (or follow up) mode.
        /// </summary>
        [DataMember]
        public string WrapUpTimeout { get; set; }

        private List<String> _members;
        /// <summary>
        /// List of members.
        /// </summary>
        [DataMember]
        public List<String> Members
        {
            get { return _members ?? (_members = new List<String>()); }
            set { _members = value; }
        }

        private List<String> _skills;
        /// <summary>
        /// List of skills assigned to the workgroup members.
        /// </summary>
        [DataMember]
        public List<String> Skills
        {
            get { return _skills ?? (_skills = new List<String>()); }
            set { _skills = value; }
        }

        private List<String> _roles;
        /// <summary>
        /// List of roles assigned to the workgroup members.
        /// </summary>
        [DataMember]
        public List<String> Roles
        {
            get { return _roles ?? (_roles = new List<String>()); }
            set { _roles = value; }
        }

        /// <summary>
        /// Preferred Language (if available)
        /// </summary>
        [DataMember]
        public string PreferredLanguage { get; set; }

        /// <summary>
        /// Workgroup id.
        /// </summary>
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// Workgroup's CIC extension (if available).
        /// </summary>
        [DataMember]
        public string Extension { get; set; }

        /// <summary>
        /// Workgroup's display name (if available).
        /// </summary>
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Id: '{0}', Number of members: '{1}', Number of skills: '{2}', etc.", Id, Members.Count, Skills.Count);
        }
    }

    /// <summary>
    /// Response passed back after calling GetWorkgroupInteractions()
    /// </summary>
    [DataContract]
    public class GetWorkgroupInteractionsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _interactionIds;
        /// <summary>
        /// Contains the list of interaction ids.
        /// </summary>
        [DataMember]
        public List<String> InteractionIds
        {
            get { return _interactionIds ?? (_interactionIds = new List<String>()); }
            set { _interactionIds = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of interaction ids: '{2}'.", Success, ErrorMessage, InteractionIds.Count);
        }
    }

    /// <summary>
    /// Response passed back after calling CreateWorkgroup()
    /// </summary>
    [DataContract]
    public class CreateWorkgroupResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred (i.e. workgroup already exists).
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, workgroup was successfully created. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetWorkgroups()
    /// </summary>
    [DataContract]
    public class GetWorkgroupsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// Contains an error message if an error occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item were retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _workgroups;
        /// <summary>
        /// Contains the list of users of the specified workgroup.
        /// </summary>
        [DataMember]
        public List<String> Workgroups
        {
            get { return _workgroups ?? (_workgroups = new List<string>()); }
            set { _workgroups = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Number of workgroups: '{1}', Error Message: '{2}'.", Success, Workgroups.Count, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling DeleteWorkgroup()
    /// </summary>
    [DataContract]
    public class DeleteWorkgroupResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was deleted successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }
    #endregion

    #region Stations
    /// <summary>
    /// Response passed back after calling GetUser()
    /// </summary>
    [DataContract]
    public class GetStationResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private SerializedStationConfiguration _stationConfiguration;
        /// <summary>
        /// Contains the station configuration.
        /// </summary>
        [DataMember]
        public SerializedStationConfiguration StationConfiguration
        {
            get { return _stationConfiguration ?? (_stationConfiguration = new SerializedStationConfiguration()); }
            set { _stationConfiguration = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Station Configuration: '{1}', Error Message: '{2}'.", Success, StationConfiguration, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetStationInteractions()
    /// </summary>
    [DataContract]
    public class GetStationInteractionsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _interactionIds;
        /// <summary>
        /// Contains the list of interaction ids.
        /// </summary>
        [DataMember]
        public List<String> InteractionIds
        {
            get { return _interactionIds ?? (_interactionIds = new List<String>()); }
            set { _interactionIds = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of interaction ids: '{2}'.", Success, ErrorMessage, InteractionIds.Count);
        }
    }

    /// <summary>
    /// Response passed back after calling CreateStation()
    /// </summary>
    [DataContract]
    public class CreateStationResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred (i.e. user already exists).
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, station was successfully created. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetStations()
    /// </summary>
    [DataContract]
    public class GetStationsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// Contains an error message if an error occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item were retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _stations;
        /// <summary>
        /// Contains the list of stations.
        /// </summary>
        [DataMember]
        public List<String> Stations
        {
            get { return _stations ?? (_stations = new List<string>()); }
            set { _stations = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Number of stations: '{1}', Error Message: '{2}'.", Success, Stations.Count, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling DeleteStation()
    /// </summary>
    [DataContract]
    public class DeleteStationResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was deleted successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Represents a user configuration.
    /// </summary>
    [DataContract]
    public class SerializedStationConfiguration
    {
        /// <summary>
        /// Station id.
        /// </summary>
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// Station's CIC extension (if available).
        /// </summary>
        [DataMember]
        public string Extension { get; set; }

        /// <summary>
        /// Station's display name (if available).
        /// </summary>
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// "true" if the station is active, "false" otherwise.
        /// </summary>
        [DataMember]
        public string IsActive { get; set; }

        /// <summary>
        /// "true" if the station is shareable, "false" otherwise.
        /// </summary>
        [DataMember]
        public string IsShareable { get; set; }

        /// <summary>
        /// Station connection address (if available).
        /// </summary>
        [DataMember]
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Station identification address (if available).
        /// </summary>
        [DataMember]
        public string IdentificationAddress { get; set; }

        /// <summary>
        /// Station contact line (if available).
        /// </summary>
        [DataMember]
        public string ContactLine { get; set; }

        /// <summary>
        /// Station type (RemoteStation, StandAloneFaxMachine, StandAlonePhone, UnifiedMessaging, Workstation)
        /// </summary>
        [DataMember]
        public string StationType { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Id: '{0}', Display Name: '{1}', Extension: '{2}', etc.", Id, DisplayName, Extension);
        }
    }

    #endregion

    #region Lines
    /// <summary>
    /// Response passed back after calling GetLine()
    /// </summary>
    [DataContract]
    public class GetLineResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item was retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private SerializedLineConfiguration _lineConfiguration;
        /// <summary>
        /// Contains the line configuration.
        /// </summary>
        [DataMember]
        public SerializedLineConfiguration LineConfiguration
        {
            get { return _lineConfiguration ?? (_lineConfiguration = new SerializedLineConfiguration()); }
            set { _lineConfiguration = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Line Configuration: '{1}', Error Message: '{2}'.", Success, LineConfiguration, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling CreateLine()
    /// </summary>
    [DataContract]
    public class CreateLineResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred (i.e. line already exists).
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, line was successfully created. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Represents a line configuration.
    /// </summary>
    [DataContract]
    public class SerializedLineConfiguration
    {
        /// <summary>
        /// If set to "true", has a queue (check <see cref="QueueType"/> to get the workgroup's queue type (ACD, round robin, etc.)). False otherwise.
        /// </summary>
        [DataMember]
        public string HasQueue { get; set; }

        /// <summary>
        /// Default interaction offering timeout.
        /// </summary>
        [DataMember]
        public string InteractionOfferingTimeout { get; set; }

        /// <summary>
        /// If true, workgroup is active. False otherwise.
        /// </summary>
        [DataMember]
        public string IsActive { get; set; }

        /// <summary>
        /// If true, wrap up is active on this workgroup.
        /// </summary>
        [DataMember]
        public string IsWrapUpActive { get; set; }

        private List<String> _monitoredMailboxes;
        /// <summary>
        /// List of monitored mailboxes (if available).
        /// </summary>
        [DataMember]
        public List<String> MonitoredMailboxes
        {
            get { return _monitoredMailboxes ?? (_monitoredMailboxes = new List<String>()); }
            set { _monitoredMailboxes = value; }
        }

        /// <summary>
        /// Status the agents are set to when not answering.
        /// </summary>
        [DataMember]
        public string NoAnswerStatus { get; set; }

        /// <summary>
        /// Status the agents are set to when on call.
        /// </summary>
        [DataMember]
        public string OnCallStatus { get; set; }

        /// <summary>
        /// Workgroup's on hold message (if available).
        /// </summary>
        [DataMember]
        public string OnHoldMessage { get; set; }

        /// <summary>
        /// Workgroup's on hold music.
        /// </summary>
        [DataMember]
        public string OnHoldMusic { get; set; }

        /// <summary>
        /// Workgroup's queue type (ACD, Round Robin, etc.).
        /// </summary>
        [DataMember]
        public string QueueType { get; set; }

        private List<String> _supervisors;
        /// <summary>
        /// List of the workgroup' supervisors.
        /// </summary>
        [DataMember]
        public List<String> Supervisors
        {
            get { return _supervisors ?? (_supervisors = new List<String>()); }
            set { _supervisors = value; }
        }

        private List<String> _wrapUpCodes;
        /// <summary>
        /// List of the workgroup' wrap up codes.
        /// </summary>
        [DataMember]
        public List<String> WrapUpCodes
        {
            get { return _wrapUpCodes ?? (_wrapUpCodes = new List<String>()); }
            set { _wrapUpCodes = value; }
        }

        /// <summary>
        /// Status the agents are set to when in wrap up (or follow up) mode.
        /// </summary>
        [DataMember]
        public string WrapUpStatus { get; set; }

        /// <summary>
        /// Maximum number of seconds before the agent' status is changed back to available when in wrap up (or follow up) mode.
        /// </summary>
        [DataMember]
        public string WrapUpTimeout { get; set; }

        private List<String> _members;
        /// <summary>
        /// List of members.
        /// </summary>
        [DataMember]
        public List<String> Members
        {
            get { return _members ?? (_members = new List<String>()); }
            set { _members = value; }
        }

        private List<String> _skills;
        /// <summary>
        /// List of skills assigned to the workgroup members.
        /// </summary>
        [DataMember]
        public List<String> Skills
        {
            get { return _skills ?? (_skills = new List<String>()); }
            set { _skills = value; }
        }

        private List<String> _roles;
        /// <summary>
        /// List of roles assigned to the workgroup members.
        /// </summary>
        [DataMember]
        public List<String> Roles
        {
            get { return _roles ?? (_roles = new List<String>()); }
            set { _roles = value; }
        }

        /// <summary>
        /// Preferred Language (if available)
        /// </summary>
        [DataMember]
        public string PreferredLanguage { get; set; }

        /// <summary>
        /// Workgroup id.
        /// </summary>
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// Workgroup's CIC extension (if available).
        /// </summary>
        [DataMember]
        public string Extension { get; set; }

        /// <summary>
        /// Workgroup's display name (if available).
        /// </summary>
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Id: '{0}', Number of members: '{1}', Number of skills: '{2}', etc.", Id, Members.Count, Skills.Count);
        }
    }

    #endregion

    #endregion

    #region Custom Notification
    /// <summary>
    /// Response passed back after calling SendCustomNotification()
    /// </summary>
    [DataContract]
    public class SendCustomNotificationResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="Success"/> is false.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, custom notification was sent successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    #endregion

    #region Statistics
    /// <summary>
    /// Response passed back after calling GetAllStatistics()
    /// </summary>
    [DataContract]
    public class GetAllStatisticsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if one occurred.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item were retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private Dictionary<String, Dictionary<String, List<String>>> _statistics;
        /// <summary>
        /// Contains the list of statistics (Category, Statistic Name, Required Parameters) available on the CIC server.
        /// </summary>
        [DataMember]
        public Dictionary<String, Dictionary<String, List<String>>> Statistics
        {
            get { return _statistics ?? (_statistics = new Dictionary<String, Dictionary<String, List<String>>>()); }
            set { _statistics = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Number of statistic categories: '{1}', Error Message: '{2}'.", Success, Statistics.Count, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling GetStatistic()
    /// </summary>
    [DataContract]
    public class GetStatisticResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item were retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Contains the list of statistics ids available on the CIC server.
        /// </summary>
        [DataMember]
        public string StatisticValue { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Statistic value: '{1}', Error Message: '{2}'.", Success, StatisticValue, ErrorMessage);
        }
    }
    #endregion

    #region Reporting
    /// <summary>
    /// Response passed back after calling SearchInteractions* methods.
    /// </summary>
    [DataContract]
    public class SearchInteractionsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item were retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<SerializedInteractionSnapshot> _interactionSnapshots;
        /// <summary>
        /// Contains a list of interactions historical information.
        /// </summary>
        [DataMember]
        public List<SerializedInteractionSnapshot> InteractionSnapshots
        {
            get { return _interactionSnapshots ?? (_interactionSnapshots = new List<SerializedInteractionSnapshot>()); }
            set { _interactionSnapshots = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Number of snapshots: '{1}', Error Message: '{2}'.", Success, InteractionSnapshots.Count, ErrorMessage);
        }
    }

    /// <summary>
    /// Represents an Interaction Snapshot.
    /// </summary>
    [DataContract]
    public class SerializedInteractionSnapshot
    {
        /// <summary>
        /// Interaction Direction
        /// </summary>
        [DataMember]
        public string Direction { get; set; }

        /// <summary>
        /// Interaction Disposition
        /// </summary>
        [DataMember]
        public string Disposition { get; set; }

        /// <summary>
        /// Interaction Initiation Time
        /// </summary>
        [DataMember]
        public DateTime InitiationTime { get; set; }

        /// <summary>
        /// Interaction Identifier
        /// </summary>
        [DataMember]
        public string InteractionId { get; set; }

        /// <summary>
        /// Interaction Id Key
        /// </summary>
        [DataMember]
        public string InteractionIdKey { get; set; }

        /// <summary>
        /// Interaction Type
        /// </summary>
        [DataMember]
        public string MediaType { get; set; }

        /// <summary>
        /// Segment Log
        /// </summary>
        [DataMember]
        public string SegmentLog { get; set; }

        /// <summary>
        /// History Sequence Number
        /// </summary>
        [DataMember]
        public int SequenceNumber { get; set; }

        /// <summary>
        /// Site Identifier
        /// </summary>
        [DataMember]
        public int SiteId { get; set; }

        private Dictionary<String, String> _summaryValues;
        /// <summary>
        /// Summary Values
        /// </summary>
        [DataMember]
        public Dictionary<String, String> SummaryValues
        {
            get { return _summaryValues ?? (_summaryValues = new Dictionary<string, string>()); }
            set { _summaryValues = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Direction: '{0}', Disposition: '{1}', Initiation Time: '{2}', Interaction Id: '{3}', Interaction Id Key: '{4}', Media Type: '{5}', Segment Log: '{6}', Sequence Number: '{7}', Site Id: '{8}', Number of Summary Values: '{9}'.",
                Direction, Disposition, InitiationTime.ToString(CultureInfo.InvariantCulture), InteractionId, InteractionIdKey, MediaType, SegmentLog, SequenceNumber.ToString(CultureInfo.InvariantCulture), SiteId.ToString(CultureInfo.InvariantCulture), SummaryValues.Count);
        }
    }
    #endregion

    #region Tracing
    /// <summary>
    /// Response passed back after calling SetTracingTopicLevel().
    /// </summary>
    [DataContract]
    public class SetTracingTopicLevelResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, trace topic level was successfully set. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }
    #endregion

    #region Directories
    /// <summary>
    /// Response passed back after calling GetUserDirectoryPhoneNumbers().
    /// </summary>
    [DataContract]
    public class GetUserDirectoryPhoneNumbersResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// The user's directory information (home phone, business phone, mobile phone, etc.).
        /// </summary>
        [DataMember]
        public SerializedContact Contact { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Error Message: '{0}', Contact: '{1}'.", ErrorMessage, Contact);
        }
    }

    /// <summary>
    /// Represents a serialized version of a Contact Entry.
    /// </summary>
    [DataContract]
    public class SerializedContact
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public SerializedContact(string userExtension, string businessPhoneDialString, string businessPhoneDisplayString, string businessPhoneNumber, string businessPhoneExtension, string businessPhone2DialString, string businessPhone2DisplayString, string businessPhone2Number, string businessPhone2Extension, string mobilePhoneDialString, string mobilePhoneDisplayString, string mobilePhoneNumber, string mobilePhoneExtension, string homePhoneDialString, string homePhoneDisplayString, string homePhoneNumber, string homePhoneExtension)
        {
            _userExtension = userExtension;

            BusinessPhoneDialString = businessPhoneDialString;
            BusinessPhoneDisplayString = businessPhoneDisplayString;
            _businessPhoneNumber = businessPhoneNumber;
            BusinessPhoneExtension = businessPhoneExtension;

            BusinessPhone2DialString = businessPhone2DialString;
            BusinessPhone2DisplayString = businessPhone2DisplayString;
            BusinessPhone2Number = businessPhone2Number;
            BusinessPhone2Extension = businessPhone2Extension;

            MobilePhoneDialString = mobilePhoneDialString;
            MobilePhoneDisplayString = mobilePhoneDisplayString;
            MobilePhoneNumber = mobilePhoneNumber;
            MobilePhoneExtension = mobilePhoneExtension;

            HomePhoneDialString = homePhoneDialString;
            HomePhoneDisplayString = homePhoneDisplayString;
            HomePhoneNumber = homePhoneNumber;
            HomePhoneExtension = homePhoneExtension;
        }

        private String _userExtension;
        /// <summary>
        /// The user's CIC extension
        /// </summary>
        [DataMember]
        public String UserExtension
        {
            get { return _userExtension; }
            set { _userExtension = value; }
        }

        #region Business Phone

        /// <summary>
        /// Use this to dial the number (and extension if there is one).
        /// </summary>
        [DataMember]
        public string BusinessPhoneDialString { get; set; }

        /// <summary>
        /// Contains the business phone number and extension.
        /// </summary>
        [DataMember]
        public string BusinessPhoneDisplayString { get; set; }

        private String _businessPhoneNumber;
        /// <summary>
        /// Business phone number (without the extension if one was specified).
        /// </summary>
        [DataMember]
        public String BusinessPhoneNumber
        {
            get { return _businessPhoneNumber; }
            set { _businessPhoneNumber = value; }
        }

        /// <summary>
        /// Business phone extension (does not contain the business phone number).
        /// </summary>
        [DataMember]
        public string BusinessPhoneExtension { get; set; }

        #endregion

        #region Business Phone2

        /// <summary>
        /// Use this to dial the number (and extension if there is one).
        /// </summary>
        [DataMember]
        public string BusinessPhone2DialString { get; set; }

        /// <summary>
        /// Contains the business phone number and extension.
        /// </summary>
        [DataMember]
        public string BusinessPhone2DisplayString { get; set; }

        /// <summary>
        /// Business phone number (without the extension if one was specified).
        /// </summary>
        [DataMember]
        public string BusinessPhone2Number { get; set; }

        /// <summary>
        /// Business phone extension (does not contain the business phone number).
        /// </summary>
        [DataMember]
        public string BusinessPhone2Extension { get; set; }

        #endregion

        #region Mobile Number

        /// <summary>
        /// Use this to dial the number (and extension if there is one).
        /// </summary>
        [DataMember]
        public string MobilePhoneDialString { get; set; }

        /// <summary>
        /// Contains the mobile phone number and extension.
        /// </summary>
        [DataMember]
        public string MobilePhoneDisplayString { get; set; }

        /// <summary>
        /// Mobile phone number (without the extension if one was specified).
        /// </summary>
        [DataMember]
        public string MobilePhoneNumber { get; set; }

        /// <summary>
        /// Mobile phone extension (does not contain the business phone number).
        /// </summary>
        [DataMember]
        public string MobilePhoneExtension { get; set; }

        #endregion

        #region Home Number

        /// <summary>
        /// Use this to dial the number (and extension if there is one).
        /// </summary>
        [DataMember]
        public string HomePhoneDialString { get; set; }

        /// <summary>
        /// Contains the home phone number and extension.
        /// </summary>
        [DataMember]
        public string HomePhoneDisplayString { get; set; }

        /// <summary>
        /// Home phone number (without the extension if one was specified).
        /// </summary>
        [DataMember]
        public string HomePhoneNumber { get; set; }

        /// <summary>
        /// Home phone extension (does not contain the business phone number).
        /// </summary>
        [DataMember]
        public string HomePhoneExtension { get; set; }

        #endregion

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Extension: '{0}', Business Phone Number: '{1}'.", _userExtension, _businessPhoneNumber);
        }
    }
    #endregion

    #region IPA
    /// <summary>
    /// Response passed back after calling GetLaunchableIPAProcesses()
    /// </summary>
    [DataContract]
    public class GetLaunchableIPAProcessesResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if <see cref="IPAProcesses"/> is empty (and there are launchable processes available in Interaction Administrator).
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, item were retrieved successfully. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _ipaProcesses;
        /// <summary>
        /// Contains the list of IPA processes that can be launched by name.
        /// </summary>
        [DataMember]
        public List<String> IPAProcesses
        {
            get { return _ipaProcesses ?? (_ipaProcesses = new List<string>()); }
            set { _ipaProcesses = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Number of launchable IPA processes: '{1}', Error Message: '{2}'.", Success, IPAProcesses.Count, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling StartIPAProcess().
    /// </summary>
    [DataContract]
    public class StartIPAProcessResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, IPA process was successfully started. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Id for this process instance.
        /// </summary>
        [DataMember]
        public string ProcessId { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Process Id: '{2}'.", Success, ErrorMessage, ProcessId);
        }
    }

    /// <summary>
    /// Response passed back after calling CancelIPAProcess().
    /// </summary>
    [DataContract]
    public class CancelIPAProcessResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, IPA process was successfully cancelled. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }
    #endregion

    #region Dialer
    /// <summary>
    /// Response passed back after calling GetAvailableCampaigns().
    /// </summary>
    [DataContract]
    public class GetAvailableCampaignsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, list of campaigns was successfully retrieved. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private List<String> _availableCampaigns;
        /// <summary>
        /// List of campaign names available for the CIC user connected in this web service, specified in the EasyIceWindowsService.config file or during Setup.
        /// </summary>
        [DataMember]
        public List<String> AvailableCampaigns
        {
            get { return _availableCampaigns ?? (_availableCampaigns = new List<String>()); }
            set { _availableCampaigns = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of campaigns: '{2}'.", Success, ErrorMessage, AvailableCampaigns == null ? "0" : AvailableCampaigns.Count.ToString());
        }
    }

    /// <summary>
    /// Response passed back after calling GetContacts().
    /// </summary>
    [DataContract]
    public class GetContactsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, list of contacts was successfully retrieved from the campaign. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private Dictionary<String, Dictionary<String, String>> _contacts;
        /// <summary>
        /// List of contacts from the specified campaign.
        /// </summary>
        [DataMember]
        public Dictionary<String, Dictionary<String, String>> Contacts
        {
            get { return _contacts ?? (_contacts = new Dictionary<String, Dictionary<String, String>>()); }
            set { _contacts = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of contacts: '{2}'.", Success, ErrorMessage, Contacts == null ? "0" : Contacts.Count.ToString());
        }
    }

    /// <summary>
    /// Response passed back after calling InsertContact().
    /// </summary>
    [DataContract]
    public class InsertContactResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, contact was successfully inserted into the campaign. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling RecycleContactList().
    /// </summary>
    [DataContract]
    public class RecycleContactListResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        /// <remarks>If there are no recycles left to process, will contain a warning message.</remarks>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, the campaign's contact list was successfully recycled. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Number of records remaining in the current recycle.
        /// </summary>
        [DataMember]
        public int RemainingRecords { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Remaining Records: '{2}'.", Success, ErrorMessage, RemainingRecords);
        }
    }

    /// <summary>
    /// Response passed back after calling ResetCampaign().
    /// </summary>
    [DataContract]
    public class ResetCampaignResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, the campaign was successfully resetted. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Number of records remaining in the current recycle.
        /// </summary>
        [DataMember]
        public int RemainingRecords { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Remaining Records: '{2}'.", Success, ErrorMessage, RemainingRecords);
        }
    }

    /// <summary>
    /// Response passed back after calling GetCampaignAgents().
    /// </summary>
    [DataContract]
    public class GetCampaignAgentsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, the campaign's agents were successfully retrieved. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private ICollection<String> _activeAgents;
        /// <summary>
        /// List of active agents in the campaign.
        /// </summary>
        [DataMember]
        public ICollection<String> ActiveAgents
        {
            get { return _activeAgents ?? (_activeAgents = new List<String>()); }
            set { _activeAgents = value; }
        }

        private ICollection<String> _eligibleAgents;
        /// <summary>
        /// List of logged in agents eligibile to participate in the campaign.
        /// </summary>
        [DataMember]
        public ICollection<String> EligibleAgents
        {
            get { return _eligibleAgents ?? (_eligibleAgents = new List<String>()); }
            set { _eligibleAgents = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Active Agents: '{2}', Eligible Agents: '{3}'.", Success, ErrorMessage, ActiveAgents.Count, EligibleAgents.Count);
        }
    }

    /// <summary>
    /// Response passed back after calling GetAgentCampaigns().
    /// </summary>
    [DataContract]
    public class GetAgentCampaignsResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, the agent's campaigns were successfully retrieved. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private ICollection<String> _activeCampaigns;
        /// <summary>
        /// List of campaigns the agent is active in.
        /// </summary>
        [DataMember]
        public ICollection<String> ActiveCampaigns
        {
            get { return _activeCampaigns ?? (_activeCampaigns = new List<String>()); }
            set { _activeCampaigns = value; }
        }

        private ICollection<String> _eligibleCampaigns;
        /// <summary>
        /// List of active campaigns the agent is eligible to participate in.
        /// </summary>
        [DataMember]
        public ICollection<String> EligibleCampaigns
        {
            get { return _eligibleCampaigns ?? (_eligibleCampaigns = new List<String>()); }
            set { _eligibleCampaigns = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Active Campaigns: '{2}', Eligible Campaigns: '{3}'.", Success, ErrorMessage, ActiveCampaigns.Count, EligibleCampaigns.Count);
        }
    }

    /// <summary>
    /// Response passed back after calling ExcludePhoneNumberFromCampaign().
    /// </summary>
    [DataContract]
    public class ExcludePhoneNumberFromCampaignResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, phone was successfully excluded from the campaign. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}'.", Success, ErrorMessage);
        }
    }

    /// <summary>
    /// Response passed back after calling ExcludePhoneNumberFromAllCampaigns().
    /// </summary>
    [DataContract]
    public class ExcludePhoneNumberFromAllCampaigns
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, phone was successfully excluded from the campaign. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private ICollection<String> _failedCampaigns;
        /// <summary>
        /// Contains the list of campaigns which failed to exclude the phone number.
        /// </summary>
        [DataMember]
        public ICollection<String> FailedCampaigns
        {
            get { return _failedCampaigns ?? (_failedCampaigns = new List<String>()); }
            set { _failedCampaigns = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of Failed Campaigns: '{2}'.", Success, ErrorMessage, FailedCampaigns.Count);
        }
    }

    /// <summary>
    /// Response passed back after calling GetCampaignsStatusMessages().
    /// </summary>
    [DataContract]
    public class GetCampaignsStatusResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, status messages were successfully retrieved. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private ICollection<DialerStatusMessage> _statusMessages;
        /// <summary>
        /// Contains the list of status messages retrieved from Dialer.
        /// </summary>
        [DataMember]
        public ICollection<DialerStatusMessage> StatusMessages
        {
            get { return _statusMessages ?? (_statusMessages = new List<DialerStatusMessage>()); }
            set { _statusMessages = value; }
        }

        private ICollection<CampaignValidationResult> _campaignValidationResults;
        /// <summary>
        /// Contains the results of the validation tests for all available campaigns.
        /// </summary>
        [DataMember]
        public ICollection<CampaignValidationResult> CampaignValidationResults
        {
            get { return _campaignValidationResults ?? (_campaignValidationResults = new List<CampaignValidationResult>()); }
            set { _campaignValidationResults = value; }
        }

        private ICollection<CampaignTestResult> _campaignTestResults;
        /// <summary>
        /// Contains the list of quick tests for all available campaigns.
        /// </summary>
        [DataMember]
        public ICollection<CampaignTestResult> CampaignTestResults
        {
            get { return _campaignTestResults ?? (_campaignTestResults = new List<CampaignTestResult>()); }
            set { _campaignTestResults = value; }
        }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of status messages: '{2}, Number of Campaign Validation results: '{3}'.", Success, ErrorMessage, StatusMessages.Count, CampaignValidationResults.Count);
        }
    }

    /// <summary>
    /// Response passed back after calling GetCampaignStatusMessages().
    /// </summary>
    [DataContract]
    public class GetCampaignStatusResponse
    {
        private string _errorMessage;
        /// <summary>
        /// A message describing an error if something bad happened.
        /// </summary>
        [DataMember]
        public string ErrorMessage
        {
            get
            {
                if (_errorMessage == null)
                    return String.Empty;

                return _errorMessage;
            }
            set { _errorMessage = value; }
        }

        /// <summary>
        /// If true, status messages were successfully retrieved. Otherwise, false.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        private ICollection<DialerStatusMessage> _statusMessages;
        /// <summary>
        /// Contains the list of status messages retrieved from Dialer for the specified campaign.
        /// </summary>
        [DataMember]
        public ICollection<DialerStatusMessage> StatusMessages
        {
            get { return _statusMessages ?? (_statusMessages = new List<DialerStatusMessage>()); }
            set { _statusMessages = value; }
        }

        /// <summary>
        /// Contains the results of the validation tests for the specified campaign.
        /// </summary>
        [DataMember]
        public CampaignValidationResult CampaignValidationResult { get; set; }

        /// <summary>
        /// Contains the results of the campaign quick tests.
        /// </summary>
        [DataMember]
        public CampaignTestResult CampaignTestResult { get; set; }

        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Success? '{0}', Error Message: '{1}', Number of status messages: '{2}, Campaign Validation Success: '{3}', Campaign Validation Error Message: '{4}', Campaign Test Success: '{5}', Campaign Test Error Message: '{6}'.", Success, ErrorMessage, StatusMessages.Count, CampaignValidationResult != null ? CampaignValidationResult.Success.ToString() : "(null)", CampaignValidationResult != null ? CampaignValidationResult.ErrorMessage : "(null)", CampaignTestResult != null ? CampaignTestResult.Success.ToString() : "(null)", CampaignTestResult != null ? CampaignTestResult.ErrorMessage : "(null)");
        }
    }

    /// <summary>
    /// Represents a dialer status message.
    /// </summary>
    [DataContract]
    public class DialerStatusMessage
    {
        /// <summary>
        /// Campaign name.
        /// </summary>
        [DataMember]
        public string Campaign { get; set; }
        /// <summary>
        /// The textual content of the message.
        /// </summary>
        [DataMember]
        public string Content { get; set; }
        /// <summary>
        /// The time that this status message was originally generated.
        /// </summary>
        [DataMember]
        public DateTime CreatedDateTime { get; set; }
        /// <summary>
        /// The last time the status of diagnostic information in this message was updated.
        /// </summary>
        [DataMember]
        public DateTime LastModifiedDateTime { get; set; }
        /// <summary>
        /// The name of the computer that generated this message.
        /// </summary>
        [DataMember]
        public string MachineName { get; set; }
        /// <summary>
        /// The type of status message.
        /// </summary>
        [DataMember]
        public string MessageType { get; set; }
        /// <summary>
        /// The name of the process that generated this message.
        /// </summary>
        [DataMember]
        public string ProcessName { get; set; }
        /// <summary>
        /// The level of seriousness of this message.
        /// </summary>
        [DataMember]
        public string Severity { get; set; }
        /// <summary>
        /// A brief summary of the message contents.
        /// </summary>
        [DataMember]
        public string Title { get; set; }
        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Machine Name: '{0}', Process Name: '{1}', Message Type: '{2}', Severity: '{3}', Campaign: '{4}', Created Date/Time: '{5}', Last Modified Date/Time: '{6}', Title: '{7}', Content: '{8}'.", MachineName, ProcessName, MessageType, Severity, Campaign, CreatedDateTime.ToString(), LastModifiedDateTime.ToString(), Title, Content);
        }
    }

    /// <summary>
    /// Represents a campaign validation result.
    /// </summary>
    [DataContract]
    public class CampaignValidationResult
    {
        /// <summary>
        /// Campaign name.
        /// </summary>
        [DataMember]
        public string Campaign { get; set; }
        /// <summary>
        /// A message indicating why the campaign validation failed.
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }
        /// <summary>
        /// True if the campaign validation succeeded, false otherwise.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }
        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Campaign: '{0}', Success: '{1}', Error Message: '{2}'.", Campaign, Success, ErrorMessage);
        }
    }
    
    /// <summary>
    /// Represents the result of a campaign test.
    /// </summary>
    [DataContract]
    public class CampaignTestResult
    {
        /// <summary>
        /// Campaign name.
        /// </summary>
        [DataMember]
        public string Campaign { get; set; }
        /// <summary>
        /// The total number of callable records in the contact list.
        /// </summary>
        [DataMember]
        public int CallableRecords { get; set; }
        /// <summary>
        /// The name of the contact list table.
        /// </summary>
        [DataMember]
        public string ContactListTable { get; set; }
        /// <summary>
        /// Contains a message indicating why the test failed.
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }
        /// <summary>
        /// The SQL clause used to filter records, as it appears in contact list queries.
        /// </summary>
        [DataMember]
        public string Filter { get; set; }
        /// <summary>
        /// The total number of callable records in the contact list after applying filters.
        /// </summary>
        [DataMember]
        public int FilteredRecords { get; set; }
        /// <summary>
        /// The total number of records in the contact list without a phone number.
        /// </summary>
        [DataMember]
        public int NoPhoneNumberRecords { get; set; }
        /// <summary>
        /// Indicates whether the Number Request and Response plug-in is in use.
        /// </summary>
        [DataMember]
        public bool NRRLib { get; set; }
        
        #region Sample Records
        private ICollection<String> _sampleRecordColumns;
        /// <summary>
        /// Contains the list of database columns corresponding to the sample data in <see cref="SampleRecords"/>.
        /// </summary>
        [DataMember]
        public ICollection<String> SampleRecordColumns
        {
            get { return _sampleRecordColumns ?? (_sampleRecordColumns = new List<String>()); }
            set { _sampleRecordColumns = value; }
        }

        private List<List<String>> _sampleRecords;
        /// <summary>
        /// Contains the list of sample callable records from the contact list. Record column names are stored in <see cref="SampleRecordColumns"/>.
        /// </summary>
        [DataMember]
        public List<List<String>> SampleRecords
        {
            get { return _sampleRecords ?? (_sampleRecords = new List<List<String>>()); }
            set { _sampleRecords = value; }
        }
        #endregion

        /// <summary>
        /// The total number of calls scheduled for records in the campaign's contact list.
        /// </summary>
        [DataMember]
        public int ScheduledCalls { get; set; }
        
        #region Skill Combinations
        private ICollection<String> _skillCombinationColumns;
        /// <summary>
        /// Contains the list of database columns corresponding to the sample data in <see cref="SkillCombinations"/>.
        /// </summary>
        [DataMember]
        public ICollection<String> SkillCombinationColumns
        {
            get { return _skillCombinationColumns ?? (_skillCombinationColumns = new List<String>()); }
            set { _skillCombinationColumns = value; }
        }

        private ICollection<SkillCombination> _skillCombinations;
        /// <summary>
        /// Contains the list of breakdowns of callable records by skill combination. Column names are stored in <see cref="SkillCombinationColumns"/>.
        /// </summary>
        [DataMember]
        public ICollection<SkillCombination> SkillCombinations
        {
            get { return _skillCombinations ?? (_skillCombinations = new List<SkillCombination>()); }
            set { _skillCombinations = value; }
        }
        #endregion

        /// <summary>
        /// The SQL clause used to sort contact records as it appears in the contact list queries.
        /// </summary>
        [DataMember]
        public string Sort { get; set; }
        /// <summary>
        /// Indicates whether the test completed without errors. If false, check <see cref="ErrorMessage"/> for details.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }
        /// <summary>
        /// The total number of records in the campaign's contact list.
        /// </summary>
        [DataMember]
        public int TotalRecords { get; set; }
        /// <summary>
        /// The total number of uncallable records in the campaign's contact list.
        /// </summary>
        [DataMember]
        public int TotalUncallableRecords { get; set; }
        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Campaign: '{0}', Success: '{1}', Error Message: '{2}'.", Campaign, Success, ErrorMessage);
        }
    }
    
    /// <summary>
    /// Represents a single combination of skill values occuring in the contact list.
    /// </summary>
    [DataContract]
    public class SkillCombination
    {
        /// <summary>
        /// The number of callable records applicable towards this skill combination.
        /// </summary>
        [DataMember]
        public int RecordCount { get; set; }
        private List<String> _skillValues;
        /// <summary>
        /// Values composing this skill combination.
        /// </summary>
        [DataMember]
        public List<String> SkillValues
        {
            get { return _skillValues ?? (_skillValues = new List<String>()); }
            set { _skillValues = value; }
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="recordCount">The number of callable records applicable towards this skill combination.</param>
        /// <param name="skillValues">Values composing this skill combination.</param>
        public SkillCombination(int recordCount, List<String> skillValues)
        {
            RecordCount = recordCount;
            SkillValues = skillValues;
        }
        /// <summary>
        /// Outputs a string mostly used for debugging purposes.
        /// </summary>
        /// <returns>A string displaying all objects from this class.</returns>
        public override string ToString()
        {
            return String.Format("Record Count: '{0}', Number of Skill Values: '{1}'.", RecordCount, SkillValues.Count);
        }
    }
    #endregion
}