﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>jQuery Tests</title>
    <script src="Scripts/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        var queryName;
        var varType;
        var varUrl;
        var varData;
        var varContentType;
        var varDataType;
        var varProcessData;

        function CallService() {
            jQuery.support.cors = true;
            $.ajax({
                type: varType,                  // GET or POST or PUT or DELETE verb
                url: varUrl,                    // Location of the service
                data: varData,                  // Data sent to server
                contentType: varContentType,    // Content type sent to server
                dataType: varDataType,          // Expected data format from server
                processdata: varProcessData,    // True or False
                success: function (msg) {
                    ServiceSucceeded(msg);      // On Successfull service call 
                },
                error: ServiceFailed            // When Service call fails
            });
        }

        function ServiceSucceeded(result) {
            // Service call is successful
            var resultObject = null;
            var textArea = document.getElementById("TextArea1");
            textArea.value = '';

            resultObject = result;

            // ====================
            // Call Number Response
            // ====================
            if (queryName == "CallNumber") {
                textArea.value += 'Success: ' + result.Success + '\n\r';
                textArea.value += 'Error Message: ' + result.ErrorMessage + '\n\r';
                textArea.value += 'Interaction Id: ' + result.InteractionId + '\n\r';
                textArea.value += 'Interaction Id Key: ' + result.InteractionIdKey + '\n\r';
            }

            // ==================
            // Get Users Response
            // ==================
            else if (queryName == "GetUsers") {
                // Show all returned users
                for (var i = 0; i < resultObject.Users.length; i++) {
                    textArea.value += resultObject.Users[i] + '\n\r';
                }
            }

            // =======================
            // Get Statistics Response
            // =======================
            else if (queryName == "GetStatistics") {
                // Show all returned statistics
                for (var statisticIndex = 0; statisticIndex < resultObject.Statistics.length; statisticIndex++) {
                    textArea.value += '===================================\n\r';
                    textArea.value += 'Key: ' + resultObject.Statistics[statisticIndex].Key + '\n\r';
                    textArea.value += 'Values:\n\r';
                    for (var j = 0; j < resultObject.Statistics[statisticIndex].Value.length; j++) {
                        textArea.value += resultObject.Statistics[statisticIndex].Value[j].Key + '\n\r';
                        for (var k = 0; k < resultObject.Statistics[statisticIndex].Value[j].Value.length; k++) {
                            textArea.value += ' Parameter: ' + resultObject.Statistics[statisticIndex].Value[j].Value[k] + '\n\r';
                        }
                    }
                }
            }

            // =================
            // Get User Response
            // =================
            else if (queryName == "GetUserInfo") {
                // Show information about returned user
                textArea.value += 'Id: ' + resultObject.UserConfiguration.Id + '\n\r';
                textArea.value += 'Display Name: ' + resultObject.UserConfiguration.DisplayName + '\n\r';
                textArea.value += 'Extension: ' + resultObject.UserConfiguration.Extension + '\n\r';
                textArea.value += 'NtDomainUser: ' + resultObject.UserConfiguration.NtDomainUser + '\n\r';
                textArea.value += '... More values are available ...';
            }

            // Clear variables used to call the service
            queryName = null; varType = null; varUrl = null; varData = null; varContentType = null; varDataType = null; varProcessData = null;
        }

        // Show Error Message if the service call failed
        function ServiceFailed(result) {
            alert('Service call failed: ' + result.status + ' ' + result.statusText);
            varType = null; varUrl = null; varData = null; varContentType = null; varDataType = null; varProcessData = null;
        }

        function GetUsers() {
            queryName = "GetUsers"; // Only used to read the output. Not used in the service call.
            varType = "GET";
            varUrl = document.getElementById("txtServiceURL").value + 'Users';
            varData = '';
            varContentType = "application/json; charset=utf-8";
            varDataType = "json";
            varProcessData = true;

            CallService();
        }

        function GetStatistics() {
            queryName = "GetStatistics"; // Only used to read the output. Not used in the service call.
            varType = "GET";
            varUrl = document.getElementById("txtServiceURL").value + 'Statistics';
            varData = '';
            varContentType = "application/json; charset=utf-8";
            varDataType = "json";
            varProcessData = true;

            CallService();
        }

        function GetUserInfo() {
            queryName = "GetUserInfo"; // Only used to read the output. Not used in the service call.
            varType = "GET";
            varUrl = document.getElementById("txtServiceURL").value + 'User/' + document.getElementById("txtUsername").value;
            varData = '{}';
            varContentType = "application/json; charset=utf-8";
            varDataType = "json";
            varProcessData = true;

            CallService();
        }

        function CallNumber() {
            queryName = "CallNumber"; // Only used to read the output. Not used in the service call.
            varType = "POST";
            varUrl = document.getElementById("txtServiceURL").value + 'Interaction/Call/?phonenumber=' + document.getElementById("txtPhoneNumber").value + '&fromworkgroup=&accountcodeid=';
            varData = '{}';
            varContentType = "application/json; charset=utf-8";
            varDataType = "json";
            varProcessData = true;
            CallService();
        }
function btnGetUsers_onclick() {

}

    </script>
    <style type="text/css">
        #Text1
        {
            width: 294px;
        }
        #txtServiceURL
        {
            width: 401px;
        }
        #image1
        {
            height: 324px;
        }
        .style1
        {
            width: 60px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <b>JQuery, WCF, JSON, XML, AJAX, REST
            <br />
        </b>
        <div>
            <div style="background-color: ButtonShadow; width: 1004px;">
                <hr />
                <br />
                <asp:Label ID="lblServiceURL" runat="server" Text="Service URL:"></asp:Label>
                <input id="txtServiceURL" type="text" value="http://localhost:9000/EasyIceService/" /><br />
                Replace &quot;localhost&quot; with the name or IP address of the machine hosting
                the Easy Ice Service.<br />
                Make sure the service URL always finishes with a slash (/)<br />
                <br />
            </div>
        </div>
        <div>
            <table>
                <tr>
                    <td valign="top">
                        <table border="1" cellpadding="5" cellspacing="5" style="background-color: #F0F0F0;
                            width: 729px;">
                            <tr>
                                <td align="left">
                                    1. Get all users using jQuery and JSON data format
                                </td>
                                <td class="style1">
                                    <input type="button" value="Invoke" id="btnGetUsers" onclick="GetUsers();" onclick="return btnGetUsers_onclick()" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    2. Get all statistics using jQuery and JSON data format (can take a few seconds
                                    to complete)
                                </td>
                                <td class="style1">
                                    <input type="button" value="Invoke" id="btnGetStatistics" onclick="GetStatistics();" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    3. Get information about user
                                    <input id="txtUsername" type="text" value="" />
                                    using jQuery and JSON data format
                                </td>
                                <td class="style1">
                                    <input type="button" value="Invoke" id="Button4" onclick="GetUserInfo();" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    4. Call the following phone number:
                                    <input id="txtPhoneNumber" type="text" value="" />
                                    using jQuery and JSON data format
                                </td>
                                <td class="style1">
                                    <input type="button" value="Invoke" id="Button5" onclick="CallNumber();" />
                                </td>
                            </tr>
                        </table>
                        <asp:TextBox runat="server" TextMode="MultiLine" ID="TextArea1" Height="142px" Width="728px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <hr />
        </div>
    </div>
    </form>
</body>
</html>
