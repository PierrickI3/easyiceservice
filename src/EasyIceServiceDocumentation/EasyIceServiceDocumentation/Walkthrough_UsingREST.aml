﻿<?xml version="1.0" encoding="utf-8"?>
<topic id="06d9b817-eb02-49cb-b0b3-3f5bd4d7ced1" revisionNumber="1">
  <developerWalkthroughDocument
    xmlns="http://ddue.schemas.microsoft.com/authoring/2003/5"
    xmlns:xlink="http://www.w3.org/1999/xlink">

    <summary>
      <para>This page will show you how to test the Easy Ice Service using REST with Chrome, Internet Explorer and Fiddler</para>
    </summary>

    <introduction>
      <para>
        REST (
        <externalLink>
          <linkText>http://en.wikipedia.org/wiki/Representational_state_transfer</linkText>
          <linkAlternateText>http://en.wikipedia.org/wiki/Representational_state_transfer</linkAlternateText>
          <linkUri>http://en.wikipedia.org/wiki/Representational_state_transfer</linkUri>
        </externalLink>) can be easily tested via a web browser. All the "GET" functions listed on the help page can simply be entered in the URL address field of a web browser to get results.
      </para>
      <autoOutline />
    </introduction>

    <prerequisites>
      <content>
        <para>
          A working EasyIceService installation. See <link xlink:href="22b83375-499d-4b3a-9dbd-99f04c668395">How to Install the Easy Ice Service</link>
        </para>
      </content>
    </prerequisites>

    <procedure>
      <title>Using REST to access the Easy Ice Service</title>
      <steps class="ordered">
        <step>
          <content>
            <para>
              First, browse to the help page (<externalLink>
                <linkText>http://localhost:9000/EasyIceService/help</linkText>
                <linkAlternateText>http://localhost:9000/EasyIceService/help</linkAlternateText>
                <linkUri>http://localhost:9000/EasyIceService/help</linkUri>
              </externalLink>) to list all available method calls using any web browser
              <mediaLink>
                <image xlink:href="Setup_EasyIceService_HelpPage"/>
              </mediaLink>
            </para>
          </content>
        </step>
        <step>
          <content>
            <para>
              We are now going to get the list of users by calling
              <externalLink>
                <linkText>http://localhost:9000/EasyIceService/Users</linkText>
                <linkAlternateText>http://localhost:9000/EasyIceService/Users</linkAlternateText>
                <linkUri>http://localhost:9000/EasyIceService/Users</linkUri>
              </externalLink>
              from a browser. Please note that not all internet browsers have the same way of showing outputs.
            </para>
            <para>
              To figure out how to get the Users, look for the /Users entry in the help page and click on "GET"
              <mediaLink>
                <image xlink:href="RESTExample_HelpPage_Users"/>
              </mediaLink>
            </para>
            <para>
              After clicking on "GET", you should see the following page. Look at the URL on top of the page as it shows you how to call this function.
            </para>
            <para>
              The rest of the page shows example responses to let you know what kind of response you should expect.
              <mediaLink>
                <image xlink:href="RESTExample_HelpPage_Users_Details"/>
              </mediaLink>
            </para>
          </content>
        </step>
      </steps>
    </procedure>

    <section address="UsingChrome">
      <title>
        Using Google Chrome
      </title>
      <content>
        <procedure>
          <steps class="ordered">
            <step>
              <content>
                <para>
                  Here is an example using Chrome
                  <externalLink>
                    <linkText>http://chrome.google.com</linkText>
                    <linkAlternateText>http://chrome.google.com</linkAlternateText>
                    <linkUri>http://chrome.google.com</linkUri>
                  </externalLink>
                  <mediaLink>
                    <image xlink:href="RESTExample_ShowUsers"/>
                  </mediaLink>
                </para>
                <para>
                  To view the actual response, right-click on the page and select "View page source"
                  <mediaLink>
                    <image xlink:href="RESTExampleChrome_ShowUsers_SelectViewPageSource"/>
                  </mediaLink>
                </para>
                <para>
                  The XML output will appear exactly how it was returned by the Easy Ice Service.
                  <mediaLink>
                    <image xlink:href="RESTExampleChrome_ShowUsers_SourceView"/>
                  </mediaLink>
                </para>
                <para>
                  The content of the XML response is as follows:
                  <example>
                    <code lang="XML" region="Sample Output" title="Sample Output">
                      <![CDATA[
                <GetUsersResponse xmlns="http://schemas.datacontract.org/2004/07/WCFEasyIceService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                  <ErrorMessage/>
                  <Success>true</Success>
                  <Users xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
                    <a:string>devlab_user</a:string>
                    <a:string>esadmin</a:string>
                    <a:string>Operator</a:string>
                    <a:string>TestUser</a:string>
                  </Users>
                </GetUsersResponse>
                ]]>
                    </code>
                  </example>
                </para>
              </content>
            </step>
          </steps>
        </procedure>
      </content>
    </section>
    <section address="UsingIE">
      <title>
        Using Internet Explorer
      </title>
      <content>
        <procedure>
          <steps class="ordered">
            <step>
              <content>
                <para>
                  Here is an example using Internet Explorer
                  <externalLink>
                    <linkText>http://windows.microsoft.com/en-US/internet-explorer/download-ie</linkText>
                    <linkAlternateText>http://windows.microsoft.com/en-US/internet-explorer/download-ie</linkAlternateText>
                    <linkUri>http://windows.microsoft.com/en-US/internet-explorer/download-ie</linkUri>
                  </externalLink>
                  <mediaLink>
                    <image xlink:href="RESTExampleIE_ShowUsers"/>
                  </mediaLink>
                </para>
                <para>
                  To view the actual response, right-click on the page and select "View source"
                  <mediaLink>
                    <image xlink:href="RESTExampleIE_ShowUsers_SelectViewSource"/>
                  </mediaLink>
                </para>
                <para>
                  The XML output will appear exactly how it was returned by the Easy Ice Service.
                  <mediaLink>
                    <image xlink:href="RESTExampleIE_ShowUsers_SourceView"/>
                  </mediaLink>
                </para>
                <para>
                  The content of the XML response is as follows:
                  <example>
                    <code lang="XML" region="Sample Output" title="Sample Output">
                      <![CDATA[
                <GetUsersResponse xmlns="http://schemas.datacontract.org/2004/07/WCFEasyIceService" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                  <ErrorMessage/>
                  <Success>true</Success>
                  <Users xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
                    <a:string>devlab_user</a:string>
                    <a:string>esadmin</a:string>
                    <a:string>Operator</a:string>
                    <a:string>TestUser</a:string>
                  </Users>
                </GetUsersResponse>
                ]]>
                    </code>
                  </example>
                </para>
              </content>
            </step>
          </steps>
        </procedure>
      </content>
    </section>
    <section address="UsingFiddler">
      <title>
        Using Fiddler
      </title>
      <content>
        <procedure>
          <steps class="ordered">
            <step>
              <content>
                <para>
                  Here is an example using Fiddler (in JSON format)
                  <externalLink>
                    <linkText>http://www.fiddler2.com</linkText>
                    <linkAlternateText>http://www.fiddler2.com</linkAlternateText>
                    <linkUri>http://www.fiddler2.com</linkUri>
                  </externalLink>
                </para>
                <para>
                  Open Fiddler, go to the "Composer" tab, enter the same URL you have used with Chrome and/or Internet Explorer to retrieve the list of users and click on "Execute".
                  <mediaLink>
                    <image xlink:href="RESTExampleFiddler_EnterURL"/>
                  </mediaLink>
                </para>
                <para>
                  Click on the last session on the left pane (selected in the screenshot below) to view the output in JSON or RAW formats.
                  <mediaLink>
                    <image xlink:href="RESTExampleFiddler_Response"/>
                  </mediaLink>
                </para>
                <para>
                  To view the output in XML format, add "Accept:text/xml" to the "Request headers" box in the "Composer" tab as shown below:
                  <mediaLink>
                    <image xlink:href="RESTExampleFiddler_Response_XML"/>
                  </mediaLink>
                </para>
                <para>
                  XML response example (click on "Raw" to view the raw XML response):
                  <mediaLink>
                    <image xlink:href="RESTExampleFiddler_Response_XML_Format"/>
                  </mediaLink>
                </para>
                <para>
                  If you get the following error: "HTTP Error 411. The request must be chunked or have a content length.", make sure you enter "Content-Length:0" in the "Request Headers" pane, after the "User-Agent" and "Host" entries.
                </para>
              </content>
            </step>
          </steps>
          <conclusion>To try other queries, go back to the help page and look at all the available functionalities.</conclusion>
        </procedure>
      </content>
    </section>

    <nextSteps>
      <content><para>Any REST-compatible software can be used to query the Easy Ice Service. Fiddler is well-known amongst developers and testers though.</para></content>
    </nextSteps>

    <relatedTopics/>
  </developerWalkthroughDocument>
</topic>
