﻿using System;
using System.Net;
using System.Runtime.Serialization.Json;
using EasyResponses;

namespace TestConsole
{
    class Program
    {
        static void Main()
        {
            try
            {
                Console.WriteLine("Press any key to list workgroups...");
                Console.ReadKey();
                Console.WriteLine("Getting workgroups...");
                Console.WriteLine();
                CallService(@"http://localhost:9000/EasyIceService/Workgroups");
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Press any key to exit...");
                Console.ReadLine();
            }
        }

        static void CallService(string uri)
        {
            var request = (HttpWebRequest)WebRequest.Create(uri);
            var dataContractJsonSerializer = new DataContractJsonSerializer(typeof(GetWorkgroupsResponse));
            var response = (GetWorkgroupsResponse)dataContractJsonSerializer.ReadObject(request.GetResponse().GetResponseStream());
            
            foreach (var workgroup in response.Workgroups)
            {
                Console.WriteLine("Workgroup: {0}", workgroup);
            }
        }
    }
}
